package com.flo.floapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.Login.Login;
import com.flo.floapp.Register.OTPVerify;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityWaitingList extends AppCompatActivity {
    Spinner spinner_salutation;
    EditText et_email,et_mobile,et_firstName,et_LastName;
    CountryCodePicker codePicker;
    Button btn_submit,btn_cancel;
    String _Ccode = "+91";
    String mSalutation,mCatId,oCCUPANCY,mPROPERTY_ID,_StartDATE,_endDATE;
    int guestCount;
    FontChangeCrawler fontChanger;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_list);

        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        spinner_salutation = findViewById(R.id.spinner_salutationIndian);
        et_firstName = findViewById(R.id.et_firstName);
        et_LastName = findViewById(R.id.et_LastName);
        et_mobile = findViewById(R.id.et_MobNumber);
        et_email = findViewById(R.id.et_Email);
        codePicker = findViewById(R.id.ccp_wait);
        btn_submit = findViewById(R.id.button_Submit);
        btn_cancel = findViewById(R.id.button_cancel);

        Intent intent = getIntent();
        mCatId = intent.getStringExtra("mCatId");
        oCCUPANCY = intent.getStringExtra("oCCUPANCY");
        mPROPERTY_ID = intent.getStringExtra("mPROPERTY_ID");
        _StartDATE = intent.getStringExtra("_StartDATE");
        _endDATE = intent.getStringExtra("_endDATE");
        guestCount = intent.getIntExtra("guestCount",0);


        codePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                _Ccode = selectedCountry.getPhoneCode();
            }
        });

        spinner_salutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSalutation = spinner_salutation.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                String mFname = et_firstName.getText().toString().trim();
                String mLname = et_LastName.getText().toString().trim();
                String mEmail = et_email.getText().toString().trim();
                String mMobile = et_mobile.getText().toString().trim();
              
                if (!mFname.equals("")&&!mLname.equals("")
                &&!mEmail.equals("")&& !mMobile.equals("")){

                    if (mEmail.matches(emailPattern)) {
                        if (mMobile.length()==10){
                            addWaitList(mFname,mLname,mEmail,mMobile,_Ccode,mSalutation);
                        }else {
                            et_mobile.setError("Please Enter Valid Mobile Number");
                            }
                            }
                            else {
                        et_email.setError("Please Enter Valid E-mail Address");
                    }


                }else {

                    Toast.makeText(ActivityWaitingList.this, "All fields are Mandatory", Toast.LENGTH_SHORT).show();
                    
                }
                
                
            }
        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        
        
    }

    private void addWaitList(final String mFname, final String mLname, final String mEmail, final String mMobile, final String ccode, final String mSalutation) {

        {
            HttpsTrustManager.allowAllSSL();
            final ProgressDialog pDialog = new ProgressDialog(this);
            pDialog.setMessage("Loading...");
            pDialog.show();
            String tag_string_req = "string_req";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Config.URL_WAIT_LIST,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pDialog.dismiss();
                            try {
                                JSONObject reg_jsonObject = new JSONObject(response);
                                String res_Status = reg_jsonObject.getString("status");
                                final String res_Message = reg_jsonObject.getString("message");
                                if (res_Status.equals("true")) {
                                    successdualog(res_Message);
                                } else {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    String message = null;
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                        Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                        Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                        Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                        Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                        Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                        Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("first", mFname);
                    params.put("last", mLname);
                    params.put("mobile", mMobile);
                    params.put("email", mEmail);
                    params.put("fromdate",_StartDATE);
                    params.put("todate",_endDATE);
                    params.put("property_id",mPROPERTY_ID);
                    params.put("occupancy",oCCUPANCY);
                    params.put("category", mCatId);
                    params.put("stdCode", ccode);
                    params.put("number_guest", String.valueOf(guestCount));
                    params.put("mSalutation", "Mr.");
                    return params;

                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }

    private void successdualog(String res_message) {

       final Dialog dialog_success = new Dialog(ActivityWaitingList.this);
        dialog_success.setContentView(R.layout.dialog_success);
        TextView tv_message = dialog_success.findViewById(R.id.tv_messageSuccess);
        Button btn_NA = dialog_success.findViewById(R.id.btn_Success);
        fontChanger.replaceFonts((ViewGroup)dialog_success.findViewById(android.R.id.content));

        tv_message.setText(res_message);
        btn_NA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_success.dismiss();
                finish();
            }
        });
        dialog_success.show();
    }
}
