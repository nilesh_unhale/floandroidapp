package com.flo.floapp.PropertyList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.flo.floapp.CategoryList.BookingDetails;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.SchedduleVisit.ScheduleVisit;
import com.flo.floapp.SearchProperty.PojoPropertyDetails;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import java.util.ArrayList;
import java.util.List;

public class PropertyListAdapter extends RecyclerView.Adapter<PropertyListAdapter.ViewHolder> {

    private ArrayList<PojoPropertyDetails> arraylist;
    private List<PojoPropertyDetails> listPojos;
    private Context context;

    public PropertyListAdapter(Context context,List<PojoPropertyDetails> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoPropertyDetails>();
        arraylist.addAll(listPojos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_property_details, null);

        int height = parent.getMeasuredHeight();
        int width = parent.getMeasuredWidth();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, height));

        PropertyListAdapter.ViewHolder rcv = new PropertyListAdapter.ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String[] mDistance;

        final PojoPropertyDetails propertyDetails =listPojos.get(position);
        holder.tv_PropertyName.setText(propertyDetails.getmPropertyNam());
        String[] mdistance = propertyDetails.getmPropertyDistance().split("\\.");
        holder.tv_PropertyDistance.setText("THIS PROPERTY OPTION IS "+mdistance[0]+" KM AWAY FROM PREFFERED LOCATION");
        holder.tv_propertyAddress.setText(propertyDetails.getmPropertyAddress());
        holder.tv_chargesSingleShare.setText("INR "+propertyDetails.getmCat_chargesharedperday()+" PER DAY");
        holder.tv_chargesShare.setText("INR " +propertyDetails.getmCat_Chargesingleperday()+" PER MONTH");

        PreferenceUtil.writeString(context, PreferenceStrings.PROPERTY_ID,propertyDetails.getmPropertyId());

        // String[] imageEndPoints = propertyDetails.getmPropertyImages().split(" ");
        ArrayList<String> allImgs=new ArrayList<>();
        allImgs = propertyDetails.getAllImages();
        for (int i=0; i<allImgs.size(); i++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(context);
            defaultSliderView.image(Config.URL_IMAGE + allImgs.get(i));
            holder.sliderLayout.addSlider(defaultSliderView);
            // Log.d("Flo Images :", imageEndPoints[i]);
        }
        holder.buttonBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BookingDetails.class);
                //intent.putExtra("propertyId",propertyDetails.getmPropertyId());
                context.startActivity(intent);
            }
        });
        holder.textview_Visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentVisit = new Intent(context, ScheduleVisit.class);
                //intent.putExtra("propertyId",propertyDetails.getmPropertyId());
                context.startActivity(intentVisit);
            }
        });
        holder.fab_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=an+panchashil tower+pune"));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPojos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_single, tv_shared, textview_Visit, tv_PropertyName,tv_PropertyDistance,
                tv_propertyAddress,tv_chargesSingleShare, tv_chargesShare, text_line;
        ImageView iv_propertyImage;
        private SliderLayout sliderLayout;
        private PagerIndicator pagerIndicator;
        Button buttonBook;
        FloatingActionButton fab_direction;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_PropertyName =(TextView)itemView.findViewById(R.id.textview_propertyName);
            tv_PropertyDistance =(TextView)itemView.findViewById(R.id.textview_distanceFrom);
            tv_propertyAddress =(TextView)itemView.findViewById(R.id.textview_propertyAddress);
            tv_chargesSingleShare =(TextView)itemView.findViewById(R.id.tv_chargesSingle);
            tv_chargesShare =itemView.findViewById(R.id.tv_chargesShare);
            text_line =itemView.findViewById(R.id.text_line);

            buttonBook =(Button)itemView.findViewById(R.id.btn_book);
            //pagerIndicator =(PagerIndicator) itemView.findViewById(R.id.slider);
           // sliderLayout =(SliderLayout) itemView.findViewById(R.id.slider);
            fab_direction =(FloatingActionButton) itemView.findViewById(R.id.fab_direction);

            tv_single =(TextView)itemView.findViewById(R.id.tv_singlePropertyList);
            tv_shared =(TextView)itemView.findViewById(R.id.tv_sharedPropertyList);
            textview_Visit =(TextView)itemView.findViewById(R.id.textview_Visit);

            Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
            tv_PropertyName.setTypeface(myCustomFontBold);
            Typeface myCustomFontRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
            tv_PropertyDistance.setTypeface(myCustomFontRegular);
            tv_propertyAddress.setTypeface(myCustomFontRegular);
            tv_chargesSingleShare.setTypeface(myCustomFontRegular);
            text_line.setTypeface(myCustomFontRegular);
            buttonBook.setTypeface(myCustomFontRegular);
            tv_single.setTypeface(myCustomFontRegular);
            tv_shared.setTypeface(myCustomFontRegular);
            textview_Visit.setTypeface(myCustomFontRegular);

        }
    }
}

