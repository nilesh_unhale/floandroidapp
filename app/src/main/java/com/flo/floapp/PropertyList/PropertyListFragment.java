package com.flo.floapp.PropertyList;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.SearchProperty.ActivityPropertyList;
import com.flo.floapp.SearchProperty.AdapterPropertyList;
import com.flo.floapp.SearchProperty.PojoPropertyDetails;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PropertyListFragment extends Fragment {
    RecyclerView recyclerViewPropertyDetails;
    AdapterPropertyList adapterPropertyList;
    public static ArrayList<PojoPropertyDetails> list_propertyDetails ;
    public static ArrayList<String> allImages;

    String mLattitude,mLongitude;
    ImageButton floatinfBtn_filter;
    Dialog dialog;
    TextView seekbar_Result;
    EditText datePreferedSelect;
    private int mYear, mMonth, mDay;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_property_list, container, false);

        recyclerViewPropertyDetails =(RecyclerView)view.findViewById(R.id.recyclerViewPropertyDetails);
        recyclerViewPropertyDetails.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewPropertyDetails.setNestedScrollingEnabled(true);
        recyclerViewPropertyDetails.setHasFixedSize(true);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewPropertyDetails);

        Intent intent = getActivity().getIntent();
        mLattitude = intent.getStringExtra("mLattitude");
        mLongitude = intent.getStringExtra("mLongitude");
        getPropertyCards();

        floatinfBtn_filter =(ImageButton) view.findViewById(R.id.floatinfBtn_filter);
        floatinfBtn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_filter);
                dialog.show();

                SeekBar seekbar_price = dialog.findViewById(R.id.seekbar_peiceFilter);
                seekbar_price.setMax(30000);
                TextView text_TitleFilter, text_TitleOCCUPANCY, textView_PRICE_title, textView_AMENITIES_title;
                EditText edittext_preferedDate;
                RadioButton radioButton_SINGLE, radioButton_SHARED, radioButton_MALE, radioButton_FEMALE;

                text_TitleFilter = dialog.findViewById(R.id.text_TitleFilter);
                text_TitleOCCUPANCY = dialog.findViewById(R.id.text_TitleOCCUPANCY);
                textView_PRICE_title = dialog.findViewById(R.id.textView_PRICE_title);
                textView_AMENITIES_title = dialog.findViewById(R.id.textView_AMENITIES_title);
                edittext_preferedDate = dialog.findViewById(R.id.edittext_preferedDate);
                radioButton_SINGLE = dialog.findViewById(R.id.radioButton_SINGLE);
                radioButton_SHARED = dialog.findViewById(R.id.radioButton_SHARED);
                radioButton_MALE = dialog.findViewById(R.id.radioButton_MALE);
                radioButton_FEMALE = dialog.findViewById(R.id.radioButton_FEMALE);

                //------------Font Applying-------------//
                Typeface myCustomFontBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
                Typeface myCustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Light.ttf");
                text_TitleFilter.setTypeface(myCustomFontBold);
                text_TitleOCCUPANCY.setTypeface(myCustomFontBold);
                textView_PRICE_title.setTypeface(myCustomFontBold);
                textView_AMENITIES_title.setTypeface(myCustomFontBold);
                edittext_preferedDate.setTypeface(myCustomFont);
                radioButton_SINGLE.setTypeface(myCustomFont);
                radioButton_SHARED.setTypeface(myCustomFont);
                radioButton_MALE.setTypeface(myCustomFont);
                radioButton_FEMALE.setTypeface(myCustomFont);

                seekbar_Result = dialog.findViewById(R.id.seekbar_Result);
                seekbar_price.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        seekbar_Result.setText(String.valueOf(progress));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                datePreferedSelect = dialog.findViewById(R.id.edittext_preferedDate);
                datePreferedSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar c = Calendar.getInstance();
                        mYear = c.get(Calendar.YEAR);
                        mMonth = c.get(Calendar.MONTH);
                        mDay = c.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        datePreferedSelect.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();
                    }
                });
                ImageButton close = dialog.findViewById(R.id.dialog_filter_close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

            }
        });
        return view;
    }

    private void getPropertyCards() {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_SEARCH_PROPERTY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            list_propertyDetails = new ArrayList<PojoPropertyDetails>();
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                for (int i = 0; i < MainArray.length(); i++) {

                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mPropertyId = propertyOBJ.getString("id");
                                    String mPropertyBrandId = propertyOBJ.getString("brand_id");
                                    String mPropertyCode = propertyOBJ.getString("property_code");
                                    String mPropertyName = propertyOBJ.getString("property_name");
                                    String mPropertyAddress = propertyOBJ.getString("address");
                                    String mPropertyLatitude = propertyOBJ.getString("cityLat");
                                    String mPropertyLongitude = propertyOBJ.getString("cityLng");
                                    String mPropertyLandmark = propertyOBJ.getString("landmark");
                                    String mPropertyDistance = propertyOBJ.getString("distance");

                                    JSONArray arrayImages = propertyOBJ.getJSONArray("allimages");
                                    JSONObject objectCategory = propertyOBJ.getJSONObject("categories");

                                    String mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,
                                            mCat_ChargeSharedpermonth,mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount
                                            ,mCat_meta,mCat_metasd,mCat_shortterm,mCat_deposit;

                                    mCategoryId = objectCategory.getString("id");
                                    mCategoryName = objectCategory.getString("name");
                                    mCa_Description = objectCategory.getString("description");
                                    mCat_yBrand_id = objectCategory.getString("brand_id");
                                    mCat_chargeSinglepermonth = objectCategory.getString("singlepermonth");
                                    mCat_ChargeSharedpermonth = objectCategory.getString("sharedpermonth");
                                    mCat_Chargesingleperday = objectCategory.getString("singleperday");
                                    mCat_chargesharedperday = objectCategory.getString("sharedperday");
                                    mCat_bookingamount = objectCategory.getString("bookingamount");
                                    mCat_meta = objectCategory.getString("meta");
                                    mCat_metasd = objectCategory.getString("metad");
                                    mCat_shortterm = objectCategory.getString("shortterm");
                                    mCat_deposit = objectCategory.getString("deposit");

                                    String mPropertyImages = null;
                                    JSONObject objectImages =null;
                                    allImages = new ArrayList<String>();
                                    for (int j = 0; j < arrayImages.length(); j++) {


                                        objectImages = arrayImages.getJSONObject(j);
                                        mPropertyImages = objectImages.getString("original_name");
                                        allImages.add(mPropertyImages);

                                    }
                                    Log.d("img :",mPropertyImages);
                                    PojoPropertyDetails details = new PojoPropertyDetails(mPropertyId,mPropertyBrandId,mPropertyCode,mPropertyName,mPropertyAddress
                                            ,mPropertyLatitude,mPropertyLongitude,mPropertyLandmark,mPropertyDistance,mPropertyImages,
                                            mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,mCat_ChargeSharedpermonth,
                                            mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount,mCat_meta,mCat_metasd,mCat_shortterm,
                                            mCat_deposit,allImages);

                                    list_propertyDetails.add(details);
                                    // allImages.clear();


                                }
                                adapterPropertyList = new AdapterPropertyList(getActivity(),list_propertyDetails);
                                recyclerViewPropertyDetails.setAdapter(adapterPropertyList);

                                adapterPropertyList.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cityLat", mLattitude);
                params.put("cityLng", mLongitude);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
