package com.flo.floapp.MyBooking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityRateProperty extends AppCompatActivity {
    String bookingId,fromDate,toDate,Pro_name,Pro_address,guest_category,guest_number,amountPaid,
    userId,mRating,guestCategory;
    TextView tv_bookingId,tv_fromdate,tv_toDdate,tv_propertyName,tv_locationAddress,tv_paidAmount
            ,tv_numberOfGuest,tv_occupancyType;
    Button btn_downloadInvoice,btn_submitFeedback;
    RatingBar ratingBar_rateProperty;
    EditText et_propertyFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_rate_property);
        tv_bookingId =(TextView)findViewById(R.id.tv_bookingId);
        tv_fromdate =(TextView)findViewById(R.id.tv_fromdate);
        tv_toDdate =(TextView)findViewById(R.id.tv_toDdate);
        tv_propertyName =(TextView)findViewById(R.id.tv_propertyName);
        tv_locationAddress =(TextView)findViewById(R.id.tv_locationAddress);
        tv_paidAmount =(TextView)findViewById(R.id.tv_paidAmount);
        tv_numberOfGuest =(TextView)findViewById(R.id.tv_numberOfGuest);
        tv_occupancyType =(TextView)findViewById(R.id.tv_occupancyType);
        tv_paidAmount =(TextView)findViewById(R.id.tv_paidAmount);
        btn_downloadInvoice =(Button)findViewById(R.id.btn_downloadInvoice);
        ratingBar_rateProperty =(RatingBar)findViewById(R.id.ratingBar_rateProperty);
        et_propertyFeedback =(EditText)findViewById(R.id.et_propertyFeedback);
        btn_submitFeedback =(Button)findViewById(R.id.btn_submitFeedback);

        userId = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.USER_ID,"");

        Intent intent = getIntent();
        bookingId = intent.getStringExtra("bookingId");
        fromDate = intent.getStringExtra("fromDate");
        toDate = intent.getStringExtra("toDate");
        Pro_name = intent.getStringExtra("Pro_name");
        Pro_address = intent.getStringExtra("Pro_address");
        guest_category = intent.getStringExtra("guest_category");
        guest_number = intent.getStringExtra("guest_number");
        amountPaid = intent.getStringExtra("amountPaid");
        guestCategory = intent.getStringExtra("guestCategory");

        tv_bookingId.setText(bookingId);
        tv_fromdate.setText(fromDate);
        tv_toDdate.setText(toDate);
        tv_propertyName.setText(Pro_name);
        tv_locationAddress.setText(Pro_address);
        tv_paidAmount.setText(amountPaid);
        tv_numberOfGuest.setText(guest_number);
        tv_occupancyType.setText(guestCategory);

        ratingBar_rateProperty.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar rtBar, float rating,boolean fromUser) {
               mRating = String.valueOf(rating);

            }
        });

        btn_submitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mFeedback = et_propertyFeedback.getText().toString();
                if(!mFeedback.equals("")){
                    PostFeedback(userId,mFeedback,mRating);

                }
                else {
                    Toast.makeText(getApplicationContext(),"Please Enter Review ",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void PostFeedback(final String userId, final String mFeedback, final String mRating) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_REVIEW_FEEDBACK,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Toast.makeText(getApplicationContext(), "FeedBack Submited Succesfully" , Toast.LENGTH_LONG).show();
                               // JSONObject strJson = reg_jsonObject.getJSONObject("response");
                               // String mEmail = strJson.getString("email");


                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userId);
                params.put("rating", mRating);
                params.put("review", mFeedback);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
