package com.flo.floapp.MyBooking;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.MyRequest.FragmentNewRequest;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentBookingDetails extends Fragment {
    View view;
    TabLayout tabLayout;
    TextView texta_all, text_current,text_upcoming,text_cancelled;
    String mImage;
    ImageView imageView_userphoto;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_booking_details, container, false);
        texta_all = view.findViewById(R.id.texta_all);
        text_current = view.findViewById(R.id.text_current);
        text_upcoming = view.findViewById(R.id.text_upcoming);
        text_cancelled = view.findViewById(R.id.text_cancelled);
        imageView_userphoto = view.findViewById(R.id.imageView_userphoto);

        mImage = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_IMAGE,"");

     /*   FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        FragmentAllBooking fragment = new FragmentAllBooking();
        ft.replace(R.id.content_frame_mybooking, fragment);
        ft.commit();*/
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        FragmentCurrentBooking fragment = new FragmentCurrentBooking();
        ft.replace(R.id.content_frame_mybooking, fragment);
        text_current.setBackgroundColor(Color.parseColor("#3dca24"));
        ft.commit();

        texta_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentAllBooking fragment = new FragmentAllBooking();
                ft.replace(R.id.content_frame_mybooking, fragment);
                ft.commit();
                //texta_all.setBackgroundResource(R.drawable.bg_booking_select);
                //text_current.setBackgroundResource(R.drawable.bg_req_not_select);
                text_current.setBackgroundColor(Color.parseColor("#3dca24"));
                text_upcoming.setBackgroundResource(R.drawable.bg_req_not_select);
                text_cancelled.setBackgroundResource(R.drawable.bg_req_not_select);

            }
        });

        text_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentCurrentBooking fragment = new FragmentCurrentBooking();
                ft.replace(R.id.content_frame_mybooking, fragment);
                ft.commit();
                texta_all.setBackgroundResource(R.drawable.bg_req_not_select);
                text_current.setBackgroundColor(Color.parseColor("#3dca24"));
                text_upcoming.setBackgroundResource(R.drawable.bg_req_not_select);
                text_cancelled.setBackgroundResource(R.drawable.bg_req_not_select);
            }
        });

        text_upcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentUpComingBooking fragment = new FragmentUpComingBooking();
                ft.replace(R.id.content_frame_mybooking, fragment);
                ft.commit();
                texta_all.setBackgroundResource(R.drawable.bg_req_not_select);
                text_current.setBackgroundResource(R.drawable.bg_req_not_select);
                text_upcoming.setBackgroundColor(Color.parseColor("#3dca24"));
                text_cancelled.setBackgroundResource(R.drawable.bg_req_not_select);
            }
        });

        text_cancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentCancelledBooking fragment = new FragmentCancelledBooking();
                ft.replace(R.id.content_frame_mybooking, fragment);
                ft.commit();
                texta_all.setBackgroundResource(R.drawable.bg_req_not_select);
                text_current.setBackgroundResource(R.drawable.bg_req_not_select);
                text_upcoming.setBackgroundResource(R.drawable.bg_req_not_select);
                text_cancelled.setBackgroundColor(Color.parseColor("#3dca24"));

            }
        });

        Glide.with(getActivity())
                .load(Config.URL_USER_IMAGE+mImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_property_thumbnail)
                .into(imageView_userphoto);

        return view;
    }

}
