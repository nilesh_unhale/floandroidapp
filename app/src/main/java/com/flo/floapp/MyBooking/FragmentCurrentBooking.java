package com.flo.floapp.MyBooking;

import android.app.ProgressDialog;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SearchProperty.ActivityPropertyList;
import com.flo.floapp.SearchProperty.AdapterPropertyList;
import com.flo.floapp.SearchProperty.PojoPropertyDetails;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCurrentBooking extends Fragment {
    View view;
    RecyclerView recyclerViewCurrentBooking;
    private APIService services;
    ArrayList<PojoCurrentBooking> arrayList_current;
    AdapterCurrentBooking adapterCurrentBooking;
    ProgressBar mProgressBar;
    TextView textViewEmpty;
    String mUSER_ID;
    ImageView iv_noBookingFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_booking_current, container, false);

        recyclerViewCurrentBooking = (RecyclerView)view.findViewById(R.id.recyclerViewCurrentBooking);
        recyclerViewCurrentBooking.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewCurrentBooking.setNestedScrollingEnabled(true);
        recyclerViewCurrentBooking.setHasFixedSize(true);
        mUSER_ID = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_current);
        textViewEmpty = (TextView) view.findViewById(R.id.tv_c_booking);
        iv_noBookingFound = view.findViewById(R.id.iv_noBookingFound);
        services = ApiUtils.getAPIService();
        arrayList_current = new ArrayList<PojoCurrentBooking>();
        getCurrentBookingDetails();
         mProgressBar.setVisibility(View.VISIBLE);
        return view;
    }

    private void getCurrentBookingDetails()
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CURRENT_BOOKING,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressBar.setVisibility(View.GONE);
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                                for (int i = 0; i < MainArray.length(); i++) {

                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mUserId = propertyOBJ.getString("user_id");
                                    String mId = propertyOBJ.getString("id");
                                    String mBookingId = propertyOBJ.getString("booking_code");

                                    String mProperty_id = propertyOBJ.getString("property_id");
                                    String mNumberOfdays = propertyOBJ.getString("noofdays");
                                   // String mGuest_category = propertyOBJ.getString("guest_category");
                                    String mNumber_guest = propertyOBJ.getString("number_guest");
                                    String mRoomcat_id = propertyOBJ.getString("roomcat_id");
                                    String mDateFrom = propertyOBJ.getString("from");
                                    String mDateTo = propertyOBJ.getString("to");
                                    String mTotalAmount = propertyOBJ.getString("total");
                                    String bookingamount = propertyOBJ.getString("bookingamount");
                                    String checkinamount = propertyOBJ.getString("checkinamount");
                                    String mStatus = propertyOBJ.getString("status");
                                    String mGender = propertyOBJ.getString("gender");
                                    String bed_id = propertyOBJ.getString("bed_id");
                                    String occupancy = propertyOBJ.getString("occupancy");
                                    String maddress = propertyOBJ.getString("address");
                                    String mMale = propertyOBJ.getString("males");
                                    String mFEMale = propertyOBJ.getString("females");
                                    String mPayment = "1";
                                    String stay_type = propertyOBJ.getString("stay_type");
                                    String mGST = propertyOBJ.getString("gst");
                                    String monthly = propertyOBJ.getString("monthly");
                                    String mProperty_name = propertyOBJ.getString("property_name");
                                    String mLandmark = propertyOBJ.getString("landmark");
                                    String is_documentverified = propertyOBJ.getString("is_documentverified");
                                   // String mBOOKING_TYPE = propertyOBJ.getString("bookingType");
                                    String amountPaid = propertyOBJ.getString("amountpaid");



                                    PojoCurrentBooking pojoCurrentBooking = new PojoCurrentBooking(mUserId,mId,mBookingId,mProperty_id,
                                            mNumberOfdays,"1",mNumber_guest,mRoomcat_id
                                            ,mDateFrom,mDateTo,mTotalAmount,bookingamount,checkinamount,
                                            mStatus,mGender,bed_id,occupancy,maddress,mMale,mFEMale,mPayment,
                                            stay_type,mGST,monthly,mProperty_name,mLandmark,is_documentverified,"none",amountPaid);
                                    arrayList_current.add(pojoCurrentBooking);
                                }
                                adapterCurrentBooking = new AdapterCurrentBooking(getActivity(),arrayList_current);
                                recyclerViewCurrentBooking.setAdapter(adapterCurrentBooking);
                            } else {
                                textViewEmpty.setVisibility(View.VISIBLE);
                                textViewEmpty.setText("You have no current Bookings");
                                iv_noBookingFound.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", mUSER_ID);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);





    }



}
