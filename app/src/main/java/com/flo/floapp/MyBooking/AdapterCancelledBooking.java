package com.flo.floapp.MyBooking;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flo.floapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterCancelledBooking extends RecyclerView.Adapter<AdapterCancelledBooking.ViewHolder> {
    private ArrayList<PojoCurrentBooking> arraylist;
    private List<PojoCurrentBooking> listPojos;
    private Context context;

    public AdapterCancelledBooking(Context context,List<PojoCurrentBooking> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoCurrentBooking>();
        arraylist.addAll(listPojos);
    }

    @Override
    public AdapterCancelledBooking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_current_booking, null);
        AdapterCancelledBooking.ViewHolder rcv = new AdapterCancelledBooking.ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterCancelledBooking.ViewHolder holder, int position) {

        final PojoCurrentBooking propertyDetails =listPojos.get(position);
        holder.tv_amount.setText("Rs. "+propertyDetails.get_amountpaid());
        holder.tv_bookingId.setText(propertyDetails.getmBookingId());
        holder.tv_bookingDuration.setText(propertyDetails.getmDateFrom());
        holder.tv_currentBookingTodates.setText(propertyDetails.getmDateTo());
        holder.tv_location.setText(propertyDetails.getmProperty_name());
       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context,ActivityRateProperty.class);
                intent.putExtra("bookingId",propertyDetails.getmBookingId());
                intent.putExtra("fromDate",propertyDetails.getmDateFrom());
                intent.putExtra("toDate",propertyDetails.getmDateTo());
                intent.putExtra("Pro_name",propertyDetails.getmProperty_name());
                intent.putExtra("Pro_address",propertyDetails.getMaddress());
                intent.putExtra("guest_category",propertyDetails.getmGuest_category());
                intent.putExtra("guest_number",propertyDetails.getmNumber_guest());
                intent.putExtra("amountPaid",propertyDetails.getCheckinamount());
                intent.putExtra("guestCategory",propertyDetails.getmGuest_category());
                context.startActivity(intent);

            }
        });*/


    }

    @Override
    public int getItemCount() {
        return this.listPojos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_bookingId,tv_bookingDuration,tv_amount,tv_location,tv_currentBookingTodates;
        public ViewHolder(View view) {
            super(view);
            tv_currentBookingTodates = (TextView)view.findViewById(R.id.tv_currentBookingTodates);
            tv_bookingId = (TextView)view.findViewById(R.id.tv_currentBookingId);
            tv_location = (TextView)view.findViewById(R.id.tv_currentBookingLocation);
            tv_bookingDuration = (TextView)view.findViewById(R.id.tv_currentBookingdates);
            tv_amount = (TextView)view.findViewById(R.id.tv_currentBookingAmount);

        }
    }
}