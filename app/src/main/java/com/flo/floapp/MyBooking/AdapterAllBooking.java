package com.flo.floapp.MyBooking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flo.floapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterAllBooking extends RecyclerView.Adapter<AdapterAllBooking.ViewHolder> {
    private ArrayList<PojoCurrentBooking> arraylist;
    private List<PojoCurrentBooking> listPojos;
    private Context context;

    public AdapterAllBooking(Context context,List<PojoCurrentBooking> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoCurrentBooking>();
        arraylist.addAll(listPojos);
    }


//LayoutInflater.from(parent.getContext()).inflate(R.layout.single_current_booking, null);
    @Override
    public AdapterAllBooking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_current_booking, null);
        AdapterAllBooking.ViewHolder rcv = new AdapterAllBooking.ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterAllBooking.ViewHolder holder, int position) {

        final PojoCurrentBooking propertyDetails =listPojos.get(position);
        holder.tv_amount.setText(propertyDetails.getCheckinamount());
        holder.tv_bookingId.setText(propertyDetails.getmBookingId());
        holder.tv_bookingDuration.setText(propertyDetails.getmDateFrom());
        holder.tv_currentBookingTodates.setText(propertyDetails.getmDateTo());
        holder.tv_location.setText(propertyDetails.getmProperty_name());
        String _BOOKingType = propertyDetails.get_mBookingType();

        if (_BOOKingType.equals("currentBooking")){
            holder.tv_bookingType.setVisibility(View.VISIBLE);
            holder.tv_bookingType.setText("Current");
            holder.tv_bookingType.setTextColor(Color.parseColor("#32CD32"));
        }else if(_BOOKingType.equals("cancelledBooking"))
        {
            holder.tv_bookingType.setVisibility(View.VISIBLE);
            holder.tv_bookingType.setText("Cancelled");
            holder.tv_bookingType.setTextColor(Color.parseColor("#CD5C5C"));
        }else if(_BOOKingType.equals("upcomingBooking"))
        {
            holder.tv_bookingType.setVisibility(View.VISIBLE);
            holder.tv_bookingType.setText("Upcoming");
            holder.tv_bookingType.setTextColor(Color.parseColor("#CD5C5C"));
        }
        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context,ActivityRateProperty.class);
                intent.putExtra("bookingId",propertyDetails.getmBookingId());
                intent.putExtra("fromDate",propertyDetails.getmDateFrom());
                intent.putExtra("toDate",propertyDetails.getmDateTo());
                intent.putExtra("Pro_name",propertyDetails.getmProperty_name());
                intent.putExtra("Pro_address",propertyDetails.getMaddress());
                intent.putExtra("guest_category",propertyDetails.getmGuest_category());
                intent.putExtra("guest_number",propertyDetails.getmNumber_guest());
                intent.putExtra("amountPaid",propertyDetails.getCheckinamount());
                intent.putExtra("guestCategory",propertyDetails.getmGuest_category());
                context.startActivity(intent);

            }
        });
*/

    }

    @Override
    public int getItemCount() {
        return this.listPojos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_bookingId,tv_bookingDuration,tv_amount,tv_location, text_BookingTitle,
                tv_currentBookingTodates, text_ToDateTitle, text_DateTitle, text_AmountTitle,tv_bookingType;
        public ViewHolder(View view) {
            super(view);

            tv_bookingType = (TextView)view.findViewById(R.id.tv_bookingType);
            tv_currentBookingTodates = (TextView)view.findViewById(R.id.tv_currentBookingTodates);
            text_ToDateTitle = (TextView)view.findViewById(R.id.text_ToDateTitle);
            tv_bookingId = (TextView)view.findViewById(R.id.tv_currentBookingId);
            tv_location = (TextView)view.findViewById(R.id.tv_currentBookingLocation);
            tv_bookingDuration = (TextView)view.findViewById(R.id.tv_currentBookingdates);
            tv_amount = (TextView)view.findViewById(R.id.tv_currentBookingAmount);
            text_BookingTitle = view.findViewById(R.id.text_BookingTitle);
            text_DateTitle = view.findViewById(R.id.text_DateTitle);
            text_AmountTitle = view.findViewById(R.id.text_AmountTitle);

            Typeface myCustomFontRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
            Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
            tv_bookingId.setTypeface(myCustomFontBold);
            tv_location.setTypeface(myCustomFontRegular);
            tv_bookingDuration.setTypeface(myCustomFontRegular);
            tv_amount.setTypeface(myCustomFontRegular);
            text_BookingTitle.setTypeface(myCustomFontBold);
            tv_currentBookingTodates.setTypeface(myCustomFontRegular);
            text_ToDateTitle.setTypeface(myCustomFontRegular);
            text_DateTitle.setTypeface(myCustomFontRegular);
            text_AmountTitle.setTypeface(myCustomFontRegular);
        }
    }
}
