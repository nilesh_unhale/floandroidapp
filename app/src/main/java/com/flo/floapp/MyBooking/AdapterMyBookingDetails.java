package com.flo.floapp.MyBooking;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterMyBookingDetails extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AdapterMyBookingDetails(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragmentAllBooking tab1 = new FragmentAllBooking();
                return tab1;
            case 1:
                FragmentCurrentBooking tab2 = new FragmentCurrentBooking();
                return tab2;
            case 2:
                FragmentUpComingBooking tab3 = new FragmentUpComingBooking();
                return tab3;

            case 3:
                FragmentCancelledBooking tab4 = new FragmentCancelledBooking();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
