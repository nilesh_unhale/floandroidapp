package com.flo.floapp.MyBooking.UpdateBooking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import com.flo.floapp.Volley.MyApplication;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UpdateBooking extends AppCompatActivity {
    FontChangeCrawler fontChanger;
    EditText et_salutation, et_FirstName,et_LastName,et_Number,et_Email,et_bookingCode,et_BookingAmount,
    et_GuestNumber;
    TextView tv_StartDate,tv_EndDate;
    Spinner spinnerCategory;
    CountryCodePicker cpp_edit;
    Button btn_editBooking,btn_clear;
    String  mSalutation, mFirstName, mLastName, mEmail, mBookingId, mFromdate,mToDate,mGuestNumber,
            mRoomCategory, mAnountPaid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_booking);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        init();

        Intent intent = getIntent();
        mSalutation = intent.getStringExtra("bookingId");
        mFirstName = intent.getStringExtra("guest_FirstName");
        mLastName = intent.getStringExtra("guest_LastName");
        mEmail = intent.getStringExtra("guest_Email");
        mBookingId = intent.getStringExtra("bookingId");
        mFromdate = intent.getStringExtra("fromDate");
        mToDate = intent.getStringExtra("toDate");
        mRoomCategory = intent.getStringExtra("guest_RoomCategory_id");
        mGuestNumber = intent.getStringExtra("guest_number");
       // mGuestNumber = intent.getStringExtra("guest_category");
        mAnountPaid = intent.getStringExtra("amountPaid");

    }

    private void init() {
        et_FirstName = findViewById(R.id.etFrstname);
        et_LastName = findViewById(R.id.et_edit_last_name);
        et_Number = findViewById(R.id.et_edit_number);
        et_Email = findViewById(R.id.et_edit_email);
        et_bookingCode = findViewById(R.id.et_edit_bookingcode);
        et_BookingAmount = findViewById(R.id.et_edit_amount);
        tv_StartDate = findViewById(R.id.tv_edit_fromdate);
        tv_EndDate = findViewById(R.id.tv_edit_todate);
        et_GuestNumber = findViewById(R.id.et_edit_numberofguest);
        et_salutation = findViewById(R.id.et_salutation);
        spinnerCategory = findViewById(R.id.spinnerCat);
        cpp_edit = findViewById(R.id.ccp_edit);
        btn_editBooking = findViewById(R.id.btn_edit_booking);
        btn_clear = findViewById(R.id.btn_clear_book);

        et_GuestNumber.setText(mGuestNumber);
        et_bookingCode.setText(mBookingId);
        tv_StartDate.setText(mFromdate);
        tv_EndDate.setText(mToDate);

        btn_editBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String mFname = et_FirstName.getText().toString().trim();
               String mLname = et_LastName.getText().toString().trim();
               String mNumber = et_Number.getText().toString().trim();
               String mEmail = et_Email.getText().toString().trim();
               String mbookingCode = et_bookingCode.getText().toString().trim();
               String mAmount = et_BookingAmount.getText().toString().trim();
               String mstartDate = tv_StartDate.getText().toString().trim();
               String mEnddate = tv_EndDate.getText().toString().trim();
               String mGuestNumber = et_GuestNumber.getText().toString().trim();

                if (!mFname.endsWith("") &&!mLname.endsWith("") &&!mNumber.endsWith("")
                        &&!mEmail.endsWith("")&&!mbookingCode.endsWith("")&&!mAmount.endsWith("")
                        &&!mstartDate.endsWith("")&&!mEnddate.endsWith("")&&!mGuestNumber.endsWith("")){
                    
                    EditBooking(mFname,mLname,mNumber,mEmail,mbookingCode,mAmount,mstartDate,
                            mEnddate,mGuestNumber);
                }else {

                    Toast.makeText(UpdateBooking.this, "Enter Mantatory Fields ",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_FirstName.setText("");
                et_LastName.setText("");
                et_Number.setText("");
                et_Email.setText("");
                tv_StartDate.setText("");
                tv_EndDate.setText("");
            }
        });
*/
    }

    private void EditBooking(String mFname, String mLname, String mNumber,
                             String mEmail, String mbookingCode, String mAmount,
                             String mstartDate, String mEnddate, String mGuestNumber) {
        final ProgressDialog pDialog = new ProgressDialog(UpdateBooking.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CONTACT_US,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(UpdateBooking.this, "" + res_Message,
                                        Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(UpdateBooking.this, "" + res_Message,
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(UpdateBooking.this, "OOPS!! Server Not Responding" ,
                        Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
              /*  params.put("name", mName);
                params.put("email", mEmail);
                params.put("mobile", mPhone);
                params.put("subject", mSubject);
                params.put("message", mMassage);*/
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
