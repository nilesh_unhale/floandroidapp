package com.flo.floapp.MyBooking;

public class PojoCurrentBooking {

    String mUserId;
    String mId ;
    String mBookingId ;
    String mProperty_id ;
    String mNumberOfdays;
    String mGuest_category;
    String mNumber_guest;
    String mRoomcat_id;
    String mDateFrom;
    String mDateTo;
    String mTotalAmount;
    String bookingamount ;
    String checkinamount ;
    String mStatus;
    String mGender;
    String bed_id;
    String occupancy ;
    String maddress;
    String mMale;
    String mFEMale;
    String mPayment;
    String monthly;
    String stay_type;
    String mGST;
    String mProperty_name;
    String mLandmark;
    String is_documentverified;
    String _mBookingType;
    String _amountpaid;

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmBookingId() {
        return mBookingId;
    }

    public void setmBookingId(String mBookingId) {
        this.mBookingId = mBookingId;
    }

    public String getmProperty_id() {
        return mProperty_id;
    }

    public void setmProperty_id(String mProperty_id) {
        this.mProperty_id = mProperty_id;
    }

    public String getmNumberOfdays() {
        return mNumberOfdays;
    }

    public void setmNumberOfdays(String mNumberOfdays) {
        this.mNumberOfdays = mNumberOfdays;
    }

    public String getmGuest_category() {
        return mGuest_category;
    }

    public void setmGuest_category(String mGuest_category) {
        this.mGuest_category = mGuest_category;
    }

    public String getmNumber_guest() {
        return mNumber_guest;
    }

    public void setmNumber_guest(String mNumber_guest) {
        this.mNumber_guest = mNumber_guest;
    }

    public String getmRoomcat_id() {
        return mRoomcat_id;
    }

    public void setmRoomcat_id(String mRoomcat_id) {
        this.mRoomcat_id = mRoomcat_id;
    }

    public String getmDateFrom() {
        return mDateFrom;
    }

    public void setmDateFrom(String mDateFrom) {
        this.mDateFrom = mDateFrom;
    }

    public String getmDateTo() {
        return mDateTo;
    }

    public void setmDateTo(String mDateTo) {
        this.mDateTo = mDateTo;
    }

    public String getmTotalAmount() {
        return mTotalAmount;
    }

    public void setmTotalAmount(String mTotalAmount) {
        this.mTotalAmount = mTotalAmount;
    }

    public String getBookingamount() {
        return bookingamount;
    }

    public void setBookingamount(String bookingamount) {
        this.bookingamount = bookingamount;
    }

    public String getCheckinamount() {
        return checkinamount;
    }

    public void setCheckinamount(String checkinamount) {
        this.checkinamount = checkinamount;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmGender() {
        return mGender;
    }

    public void setmGender(String mGender) {
        this.mGender = mGender;
    }

    public String getBed_id() {
        return bed_id;
    }

    public void setBed_id(String bed_id) {
        this.bed_id = bed_id;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getMaddress() {
        return maddress;
    }

    public void setMaddress(String maddress) {
        this.maddress = maddress;
    }

    public String getmMale() {
        return mMale;
    }

    public void setmMale(String mMale) {
        this.mMale = mMale;
    }

    public String getmFEMale() {
        return mFEMale;
    }

    public void setmFEMale(String mFEMale) {
        this.mFEMale = mFEMale;
    }

    public String getmPayment() {
        return mPayment;
    }

    public void setmPayment(String mPayment) {
        this.mPayment = mPayment;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getStay_type() {
        return stay_type;
    }

    public void setStay_type(String stay_type) {
        this.stay_type = stay_type;
    }

    public String getmGST() {
        return mGST;
    }

    public void setmGST(String mGST) {
        this.mGST = mGST;
    }

    public String getmProperty_name() {
        return mProperty_name;
    }

    public void setmProperty_name(String mProperty_name) {
        this.mProperty_name = mProperty_name;
    }

    public String getmLandmark() {
        return mLandmark;
    }

    public void setmLandmark(String mLandmark) {
        this.mLandmark = mLandmark;
    }

    public String getIs_documentverified() {
        return is_documentverified;
    }

    public void setIs_documentverified(String is_documentverified) {
        this.is_documentverified = is_documentverified;
    }

    public String get_mBookingType() {
        return _mBookingType;
    }

    public void set_mBookingType(String _mBookingType) {
        this._mBookingType = _mBookingType;
    }

    public String get_amountpaid() {
        return _amountpaid;
    }

    public void set_amountpaid(String _amountpaid) {
        this._amountpaid = _amountpaid;
    }

    public PojoCurrentBooking(String mUserId, String mId, String mBookingId, String mProperty_id, String mNumberOfdays, String mGuest_category, String mNumber_guest, String mRoomcat_id, String mDateFrom, String mDateTo, String mTotalAmount, String bookingamount, String checkinamount, String mStatus, String mGender, String bed_id, String occupancy, String maddress, String mMale, String mFEMale, String mPayment, String monthly, String stay_type, String mGST, String mProperty_name, String mLandmark, String is_documentverified, String _mBookingType, String _amountpaid) {
        this.mUserId = mUserId;
        this.mId = mId;
        this.mBookingId = mBookingId;
        this.mProperty_id = mProperty_id;
        this.mNumberOfdays = mNumberOfdays;
        this.mGuest_category = mGuest_category;
        this.mNumber_guest = mNumber_guest;
        this.mRoomcat_id = mRoomcat_id;
        this.mDateFrom = mDateFrom;
        this.mDateTo = mDateTo;
        this.mTotalAmount = mTotalAmount;
        this.bookingamount = bookingamount;
        this.checkinamount = checkinamount;
        this.mStatus = mStatus;
        this.mGender = mGender;
        this.bed_id = bed_id;
        this.occupancy = occupancy;
        this.maddress = maddress;
        this.mMale = mMale;
        this.mFEMale = mFEMale;
        this.mPayment = mPayment;
        this.monthly = monthly;
        this.stay_type = stay_type;
        this.mGST = mGST;
        this.mProperty_name = mProperty_name;
        this.mLandmark = mLandmark;
        this.is_documentverified = is_documentverified;
        this._mBookingType = _mBookingType;
        this._amountpaid = _amountpaid;
    }
}