package com.flo.floapp.MyBooking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.GuestList.ActivityGuestList;
import com.flo.floapp.MyBooking.UpdateBooking.UpdateBooking;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterUpComingBooking extends RecyclerView.Adapter<AdapterUpComingBooking.ViewHolder> {
    private ArrayList<PojoCurrentBooking> arraylist;
    private List<PojoCurrentBooking> listPojos;
    private Context context;
    FontChangeCrawler fontChanger;

    public AdapterUpComingBooking(Context context,List<PojoCurrentBooking> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoCurrentBooking>();
        arraylist.addAll(listPojos);
    }

    @Override
    public AdapterUpComingBooking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_upcoming_booking, null);
        fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)layoutView);
        AdapterUpComingBooking.ViewHolder rcv = new AdapterUpComingBooking.ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final AdapterUpComingBooking.ViewHolder holder, final int position) {

       final PojoCurrentBooking propertyDetails =listPojos.get(position);
        holder.tv_amount.setText("Rs. "+propertyDetails.get_amountpaid());
        holder.tv_bookingId.setText(propertyDetails.getmBookingId());
        holder.tv_bookingDuration.setText(propertyDetails.getmDateFrom());
        holder.tv_currentBookingTodates.setText(propertyDetails.getmDateTo());
        holder.tv_location.setText(propertyDetails.getmProperty_name());
        holder.tv_landmark.setText(propertyDetails.getmProperty_name()+", "  + propertyDetails.getMaddress()+", "
                + propertyDetails.getmLandmark());

        if (holder.tv_occupancyType.equals(0)){
            holder.tv_occupancyType.setText("Occupancy Type : Single");
        }else{
            holder.tv_occupancyType.setText("Occupancy Type : Shared");
        }

        if(propertyDetails.getIs_documentverified().equals("1")){
            holder.btn_upload.setText("DOCUMENT UPLOADED");
            holder.btn_upload.setClickable(false);
        }else {
            holder.btn_upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,ActivityGuestList.class);
                    intent.putExtra("booking_id",propertyDetails.getmId());
                    intent.putExtra("mbooking_code",propertyDetails.getmBookingId());
                    intent.putExtra("femaleGuestCount", Integer.parseInt(propertyDetails.getmFEMale()));
                    intent.putExtra("maleGuestCount", Integer.parseInt(propertyDetails.getmMale()));
                    intent.putExtra("mStartDate",propertyDetails.getmDateFrom());
                    intent.putExtra("mEndDate",propertyDetails.getmDateTo());
                    intent.putExtra("mPropertyName",propertyDetails.getmProperty_name());
                    intent.putExtra("mUserFirstName", PreferenceUtil.readString(context, PreferenceStrings.FIRST_NAME,""));
                    intent.putExtra("mUserLastName", PreferenceUtil.readString(context, PreferenceStrings.LAST_NAME,""));
                    context.startActivity(intent);
                }
            });

        }



       holder.img_ButtonEditBooking.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(context, UpdateBooking.class);
               intent.putExtra("bookingId",propertyDetails.getmBookingId());
               intent.putExtra("fromDate",propertyDetails.getmDateFrom());
               intent.putExtra("toDate",propertyDetails.getmDateTo());
               intent.putExtra("Pro_name",propertyDetails.getmProperty_name());
               intent.putExtra("Pro_address",propertyDetails.getMaddress());
               intent.putExtra("guest_category",propertyDetails.getmGuest_category());
               intent.putExtra("guest_number",propertyDetails.getmNumber_guest());
               intent.putExtra("amountPaid",propertyDetails.getCheckinamount());
               intent.putExtra("guest_FirstName",propertyDetails.getCheckinamount());
               intent.putExtra("guest_LastName",propertyDetails.getCheckinamount());
               intent.putExtra("guest_Email",propertyDetails.getCheckinamount());
               intent.putExtra("guest_RoomCategory_id",propertyDetails.getmRoomcat_id());
             //  intent.putExtra("amountPaid",propertyDetails.getmPayment());
               context.startActivity(intent);
           }
       });

       holder.img_ButtonDeleteBooking.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               cancelMyBooking(position,holder,propertyDetails.getmId());
           }
       });

    }
    private void cancelMyBooking(final int position, ViewHolder holder, final String booking_id) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_BOOK_CANCEL,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                                listPojos.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,listPojos.size());
                            }
                            else {
                                Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.d("Upcoming :",error.toString());
                Toast.makeText(context, "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("booking_id", booking_id);
                params.put("Content-Type","application/x-www-form-urlencoded");

                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public int getItemCount() {
        return this.listPojos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_bookingId,tv_bookingDuration,tv_amount,tv_location,
                tv_currentBookingTodates, text_downloadInvoice, tv_landmark, tv_occupancyType;
        private Button btn_upload;
        private ImageButton img_ButtonEditBooking, img_ButtonDeleteBooking;
        public ViewHolder(View view) {
            super(view);
            tv_currentBookingTodates = (TextView)view.findViewById(R.id.tv_currentBookingTodates);
            tv_bookingId = (TextView)view.findViewById(R.id.tv_currentBookingId);
            tv_location = (TextView)view.findViewById(R.id.tv_currentBookingLocation);
            text_downloadInvoice = (TextView)view.findViewById(R.id.text_downloadInvoice);
            tv_bookingDuration = (TextView)view.findViewById(R.id.tv_currentBookingdates);
            tv_amount = (TextView)view.findViewById(R.id.tv_currentBookingAmount);
            btn_upload = (Button) view.findViewById(R.id.btn_upload);
            img_ButtonEditBooking = itemView.findViewById(R.id.img_ButtonEditBooking);
            img_ButtonDeleteBooking = itemView.findViewById(R.id.img_ButtonDeleteBooking);
            text_downloadInvoice = itemView.findViewById(R.id.text_downloadInvoice);
            tv_landmark = itemView.findViewById(R.id.tv_landmark);
            tv_occupancyType = itemView.findViewById(R.id.tv_occupancyType);
        }
    }
}