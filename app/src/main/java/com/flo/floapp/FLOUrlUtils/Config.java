package com.flo.floapp.FLOUrlUtils;


public class Config {
   /* public static final String DOMAIN_NAME = "http://inubex.in/output/FLO/uat/api/";
    public static final String URL_IMAGE = "http://inubex.in/output/FLO/uat/public/uploads/full_size/";*/

   /* public static final String DOMAIN_NAME = "https://flocolive.com/stage1/api/";
    public static final String URL_IMAGE = "https://flocolive.com/stage1/public/uploads/full_size/";
    public static final String URL_USER_IMAGE = "https://flocolive.com/stage1/public/uploads/users/profile/";
    public static final String URL_VDO = "https://flocolive.com/stage1/public/uploads/properties/video/";
    public static final String URL_PDF = "https://flocolive.com/stage1/public/uploads/properties/pdf/";*/


    public static final String DOMAIN_NAME = "https://www.flocolive.com/api/";
    public static final String URL_IMAGE = "https://www.flocolive.com/public/uploads/full_size/";
    public static final String URL_USER_IMAGE = "https://www.flocolive.com/public/uploads/users/profile/";
    public static final String URL_VDO = "https://www.flocolive.com/public/uploads/properties/video/";
    public static final String URL_PDF = "https://www.flocolive.com/public/uploads/properties/pdf/";


    public static final String URL_CURRENT_BOOKING = DOMAIN_NAME+"currentBooking";
    public static final String URL_ALL_BOOKING = DOMAIN_NAME+"allBooking";
    public static final String URL_UPCOMING_BOOKING = DOMAIN_NAME+"upcomingBooking";
    public static final String URL_CANCELLED_BOOKING = DOMAIN_NAME+"cancelledBooking";
    public static final String URL_REVIEW_FEEDBACK = DOMAIN_NAME+"review";
    public static final String URL_ALL_REQUEST = DOMAIN_NAME+"getallServices";
    public static final String URL_POST_REQUEST = DOMAIN_NAME+"setServiceRequest";
    public static final String URL_MY_REQUEST = DOMAIN_NAME+"getServiceRequestByUser";
    public static final String URL_CLOSED_REQUEST = DOMAIN_NAME+"closeServices";
    public static final String URL_DELETE_REQUEST = DOMAIN_NAME+"cancelServiceRequestByUser";
    public static final String URL_UPDATE_REQUEST = DOMAIN_NAME+"updateServiceRequestByUser";
    public static final String URL_COUNTRY = DOMAIN_NAME+"country";
    public static final String URL_NATIONALITY = DOMAIN_NAME+"nationality";
    public static final String URL_STATE = DOMAIN_NAME+"state";
    public static final String URL_CITY = DOMAIN_NAME+"city";
    public static final String URL_OCCUPATION = DOMAIN_NAME+"occupations";
    public static final String URL_HOBBIES = DOMAIN_NAME+"hobbies";
    public static final String URL_UPDATE_PROFILE = DOMAIN_NAME+"setProfileDetails";
    public static final String URL_UPDATE_IMAGE = DOMAIN_NAME+"updateuserimage";
    public static final String URL_GET_PROPERTY = DOMAIN_NAME+"getProperty";
    public static final String URL_SEARCH_PROPERTY = DOMAIN_NAME+"searchProperty";
    public static final String URL_FILTER = DOMAIN_NAME+"filter";
    public static final String URL_DATE_AVILABILITY = DOMAIN_NAME+"checkRoomAvailability";
    public static final String URL_GST = DOMAIN_NAME+"getgstrate";
    public static final String URL_SAVE_PAYMENT_DETAILS = DOMAIN_NAME+"appAirpaymentSuccess";
    public static final String URL_GET_PREFRENCE = DOMAIN_NAME+"getpreference";
    public static final String URL_SAVE_PREFRENCE = DOMAIN_NAME+"uploadpreferences";
    public static final String URL_REFFERAL = DOMAIN_NAME+"reffere";
    public static final String URL_NOTIFICATION = DOMAIN_NAME+"getappnotification";
    public static final String URL_CHECKOUT = DOMAIN_NAME+"billingdetails";
    public static final String URL_SCHEDULE_VISIT = DOMAIN_NAME+"schedulevisit";
    public static final String URL_SCHEDULE_VISIT_VERIFY_OTP = DOMAIN_NAME+"checkscehduleotp";
    public static final String URL_CONTACT_US = DOMAIN_NAME+"contact";
//---------LOGIN --
    public static final String URL_LOGIN = DOMAIN_NAME+"login";
    public static final String FORGET_PASSWORD = DOMAIN_NAME+"forgetPassword";
    public static final String LOGIN_NEW_PASSWORD = DOMAIN_NAME+"newPassword";
    public static final String LOGIN_VERIFY_OTP = DOMAIN_NAME+"verifyOTP";
//--------REGISTER--
    public static final String REISTER_VERIFY_OTP = DOMAIN_NAME+"verifyOTP";
    public static final String URL_REISTER = DOMAIN_NAME+"register";

//---booking guest--
    public static final String URL_GUEST_DETAILS = DOMAIN_NAME+"useridentity";
    public static final String URL_BOOK = DOMAIN_NAME+"bookProperty";
    public static final String URL_BOOK_CANCEL = DOMAIN_NAME+"calcelmybooking";
//--profile
    public static final String URL_GET_PROFILE = DOMAIN_NAME+"getProfileDetails";

//--Account Setting
    public static final String URL_GET_ACCOUNT_SETTING = DOMAIN_NAME+"getAccSetting";

//--Account Setting update
    public static final String URL_POST_ACCOUNT_SETTING = DOMAIN_NAME+"accountsetting";
    public static final String URL_PROMOCODE = DOMAIN_NAME+"checkpromocode";

//--Login With Google plus
    public static final String URL_GOOGLE_LOGIN= DOMAIN_NAME+"createGoogleUser";
    public static final String URL_FACEBOOK_LOGIN= DOMAIN_NAME+"createFacebookUser";
    public static final String URL_ADDITIONAL_DERTAILS= DOMAIN_NAME+"updatesocialprofile";
    public static final String URL_ADDITIONAL_DERTAILS_OTP= DOMAIN_NAME+"verifysocialOTP";
//--Guest Details
    public static final String URL_GUEST_DETAIL_INDIAN= DOMAIN_NAME+"useridentity";
    public static final String URL_WAIT_LIST= DOMAIN_NAME+"bookingWaitingList";


}
