package com.flo.floapp.MyRequest;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;

public class FragmentRequest extends Fragment {
    View view;
    TabLayout tabLayout;
    TextView textViewNew, textViewOpen,textViewClose;
    String mImage;
    ImageView imageView_userphoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_request_main, container, false);
        textViewNew = view.findViewById(R.id.text1);
        textViewOpen = view.findViewById(R.id.text2);
        textViewClose =view.findViewById(R.id.text3);
        imageView_userphoto = view.findViewById(R.id.imageView_userphoto);

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        FragmentNewRequest fragment = new FragmentNewRequest();
        ft.replace(R.id.content_frame_request, fragment);
        ft.commit();
        /*FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        FragmentNewRequest fragment = new FragmentNewRequest();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();*/
        mImage = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_IMAGE,"");

        textViewNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentNewRequest fragment = new FragmentNewRequest();
                ft.replace(R.id.content_frame_request, fragment);
                ft.commit();
                textViewNew.setBackgroundColor(Color.parseColor("#3dca24"));
                textViewOpen.setBackgroundResource(R.drawable.bg_req_not_select);
                textViewClose.setBackgroundResource(R.drawable.bg_req_not_select);
            }
        });

        textViewOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentMyRequst fragment = new FragmentMyRequst();
                ft.replace(R.id.content_frame_request, fragment);
                ft.commit();
                textViewOpen.setBackgroundColor(Color.parseColor("#3dca24"));
                textViewNew.setBackgroundResource(R.drawable.bg_req_not_select);
                textViewClose.setBackgroundResource(R.drawable.bg_req_not_select);
            }
        });

        textViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentClosedRequest fragment = new FragmentClosedRequest();
                ft.replace(R.id.content_frame_request, fragment);
                ft.commit();
                textViewOpen.setBackgroundResource(R.drawable.bg_req_not_select);
                textViewNew.setBackgroundResource(R.drawable.bg_req_not_select);
                textViewClose.setBackgroundColor(Color.parseColor("#3dca24"));
            }
        });

        /*final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager_request);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout_request);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();*/

        Glide.with(getActivity())
                .load(Config.URL_USER_IMAGE+mImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_property_thumbnail)
                .into(imageView_userphoto);

        return view;
    }

     /*  private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.request_tab_text, null);
        tabOne.setText("New Request");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.request_tab_text, null);
        tabTwo.setText("My Request");
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Light.ttf");
        tabOne.setTypeface(myCustomFontBold);
        tabTwo.setTypeface(myCustomFontBold);
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentRequest.ViewPagerAdapter adapter = new FragmentRequest.ViewPagerAdapter(getFragmentManager());
        adapter.addFrag(new FragmentNewRequest(), "New Request");
        adapter.addFrag(new FragmentMyRequst(), "My Request");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }*/


}