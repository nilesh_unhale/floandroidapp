package com.flo.floapp.MyRequest;

public class PojoMyRequest {
    String srNumber;
    String requestCategory;
    String requestCharge;
    String requestId;
    String remark;

    public PojoMyRequest(String srNumber, String requestCategory, String requestCharge, String requestId, String remark) {
        this.srNumber = srNumber;
        this.requestCategory = requestCategory;
        this.requestCharge = requestCharge;
        this.requestId = requestId;
        this.remark = remark;
    }

    public String getSrNumber() {
        return srNumber;
    }

    public void setSrNumber(String srNumber) {
        this.srNumber = srNumber;
    }

    public String getRequestCategory() {
        return requestCategory;
    }

    public void setRequestCategory(String requestCategory) {
        this.requestCategory = requestCategory;
    }

    public String getRequestCharge() {
        return requestCharge;
    }

    public void setRequestCharge(String requestCharge) {
        this.requestCharge = requestCharge;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
