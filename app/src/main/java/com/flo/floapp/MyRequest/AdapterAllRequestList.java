package com.flo.floapp.MyRequest;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.MyBooking.ActivityRateProperty;
import com.flo.floapp.MyBooking.AdapterCurrentBooking;
import com.flo.floapp.MyBooking.PojoCurrentBooking;
import com.flo.floapp.R;
import com.flo.floapp.SearchProperty.ActivityPropertyList;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterAllRequestList extends RecyclerView.Adapter<AdapterAllRequestList.ViewHolder> {
    private ArrayList<PojoAllRequest> arraylist;
    private List<PojoAllRequest> listPojos;
    private Context context;

    public AdapterAllRequestList(Context context,List<PojoAllRequest> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoAllRequest>();
        arraylist.addAll(listPojos);
    }

    @Override
    public AdapterAllRequestList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.single_all_request, parent, false);
        AdapterAllRequestList.ViewHolder rcv = new AdapterAllRequestList.ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final AdapterAllRequestList.ViewHolder holder, int position) {

        final PojoAllRequest pojoAllRequest =listPojos.get(position);
        holder.tv_name.setText(pojoAllRequest.getRequestName());
        holder.tv_amount.setText("INR. "+pojoAllRequest.getRequestAmount());

        holder.btn_postRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String userId= PreferenceUtil.readString(context, PreferenceStrings.USER_ID,"");
               String remarks= holder.et_requestRemark.getText().toString();

              if (!remarks.equals("")){
                    postRequest(pojoAllRequest.getRequestId(), remarks,pojoAllRequest.getRequestType(),userId);

                }else {
                  Toast.makeText(context,"Please Enter Remark ",Toast.LENGTH_LONG).show();

              }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.listPojos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_name,tv_amount, text_ESTIMATE;
        private EditText et_requestRemark;
        private Button btn_postRequest;
        public ViewHolder(View view) {
            super(view);

            tv_amount = (TextView)view.findViewById(R.id.tv_requestCharge);
            tv_name = (TextView)view.findViewById(R.id.tv_requestName);
            btn_postRequest = (Button)view.findViewById(R.id.btn_postRequest);
            et_requestRemark = (EditText)view.findViewById(R.id.et_requestRemark);
            text_ESTIMATE = (TextView)view.findViewById(R.id.text_ESTIMATE);;

            //------------Font Applying-------------//
            Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
            Typeface myCustomFont = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
            Typeface myCustomFontExtraBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-ExtraBold.ttf");

            tv_name.setTypeface(myCustomFontBold);
            tv_amount.setTypeface(myCustomFontBold);
            et_requestRemark.setTypeface(myCustomFont);
            btn_postRequest.setTypeface(myCustomFontBold);
            text_ESTIMATE.setTypeface(myCustomFontBold);
        }
    }

    private void postRequest(final String requestId, final String remark, final String requestType, final String userId) {
        {
            HttpsTrustManager.allowAllSSL();
            String tag_string_req = "string_req";
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Config.URL_POST_REQUEST,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pDialog.dismiss();
                            try {
                                JSONObject reg_jsonObject = new JSONObject(response);
                                String res_Status = reg_jsonObject.getString("status");
                                final String res_Message = reg_jsonObject.getString("message");
                                if (res_Status.equals("true")) {
                                   final Dialog dialog = new Dialog(context);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.dialog_servive_post_confirmation);

                                    TextView text_TitleRequestSend = dialog.findViewById(R.id.text_TitleRequestSend);
                                    TextView text_ThanksUser = dialog.findViewById(R.id.text_ThanksUser);
                                    TextView text_YourReqSent = dialog.findViewById(R.id.text_YourReqSent);
                                    TextView text_FloOperationTeam = dialog.findViewById(R.id.text_FloOperationTeam);
                                    Button button_service = dialog.findViewById(R.id.button_service);

                                    //------------Font Applying-------------//
                                    Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
                                    Typeface myCustomFont = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
                                    text_TitleRequestSend.setTypeface(myCustomFontBold);
                                    text_ThanksUser.setTypeface(myCustomFont);
                                    text_ThanksUser.setText("Thank you "+PreferenceUtil.readString(context, PreferenceStrings.FIRST_NAME,"")+",");
                                    text_YourReqSent.setTypeface(myCustomFont);
                                    text_FloOperationTeam.setTypeface(myCustomFont);
                                    button_service.setTypeface(myCustomFont);

                                    button_service.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();

                                        }
                                    });
                                    dialog.show();
                                }
                                    else {
                                    Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    Toast.makeText(context, "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("concierge_id", requestId);
                    params.put("description", remark);
                    params.put("type", requestType);
                    params.put("user_id",PreferenceUtil.readString(context, PreferenceStrings.USER_ID,""));
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

        }
    }
}