package com.flo.floapp.MyRequest;

import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.SearchProperty.ResponceProList;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterMyRequest extends RecyclerView.Adapter<AdapterMyRequest.ViewHolder> {
    private ArrayList<PojoMyRequest> arraylist;
    private List<PojoMyRequest> listPojos;
    private Context context;
    private FragmentMyRequst fragmentMyRequst;

    public AdapterMyRequest(Context context,List<PojoMyRequest> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoMyRequest>();
        arraylist.addAll(listPojos);
    }

    @Override
    public AdapterMyRequest.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.single_my_request, parent, false);
        AdapterMyRequest.ViewHolder rcv = new AdapterMyRequest.ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final AdapterMyRequest.ViewHolder holder, final int position) {

        final PojoMyRequest pojo =listPojos.get(position);
         holder.tvRemark.setText(pojo.getRequestCharge());
         holder.tvRequestCategory.setText(pojo.getRequestCategory());
         holder.tvRequestId.setText(pojo.getRequestId());
         holder.tvRequestCharges.setText(pojo.getRemark());

       holder.iv_editRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_update_my_request);
                TextView updateRequest = dialog.findViewById(R.id.text_titleUpdateRequest);
                Button button_update = dialog.findViewById(R.id.button_service_update);
                final EditText et_update = dialog.findViewById(R.id.et_update_request);

                //------------Font Applying-------------//
                Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
                Typeface myCustomFont = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
                updateRequest.setTypeface(myCustomFontBold);
                button_update.setTypeface(myCustomFont);
                et_update.setTypeface(myCustomFont);

                button_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!et_update.getText().toString().equals("")){
                            updateServiceRequest(pojo.getSrNumber(),et_update.getText().toString().trim());
                            dialog.dismiss();
                        }
                        else {
                            Toast.makeText(context,"Enter Remark ",Toast.LENGTH_LONG).show();
                        }
                    }
                });
                dialog.show();

            }
        });

        holder.iv_delete_rwquest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                               // String itemLabel = String.valueOf(arraylist.get(position));
                                listPojos.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,listPojos.size());
                                deleteServiceRequest(pojo.getSrNumber());
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
    }

    private void updateServiceRequest(final String id, final String remark)
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.show();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_UPDATE_REQUEST,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(context, "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                params.put("description", remark);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);





    }


    private void deleteServiceRequest(final String cancelid) {
        {
            HttpsTrustManager.allowAllSSL();
            String tag_string_req = "string_req";
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Config.URL_DELETE_REQUEST,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pDialog.dismiss();
                            try {
                                JSONObject reg_jsonObject = new JSONObject(response);
                                String res_Status = reg_jsonObject.getString("status");
                                final String res_Message = reg_jsonObject.getString("message");
                                if (res_Status.equals("true")) {
                                    Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                                }
                                else {
                                    Toast.makeText(context, "" + res_Message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    Toast.makeText(context, "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("conciergeRequest_id", cancelid);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);





        }
    }

    @Override
    public int getItemCount() {
        return this.listPojos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvRemark,tvRequestId,tvRequestCharges,tvRequestCategory,
                text_CHARGES, text_REQUESTID, text_REMARK;
        private EditText et_requestRemark;
        private Button btn_postRequest;
        ImageView iv_editRequest,iv_delete_rwquest;

        public ViewHolder(View view) {
            super(view);

            tvRequestCategory = (TextView)view.findViewById(R.id.tvRequestCategory);
            tvRequestCharges = (TextView) view.findViewById(R.id.tvRequestCharges);
            tvRequestId = (TextView)view.findViewById(R.id.tvRequestId);
            tvRemark = (TextView)view.findViewById(R.id.tvRemark);
            iv_editRequest = (ImageView) view.findViewById(R.id.iv_editRequest);
            iv_delete_rwquest = (ImageView)view.findViewById(R.id.iv_DeleteRequest);

            text_CHARGES = (TextView) view.findViewById(R.id.text_CHARGES);
            text_REQUESTID = (TextView)view.findViewById(R.id.text_REQUESTID);
            text_REMARK = (TextView)view.findViewById(R.id.text_REMARK);

            //------------Font Applying-------------//
            Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
            Typeface myCustomFont = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
            tvRequestCategory.setTypeface(myCustomFont);
            tvRequestCharges.setTypeface(myCustomFont);
            tvRequestId.setTypeface(myCustomFont);
            tvRemark.setTypeface(myCustomFont);
            text_CHARGES.setTypeface(myCustomFontBold);
            text_REQUESTID.setTypeface(myCustomFontBold);
            text_REMARK.setTypeface(myCustomFontBold);

        }
    }

}
