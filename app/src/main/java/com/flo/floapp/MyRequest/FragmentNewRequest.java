package com.flo.floapp.MyRequest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.MyBooking.AdapterCurrentBooking;
import com.flo.floapp.MyBooking.AdapterMyBookingDetails;
import com.flo.floapp.MyBooking.PojoCurrentBooking;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentNewRequest extends Fragment {
    View view;
    RecyclerView recyclerViewAllRequest;
    ProgressBar progressbarAllRequest;
    ArrayList<PojoAllRequest> arrayList_AllRequest;
    AdapterAllRequestList adapterAllRequestList;
    TextView textViewEmptyRequest;
    String mUSER_ID;
    ImageView iv_noBookingFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_request, container, false);
        textViewEmptyRequest = (TextView) view.findViewById(R.id.tv_emptyRequest);
        progressbarAllRequest = (ProgressBar) view.findViewById(R.id.progressbarAllRequest);
        recyclerViewAllRequest = (RecyclerView)view.findViewById(R.id.recyclerViewAllRequest);
        iv_noBookingFound = view.findViewById(R.id.iv_noBookingFound);

        mUSER_ID = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");

        recyclerViewAllRequest.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewAllRequest.setNestedScrollingEnabled(true);
        recyclerViewAllRequest.setHasFixedSize(true);

        arrayList_AllRequest = new ArrayList<PojoAllRequest>();
        getAllRequest();
        progressbarAllRequest.setVisibility(View.VISIBLE);
        return view;
    }

    private void getAllRequest() {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_ALL_REQUEST,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbarAllRequest.setVisibility(View.GONE);
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                                for (int i = 0; i < MainArray.length(); i++) {

                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String requestId = propertyOBJ.getString("id");
                                    String requestName = propertyOBJ.getString("name");
                                    String requestDescription = propertyOBJ.getString("description");
                                    String requestType = propertyOBJ.getString("type");
                                    String requestAmount = propertyOBJ.getString("price");

                                    PojoAllRequest pojo = new PojoAllRequest(requestId,requestName,requestAmount,requestDescription,requestType);
                                    arrayList_AllRequest.add(pojo);
                                }
                                adapterAllRequestList = new AdapterAllRequestList(getActivity(),arrayList_AllRequest);
                                recyclerViewAllRequest.setAdapter(adapterAllRequestList);
                            } else {
                                Toast.makeText(getActivity(), "" + res_Message, Toast.LENGTH_LONG).show();
                                textViewEmptyRequest.setVisibility(View.VISIBLE);
                                //textViewEmptyRequest.setText(res_Message);
                                iv_noBookingFound.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbarAllRequest.setVisibility(View.GONE);
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mUSER_ID);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
