package com.flo.floapp.MyRequest;

public class PojoAllRequest {
    String requestId;
    String requestName;
    String requestAmount;
    String requestDescription;
    String requestType;

    public PojoAllRequest(String requestId, String requestName, String requestAmount, String requestDescription, String requestType) {
        this.requestId = requestId;
        this.requestName = requestName;
        this.requestAmount = requestAmount;
        this.requestDescription = requestDescription;
        this.requestType = requestType;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(String requestAmount) {
        this.requestAmount = requestAmount;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
