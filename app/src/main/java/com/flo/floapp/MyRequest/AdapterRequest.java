package com.flo.floapp.MyRequest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.flo.floapp.MyBooking.FragmentAllBooking;
import com.flo.floapp.MyBooking.FragmentCancelledBooking;
import com.flo.floapp.MyBooking.FragmentCurrentBooking;
import com.flo.floapp.MyBooking.FragmentUpComingBooking;

public class AdapterRequest extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AdapterRequest(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragmentNewRequest tab1 = new FragmentNewRequest();
                return tab1;
            case 1:
                FragmentMyRequst tab2 = new FragmentMyRequst();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
