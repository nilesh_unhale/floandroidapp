package com.flo.floapp.GUESTDetaills.GDPurposeVisitInfo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.PojoBasicInfo;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.PojoPassportDetails;
import com.flo.floapp.GUESTDetaills.GDVisaInfo.PojoVisaDetails;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PojoPermanentresidancy;
import com.flo.floapp.R;

public class GDPurposeOfVisit extends AppCompatActivity {
    FontChangeCrawler fontChanger;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gdpurpose_of_visit);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
    }
}
