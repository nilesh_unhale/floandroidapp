package com.flo.floapp.GUESTDetaills.GDVisaInfo;

import android.graphics.Bitmap;

import java.io.Serializable;

public class PojoVisaDetails implements Serializable {


    String mVisaDateOfIssue;
    String mVisaNumber ;
    String mVisaValidTill ;
    String mVisaPlaceIssue ;
    String mVisaType ;
    String mPerpose_ofVisit ;
    Bitmap mVisaImg;

    public String getmVisaDateOfIssue() {
        return mVisaDateOfIssue;
    }

    public void setmVisaDateOfIssue(String mVisaDateOfIssue) {
        this.mVisaDateOfIssue = mVisaDateOfIssue;
    }

    public String getmVisaNumber() {
        return mVisaNumber;
    }

    public void setmVisaNumber(String mVisaNumber) {
        this.mVisaNumber = mVisaNumber;
    }

    public String getmVisaValidTill() {
        return mVisaValidTill;
    }

    public void setmVisaValidTill(String mVisaValidTill) {
        this.mVisaValidTill = mVisaValidTill;
    }

    public String getmVisaPlaceIssue() {
        return mVisaPlaceIssue;
    }

    public void setmVisaPlaceIssue(String mVisaPlaceIssue) {
        this.mVisaPlaceIssue = mVisaPlaceIssue;
    }

    public String getmVisaType() {
        return mVisaType;
    }

    public void setmVisaType(String mVisaType) {
        this.mVisaType = mVisaType;
    }

    public String getmPerpose_ofVisit() {
        return mPerpose_ofVisit;
    }

    public void setmPerpose_ofVisit(String mPerpose_ofVisit) {
        this.mPerpose_ofVisit = mPerpose_ofVisit;
    }

    public Bitmap getmVisaImg() {
        return mVisaImg;
    }

    public void setmVisaImg(Bitmap mVisaImg) {
        this.mVisaImg = mVisaImg;
    }

    public PojoVisaDetails(String mVisaDateOfIssue, String mVisaNumber, String mVisaValidTill, String mVisaPlaceIssue, String mVisaType, String mPerpose_ofVisit, Bitmap mVisaImg) {
        this.mVisaDateOfIssue = mVisaDateOfIssue;
        this.mVisaNumber = mVisaNumber;
        this.mVisaValidTill = mVisaValidTill;
        this.mVisaPlaceIssue = mVisaPlaceIssue;
        this.mVisaType = mVisaType;
        this.mPerpose_ofVisit = mPerpose_ofVisit;
        this.mVisaImg = mVisaImg;
    }

    public PojoVisaDetails() {
    }


}
