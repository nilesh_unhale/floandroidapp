package com.flo.floapp.GUESTDetaills.GDLocalContactDetails;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.ActivityGuestOptions;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.GDInfo;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.PojoBasicInfo;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.PojoPassportDetails;
import com.flo.floapp.GUESTDetaills.GDPurposeVisitInfo.GDPurposeOfVisit;
import com.flo.floapp.GUESTDetaills.GDVisaInfo.PojoVisaDetails;
import com.flo.floapp.GUESTDetaills.GuestList.ActivityGuestList;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PojoPermanentresidancy;
import com.flo.floapp.MultipartRequest.MultipartRequest;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.Profile.ActivityUpdateProfile;
import com.flo.floapp.R;
import com.flo.floapp.SchedduleVisit.ActivityScheduleVisitVerify;
import com.flo.floapp.SearchProperty.ActivityPropertyList;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.rilixtech.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class LocalContact extends AppCompatActivity {
    Button button_NextLocalContact,button_BackLocalContact;
    FontChangeCrawler fontChanger;
    Spinner spinner_Lg_salutation,spinner_LG_Country,spinner_LG_state,spinner_LG_city;
    CountryCodePicker ccp_LG;
    EditText et_ref_name1,et_ref_name2,et_LG_lname,et_LG_number,
            et_LG_address1,et_LG_address2,et_LG_pincode,et_r_mobile1,et_r_mobile2;
    String[] list_country_name,list_country_Id,list_state_name,list_state_Id;
    String user_country,user_state,user_city;
    PojoBasicInfo pojoBasicInfo;
    PojoPermanentresidancy pojoPermanentresidancy;
    PojoPassportDetails pojoPassportDetails;
    PojoVisaDetails pojoVisaDetails;
    String mUSER_ID,mBOOKING_ID;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
    public static boolean isUploaded_nonIndian = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_local_contact);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        mUSER_ID = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.USER_ID,"");
        mBOOKING_ID = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.BOOKING_ID_FOR_NON_INDIAN,"");
        initUI();

        Intent intent = getIntent();
        pojoBasicInfo = (PojoBasicInfo)intent.getSerializableExtra("pojoBasicInfo");
        pojoPermanentresidancy = (PojoPermanentresidancy)intent.getSerializableExtra("pojoPermanentresidancy");
        pojoPassportDetails = (PojoPassportDetails) intent.getSerializableExtra("pojoPassportDetails");
        pojoVisaDetails = (PojoVisaDetails) intent.getSerializableExtra("pojoVisaDetails");

        getCountryList();
    }
    public void initUI(){
        button_NextLocalContact = findViewById(R.id.button_NextLocalContact);
        button_BackLocalContact = findViewById(R.id.button_BackLocalContact);
       // spinner_Lg_salutation = findViewById(R.id.spinner_LC_salutation);
        spinner_LG_Country = findViewById(R.id.spinner_LG_Country);
        spinner_LG_state = findViewById(R.id.spinner_LG_state);
        spinner_LG_city = findViewById(R.id.spinner_LG_city);

        et_ref_name1 = findViewById(R.id.et_LG_fname);
        et_ref_name2 = findViewById(R.id.et_ref_name2);

        et_r_mobile1 = findViewById(R.id.et_r_mobile1);
        et_r_mobile2 = findViewById(R.id.et_r_mobile2);

       // ccp_LG = findViewById(R.id.ccp_LG);
     //   et_LG_number = findViewById(R.id.et_LG_number);
        et_LG_address1 = findViewById(R.id.et_LG_address1);
        et_LG_address2 = findViewById(R.id.et_LG_address2);
        et_LG_pincode = findViewById(R.id.et_LG_pincode);

        button_BackLocalContact = findViewById(R.id.button_BackLocalContact);

        button_NextLocalContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String _fname1 = et_ref_name1.getText().toString().trim();
                String _fname2 = et_ref_name2.getText().toString().trim();
                String  mobile1 = et_ref_name1.getText().toString().trim();
                String  mobile2 = et_ref_name2.getText().toString().trim();

              //  String _lname = et_LG_lname.getText().toString().trim();
              //  String _Number = et_LG_number.getText().toString().trim();
                String _addrs1 = et_LG_address1.getText().toString().trim();
                String _addrs2 = et_LG_address2.getText().toString().trim();
                String _pincode = et_LG_pincode.getText().toString().trim();

                if(!_fname1.equals("") && !mobile1.equals("")&&
                        !_addrs1.equals("") && !_addrs2.equals("") && !_pincode.equals("")){

                   /* PojoLocalContact pojoLocalContact = new PojoLocalContact();
                    pojoLocalContact.set_fname(_fname);
                    pojoLocalContact.set_lname(_lname);
                    pojoLocalContact.set_Number(_Number);
                    pojoLocalContact.set_addrs1(_addrs1);
                    pojoLocalContact.set_addrs2(_addrs2);
                    pojoLocalContact.set_pincode(_pincode);
                    pojoLocalContact.set_Country(user_country);
                    pojoLocalContact.set_State(user_state);
                    pojoLocalContact.set_City(user_city);*/

                    postGuestDetailsForeigner(_fname1,_fname2,mobile1,mobile2, _addrs1, _addrs2, _pincode,
                            user_country, user_state,user_city);
                  /* if(mobile1.length() == 10 ){

                   }else {
                       Toast.makeText(LocalContact.this, "Enter Valid Mobile Numer", Toast.LENGTH_SHORT).show();
                   }*/

                }else {

                    Toast.makeText(LocalContact.this, "Enter All Fiels ", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
    public void postGuestDetailsForeigner(final String _fname1, String fname1, final String mobile1, final String mobile2, final String _addrs1, final String _addrs2, final String _pincode, String user_country, String user_state, String user_city) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Updating Details...");
        pDialog.show();
        MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST,"https://flocolive.com/stage1/api/useridentity",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                            Log.d("res",response.data.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                confirmationDialog(res_Message);
                            } else {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.d("error",error.toString());
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("booking_id", pojoBasicInfo.getBookingId());
                params.put("user_id", pojoBasicInfo.getUserId());

                params.put("fname", pojoBasicInfo.getmFirstName());
                params.put("lname", pojoBasicInfo.getmLastName());
                params.put("mobile", pojoBasicInfo.getmMobileNumber());
                params.put("email", pojoBasicInfo.getmEmailId());


             /*   //---------passport---------
                params.put("passport", pojoPassportDetails.getmPassport_Number());
                params.put("passport_place", pojoPassportDetails.getmPassport_Place_issue());
                params.put("passport_date", pojoPassportDetails.getmPassport_Issuedate());
                params.put("passport_expiry", pojoPassportDetails.getmPassport_ValidTill());


               //------permenent Address----
                params.put("address", pojoPermanentresidancy.getmAddrs1());
                params.put("address1", pojoPermanentresidancy.getmAddrs2());
                params.put("pincode", pojoPermanentresidancy.getmPin());
                params.put("city", pojoPermanentresidancy.getmCityy());
                params.put("state", pojoPermanentresidancy.getmState());
                params.put("country", pojoPermanentresidancy.getmCountry());
                params.put("nationality", pojoPermanentresidancy.getNationalityId());
                //---visa details-----------
                params.put("visa", pojoVisaDetails.getmVisaNumber());
                params.put("visa_from", pojoVisaDetails.getmVisaDateOfIssue());
                params.put("visa_to", pojoVisaDetails.getmVisaValidTill());
               // params.put("visa_type", pojoVisaDetails.getmVisaType());//---null
                params.put("visa_place", pojoVisaDetails.getmVisaPlaceIssue());
                params.put("purpose_visit", pojoVisaDetails.getmPerpose_ofVisit());
*/
                //---local address-----------
                params.put("r_address", _addrs1);
                params.put("r_address1", _addrs2);
                params.put("r_pincode", _pincode);
                params.put("r_country", LocalContact.this.user_country);
                params.put("r_state", LocalContact.this.user_state);
                params.put("r_city", LocalContact.this.user_city);
                params.put("r_mobile", mobile1);
                params.put("r_name", _fname1);
                params.put("r_name1", _fname1);
                params.put("r_mobile1", mobile2);


                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
              //  params.put("photoid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(pojoPassportDetails.getmPassportFrontImg())));
              //  params.put("addressid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(pojoPassportDetails.getmPassportBackImg())));
              //  params.put("visa_image",  new DataPart(imagename + ".png", getFileDataFromDrawable(pojoVisaDetails.getmVisaImg())));

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);

    }

   /* public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }*/


    //---get country
    private void getCountryList()
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_COUNTRY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_country_Id = new String[MainArray.length()];
                            list_country_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_country_Id[i] = mId;
                                    list_country_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(LocalContact.this,
                                        R.layout.spinner_item, list_country_name);
                                Adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_LG_Country.setAdapter(Adapter1);
                                spinner_LG_Country.setSelection(100);
                                spinner_LG_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        getStates(list_country_Id[position].toString().trim());
                                        user_country = list_country_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //----------------get states ------------
    private void getStates(final String id) {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_STATE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_state_Id = new String[MainArray.length()];
                            list_state_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_state_Id[i] = mId;
                                    list_state_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(LocalContact.this,
                                        R.layout.spinner_item, list_state_name);
                                Adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_LG_state.setAdapter(Adapter2);
                                spinner_LG_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        user_state = list_state_Id[position].toString().trim();
                                        getCity(user_state);

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("country_id", id);
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCity(final String user_state)
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req1";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            Log.d("flo city:",reg_jsonObject.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                final String[] list_city_Id = new String[MainArray.length()];
                                String[] list_city_name = new String[MainArray.length()];
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    Log.e("flo c:","id :"+mId +" name :"+mname);
                                    list_city_Id[i] = mId;
                                    list_city_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter3 = new ArrayAdapter<String>(LocalContact.this,
                                        R.layout.spinner_item, list_city_name);
                                Adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_LG_city.setAdapter(Adapter3);
                                spinner_LG_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        //  getCity(list_state_Id[position].toString().trim());
                                        user_city = list_city_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });


                            } else {
                                Toast.makeText(getApplicationContext(), "No City Found", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("state_id", user_state);
                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    //--confi
    public void confirmationDialog(String res_Message)
    {
        Dialog  dialog_visitSuccess = new Dialog(LocalContact.this);
        dialog_visitSuccess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_visitSuccess.setContentView(R.layout.dialog_confirmation);
        dialog_visitSuccess.show();
        TextView tv_title = dialog_visitSuccess.findViewById(R.id.tv_title);
        TextView tv_msg = dialog_visitSuccess.findViewById(R.id.msg);
        tv_title.setText("Upload Confirmation");
        tv_msg.setText(res_Message);
        Button button_service = dialog_visitSuccess.findViewById(R.id.button_service);

        button_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GDInfo.CountUploaded++;
                if (ActivityGuestList.mGuestCount == GDInfo.CountUploaded )
                {
                    Intent intent = new Intent(LocalContact.this, ActivityGuestOptions.class);
                    intent.putExtra("AllDone","AllDone");
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = getIntent();
                    intent.putExtra("name",pojoBasicInfo.getmGuestNameText());
                    intent.putExtra("position",pojoBasicInfo.getmGuestNumber());
                    setResult(2,intent);
                    finish();
                }
            }
        });


    }
}
