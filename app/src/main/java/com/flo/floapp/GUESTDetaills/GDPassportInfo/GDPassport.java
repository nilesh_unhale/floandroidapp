package com.flo.floapp.GUESTDetaills.GDPassportInfo;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.GDInfo;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.PojoBasicInfo;
import com.flo.floapp.GUESTDetaills.GDVisaInfo.GDVisaDetails;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PermanentResidency;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PojoPermanentresidancy;
import com.flo.floapp.MultipartRequest.MultipartRequest;
import com.flo.floapp.R;
import com.flo.floapp.SearchProperty.PojoPropertyDetails;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class GDPassport extends AppCompatActivity {
    FontChangeCrawler fontChanger;
    Button buttonNextPassport;
    EditText edittext_passportNo, edittext_dateOfIssue, edittext_ValidTill,edittext_placeIssue;
    private int mYear, mMonth, mDay;
    PojoBasicInfo pojoBasicInfo;
    PojoPermanentresidancy pojoPermanentresidancy;
    ImageView iv_passFront,iv_passBack;
    TextView tv_passSelectImg,tv_passSelectImgBack;
    Dialog dialogPassPort;
    int PICK_IMAGE_REQUEST = 111;
    int PICK_IMAGE_REQUEST1 = 112;
    Bitmap bitmapFront, bitmapBack;
    Uri filePath;
    PojoPassportDetails pojoPassportDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gdpassport);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

        Intent intent = getIntent();
        pojoBasicInfo = (PojoBasicInfo) intent.getSerializableExtra("pojoBasicInfo");
        pojoPermanentresidancy = (PojoPermanentresidancy) intent.getSerializableExtra("pojoPermanentresidancy");


        initUI();
    }
    public void initUI(){
        edittext_passportNo = findViewById(R.id.edittext_passportNo);
        edittext_dateOfIssue = findViewById(R.id.edittext_dateOfIssue);
        edittext_ValidTill = findViewById(R.id.edittext_ValidTill);
        edittext_placeIssue = findViewById(R.id.edittext_placeIssue);
        buttonNextPassport = findViewById(R.id.buttonNextPassport);
        iv_passFront = findViewById(R.id.iv_passBack);
        iv_passBack = findViewById(R.id.iv_passFront);
        tv_passSelectImg = findViewById(R.id.tv_passSelectImg);
        tv_passSelectImgBack = findViewById(R.id.tv_passSelectImgBack);


        tv_passSelectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });

        tv_passSelectImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST1);

            }
        });
        buttonNextPassport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mPassport_Number = edittext_passportNo.getText().toString().trim();
                String mPassport_Issuedate = edittext_dateOfIssue.getText().toString().trim();
                String mPassport_ValidTill = edittext_ValidTill.getText().toString().trim();
                String mPassport_Place_issue = edittext_placeIssue.getText().toString().trim();

                if(!mPassport_Number.equals("") && !mPassport_Issuedate.equals("")&& !mPassport_ValidTill.equals("")
                        && !mPassport_Place_issue.equals("")){

                    if (iv_passFront.getDrawable() == null || iv_passBack.getDrawable() ==null){

                        Toast.makeText(GDPassport.this, "Please Select Front and back side of PASSPORT", Toast.LENGTH_SHORT).show();

                    }else {
                        pojoPassportDetails = new PojoPassportDetails();
                        pojoPassportDetails.setmPassport_Issuedate(mPassport_Issuedate);
                        pojoPassportDetails.setmPassport_Number(mPassport_Number);
                        pojoPassportDetails.setmPassport_ValidTill(mPassport_ValidTill);
                        pojoPassportDetails.setmPassport_Place_issue(mPassport_Place_issue);
                        pojoPassportDetails.setmPassportFrontImg(String.valueOf(bitmapFront));
                        pojoPassportDetails.setmPassportBackImg(String.valueOf(bitmapBack));

                        uploadPasspordetails();
                    }

                }else {
                    Toast.makeText(GDPassport.this, "Enter All Fields", Toast.LENGTH_SHORT).show();
                }

            }
        });

        edittext_dateOfIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                Log.e("iNubex", mYear+"-"+mMonth+"-"+mDay);
                DatePickerDialog datePickerDialog = new DatePickerDialog(GDPassport.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edittext_dateOfIssue.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
        edittext_ValidTill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                Log.e("iNubex", mYear+"-"+mMonth+"-"+mDay);
                DatePickerDialog datePickerDialog = new DatePickerDialog(GDPassport.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edittext_ValidTill.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
    }

    public void openDialog(){
        dialogPassPort = new Dialog(GDPassport.this);
        dialogPassPort.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPassPort.setContentView(R.layout.dialog_front_back_img_selection);

        Button btn_upload_FrontImg = dialogPassPort.findViewById(R.id.btn_fSection);
        Button btn_upload_BackImg = dialogPassPort.findViewById(R.id.btn_bSection);

        btn_upload_FrontImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });

        btn_upload_BackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST1);

            }
        });
        dialogPassPort.show();
    }

    //------------result---
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {

                bitmapFront = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iv_passFront.setImageBitmap(bitmapFront);
              //  dialogPassPort.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == PICK_IMAGE_REQUEST1 && resultCode == RESULT_OK && data != null && data.getData() != null)

        {
            filePath = data.getData();
            if (filePath == null) {
                Toast.makeText(this, "Please Select Back Image", Toast.LENGTH_SHORT).show();
            } else {
                try {

                    bitmapBack = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    iv_passBack.setImageBitmap(bitmapBack);
                  //  dialogPassPort.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }


    //---------------passport uload

    public void uploadPasspordetails(){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST, Config.URL_GUEST_DETAIL_INDIAN,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                            Log.d("res",response.data.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Intent intent = new Intent(GDPassport.this, GDVisaDetails.class);
                                intent.putExtra("pojoPassportDetails",pojoPassportDetails);
                                intent.putExtra("pojoBasicInfo",pojoBasicInfo);
                                intent.putExtra("pojoPermanentresidancy",pojoPermanentresidancy);
                                startActivity(intent);

                            } else {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("booking_id", pojoBasicInfo.getBookingId());
                params.put("user_id", pojoBasicInfo.getUserId());
                params.put("fname", pojoBasicInfo.getmFirstName());
                params.put("lname", pojoBasicInfo.getmLastName());
                params.put("mobile", pojoBasicInfo.getmMobileNumber());
                params.put("email", pojoBasicInfo.getmEmailId());

               /* //--permanant residancy --
                params.put("address", pojoPermanentresidancy.getmAddrs1());
                params.put("address1", pojoPermanentresidancy.getmAddrs2());
                params.put("pincode", pojoPermanentresidancy.getmPin());
                params.put("city", pojoPermanentresidancy.getmCityy());
                params.put("state", pojoPermanentresidancy.getmState());
                params.put("country", pojoPermanentresidancy.getmCountry());*/

               //-----pasport details--
                params.put("passport", pojoPassportDetails.getmPassport_Number());
                params.put("passport_place", pojoPassportDetails.getmPassport_Place_issue());
                params.put("passport_date", pojoPassportDetails.getmPassport_Issuedate());
                params.put("passport_expiry", pojoPassportDetails.getmPassport_ValidTill());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                   params.put("photoid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmapFront)));
                   params.put("addressid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmapBack)));
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);


    }

       public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

}
