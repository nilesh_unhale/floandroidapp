package com.flo.floapp.GUESTDetaills.GDPassportInfo;

import android.graphics.Bitmap;

import java.io.Serializable;

public class PojoPassportDetails implements Serializable {

    String mPassport_Number ;
    String mPassport_Issuedate ;
    String mPassport_ValidTill ;
    String mPassport_Place_issue ;
    String mPassportFrontImg;
    String mPassportBackImg;

    public PojoPassportDetails() {
    }

    public String getmPassport_Number() {
        return mPassport_Number;
    }

    public void setmPassport_Number(String mPassport_Number) {
        this.mPassport_Number = mPassport_Number;
    }

    public String getmPassport_Issuedate() {
        return mPassport_Issuedate;
    }

    public void setmPassport_Issuedate(String mPassport_Issuedate) {
        this.mPassport_Issuedate = mPassport_Issuedate;
    }

    public String getmPassport_ValidTill() {
        return mPassport_ValidTill;
    }

    public void setmPassport_ValidTill(String mPassport_ValidTill) {
        this.mPassport_ValidTill = mPassport_ValidTill;
    }

    public String getmPassport_Place_issue() {
        return mPassport_Place_issue;
    }

    public void setmPassport_Place_issue(String mPassport_Place_issue) {
        this.mPassport_Place_issue = mPassport_Place_issue;
    }

    public String getmPassportFrontImg() {
        return mPassportFrontImg;
    }

    public void setmPassportFrontImg(String mPassportFrontImg) {
        this.mPassportFrontImg = mPassportFrontImg;
    }

    public String getmPassportBackImg() {
        return mPassportBackImg;
    }

    public void setmPassportBackImg(String mPassportBackImg) {
        this.mPassportBackImg = mPassportBackImg;
    }
}
