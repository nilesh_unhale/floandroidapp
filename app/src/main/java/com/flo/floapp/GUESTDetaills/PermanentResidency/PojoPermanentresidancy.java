package com.flo.floapp.GUESTDetaills.PermanentResidency;

import java.io.Serializable;

public class PojoPermanentresidancy implements Serializable {

    String mCountry;
    String mState;
    String mCityy;
    String mAddrs1;
    String mAddrs2 ;
    String mPin ;
    String nationalityId;

    public PojoPermanentresidancy() {
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public String getmCityy() {
        return mCityy;
    }

    public void setmCityy(String mCityy) {
        this.mCityy = mCityy;
    }

    public String getmAddrs1() {
        return mAddrs1;
    }

    public void setmAddrs1(String mAddrs1) {
        this.mAddrs1 = mAddrs1;
    }

    public String getmAddrs2() {
        return mAddrs2;
    }

    public void setmAddrs2(String mAddrs2) {
        this.mAddrs2 = mAddrs2;
    }

    public String getmPin() {
        return mPin;
    }

    public void setmPin(String mPin) {
        this.mPin = mPin;
    }

    public String getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(String nationalityId) {
        this.nationalityId = nationalityId;
    }
}
