package com.flo.floapp.GUESTDetaills.GDBasicInfo;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class PojoBasicInfo implements Serializable {

    String mCountry;
    String mSalutation;
    String mFirstName;
    String mLastName;
    String mCountryCode;
    String mMobileNumber;
    String mEmailId;
    String mIsEmployeeInIndia;
    String mGender;
    String userId;
    String bookingId;
    Bitmap mImage;
    String mGuestNameText;
    int mGuestNumber;
    String mStartDate;
    String mEndDate;
    String mbooking_code;
    String mPropertyName;

    public PojoBasicInfo(String mCountry, String mSalutation, String mFirstName, String mLastName, String mCountryCode, String mMobileNumber, String mEmailId, String mIsEmployeeInIndia, String mGender, String userId, String bookingId, Bitmap mImage, String mGuestNameText, int mGuestNumber, String mStartDate, String mEndDate, String mbooking_code, String mPropertyName) {
        this.mCountry = mCountry;
        this.mSalutation = mSalutation;
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mCountryCode = mCountryCode;
        this.mMobileNumber = mMobileNumber;
        this.mEmailId = mEmailId;
        this.mIsEmployeeInIndia = mIsEmployeeInIndia;
        this.mGender = mGender;
        this.userId = userId;
        this.bookingId = bookingId;
        this.mImage = mImage;
        this.mGuestNameText = mGuestNameText;
        this.mGuestNumber = mGuestNumber;
        this.mStartDate = mStartDate;
        this.mEndDate = mEndDate;
        this.mbooking_code = mbooking_code;
        this.mPropertyName = mPropertyName;
    }

    public PojoBasicInfo() {
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getmSalutation() {
        return mSalutation;
    }

    public void setmSalutation(String mSalutation) {
        this.mSalutation = mSalutation;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmCountryCode() {
        return mCountryCode;
    }

    public void setmCountryCode(String mCountryCode) {
        this.mCountryCode = mCountryCode;
    }

    public String getmMobileNumber() {
        return mMobileNumber;
    }

    public void setmMobileNumber(String mMobileNumber) {
        this.mMobileNumber = mMobileNumber;
    }

    public String getmEmailId() {
        return mEmailId;
    }

    public void setmEmailId(String mEmailId) {
        this.mEmailId = mEmailId;
    }

    public String getmIsEmployeeInIndia() {
        return mIsEmployeeInIndia;
    }

    public void setmIsEmployeeInIndia(String mIsEmployeeInIndia) {
        this.mIsEmployeeInIndia = mIsEmployeeInIndia;
    }

    public String getmGender() {
        return mGender;
    }

    public void setmGender(String mGender) {
        this.mGender = mGender;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Bitmap getmImage() {
        return mImage;
    }

    public void setmImage(Bitmap mImage) {
        this.mImage = mImage;
    }

    public String getmGuestNameText() {
        return mGuestNameText;
    }

    public void setmGuestNameText(String mGuestNameText) {
        this.mGuestNameText = mGuestNameText;
    }

    public int getmGuestNumber() {
        return mGuestNumber;
    }

    public void setmGuestNumber(int mGuestNumber) {
        this.mGuestNumber = mGuestNumber;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public String getmEndDate() {
        return mEndDate;
    }

    public void setmEndDate(String mEndDate) {
        this.mEndDate = mEndDate;
    }

    public String getMbooking_code() {
        return mbooking_code;
    }

    public void setMbooking_code(String mbooking_code) {
        this.mbooking_code = mbooking_code;
    }

    public String getmPropertyName() {
        return mPropertyName;
    }

    public void setmPropertyName(String mPropertyName) {
        this.mPropertyName = mPropertyName;
    }
}
