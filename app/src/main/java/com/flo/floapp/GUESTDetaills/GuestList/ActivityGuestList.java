package com.flo.floapp.GUESTDetaills.GuestList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flo.floapp.GUESTDetaills.GDBasicInfo.GDInfo;
import com.flo.floapp.Login.Login;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.VirtualTour.ActivityVirtaulTour;

import java.util.ArrayList;

public class ActivityGuestList extends AppCompatActivity {
    String booking_id, mStartDate, mEndDate,
            mUserFirstName, mUserLastName, mbooking_code, mPropertyName;
    int maleGuestCount, femaleGuestCount;
    public static int mGuestCount;
    ListView listView;
    ArrayList<String> list_guest = new ArrayList<>();
    ArrayAdapter<String> adapter;
    int selectedPos = -1;
    int mGuestNumber;
    TextView buttonDoItLater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_guest_list);
        listView = (ListView) findViewById(R.id.lv);
        buttonDoItLater = findViewById(R.id.buttonDoItLater);
        Intent intent = getIntent();
        booking_id = intent.getStringExtra("booking_id");
        maleGuestCount = intent.getIntExtra("maleGuestCount", 0);
        femaleGuestCount  = intent.getIntExtra("femaleGuestCount", 0);
        mStartDate = intent.getStringExtra("mStartDate");
        mEndDate = intent.getStringExtra("mEndDate");
        mUserFirstName = intent.getStringExtra("mUserFirstName");
        mUserLastName = intent.getStringExtra("mUserLastName");
        mbooking_code = intent.getStringExtra("mbooking_code");
        mPropertyName = intent.getStringExtra("mPropertyName");

     //---for guest options screen
        PreferenceUtil.writeString(getApplicationContext(),PreferenceStrings.PROPERTY_NAME,mPropertyName);
        PreferenceUtil.writeString(getApplicationContext(),PreferenceStrings.PROPERTY_CODE,mbooking_code);
        PreferenceUtil.writeString(getApplicationContext(),PreferenceStrings.FIRST_NAME,mUserFirstName);
        PreferenceUtil.writeString(getApplicationContext(),PreferenceStrings.START_DATE,mStartDate);
        PreferenceUtil.writeString(getApplicationContext(),PreferenceStrings.END_DATE,mEndDate);

        mGuestCount = maleGuestCount + femaleGuestCount;

        buttonDoItLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityGuestList.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        if(femaleGuestCount !=0 && maleGuestCount !=0){
            prepareListData();
        }else if (femaleGuestCount !=0){
            prepareListFemale();
        }else if (maleGuestCount !=0){
            prepareListMale();
        }

    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            if (requestCode == 2) {
                String name = data.getStringExtra("name");
                int mPosition = data.getIntExtra("position",0);
                list_guest.set(selectedPos, name + " (uploaded)");
                listView.getChildAt(mPosition).setOnClickListener(null);
                adapter.notifyDataSetChanged();

            }
        }

    }

    //---male --
    public void prepareListMale(){
        int i;
        for (i = 1; i <= maleGuestCount; i++) {
            list_guest.add((i)+"."+"GUEST (FEMALE)");
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list_guest);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedPos = i;
                    String name = list_guest.get(i);
                    Intent intent = new Intent(ActivityGuestList.this, GDInfo.class);
                    intent.putExtra("name", name);
                    intent.putExtra("position", i);
                    intent.putExtra("mStartDate", mStartDate);
                    intent.putExtra("mEndDate", mEndDate);
                    intent.putExtra("mUserFirstName", mUserFirstName);
                    intent.putExtra("mbooking_code", mbooking_code);
                    intent.putExtra("mPropertyName", mPropertyName);
                    intent.putExtra("booking_id", booking_id);
                    startActivityForResult(intent, 2);
                    //Toast.makeText(ActivityGuestList.this, ""+(i+1), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    //---female --
    public void prepareListFemale(){
        int i;
        for (i = 1; i <= femaleGuestCount; i++) {
            list_guest.add((i)+"."+"GUEST (FEMALE)");
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list_guest);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedPos = i;
                    String name = list_guest.get(i);
                    Intent intent = new Intent(ActivityGuestList.this, GDInfo.class);
                    intent.putExtra("name", name);
                    intent.putExtra("position", i);
                    intent.putExtra("mStartDate", mStartDate);
                    intent.putExtra("mEndDate", mEndDate);
                    intent.putExtra("mUserFirstName", mUserFirstName);
                    intent.putExtra("mbooking_code", mbooking_code);
                    intent.putExtra("mPropertyName", mPropertyName);
                    intent.putExtra("booking_id", booking_id);
                    startActivityForResult(intent, 2);
                    //Toast.makeText(ActivityGuestList.this, ""+(i+1), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    @SuppressLint("ResourceType")
    private void prepareListData() {

        int i;
//----for female---
        for (i = 1; i <= maleGuestCount; i++) {
            list_guest.add(i+"."+"GUEST (MALE)");
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list_guest);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedPos = i;
                    String name = list_guest.get(i);
                    Intent intent = new Intent(ActivityGuestList.this, GDInfo.class);
                    intent.putExtra("name", name);
                    intent.putExtra("position", i);
                    intent.putExtra("mStartDate", mStartDate);
                    intent.putExtra("mEndDate", mEndDate);
                    intent.putExtra("mUserFirstName", mUserFirstName);
                    intent.putExtra("mbooking_code", mbooking_code);
                    intent.putExtra("mPropertyName", mPropertyName);
                    intent.putExtra("booking_id", booking_id);
                    startActivityForResult(intent, 2);

                   // Toast.makeText(ActivityGuestList.this, ""+(i+1), Toast.LENGTH_SHORT).show();
                }
            });
        }

        //----for female---
        for (i = 1; i <= femaleGuestCount; i++) {
            list_guest.add((i+1)+"."+"GUEST (FEMALE)");
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list_guest);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedPos = i;
                    String name = list_guest.get(i);
                    Intent intent = new Intent(ActivityGuestList.this, GDInfo.class);
                    intent.putExtra("name", name);
                    intent.putExtra("position", i);
                    intent.putExtra("mStartDate", mStartDate);
                    intent.putExtra("mEndDate", mEndDate);
                    intent.putExtra("mUserFirstName", mUserFirstName);
                    intent.putExtra("mbooking_code", mbooking_code);
                    intent.putExtra("mPropertyName", mPropertyName);
                    intent.putExtra("booking_id", booking_id);
                    startActivityForResult(intent, 2);
                    //Toast.makeText(ActivityGuestList.this, ""+(i+1), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

}
