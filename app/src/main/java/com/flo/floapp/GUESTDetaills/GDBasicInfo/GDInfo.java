package com.flo.floapp.GUESTDetaills.GDBasicInfo;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.ActivityGuestOptions;
import com.flo.floapp.GUESTDetaills.GDLocalContactDetails.LocalContact;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.GDPassport;
import com.flo.floapp.GUESTDetaills.GDVisaInfo.GDVisaDetails;
import com.flo.floapp.GUESTDetaills.GuestList.ActivityGuestList;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PermanentResidency;
import com.flo.floapp.MultipartRequest.MultipartRequest;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class GDInfo extends AppCompatActivity {
    FontChangeCrawler fontChanger;
    Button button_skip_Indian_GD, button_SubmitIndian_GD, btn_ForeignerPassportfront, btn_ForeignerVisa, btn_ForeignerPassportback,
            buttonSubmitForeigner;
    Spinner spinner_country, spinner_salutationIndian, foreinr_spinner_salutation, spinner_foreignerNationality;
    EditText et_firstName, et_LastName, et_MobNumber, et_Email, foreinr_firstName,
            foreinerLastName, et_numberNonIndian, et_emailNonIndian;
    RadioGroup genderGuestIndian, radiogroup_IdType, radiogroup_IdTypeForeigner,
            rg_Gender, radiogroup_nationality;
    RadioButton rb_maleIndian, rb_femaleIndian, rb_aadharCard, rb_passportForeigner, rb_VisaForeigner,
            rb_passport, rb_voterID, rb_DrivingLicence, indian_rb, foreinr_rb,
            rb_male_nonIndian, rb_female_nonIndian;

    ImageView id_PhotoPreview,id_PhotoPreviewBack, iv_visaForeigner, iv_FrontPassportForeigner, iv_BackPassportForeigner;
    String mGender, mSelectID, mSalutation, mCountry, nationalityId;
    CountryCodePicker ccpIndian, ccpNonIndian;
    Bitmap bitmap, bitmapBack,bit_passFront,bit_passBack, bit_visa;
    int PICK_IMAGE_REQUEST = 111;
    int PICK_IMAGE_REQUEST1 = 112;
    int PICK_IMAGE_REQUEST2 = 113;
    int PICK_IMAGE_REQUEST3 = 114;
    int PICK_IMAGE_REQUEST4 = 115;
    private static final int CAMERA_IND_FRONT = 1888;
    private static final int CAMERA_IND_BACK = 1889;
    private static final int CAMERA_PASS_FRONT = 1881;
    private static final int CAMERA_PASS_BACK = 1882;
    private static final int CAMERA_VISA = 1883;
    private static final int REQUEST_WRITE_STORAGE = 1;
    String imgDecodableString, mUSER_ID, mBookingid,mNAME;
    LinearLayout ll_layout_nonIndian, ll_layout_indian;
    CheckBox cb_emplyeeInIndia;
    PojoBasicInfo pojoBasicInfo;
    String[] list_country_name, list_country_Id;
    String mIndianOrNot, imgString;
    Dialog dialogIndianGuest;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
    Uri filePath;
    public static int CountUploaded = 0 ;
    String mEmployeedInIndia = "2";
    String cc_indian,cc_ninIndian;
    int mFemaleGuestCount,mMaleGuestCount;
    Button btn_ind_front,btn_ind_back;
    int mGuestNumber;
    ImageButton imageButtonBack;
    String name,mStartDate,mEndDate,mUserFirstName,mUserLastName,mbooking_code,mPropertyName;
    int mPosition,_selectedNationality;
    String[] mlist_nationality_name, mlist_nationality_Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gdinfo);
        EnableRuntimePermissionToAccessCamera();
        ActivityCompat.requestPermissions(GDInfo.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
        initUI();
        Intent intent = getIntent();
        mMaleGuestCount = intent.getIntExtra("maleGuestCount",0);
        name = intent.getStringExtra("name");
        mPosition = intent.getIntExtra("position",0);
        mFemaleGuestCount = intent.getIntExtra("femaleGuestCount",0);
        mStartDate = intent.getStringExtra("mStartDate");
        mEndDate = intent.getStringExtra("mEndDate");
        mUserFirstName = intent.getStringExtra("mUserFirstName");
        mUserLastName = intent.getStringExtra("mUserLastName");
        mbooking_code = intent.getStringExtra("mbooking_code");
        mPropertyName = intent.getStringExtra("mPropertyName");
      //  mBookingid = intent.getStringExtra("booking_id");
        mBookingid ="194";
        PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.BOOKING_ID_FOR_NON_INDIAN, mBookingid);
        ///mUSER_ID = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.USER_ID, "");
        mUSER_ID = "154";
        mNAME = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.FIRST_NAME, "");
        //_selectedNationality = 85;
      //  _selectedNationality = Integer.parseInt(PreferenceUtil.readString(getApplicationContext(),PreferenceStrings.USER_NATIONALITY,""));
       // Toast.makeText(this, "gd:  "+_selectedNationality, Toast.LENGTH_SHORT).show();
        mGuestNumber = mPosition+1;
        getNationality();
    }

    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(GDInfo.this,
                Manifest.permission.CAMERA))
        {

            // Printing toast message after enabling runtime permission.
            Toast.makeText(GDInfo.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(GDInfo.this,new String[]{Manifest.permission.CAMERA}, 100);

        }
    }



    private void getNationality() {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_NATIONALITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            mlist_nationality_Id = new String[MainArray.length()];
                            mlist_nationality_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    mlist_nationality_Id[i] = mId;
                                    mlist_nationality_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(GDInfo.this,
                                        R.layout.spinner_item, mlist_nationality_name);
                                spinner_foreignerNationality.setAdapter(Adapter1);
                                spinner_foreignerNationality.setSelection(86);
                                spinner_foreignerNationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        nationalityId = mlist_nationality_Id[position].toString().trim();

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void initUI() {
        ll_layout_nonIndian = findViewById(R.id.layout_nonIndian);
        ll_layout_indian = findViewById(R.id.layout_indian);
        button_SubmitIndian_GD = findViewById(R.id.button_SubmitIndian_GD);
     //   button_Indian_GD = findViewById(R.id.button_Indian_GD);
        spinner_country = findViewById(R.id.spinner_country);
        spinner_salutationIndian = findViewById(R.id.spinner_salutationIndian);
        et_firstName = findViewById(R.id.et_firstName);
        et_LastName = findViewById(R.id.et_LastName);
        et_MobNumber = findViewById(R.id.et_MobNumber);
        et_Email = findViewById(R.id.et_Email);
        btn_ind_front = findViewById(R.id.btn_indfront);
        btn_ind_back = findViewById(R.id.btn_indback);
        genderGuestIndian = findViewById(R.id.genderGuestIndian);
        radiogroup_IdType = findViewById(R.id.radiogroup_IdType);

        radiogroup_nationality = findViewById(R.id.rb_nationality);
        indian_rb = findViewById(R.id.indian_rb);
        foreinr_rb = findViewById(R.id.foreinr_rb);
        rb_maleIndian = findViewById(R.id.rb_maleIndian);
        rb_femaleIndian = findViewById(R.id.rb_femaleIndian);
        rb_aadharCard = findViewById(R.id.rb_aadharCard);
        rb_passport = findViewById(R.id.rb_passport);
        rb_voterID = findViewById(R.id.rb_voterID);
        rb_DrivingLicence = findViewById(R.id.rb_DrivingLicence);
        ccpIndian = findViewById(R.id.ccpIndian);
        id_PhotoPreview = findViewById(R.id.id_PhotoPreview);
        id_PhotoPreviewBack = findViewById(R.id.id_PhotoPreviewBack);
        imageButtonBack = findViewById(R.id.imageButtonBack);
        button_skip_Indian_GD = findViewById(R.id.button_skip_Indian_GD);

        //--------foreiner layouts-------------
        foreinr_spinner_salutation = findViewById(R.id.foreinr_spinner_salutation);
        foreinr_firstName = findViewById(R.id.foreinr_fanme);
        foreinerLastName = findViewById(R.id.foreinr_lname);
        ccpNonIndian = findViewById(R.id.foreinr_ccp);
        et_numberNonIndian = findViewById(R.id.foreinr_number);
        et_emailNonIndian = findViewById(R.id.foreinr_email);
        cb_emplyeeInIndia = findViewById(R.id.foreinr_cb_employee);
        rg_Gender = findViewById(R.id.foreinr_rg);
        rb_male_nonIndian = findViewById(R.id.foreinr_rb_male);
        rb_female_nonIndian = findViewById(R.id.foreinr_rb_female);

        buttonSubmitForeigner = findViewById(R.id.buttonSubmitForeigner);
        btn_ForeignerPassportfront = findViewById(R.id.btn_ForeignerPassportfront);
        btn_ForeignerPassportback = findViewById(R.id.btn_ForeignerPassportback);
        btn_ForeignerVisa = findViewById(R.id.btn_ForeignerVisa);
        radiogroup_IdTypeForeigner = findViewById(R.id.radiogroup_IdTypeForeigner);
        rb_passportForeigner = findViewById(R.id.rb_passportForeigner);
        rb_VisaForeigner = findViewById(R.id.rb_VisaForeigner);
        iv_FrontPassportForeigner = findViewById(R.id.iv_FrontPassportForeigner);
        iv_visaForeigner = findViewById(R.id.iv_visaForeigner);
        iv_BackPassportForeigner = findViewById(R.id.iv_BackPassportForeigner);
        spinner_foreignerNationality = findViewById(R.id.spinner_foreignerNationality);



        /*if (mGender.equals("1")){
            rb_maleIndian.setChecked(true);
            rb_male_nonIndian.setChecked(true);

        }else {
            rb_female_nonIndian.setChecked(true);
            rb_femaleIndian.setChecked(true);
        }*/
        button_skip_Indian_GD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ccpIndian.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                cc_indian = selectedCountry.getPhoneCode();
            }
        });

        ccpNonIndian.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                cc_ninIndian = selectedCountry.getPhoneCode();

            }
        });

        rb_male_nonIndian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = String.valueOf(genderGuestIndian.getCheckedRadioButtonId());
                mGender = "1";
            }
        });
        rb_female_nonIndian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = String.valueOf(genderGuestIndian.getCheckedRadioButtonId());
                mGender = "2";
            }
        });

        indian_rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIndianOrNot = String.valueOf(radiogroup_nationality.getCheckedRadioButtonId());
                mIndianOrNot = "1";
            }
        });
        foreinr_rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIndianOrNot = String.valueOf(radiogroup_nationality.getCheckedRadioButtonId());
                mIndianOrNot = "2";
            }
        });


        radiogroup_nationality.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb) {
                    if (rb.getText().toString().equals(" Indian")) {
                        ll_layout_nonIndian.setVisibility(View.GONE);
                        ll_layout_indian.setVisibility(View.VISIBLE);

                    } else {
                        ll_layout_nonIndian.setVisibility(View.VISIBLE);
                        ll_layout_indian.setVisibility(View.GONE);


                    }
                }

            }
        });

        foreinr_spinner_salutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSalutation = foreinr_spinner_salutation.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cb_emplyeeInIndia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b==true){
                    mEmployeedInIndia = "1";
                }
                else {
                    mEmployeedInIndia = "2";
                }
            }
        });


        buttonSubmitForeigner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String _Email = et_emailNonIndian.getText().toString().trim();
                String _Number = et_numberNonIndian.getText().toString().trim();
                String _nameFirst = foreinr_firstName.getText().toString().trim();
                String _nameLast = foreinerLastName.getText().toString().trim();


                if (!_Email.equals("") && !_Number.equals("") && !_nameFirst.equals("") && !_nameLast.equals("")) {
                    if (_Email.matches(emailPattern)) {

                            if (_Number.length()==10){

                                if (iv_FrontPassportForeigner.getDrawable() == null || iv_BackPassportForeigner.getDrawable() ==null){

                                    Toast.makeText(GDInfo.this, "Please Select Front and back side of Passport", Toast.LENGTH_SHORT).show();
                                }else {

                                    if(iv_visaForeigner.getDrawable() == null){
                                        Toast.makeText(GDInfo.this, "Please Select Front side of Visa ", Toast.LENGTH_SHORT).show();

                                    }else {

                                        /*if (rg_Gender.getCheckedRadioButtonId() == -1) {
                                            Toast.makeText(GDInfo.this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                                        } else {

                                        }
                                        */
                                        pojoBasicInfo = new PojoBasicInfo();
                                        pojoBasicInfo.setmEmailId(_Email);
                                        pojoBasicInfo.setmMobileNumber(_Number);
                                        pojoBasicInfo.setmFirstName(_nameFirst);
                                        pojoBasicInfo.setmLastName(_nameLast);
                                        pojoBasicInfo.setmCountry("2");
                                        pojoBasicInfo.setmCountryCode(cc_ninIndian);
                                        pojoBasicInfo.setmSalutation(mSalutation);
                                        pojoBasicInfo.setmGender(mGender);
                                        pojoBasicInfo.setmIsEmployeeInIndia(mEmployeedInIndia);
                                        pojoBasicInfo.setUserId(mUSER_ID);
                                        pojoBasicInfo.setBookingId(mBookingid);
                                        pojoBasicInfo.setmGuestNameText(name);
                                        pojoBasicInfo.setmGuestNumber(mPosition);
                                        pojoBasicInfo.setmStartDate(mStartDate);
                                        pojoBasicInfo.setmEndDate(mEndDate);
                                        pojoBasicInfo.setMbooking_code(mbooking_code);
                                        pojoBasicInfo.setmPropertyName(mPropertyName);
                                        uploadGDBasicNon();
                                    }

                                }


                            }else {
                                et_numberNonIndian.setError("Please Enter Valid Mobile Number");

                            }


                        } else {
                            et_emailNonIndian.setError("Please Enter Valid E-mail Address");
                        }


                } else {
                    Toast.makeText(GDInfo.this, "All Fields Are Mandatory", Toast.LENGTH_SHORT).show();

                }

            }
        });


//-------------Select Gender RadioButton
        rb_maleIndian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = String.valueOf(genderGuestIndian.getCheckedRadioButtonId());
                mGender = "1";
            }
        });
        rb_femaleIndian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = String.valueOf(genderGuestIndian.getCheckedRadioButtonId());
                mGender = "2";
            }
        });

//-------------Select ID RadioButton
        rb_aadharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectID = String.valueOf(radiogroup_IdType.getCheckedRadioButtonId());
                mSelectID = "1";
            }
        });
        rb_passport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectID = String.valueOf(radiogroup_IdType.getCheckedRadioButtonId());
                mSelectID = "2";
            }
        });
        rb_voterID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectID = String.valueOf(radiogroup_IdType.getCheckedRadioButtonId());
                mSelectID = "3";
            }
        });
        rb_DrivingLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectID = String.valueOf(radiogroup_IdType.getCheckedRadioButtonId());
                mSelectID = "4";
            }
        });
//-------------Select Foreigner RadioButton
        rb_passportForeigner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectID = String.valueOf(radiogroup_IdType.getCheckedRadioButtonId());
                mSelectID = "1";
            }
        });
        rb_VisaForeigner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectID = String.valueOf(radiogroup_IdType.getCheckedRadioButtonId());
                mSelectID = "2";
            }
        });

//-----Select SalutationIndian--------//
        spinner_salutationIndian.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSalutation = spinner_salutationIndian.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


//------Foreigner upload photo

        btn_ForeignerPassportfront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               foreinPassFront();
            }
        });
        btn_ForeignerPassportback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foreinPassBack();
            }
        });
        btn_ForeignerVisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foreinVisa();
            }
        });


// Indian photo upload
        btn_ind_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indianChooseOptionsFront();
            }
        });



        btn_ind_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indianChooseOptionsBack();
            }
        });
        button_SubmitIndian_GD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mFirstName = et_firstName.getText().toString().trim();
                String mLastName = et_LastName.getText().toString().trim();
                String mMobNumber = et_MobNumber.getText().toString().trim();
                String mEmail = et_Email.getText().toString().trim();

                if (mFirstName == "" && mLastName == "" && mMobNumber == "" && mEmail == "") {
                    Toast.makeText(GDInfo.this, "Please enter all fields", Toast.LENGTH_SHORT).show();
                } else if (genderGuestIndian.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(GDInfo.this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                } else if (radiogroup_IdType.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(GDInfo.this, "Please Select ID proof", Toast.LENGTH_SHORT).show();
                } else {

                    if (id_PhotoPreview.getDrawable() == null || id_PhotoPreviewBack.getDrawable() ==null){

                        Toast.makeText(GDInfo.this, "Please Select Front and back side of ID proof", Toast.LENGTH_SHORT).show();
                    }else {

                        if (mMobNumber.matches(mobilePattern) && mEmail.matches(emailPattern)){
                            postGuestDetailsIndianGuest(mCountry, mSalutation, mFirstName, mLastName, mMobNumber, mEmail,
                                    mGender, mSelectID);
                        }else {
                            Toast.makeText(GDInfo.this, "Please Enter valid Mobile and password", Toast.LENGTH_SHORT).show();

                        }


                    }

                }
            }
        });

    }

    private void indianChooseOptionsFront() {
        try {
                PackageManager pm = getPackageManager();
                int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                    final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GDInfo.this);
                    builder.setTitle("Select Option");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {
                                dialog.dismiss();
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_IND_FRONT);
                            } else if (options[item].equals("Choose From Gallery")) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST);
                            } else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();
                } else
                    Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    private void indianChooseOptionsBack() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GDInfo.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_IND_BACK);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST1);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    private void foreinPassFront() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GDInfo.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_PASS_FRONT);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST2);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    private void foreinPassBack() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GDInfo.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_PASS_BACK);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST3);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    private void foreinVisa() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GDInfo.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_VISA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST4);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //----------------------indian-------------------
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
             filePath = data.getData();
            try {

                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                id_PhotoPreview.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == CAMERA_IND_FRONT && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            id_PhotoPreview.setImageBitmap(bitmap);
        }

        if (requestCode == PICK_IMAGE_REQUEST1 && resultCode == RESULT_OK && data != null && data.getData() != null)

        {
             filePath = data.getData();
            if (filePath == null) {
                Toast.makeText(this, "Please Select Back Image", Toast.LENGTH_SHORT).show();
            } else {
                try {

                    bitmapBack = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    id_PhotoPreviewBack.setImageBitmap(bitmapBack);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (requestCode == CAMERA_IND_BACK && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            id_PhotoPreview.setImageBitmap(bitmapBack);
        }
//----------------------indian-end------------------


        if (requestCode == PICK_IMAGE_REQUEST2 && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            filePath = data.getData();
            try {

                bit_passFront = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iv_FrontPassportForeigner.setImageBitmap(bit_passFront);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_PASS_FRONT && resultCode == RESULT_OK) {
            bit_passFront = (Bitmap) data.getExtras().get("data");
            iv_FrontPassportForeigner.setImageBitmap(bit_passFront);
        }


        if (requestCode == PICK_IMAGE_REQUEST3 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {

                bit_passBack = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iv_BackPassportForeigner.setImageBitmap(bit_passBack);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_PASS_BACK && resultCode == RESULT_OK) {
            bit_passBack = (Bitmap) data.getExtras().get("data");
            iv_BackPassportForeigner.setImageBitmap(bit_passBack);
        }
        if (requestCode == PICK_IMAGE_REQUEST4 && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            filePath = data.getData();
            try {

                bit_visa = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iv_visaForeigner.setImageBitmap(bit_visa);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_VISA && resultCode == RESULT_OK) {
            bit_visa = (Bitmap) data.getExtras().get("data");
            iv_visaForeigner.setImageBitmap(bit_visa);
        }

    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void postGuestDetailsIndianGuest(final String mCountry, final String mSalutation, final String mFirstName,
                                            final String mLastName, final String mMobNumber, final String mEmail,
                                            final String mGender, final  String mSelectID) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Updating Details...");
        pDialog.show();
        MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST, Config.URL_GUEST_DETAIL_INDIAN,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                            Log.d("res",response.data.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                confirmationDialog(res_Message);
                            } else {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("fname", mFirstName);
                params.put("lname", mLastName);
                params.put("mobile", mMobNumber);
                params.put("email", mEmail);
                params.put("gender", mGender);
                params.put("photoid_type", "Passport");
                params.put("salutation", "1");
                params.put("photoid", mSelectID);
                params.put("booking_id", mBookingid);
                params.put("nationality", "1");
                params.put("user_id", mUSER_ID);
                params.put("guest_number", String.valueOf(mGuestNumber));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("photoid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                params.put("addressid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmapBack)));
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);


    }

    public void confirmationDialog(String res_Message)
    {
        final Dialog  dialog_visitSuccess = new Dialog(GDInfo.this);
        dialog_visitSuccess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_visitSuccess.setContentView(R.layout.dialog_confirmation);
        dialog_visitSuccess.show();
        TextView tv_title = dialog_visitSuccess.findViewById(R.id.tv_title);
        TextView tv_msg = dialog_visitSuccess.findViewById(R.id.msg);
        tv_title.setText("Upload Confirmation");
        tv_msg.setText(res_Message);
        Button button_service = dialog_visitSuccess.findViewById(R.id.button_service);

        button_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountUploaded++;
                if (ActivityGuestList.mGuestCount == CountUploaded)
                {
                    Intent intent = new Intent(GDInfo.this, ActivityGuestOptions.class);
                    intent.putExtra("AllDone","AllDone");
                    startActivity(intent);
                    finish();
                }else {

                    Intent intent = getIntent();
                    intent.putExtra("name",name);
                    intent.putExtra("position",mPosition);
                    setResult(2,intent);
                    finish();
                }
                dialog_visitSuccess.dismiss();
            }
        });


    }

    ///-------------------------------------NON -INDIAN-----------------------------------

    public void uploadGDBasicNon(){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST, Config.URL_GUEST_DETAIL_INDIAN,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                            Log.d("res",response.data.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                confirmationDialog(res_Message);

                                } else {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("fname", pojoBasicInfo.getmFirstName());
                params.put("lname", pojoBasicInfo.getmLastName());
                params.put("mobile", pojoBasicInfo.getmMobileNumber());
                params.put("email", pojoBasicInfo.getmEmailId());
                params.put("gender", pojoBasicInfo.getmGender());
                params.put("salutation", pojoBasicInfo.getmSalutation());
                params.put("guest_number", String.valueOf(mGuestNumber));
                params.put("booking_id", mBookingid);
                params.put("nationality", nationalityId);
                params.put("photoid_type", "Passport");
                params.put("user_id", mUSER_ID);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("photoid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bit_passFront)));
                params.put("addressid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bit_passBack)));
                params.put("visa_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bit_visa)));
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);

    }




}