package com.flo.floapp.GUESTDetaills.GDLocalContactDetails;

import java.io.Serializable;

public class PojoLocalContact implements Serializable {

    String _fname;
    String _lname;
    String _Number;
    String _addrs1;
    String _addrs2;
    String _pincode;
    String _Country;
    String _City;
    String _State;


    public String get_fname() {
        return _fname;
    }

    public void set_fname(String _fname) {
        this._fname = _fname;
    }

    public String get_lname() {
        return _lname;
    }

    public void set_lname(String _lname) {
        this._lname = _lname;
    }

    public String get_Number() {
        return _Number;
    }

    public void set_Number(String _Number) {
        this._Number = _Number;
    }

    public String get_addrs1() {
        return _addrs1;
    }

    public void set_addrs1(String _addrs1) {
        this._addrs1 = _addrs1;
    }

    public String get_addrs2() {
        return _addrs2;
    }

    public void set_addrs2(String _addrs2) {
        this._addrs2 = _addrs2;
    }

    public String get_pincode() {
        return _pincode;
    }

    public void set_pincode(String _pincode) {
        this._pincode = _pincode;
    }

    public String get_Country() {
        return _Country;
    }

    public void set_Country(String _Country) {
        this._Country = _Country;
    }

    public String get_City() {
        return _City;
    }

    public void set_City(String _City) {
        this._City = _City;
    }

    public String get_State() {
        return _State;
    }

    public void set_State(String _State) {
        this._State = _State;
    }

    public PojoLocalContact() {
    }

    public PojoLocalContact(String _fname, String _lname, String _Number, String _addrs1,
                            String _addrs2, String _pincode, String _Country,
                            String _City, String _State) {
        this._fname = _fname;
        this._lname = _lname;
        this._Number = _Number;
        this._addrs1 = _addrs1;
        this._addrs2 = _addrs2;
        this._pincode = _pincode;
        this._Country = _Country;
        this._City = _City;
        this._State = _State;
    }
}
