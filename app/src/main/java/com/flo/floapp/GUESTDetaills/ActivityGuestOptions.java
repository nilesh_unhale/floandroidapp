package com.flo.floapp.GUESTDetaills;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.BookingDetails.UploadDocument.ActivityUploadDocument;
import com.flo.floapp.BookingDetails.UploadDocument.ExpandableListAdapter;
import com.flo.floapp.BookingDetails.UploadDocument.GuestDetailsModel;
import com.flo.floapp.BookingDetails.UploadDocument.GuestDetailsRequestModel;
import com.flo.floapp.BookingDetails.UploadDocument.GuestResponce;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.GDInfo;
import com.flo.floapp.GUESTDetaills.GDLocalContactDetails.LocalContact;
import com.flo.floapp.GUESTDetaills.GuestList.ActivityGuestList;
import com.flo.floapp.Login.ForgetPassword;
import com.flo.floapp.Login.VerifyForgetOTP;
import com.flo.floapp.MyPreferences.AdapterPreference;
import com.flo.floapp.MyPreferences.PreferenceRequestModel;
import com.flo.floapp.MyPreferences.PreferenceResponceModel;
import com.flo.floapp.MyPreferences.SinglePreference;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.Profile.ActivityUpdateProfile;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class ActivityGuestOptions extends AppCompatActivity {
    ImageButton _SwitchCheckIn,_SwitchCheckOut,_SwitchCheckPreference;
    Dialog dialogCHECK_IN,dialogCheckout,dialogPreferences;
    String mUSER_ID;
    ArrayList<SinglePreference> mList;
    AdapterPreference _Adapter;
    RecyclerView _rv_preference;
    ProgressBar progressBar;
    FontChangeCrawler fontChanger;
    String user_city,user_state;
    Button button_guestOptDoItLater;
    JSONArray jsArray;
    private APIService services;

    //---guest doc details
    String mDocType;
    private ArrayList<HashMap<String, String>> guestDetailsListMap = new ArrayList<HashMap<String, String>>();
    ImageView myImage;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    List<String> list_child;
    LinearLayout container;
    int maleGuestCount, femaleGuestCount;
    private ArrayList<GuestDetailsModel> guestDetailsList = new ArrayList<GuestDetailsModel>();
    HashMap<String, String> meMap;
    private static String KEY_MALE_COUNT = "KEY_MALE_COUNT";
    private static String KEY_FEMALE_COUNT = "KEY_FEMALE_COUNT";
    private static String KEY_START_DATE = "KEY_START_DATE";
    private static String KEY_END_DATE = "KEY_END_DATE";
    private static String KEY_FIRST_NAME = "KEY_FIRST_NAME";
    private static String KEY_LAST_NAME = "KEY_LAST_NAME";
    private static String KEY_BOOKINGCODE = "KEY_BOOKINGCODE";
    private static String KEY_BOOKING_ID = "KEY_BOOKING_ID";
    private static String KEY_PROPERTY_NAME = "KEY_PROPERTY_NAME";
    Button btn_submit,btn_Clear;
    RadioGroup radioGroup_idType;
    RadioButton rb_pancard,rb_aadharcard,rb_drivinglicence;
    Spinner spinner_salutation;
    EditText fName,et_guestLast,et_guest_email,et_guest_number,et_document_no;
    Button btn_selectGuestImage;
    private static final int PICK_IMAGE_REQUEST = 1;
    Bitmap bitmap;
    View convertview;
    int selectedId;
    String[] list_country_name,list_country_Id,list_state_name,list_state_Id,list_city_Id
            ,list_city_name;

    String user_country,mPropertyname,mAllDone;
    Spinner spinner_user_city,spinner_user_state,spinner_user_country;
    TextView tv_bookingDuration,tv_propertyName,tv_guestTitle,tv_book_msg;
    String mStartDate,mEndDate,mUserFirstName,mUserLastName,mbooking_code,mbooking_id,mPropertyName;
    String _pname,_pcode,_sdate,_edate;


    public static Intent getActivityGuestOptions(Context context, int maleCount,
                                                 int femaleCount,String startDate,String endDate,
                                                 String firstname,String lastname,String bookingcode,String mBookingId,String mPropertyName){
        Intent intent = new Intent(context, ActivityGuestOptions.class);
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_MALE_COUNT,maleCount);
        bundle.putInt(KEY_FEMALE_COUNT,femaleCount);
        bundle.putString(KEY_START_DATE,startDate);
        bundle.putString(KEY_END_DATE,endDate);
        bundle.putString(KEY_FIRST_NAME,firstname);
        bundle.putString(KEY_LAST_NAME,lastname);
        bundle.putString(KEY_BOOKINGCODE,bookingcode);
        bundle.putString(KEY_BOOKING_ID,mBookingId);
        bundle.putString(KEY_PROPERTY_NAME,mPropertyName);
        intent.putExtras(bundle);
        return intent;
    }
    //---------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_guest_options);

        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));


        _SwitchCheckIn = findViewById(R.id.switch_checkIn);
        _SwitchCheckOut = findViewById(R.id.switch_checkOut);
        _SwitchCheckPreference = findViewById(R.id.switch_Preference);
        tv_bookingDuration = findViewById(R.id.tv_bookingDuration);
        tv_propertyName = findViewById(R.id.tv_propertyName);
        tv_guestTitle = findViewById(R.id._guestTitle);
        tv_book_msg = findViewById(R.id.tv_book_msg);
        button_guestOptDoItLater = findViewById(R.id.button_guestOptDoItLater);
        mUSER_ID = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.USER_ID,"");
         mUserFirstName = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.FIRST_NAME,"");
        _pname = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.PROPERTY_NAME,"");
        _pcode = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.PROPERTY_CODE,"");
        _sdate = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.START_DATE,"");
        _edate = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.END_DATE,"");



        //--guest details--
        Bundle bundle = getIntent().getExtras();
        maleGuestCount = bundle.getInt(KEY_MALE_COUNT);
        femaleGuestCount = bundle.getInt(KEY_FEMALE_COUNT);
        mStartDate = bundle.getString(KEY_START_DATE);
        mEndDate = bundle.getString(KEY_END_DATE);
        //mUserFirstName = bundle.getString(KEY_FIRST_NAME);
        mUserLastName = bundle.getString(KEY_LAST_NAME);
        mbooking_code = bundle.getString(KEY_BOOKINGCODE);
        mbooking_id = bundle.getString(KEY_BOOKING_ID);
        mPropertyName = bundle.getString(KEY_PROPERTY_NAME);

        tv_bookingDuration.setText("FROM "+mStartDate +" TO "+mEndDate);
        tv_guestTitle.setText("Hi ! "+mUserFirstName);
        //tv_guestTitle.setText("Hi ! ");
        tv_propertyName.setText(mPropertyName);
        tv_book_msg.setText("PLEASE REFERE YOUR BOOKING ID "+mbooking_code +" FOR ALL YOUR FUTURE COMMUNICATIONS. LOOOK FOR FORWARD TO HOST YOU");

        services = ApiUtils.getAPIService();
        if (getIntent().getStringExtra("AllDone")!=null){
            _SwitchCheckIn.setImageResource(R.drawable.ic_details_uploaded);
            _SwitchCheckIn.setClickable(false);

            tv_bookingDuration.setText("FROM "+_sdate +" TO "+_edate);
            tv_guestTitle.setText("Hi ! "+mUserFirstName);
            tv_propertyName.setText(_pname);
            tv_book_msg.setText("PLEASE REFERE YOUR BOOKING ID "+_pcode +" FOR ALL YOUR FUTURE COMMUNICATIONS. LOOOK FOR FORWARD TO HOST YOU");


        }
        //--guest details--
        _SwitchCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityGuestOptions.this, ActivityGuestList.class);
                intent.putExtra("booking_id",mbooking_id);
                intent.putExtra("maleGuestCount",maleGuestCount);
                intent.putExtra("femaleGuestCount",femaleGuestCount);
                intent.putExtra("mStartDate",mStartDate);
                intent.putExtra("mEndDate",mEndDate);
                intent.putExtra("mUserFirstName",mUserFirstName);
                intent.putExtra("mUserLastName",mUserLastName);
                intent.putExtra("mbooking_code",mbooking_code);
                intent.putExtra("mPropertyName",mPropertyName);
                startActivity(intent);
           /*     dialogCHECK_IN = new Dialog(ActivityGuestOptions.this,R.style.AppBaseTheme);
                dialogCHECK_IN.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogCHECK_IN.setContentView(R.layout.dialog_avoid_checkin);
                fontChanger.replaceFonts((ViewGroup)dialogCHECK_IN.findViewById(android.R.id.content));
                dialogCHECK_IN.show();
                Window window = dialogCHECK_IN.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                container = (LinearLayout) dialogCHECK_IN.findViewById(R.id.container_checkin);
                prepareListData();
                //
                //  ImageButton close = dialogCHECK_IN.findViewById(R.id.dialog_filter_close);
                btn_submit = (Button)dialogCHECK_IN.findViewById(R.id.btn_checkin_guest_details);
                btn_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareDate();
                    }
                });*/
            }
        });

        _SwitchCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCheckout = new Dialog(ActivityGuestOptions.this,R.style.AppBaseTheme);
                dialogCheckout.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogCheckout.setContentView(R.layout.dialog_avoid_checkout);

                final EditText et_companyName,et_GSTNumber,et_Address1,et_Address2,et_Pincode;
                CheckBox checkBox_billOnCompnyAddrss;
                Button btn_Back,btn_Done;
                et_companyName = dialogCheckout.findViewById(R.id.et_checkout_companyanme);
                et_GSTNumber = dialogCheckout.findViewById(R.id.et_checkout_gstnNumber);
                et_Pincode = dialogCheckout.findViewById(R.id.et_checkout_pincode);
                et_Address1 = dialogCheckout.findViewById(R.id.et_checkout_address1);
                et_Address2 = dialogCheckout.findViewById(R.id.et_checkout_address2);
                spinner_user_country = dialogCheckout.findViewById(R.id.spinner_checkout_country);
                spinner_user_city = dialogCheckout.findViewById(R.id.spinner_checkout_city);
                spinner_user_state = dialogCheckout.findViewById(R.id.spinner_checkout_state);
                checkBox_billOnCompnyAddrss = dialogCheckout.findViewById(R.id.cb_billOncomapny);
                btn_Back = dialogCheckout.findViewById(R.id.buttonCheckoutback);
                btn_Done = dialogCheckout.findViewById(R.id.buttonCheckoutSave);
                getCountryList();

                btn_Done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String mComapnyName= et_companyName.getText().toString();
                        String mGSTnUMBER= et_GSTNumber.getText().toString();
                        String mAddress1= et_Address1.getText().toString();
                        String mAddress2= et_Address2.getText().toString();
                        String mPin = et_Pincode.getText().toString();
                        if (!mAddress1.equals("")&& !mAddress2.equals("") && !mPin.equals("")){
                            PostBillingDetails(mComapnyName,mGSTnUMBER,mAddress1,mAddress2,mPin,user_country,user_state,user_city);

                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Enter Mandatory Fields ",Toast.LENGTH_LONG).show();
                        }

                    }
                });
                fontChanger.replaceFonts((ViewGroup)dialogCheckout.findViewById(android.R.id.content));
                dialogCheckout.show();
                Window window = dialogCheckout.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            }
        });

        _SwitchCheckPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllPreferences();
                dialogPreferences = new Dialog(ActivityGuestOptions.this,R.style.AppBaseTheme);
                dialogPreferences.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogPreferences.setContentView(R.layout.dialog_prefference);
                progressBar = dialogPreferences.findViewById(R.id.pb_guest);

                fontChanger.replaceFonts((ViewGroup)dialogPreferences.findViewById(android.R.id.content));
                progressBar.setVisibility(View.VISIBLE);

                Button btn_savepref = dialogPreferences.findViewById(R.id.buttonSave);
                Button btn_back = dialogPreferences.findViewById(R.id.buttonClaerAll);
                _rv_preference = dialogPreferences.findViewById(R.id.rv_preferences);
                _rv_preference.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                _rv_preference.setNestedScrollingEnabled(true);
                _rv_preference.setHasFixedSize(true);
                mList = new ArrayList<SinglePreference>();

                btn_savepref.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        savePreference();
                    }
                });
                btn_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogPreferences.dismiss();
                    }
                });
                dialogPreferences.show();
                Window window = dialogPreferences.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            }
        });

        button_guestOptDoItLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_Go_Home = new Intent(ActivityGuestOptions.this, MainActivity.class);
                startActivity(intent_Go_Home);
                finish();
            }
        });
    }

    private void savePreference() {
        {
            HttpsTrustManager.allowAllSSL();
            final ProgressDialog pDialog = new ProgressDialog(ActivityGuestOptions.this);
            pDialog.setMessage("Please wait ...");
            pDialog.show();
            try{
                PreferenceRequestModel paramsData = new PreferenceRequestModel();
                paramsData.setUserId(mUSER_ID);
                paramsData.setPreference(AdapterPreference._arrayPreference);

                services.savePreference(paramsData).enqueue(new Callback<PreferenceResponceModel>() {
                    @Override
                    public void onResponse(Call<PreferenceResponceModel> call, Response<PreferenceResponceModel> response) {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage().toString(),Toast.LENGTH_LONG).show();
                        dialogPreferences.dismiss();
                        Log.d("pr : ",response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<PreferenceResponceModel> call, Throwable t) {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void PostBillingDetails(final String mComapnyName, final String mGSTnUMBER, final String mAddress1, final String mAddress2, final String mPin, final String user_country, final String user_state, final String user_city) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CHECKOUT,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("company_name", mComapnyName);
                params.put("gst_no", mGSTnUMBER);
                params.put("companyPincode",mPin);
                params.put("cmpaddress", mAddress1);
                params.put("cmpaddress1", mAddress2);
                params.put("companyCountry",user_country);
                params.put("companyState", user_state);
                params.put("companyCity", user_city);
                params.put("billing_name", mComapnyName);
                params.put("user_id", mUSER_ID);
                return params;

            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void prepareDate() {

            for (int i = 1; i<= maleGuestCount;i++){

                convertview = (View)container.findViewById(10+i);
                GuestDetailsModel singleGuestDetails = new GuestDetailsModel();
                radioGroup_idType =convertview.findViewById(R.id.radioGroup_idType);
                rb_pancard =convertview.findViewById(R.id.rb_pancard);
                rb_aadharcard =convertview.findViewById(R.id.rb_aadharcard);
                rb_drivinglicence =convertview.findViewById(R.id.rb_drivinglicence);
                spinner_salutation = convertview.findViewById(R.id.spinner_salutation);
                fName = convertview.findViewById(R.id.et_guest_fname);
                et_guestLast = convertview.findViewById(R.id.et_guestLast);
                et_guest_email = convertview.findViewById(R.id.et_guest_email);
                et_guest_number = convertview.findViewById(R.id.et_guest_number);
                et_document_no = convertview.findViewById(R.id.et_document_no);
                btn_selectGuestImage = convertview.findViewById(R.id.btn_selectGuestImage);

                rb_aadharcard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedId = radioGroup_idType.getCheckedRadioButtonId();
                        mDocType = "AADHARCARD";

                    }
                });
                rb_pancard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedId = radioGroup_idType.getCheckedRadioButtonId();
                        mDocType = "PANCARD";

                    }
                });
                rb_drivinglicence.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedId = radioGroup_idType.getCheckedRadioButtonId();
                        mDocType = "DRIVINGLICENCE";

                    }
                });
                btn_selectGuestImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(),"hi",Toast.LENGTH_LONG).show();
                        //_selectImage();
                    }
                });
                // image_PreviewGuestImage = convertview.findViewById(R.id.image_PreviewGuestImage);

                if(!fName.getText().toString().equals("") && !et_document_no.getText().toString().equals("") && !et_guestLast.getText().toString().equals("")
                        && !et_guest_email.getText().toString().equals("") && !et_guest_number.getText().toString().equals("")) {

                    singleGuestDetails.setFirstName(fName.getText().toString());
                    singleGuestDetails.setLastName(et_guestLast.getText().toString());
                    singleGuestDetails.setEmailId(et_guest_email.getText().toString());
                    singleGuestDetails.setMobileNumber(et_guest_number.getText().toString());
                    singleGuestDetails.setPhotoIdNumber(et_document_no.getText().toString());
                    singleGuestDetails.setPhotoIdType("gg");
                    singleGuestDetails.setSalutation("Mr.");
                    singleGuestDetails.setGender("1");
                    singleGuestDetails.setUserId("1");

                    meMap=new HashMap<String, String>();
                    meMap.put("fname",singleGuestDetails.getFirstName());
                    meMap.put("lname",singleGuestDetails.getLastName());
                    meMap.put("email",singleGuestDetails.getEmailId());
                    meMap.put("gender",singleGuestDetails.getGender());
                    meMap.put("mobile",singleGuestDetails.getMobileNumber());
                    meMap.put("salutation",singleGuestDetails.getSalutation());
                    meMap.put("photoid_type","hhh");
                    meMap.put("photoid_image","www");
                    meMap.put("photoid",singleGuestDetails.getPhotoIdNumber());

                }else {
                    Toast.makeText(getApplicationContext(),"Please Enter All Guest Details",Toast.LENGTH_LONG).show();
                }
                guestDetailsListMap.add(meMap);
                // meMap.put("Color1","Red");
            }
            for (int i =1; i<=femaleGuestCount;i++){
                convertview = (View)container.findViewById(20+i);

                GuestDetailsModel singleGuestDetails = new GuestDetailsModel();
                singleGuestDetails.setSalutation("Miss");
                radioGroup_idType =convertview.findViewById(R.id.radioGroup_idType);
                rb_pancard =convertview.findViewById(R.id.rb_pancard);
                rb_aadharcard =convertview.findViewById(R.id.rb_aadharcard);
                rb_drivinglicence =convertview.findViewById(R.id.rb_drivinglicence);
                spinner_salutation = convertview.findViewById(R.id.spinner_salutation);
                fName = convertview.findViewById(R.id.et_guest_fname);
                et_guestLast = convertview.findViewById(R.id.et_guestLast);
                et_guest_email = convertview.findViewById(R.id.et_guest_email);
                et_guest_number = convertview.findViewById(R.id.et_guest_number);
                et_document_no = convertview.findViewById(R.id.et_document_no);

                rb_aadharcard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedId = radioGroup_idType.getCheckedRadioButtonId();
                        mDocType = "AADHARCARD";

                    }
                });
                rb_pancard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedId = radioGroup_idType.getCheckedRadioButtonId();
                        mDocType = "PANCARD";

                    }
                });
                rb_drivinglicence.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedId = radioGroup_idType.getCheckedRadioButtonId();
                        mDocType = "DRIVINGLICENCE";

                    }
                });

                if(!fName.getText().toString().equals("") && !et_document_no.getText().toString().equals("") && !et_guestLast.getText().toString().equals("")
                        && !et_guest_email.getText().toString().equals("") && !et_guest_number.getText().toString().equals("")) {

                    singleGuestDetails.setFirstName(fName.getText().toString());
                    singleGuestDetails.setLastName(et_guestLast.getText().toString());
                    singleGuestDetails.setEmailId(et_guest_email.getText().toString());
                    singleGuestDetails.setMobileNumber(et_guest_number.getText().toString());
                    singleGuestDetails.setPhotoIdNumber(et_document_no.getText().toString());
                    singleGuestDetails.setPhotoIdType(mDocType);
                    singleGuestDetails.setSalutation("Miss");
                    singleGuestDetails.setGender("2");
                    // singleGuestDetails.setUserId("1");

                    meMap=new HashMap<String, String>();
                    meMap.put("fname",singleGuestDetails.getFirstName());
                    meMap.put("lname",singleGuestDetails.getLastName());
                    meMap.put("email",singleGuestDetails.getEmailId());
                    meMap.put("gender",singleGuestDetails.getGender());
                    meMap.put("mobile",singleGuestDetails.getMobileNumber());
                    meMap.put("salutation",singleGuestDetails.getSalutation());
                    meMap.put("photoid_type","ff");
                    meMap.put("photoid_image","PAN");
                    meMap.put("photoid","123");


                }else {
                    Toast.makeText(getApplicationContext(),"Please Enter All the Fields",Toast.LENGTH_LONG).show();
                }

                guestDetailsListMap.add(meMap);
            }
            Log.e("MAP", guestDetailsListMap.toString());
            postGuestAPI();

    }

    private void postGuestAPI() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait Uploading Guest Details...");
        pDialog.show();
        try{


            String json = new Gson().toJson(guestDetailsListMap);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("booking_id", "2");
            params.put("user_id", "1");
            params.put("guest_array",json);

            //    "[{"fname":"raJU","lname":"baburao","email":"nee@gmail.com","gender":"2","mobile":"9545","salutation":"mr","photoid_type":"pan","photoid_image":"pan","photoid":"11122" },	{"fname":"raJU", "lname":"baburao","email":"nee@gmail1.com","gender":"2","mobile":"9545","salutation":"mr","photoid_type":"pan","photoid_image":"pan","photoid":"11122" }]"

            String data = new Gson().toJson(params);

            JSONObject jsonobj = new JSONObject(params);

            GuestDetailsRequestModel paramsData = new GuestDetailsRequestModel();
            paramsData.setBooking_id("2");
            paramsData.setUser_id("1");
            paramsData.setGuest_data(guestDetailsListMap);

            services.postGuest(paramsData/*,"1",json*/).enqueue(new Callback<GuestResponce>() {
                @Override
                public void onResponse(Call<GuestResponce> call, Response<GuestResponce> response) {
                    pDialog.dismiss();
                    alerUploadGuestDialog();
                    // Toast.makeText(getApplicationContext(),"xxx"+response.body().getMessage().toString(),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onFailure(Call<GuestResponce> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void alerUploadGuestDialog() {

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Guest details Successfully Uploaded...");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        Intent intent = new Intent(ActivityGuestOptions.this, MainActivity.class);
                        startActivity(intent);
                        arg0.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void prepareListData() {

        LayoutInflater infalInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //For male entries ID's for View we are starting from (10+i) and for female entries starting from (20+i)
        for (int i=1 ;i<= maleGuestCount;i++){

            final View convertView = infalInflater.inflate(R.layout.temp_linear_data_holder, null);
            convertView.setId(10+i);
            final LinearLayout l1 = (LinearLayout)convertView.findViewById(R.id.maincontainer);
            TextView text_guestCountAndGender = convertView.findViewById(R.id.text_guestCountAndGender);
            text_guestCountAndGender.setText("GUEST (MALE) "+(i));
            ImageView iv,iv_uploaded;
            iv = convertView.findViewById(R.id.iv);
            iv_uploaded = convertView.findViewById(R.id.iv_uploaded);
            RelativeLayout guest_Layout = convertView.findViewById(R.id.guest_Layout);
            guest_Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*if(l1.getVisibility()== View.VISIBLE){
                        l1.setVisibility(View.GONE);
                    }else{
                        l1.setVisibility(View.VISIBLE);
                    }*/
                    Intent intent = new Intent(convertView.getContext(), GDInfo.class);
                    intent.putExtra("booking_id",mbooking_id);
                    intent.putExtra("maleGuestCount",maleGuestCount);
                    intent.putExtra("femaleGuestCount",femaleGuestCount);
                    intent.putExtra("mStartDate",mStartDate);
                    intent.putExtra("mEndDate",mEndDate);
                    intent.putExtra("mUserFirstName",mUserFirstName);
                    intent.putExtra("mUserLastName",mUserLastName);
                    intent.putExtra("mbooking_id",mbooking_id);
                    intent.putExtra("mbooking_code",mbooking_code);
                    intent.putExtra("mPropertyName",mPropertyName);
                    startActivity(intent);

                }
            });
            /*final LinearLayout l2 = (LinearLayout)convertView.findViewById(R.id.maincontainer1);
            Switch switch_IndianOrNot = convertView.findViewById(R.id.switch_IndianOrNot);
            switch_IndianOrNot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(l2.getVisibility()== View.VISIBLE){
                        l2.setVisibility(View.GONE);
                    }else{
                        l2.setVisibility(View.VISIBLE);
                    }
                }
            });*/
            final ImageView image_PreviewGuestImage = convertView.findViewById(R.id.image_PreviewGuestImage);

            Button btn_selectGuestImage = convertView.findViewById(R.id.btn_selectGuestImage);
            btn_selectGuestImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  _selectImage(image_PreviewGuestImage);
                }
            });
            container.addView(convertView);
        }

        for (int i=1 ;i<=femaleGuestCount;i++){
            final View convertView = infalInflater.inflate(R.layout.temp_linear_data_holder, null);
            convertView.setId(20+i);
            final LinearLayout l1 = (LinearLayout)convertView.findViewById(R.id.maincontainer);
            TextView text_guestCountAndGender = convertView.findViewById(R.id.text_guestCountAndGender);
            text_guestCountAndGender.setText("GUEST (FEMALE) "+(i));
            RelativeLayout guest_Layout = convertView.findViewById(R.id.guest_Layout);
            guest_Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*if(l1.getVisibility()== View.VISIBLE){
                        l1.setVisibility(View.GONE);
                    }else{
                        l1.setVisibility(View.VISIBLE);
                    }*/

                    Intent intent = new Intent(convertView.getContext(), GDInfo.class);
                    intent.putExtra("booking_id",mbooking_id);
                    intent.putExtra("maleGuestCount",maleGuestCount);
                    intent.putExtra("femaleGuestCount",femaleGuestCount);
                    intent.putExtra("mStartDate",mStartDate);
                    intent.putExtra("mEndDate",mEndDate);
                    intent.putExtra("mUserFirstName",mUserFirstName);
                    intent.putExtra("mUserLastName",mUserLastName);
                    intent.putExtra("mbooking_id",mbooking_id);
                    intent.putExtra("mbooking_code",mbooking_code);
                    intent.putExtra("mPropertyName",mPropertyName);
                    startActivity(intent);
                }
            });
            final LinearLayout l2 = (LinearLayout)convertView.findViewById(R.id.maincontainer1);
            Switch switch_IndianOrNot = convertView.findViewById(R.id.switch_IndianOrNot);
            switch_IndianOrNot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(l2.getVisibility()== View.VISIBLE){
                        l2.setVisibility(View.GONE);
                    }else{
                        l2.setVisibility(View.VISIBLE);
                    }
                }
            });
            final ImageView image_PreviewGuestImage = convertView.findViewById(R.id.image_PreviewGuestImage);

            Button btn_selectGuestImage = convertView.findViewById(R.id.btn_selectGuestImage);
            btn_selectGuestImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  _selectImage(image_PreviewGuestImage);
                }
            });
            container.addView(convertView);
        }

    }

    private void getAllPreferences() {
        final ProgressDialog pDialog = new ProgressDialog(ActivityGuestOptions.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GET_PREFRENCE,
                new com.android.volley.Response.Listener<String>() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray array_preference = reg_jsonObject.getJSONArray("response");

                                for (int i=0;i<array_preference.length();i++){

                                    JSONObject mObj = array_preference.getJSONObject(i);
                                    String mPRE_ID = mObj.getString("id");
                                    String mPRE_NAME = mObj.getString("name");
                                    String mPRE_DISCRIPTION = mObj.getString("description");
                                    String mIs_selected = mObj.getString("selected");
                                    SinglePreference sp = new SinglePreference();

                                    sp.set_preferenceId(mPRE_ID);
                                    sp.set_preferenceName(mPRE_NAME);
                                    sp.set_preferenceDiscription(mPRE_DISCRIPTION);
                                    sp.set_preferenceIS_SELECTED(mIs_selected);
                                    mList.add(sp);
                                }
                                _Adapter = new AdapterPreference(getApplicationContext(), mList);
                                _rv_preference.setAdapter(_Adapter);


                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mUSER_ID);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCountryList() {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_COUNTRY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_country_Id = new String[MainArray.length()];
                            list_country_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_country_Id[i] = mId;
                                    list_country_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(ActivityGuestOptions.this,
                                        R.layout.spinner_item, list_country_name);
                                Adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_user_country.setAdapter(Adapter1);
                                spinner_user_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        getStates(list_country_Id[position].toString().trim());
                                        user_country = list_country_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getStates(final String id) {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_STATE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_state_Id = new String[MainArray.length()];
                            list_state_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_state_Id[i] = mId;
                                    list_state_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(ActivityGuestOptions.this,
                                        R.layout.spinner_item, list_state_name);
                                Adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_user_state.setAdapter(Adapter2);
                                spinner_user_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        user_state = list_state_Id[position].toString().trim();
                                        getCity(user_state);

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("country_id", id);
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCity(final String user_state)
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req1";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            Log.d("flo city:",reg_jsonObject.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                final String[] list_city_Id = new String[MainArray.length()];
                                String[] list_city_name = new String[MainArray.length()];
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    Log.e("flo c:","id :"+mId +" name :"+mname);
                                    list_city_Id[i] = mId;
                                    list_city_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter3 = new ArrayAdapter<String>(ActivityGuestOptions.this,
                                        R.layout.spinner_item, list_city_name);
                                Adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_user_city.setAdapter(Adapter3);
                                spinner_user_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        //  getCity(list_state_Id[position].toString().trim());
                                        user_city = list_city_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });


                            } else {
                                Toast.makeText(getApplicationContext(), "No City Found", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("state_id", user_state);
                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


}
