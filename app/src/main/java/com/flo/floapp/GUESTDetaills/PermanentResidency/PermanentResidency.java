package com.flo.floapp.GUESTDetaills.PermanentResidency;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.GDInfo;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.PojoBasicInfo;
import com.flo.floapp.GUESTDetaills.GDLocalContactDetails.LocalContact;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.GDPassport;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.PojoPassportDetails;
import com.flo.floapp.GUESTDetaills.GDVisaInfo.GDVisaDetails;
import com.flo.floapp.MultipartRequest.MultipartRequest;
import com.flo.floapp.R;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
public class PermanentResidency extends AppCompatActivity {
    Button button_NextPermanentReidency;
    FontChangeCrawler fontChanger;
    EditText edittext_Address1, edittext_Address2, edittext_pincode;
    Spinner spinner_Country, spinner_state, spinner_city,spinner_nationality;
    String[] list_country_name,list_country_Id,list_state_name,list_state_Id;
    String user_country,user_state,user_city,nationalityId;
    PojoBasicInfo pojoBasicInfo;
    String[] mlist_country_name, mlist_country_Id;
    PojoPermanentresidancy pojoPermanentresidancy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_permanent_residency);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

        Intent intent = getIntent();

        pojoBasicInfo = (PojoBasicInfo)intent.getSerializableExtra("pojoBasicInfo");
        getCountryList();
        //getNationality();
        initUI();
    }
    public void initUI(){
        edittext_Address1 = findViewById(R.id.edittext_Address1);
        edittext_Address2 = findViewById(R.id.edittext_Address2);
        edittext_pincode = findViewById(R.id.edittext_pincode);

        spinner_Country = findViewById(R.id.spinner_Country);
        spinner_state = findViewById(R.id.spinner_state);
        spinner_city = findViewById(R.id.spinner_city);
        spinner_nationality = findViewById(R.id.spinner_nationality);


        button_NextPermanentReidency = findViewById(R.id.button_NextPermanentReidency);
        button_NextPermanentReidency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mAddrs1 = edittext_Address1.getText().toString().trim();
                String mAddrs2 = edittext_Address2.getText().toString().trim();
                String mPin = edittext_pincode.getText().toString().trim();

                if(!mAddrs1.equals("") && !mAddrs2.equals("")&& !mPin.equals("")){

                     pojoPermanentresidancy = new PojoPermanentresidancy();
                    pojoPermanentresidancy.setmAddrs1(mAddrs1);
                    pojoPermanentresidancy.setmAddrs2(mAddrs2);
                    pojoPermanentresidancy.setmPin(mPin);
                    pojoPermanentresidancy.setmCountry(user_country);
                    pojoPermanentresidancy.setmState(user_state);
                    pojoPermanentresidancy.setmCityy(user_city);
                    uploadPermanantResidancy();

                }else {
                    Toast.makeText(PermanentResidency.this, "Enter All Fields", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    private void getCountryList()
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_COUNTRY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_country_Id = new String[MainArray.length()];
                            list_country_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_country_Id[i] = mId;
                                    list_country_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(PermanentResidency.this,
                                        R.layout.spinner_item, list_country_name);
                                Adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_Country.setAdapter(Adapter1);
                                spinner_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        getStates(list_country_Id[position].toString().trim());
                                        user_country = list_country_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //----------------get states ------------
    private void getStates(final String id) {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_STATE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_state_Id = new String[MainArray.length()];
                            list_state_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_state_Id[i] = mId;
                                    list_state_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(PermanentResidency.this,
                                        R.layout.spinner_item, list_state_name);
                                spinner_state.setAdapter(Adapter2);
                                Adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        user_state = list_state_Id[position].toString().trim();
                                        getCity(user_state);

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("country_id", id);
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCity(final String user_state)
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req1";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            Log.d("flo city:",reg_jsonObject.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                final String[] list_city_Id = new String[MainArray.length()];
                                String[] list_city_name = new String[MainArray.length()];
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    Log.e("flo c:","id :"+mId +" name :"+mname);
                                    list_city_Id[i] = mId;
                                    list_city_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter3 = new ArrayAdapter<String>(PermanentResidency.this,
                                        android.R.layout.simple_spinner_dropdown_item, list_city_name);
                                spinner_city.setAdapter(Adapter3);
                                Adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        //  getCity(list_state_Id[position].toString().trim());
                                        user_city = list_city_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });


                            } else {
                                Toast.makeText(getApplicationContext(), "No City Found", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("state_id", user_state);
                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void getNationality()
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_NATIONALITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            mlist_country_Id = new String[MainArray.length()];
                            mlist_country_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_country_Id[i] = mId;
                                    list_country_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(PermanentResidency.this,
                                        R.layout.spinner_item, list_country_name);
                                spinner_nationality.setAdapter(Adapter1);
                                spinner_nationality.setSelection(85);
                                spinner_nationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        nationalityId = list_country_Id[position].toString().trim();

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public void uploadPermanantResidancy(){
        {
            HttpsTrustManager.allowAllSSL();
            final ProgressDialog pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please Wait...");
            pDialog.show();
            MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST, Config.URL_GUEST_DETAIL_INDIAN,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            pDialog.dismiss();
                            try {
                                JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                                Log.d("res",response.data.toString());
                                String res_Status = reg_jsonObject.getString("status");
                                final String res_Message = reg_jsonObject.getString("message");
                                if (res_Status.equals("true")) {

                                    Intent intent = new Intent(PermanentResidency.this, GDPassport.class);
                                    intent.putExtra("pojoPermanentresidancy",pojoPermanentresidancy);
                                    intent.putExtra("pojoBasicInfo",pojoBasicInfo);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pDialog.dismiss();
                            String message = null;
                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                            } else if (error instanceof ServerError) {
                                message = "The server could not be found. Please try again after some time!!";
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                            }
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("booking_id", pojoBasicInfo.getBookingId());
                    params.put("user_id", pojoBasicInfo.getUserId());

                    params.put("fname", pojoBasicInfo.getmFirstName());
                    params.put("lname", pojoBasicInfo.getmLastName());
                    params.put("mobile", pojoBasicInfo.getmMobileNumber());
                    params.put("email", pojoBasicInfo.getmEmailId());

                    //--permanant residancy --
                    params.put("address", pojoPermanentresidancy.getmAddrs1());
                    params.put("address1", pojoPermanentresidancy.getmAddrs2());
                    params.put("pincode", pojoPermanentresidancy.getmPin());
                    params.put("city", pojoPermanentresidancy.getmCityy());
                    params.put("state", pojoPermanentresidancy.getmState());
                    params.put("country", pojoPermanentresidancy.getmCountry());
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long imagename = System.currentTimeMillis();
                    //   params.put("photoid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                    //   params.put("addressid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmapBack)));
                    return params;
                }
            };
            //adding the request to volley
            Volley.newRequestQueue(this).add(volleyMultipartRequest);


        }
    }
}
