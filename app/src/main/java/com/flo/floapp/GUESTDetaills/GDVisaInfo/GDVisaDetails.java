package com.flo.floapp.GUESTDetaills.GDVisaInfo;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.GDBasicInfo.PojoBasicInfo;
import com.flo.floapp.GUESTDetaills.GDLocalContactDetails.LocalContact;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.GDPassport;
import com.flo.floapp.GUESTDetaills.GDPassportInfo.PojoPassportDetails;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PojoPermanentresidancy;
import com.flo.floapp.MultipartRequest.MultipartRequest;
import com.flo.floapp.R;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class GDVisaDetails extends AppCompatActivity {
    Button buttonNextVisa;
    FontChangeCrawler fontChanger;
    EditText edittext_VisaDateOfIssue, edittext_visaNumber, edittext_VisaValidTill,edittext_VisaPlaceIssue;
    Spinner spinner_PlaceOfIssue, spinner_VisaType,spinner_perpose_ofVisit;
    private int mYear, mMonth, mDay;
    PojoBasicInfo pojoBasicInfo;
    PojoPermanentresidancy pojoPermanentresidancy;
    PojoPassportDetails pojoPassportDetails;
    String mVisaType,mPerpose_ofVisit;
    TextView tv_uploadVisa;
    ImageView iv_previewPass;
    int PICK_IMAGE_REQUEST = 111;
    Bitmap bitmap;
    Uri filePath;
    PojoVisaDetails pojoVisaDetails;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gdvisa_details);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));


        Intent intent = getIntent();
        pojoBasicInfo = (PojoBasicInfo)intent.getSerializableExtra("pojoBasicInfo");
        pojoPermanentresidancy = (PojoPermanentresidancy)intent.getSerializableExtra("pojoPermanentresidancy");
        pojoPassportDetails = (PojoPassportDetails) intent.getSerializableExtra("pojoPassportDetails");
        initUI();
    }
    public void initUI(){

        tv_uploadVisa = findViewById(R.id.tv_uploadVisa);
        iv_previewPass = findViewById(R.id.iv_previewPass);
        edittext_VisaDateOfIssue = findViewById(R.id.edittext_VisaDateOfIssue);
        edittext_VisaPlaceIssue = findViewById(R.id.edittext_VisaPlaceIssue);
        edittext_visaNumber = findViewById(R.id.edittext_visaNumber);
        edittext_VisaValidTill = findViewById(R.id.edittext_VisaValidTill);
        spinner_VisaType = findViewById(R.id.spinner_VisaType);
        spinner_perpose_ofVisit = findViewById(R.id.spinner_perpose_ofVisit);

        tv_uploadVisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });

        spinner_perpose_ofVisit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mVisaType = spinner_perpose_ofVisit.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_VisaType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mVisaType = spinner_VisaType.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buttonNextVisa = findViewById(R.id.buttonNextVisa);
        buttonNextVisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mVisaDateOfIssue = edittext_VisaDateOfIssue.getText().toString().trim();
                String mVisaNumber = edittext_visaNumber.getText().toString().trim();
                String mVisaValidTill = edittext_VisaValidTill.getText().toString().trim();
                String mVisaPlaceIssue = edittext_VisaPlaceIssue.getText().toString().trim();

                if (!mVisaDateOfIssue.equals("") && !mVisaNumber.equals("")
                        && !mVisaValidTill.equals("")&& !mVisaPlaceIssue.equals("")){

                    pojoVisaDetails = new PojoVisaDetails();
                    pojoVisaDetails.setmVisaNumber(mVisaNumber);
                    pojoVisaDetails.setmVisaDateOfIssue(mVisaDateOfIssue);
                    pojoVisaDetails.setmVisaValidTill(mVisaValidTill);
                    pojoVisaDetails.setmVisaPlaceIssue(mVisaPlaceIssue);
                    pojoVisaDetails.setmPerpose_ofVisit(mPerpose_ofVisit);
                  //  pojoVisaDetails.setmVisaImg(bitmap);

                    uploadVisa();


                }else {
                    Toast.makeText(GDVisaDetails.this, "Enter All Fields", Toast.LENGTH_SHORT).show();
                }

            }
        });



        edittext_VisaDateOfIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                Log.e("iNubex", mYear+"-"+mMonth+"-"+mDay);
                DatePickerDialog datePickerDialog = new DatePickerDialog(GDVisaDetails.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edittext_VisaDateOfIssue.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        edittext_VisaValidTill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                Log.e("iNubex", mYear+"-"+mMonth+"-"+mDay);
                DatePickerDialog datePickerDialog = new DatePickerDialog(GDVisaDetails.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edittext_VisaValidTill.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
    }

    private void uploadVisa() {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST, Config.URL_GUEST_DETAIL_INDIAN,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                            Log.d("res",response.data.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Intent intent = new Intent(GDVisaDetails.this, LocalContact.class);
                                intent.putExtra("pojoBasicInfo",pojoBasicInfo);
                                intent.putExtra("pojoPermanentresidancy",pojoPermanentresidancy);
                                intent.putExtra("pojoPassportDetails",pojoPassportDetails);
                                intent.putExtra("pojoVisaDetails",pojoVisaDetails);
                                startActivity(intent);

                            } else {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("booking_id", pojoBasicInfo.getBookingId());
                params.put("user_id", pojoBasicInfo.getUserId());

                params.put("fname", pojoBasicInfo.getmFirstName());
                params.put("lname", pojoBasicInfo.getmLastName());
                params.put("mobile", pojoBasicInfo.getmMobileNumber());
                params.put("email", pojoBasicInfo.getmEmailId());

               /* //--permanant residancy --
                params.put("address", pojoPermanentresidancy.getmAddrs1());
                params.put("address1", pojoPermanentresidancy.getmAddrs2());
                params.put("pincode", pojoPermanentresidancy.getmPin());
                params.put("city", pojoPermanentresidancy.getmCityy());
                params.put("state", pojoPermanentresidancy.getmState());
                params.put("country", pojoPermanentresidancy.getmCountry());
                //--pasport details--
                params.put("passport", pojoPassportDetails.getmPassport_Number());
                params.put("passport_place", pojoPassportDetails.getmPassport_Place_issue());
                params.put("passport_date", pojoPassportDetails.getmPassport_Issuedate());
                params.put("passport_expiry", pojoPassportDetails.getmPassport_ValidTill());*/

                params.put("passport", pojoPassportDetails.getmPassport_Number());
                params.put("passport_place", pojoPassportDetails.getmPassport_Place_issue());
                params.put("passport_date", pojoPassportDetails.getmPassport_Issuedate());
                params.put("passport_expiry", pojoPassportDetails.getmPassport_ValidTill());

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                //   params.put("photoid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                //   params.put("addressid_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmapBack)));
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {

                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iv_previewPass.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

}
