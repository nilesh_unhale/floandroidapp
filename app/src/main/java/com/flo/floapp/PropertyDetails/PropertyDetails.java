package com.flo.floapp.PropertyDetails;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.flo.floapp.R;
import com.flo.floapp.SearchFilter.FilterActivity;

public class PropertyDetails extends AppCompatActivity {

    ImageView imageViewFilter;
    LinearLayout ll_categoryDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageViewFilter = (ImageView)findViewById(R.id.ivFilter);
        ll_categoryDetails = (LinearLayout) findViewById(R.id.ll_categoryDetails);
        imageViewFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PropertyDetails.this,FilterActivity.class);
                startActivity(intent);
            }
        });

        ll_categoryDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PropertyDetails.this,CategoryList.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
                // if this doesn't work as desired, another possibility is to call `finish()` here.
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
