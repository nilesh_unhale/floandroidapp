package com.flo.floapp.PropertyDetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.flo.floapp.R;

import java.util.ArrayList;
import java.util.List;

public class CategoryList extends AppCompatActivity {
    List<CatogoryPojo> productList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getting the recyclerview from xml
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCategoryList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //initializing the productlist
        productList = new ArrayList<>();


        //adding some items to our list
        productList.add(
                new CatogoryPojo(
                        R.mipmap.ic_launcher,
                        "Category 01",
                        "1000",
                        "10000"));

        productList.add(
                new CatogoryPojo(
                        R.mipmap.ic_launcher,
                        "Category 02",
                        "1000",
                        "10000"));

        productList.add(
                new CatogoryPojo(
                        R.mipmap.ic_launcher,
                        "Category 03",
                        "1000",
                        "10000"));

        productList.add(
                new CatogoryPojo(
                        R.mipmap.ic_launcher,
                        "Category 04",
                        "1000",
                        "10000"));

        //creating recyclerview adapter
        CategoryListAdapter adapter = new CategoryListAdapter(this, productList);

        //setting adapter to recyclerview
        recyclerView.setAdapter(adapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
                // if this doesn't work as desired, another possibility is to call `finish()` here.
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
