package com.flo.floapp.PropertyDetails;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flo.floapp.R;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryHolder> {
    private Context mCtx;
    private List<CatogoryPojo> catogoryList;

    public CategoryListAdapter(Context mCtx, List<CatogoryPojo> catogoryList) {
        this.mCtx = mCtx;
        this.catogoryList = catogoryList;
    }

    @Override
    public CategoryListAdapter.CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.single_category_list, null);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryListAdapter.CategoryHolder holder, int position) {
        CatogoryPojo catogoryPojo = catogoryList.get(position);

        //binding the data with the viewholder views
        holder.textViewCatTitle.setText(catogoryPojo.getCatogoryTitle());
        holder.perDayRent.setText(catogoryPojo.getPerDayRent());
        holder.perMonthRent.setText(catogoryPojo.getPerMonthRent());

        holder.imageViewProductImage.setImageDrawable(mCtx.getResources().getDrawable(catogoryPojo.getProductImage()));

    }

    @Override
    public int getItemCount() {
        return catogoryList.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {

        TextView textViewCatTitle, perDayRent, perMonthRent;
        ImageView imageViewProductImage;

        public CategoryHolder(final View itemView) {
            super(itemView);
            textViewCatTitle = itemView.findViewById(R.id.tv_categoryTitle);
            perDayRent = itemView.findViewById(R.id.tv_singleRoomrent);
            perMonthRent = itemView.findViewById(R.id.textViewShredRent);
            imageViewProductImage = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   /* Intent i =new Intent(mCtx,CategoryDetails.class);
                    mCtx.startActivity(i);*/
                }
            });

        }
    }
}
