package com.flo.floapp.PropertyDetails;

public class CatogoryPojo {

    int productImage;
    String catogoryTitle;
    String perDayRent;
    String perMonthRent;

    public CatogoryPojo() {
    }

    public CatogoryPojo(int productImage, String catogoryTitle, String perDayRent, String perMonthRent) {
        this.productImage = productImage;
        this.catogoryTitle = catogoryTitle;
        this.perDayRent = perDayRent;
        this.perMonthRent = perMonthRent;
    }

    public int getProductImage() {
        return productImage;
    }

    public void setProductImage(int productImage) {
        this.productImage = productImage;
    }

    public String getCatogoryTitle() {
        return catogoryTitle;
    }

    public void setCatogoryTitle(String catogoryTitle) {
        this.catogoryTitle = catogoryTitle;
    }

    public String getPerDayRent() {
        return perDayRent;
    }

    public void setPerDayRent(String perDayRent) {
        this.perDayRent = perDayRent;
    }

    public String getPerMonthRent() {
        return perMonthRent;
    }

    public void setPerMonthRent(String perMonthRent) {
        this.perMonthRent = perMonthRent;
    }
}
