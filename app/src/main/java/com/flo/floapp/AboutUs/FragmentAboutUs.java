package com.flo.floapp.AboutUs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;

// implementation 'com.michaelmuenzer.android:ScrollableNumberPicker:0.2.2'

public class FragmentAboutUs extends Fragment {
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_aboutus, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }
}
