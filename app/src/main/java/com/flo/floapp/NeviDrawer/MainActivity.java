package com.flo.floapp.NeviDrawer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.fontutils.FontUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.flo.floapp.AboutUs.FragmentAboutUs;
import com.flo.floapp.ContactUs.FragmentContactUs;
import com.flo.floapp.FAQ.FragmentFAQ;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.GUESTDetaills.ActivityGuestOptions;
import com.flo.floapp.Login.Login;
import com.flo.floapp.MyBooking.FragmentBookingDetails;
import com.flo.floapp.MyNotification.FragmentNotification;
import com.flo.floapp.MyPreferences.FragmentMyPreferences;
import com.flo.floapp.MyRequest.FragmentRequest;
import com.flo.floapp.Profile.ProfileFragment;
import com.flo.floapp.R;
import com.flo.floapp.Refferal.FragmentRefferal;
import com.flo.floapp.SearchProperty.SearchPropertyFragment;
import com.flo.floapp.Setting.FragmentAccountSetting;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import de.hdodenhof.circleimageview.CircleImageView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView tv_toolbarTiltle;
    TextView textViewname;
    TextView textviewWelcome;
    String mUSER_ID,mFIRST_NAME,_Userimage;
    boolean isLoginByFB,isLoginByApp,isLoginByGoogle;
    private GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;
    CircleImageView iv_drawer_user_icon;
    NavigationView navigationView;
    MenuItem nav_login;
    long back_pressed;
    public static ImageView ib_right_nevigation;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
       //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       //setSupportActionBar(toolbar);
        FacebookSdk.sdkInitialize(getApplicationContext());
        isLoginByApp = PreferenceUtil.readBoolean(getApplicationContext(),PreferenceStrings.IS_LOGGED_IN,false);
        isLoginByFB = PreferenceUtil.readBoolean(getApplicationContext(),PreferenceStrings.LOGIN_BY_FB,false);
        isLoginByGoogle = PreferenceUtil.readBoolean(getApplicationContext(),PreferenceStrings.LOGIN_BY_GOOGLE,false);
        _Userimage = PreferenceUtil.readString(getApplicationContext(),PreferenceStrings.USER_IMAGE,"");
        mUSER_ID = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.USER_ID,"");
        mFIRST_NAME = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.FIRST_NAME,"");

        //Applying Custom Font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        FontUtils fontUtils = new FontUtils();

        View convertView = navigationView.getHeaderView(0);
        textViewname = (TextView) convertView.findViewById(R.id.textViewname);
        iv_drawer_user_icon = convertView.findViewById(R.id.iv_drawer_user_icon);
        textviewWelcome  = (TextView)convertView.findViewById(R.id.textviewWelcome);
        fontUtils.applyFontToView(textViewname,typeface1);
        fontUtils.applyFontToView(textviewWelcome,typeface1);

        textViewname.setText(""+mFIRST_NAME);
        Glide.with(MainActivity.this)
                .load(Config.URL_USER_IMAGE+_Userimage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.icon_profile_user)
                .into(iv_drawer_user_icon);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
         ib_right_nevigation = (ImageView) findViewById(R.id.menuRight);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        nav_login = menu.findItem(R.id.nav_logut);

        if (mUSER_ID.equals("")){
            nav_login.setTitle("LOGIN");
        }
        else {
            nav_login.setTitle("LOGOUT");
        }

        // Apply font to NavigationView
        fontUtils.applyFontToNavigationView(navigationView, typeface);
        displaySelectedScreen(R.id.searchProperty);

        //      ------nev
        ib_right_nevigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        displaySelectedScreen(id);
        return true;
    }

    private void displaySelectedScreen(int id) {

        Fragment fragment = null;
        if (id == R.id.searchProperty) {
            fragment = new SearchPropertyFragment();
        }
        else if (id == R.id.nav_profile) {
            if (mUSER_ID.equals("")){
                showDialog();
            }
            else {
                fragment = new ProfileFragment();
            }
        }
        else if (id == R.id.myPreferences) {
            if (mUSER_ID.equals("")){
                showDialog();
            }
            else {
                fragment = new FragmentMyPreferences();
            }
        }
        else if (id == R.id.myBooking) {
            if (mUSER_ID.equals("")){
                showDialog();
            }
            else {
                fragment = new FragmentBookingDetails();
            }
        }
       /* else if (id == R.id.nav_notification){
            fragment = new FragmentNotification();
        }*/
        else if (id == R.id.nav_refferel) {
            if (mUSER_ID.equals("")){
                showDialog();
            }
            else {
                fragment = new FragmentRefferal();
            }

        }
        else if (id == R.id.nav_request) {
            if (mUSER_ID.equals("")){
                showDialog();
            }
            else {
                fragment = new FragmentRequest();
            }
        }
        /*else if (id == R.id.nav_setting) {
            fragment = new FragmentAccountSetting();
        }*/
        else if (id == R.id.nav_logut) {
            if (mUSER_ID.equals("")){
                showDialog();
            }
            else {

                if(isLoginByApp=true) {
                    PreferenceUtil.clearPreference(getApplicationContext());
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    intent.putExtra("REDIRECT_LOGIN", "directlogin");
                    startActivity(intent);
                    finish();
                }else if (isLoginByFB=true){
                    LoginManager.getInstance().logOut();
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    intent.putExtra("REDIRECT_LOGIN", "directlogin");
                    startActivity(intent);
                    finish();
                }else if (isLoginByGoogle){
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    Intent intent = new Intent(MainActivity.this, Login.class);
                                    intent.putExtra("REDIRECT_LOGIN", "directlogin");
                                    startActivity(intent);
                                    finish();
                                }
                            });
                }
            }
        }
        else if (id == R.id.nav_aboutus) {

            fragment = new FragmentAboutUs();
        }

        else if (id == R.id.nav_contactus) {

            fragment = new FragmentContactUs();
        }
        else if (id == R.id.nav_faq) {

          /* startActivity(ActivityGuestOptions.getActivityGuestOptions(getApplicationContext(),
                       Integer.parseInt("2"),Integer.parseInt("2"),
                        "9-12-2018","9-12-2018"
                       ,"Neel", "Unhale","97654","116","EON"));*/

           fragment = new FragmentFAQ();
        }
        if (fragment != null) {
           /* FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();*/
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("fragBack").commit();

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);



    }
    private void showDialog() {

       /* final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Please Login First...");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {*/
                           Intent intent = new Intent(getApplicationContext(), Login.class);
                           intent.putExtra("REDIRECT_LOGIN","my_booking");
                           startActivity(intent);
                   /* }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();*/
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
      if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            getFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SearchPropertyFragment()).addToBackStack(null).commit();

        }

        else {
            super.onBackPressed();
        }

        //---to exit app --

        if (back_pressed + 1000 > System.currentTimeMillis()){
           finish();
        }
        else{
            Toast.makeText(getBaseContext(),
                    "Press once again to exit!", Toast.LENGTH_SHORT)
                    .show();
        }
        back_pressed = System.currentTimeMillis();
    }



}
