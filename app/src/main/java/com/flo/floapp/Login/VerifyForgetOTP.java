package com.flo.floapp.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import com.flo.floapp.Register.OTPVerify;
import com.flo.floapp.Register.RegisterOTPVerifyResponce;
import com.flo.floapp.Register.RegisterResponce;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyForgetOTP extends AppCompatActivity {
    Button buttonVerify;
    EditText editText_verifyotpForget;
    TextView text_VERIFY_MOBILE_NUMBERS, textDUMMY_TEXT, textView_ResendCode;
    private APIService services;
    String mEmail;
    FontChangeCrawler fontChanger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_verify_forget_otp);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        Intent intent = getIntent();
        mEmail = intent.getStringExtra("mEmail");
        init();
    }

    private void init() {

        buttonVerify = (Button)findViewById(R.id.btn_verify_numberForget);
        editText_verifyotpForget = (EditText) findViewById(R.id.editText_verifyotpForget);

        text_VERIFY_MOBILE_NUMBERS = findViewById(R.id.text_VERIFY_MOBILE_NUMBERS);
        textDUMMY_TEXT = findViewById(R.id.textDUMMY_TEXT);
        textView_ResendCode = findViewById(R.id.textView_ResendCode);

        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Light.ttf");
        buttonVerify.setTypeface(myCustomFont);
        editText_verifyotpForget.setTypeface(myCustomFont);
        text_VERIFY_MOBILE_NUMBERS.setTypeface(myCustomFontBold);
        textDUMMY_TEXT.setTypeface(myCustomFont);
        textView_ResendCode.setTypeface(myCustomFont);

        services = ApiUtils.getAPIService();
        buttonVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mOTP = editText_verifyotpForget.getText().toString().trim();
               // String mEmail = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.EMAIL,"");
                if (!mOTP.equals("")){

                    verifyForgetOTP(mEmail,mOTP);

                }else {
                    Toast.makeText(getApplicationContext(),"Enter 4 digit OTP",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void verifyForgetOTP(final String mEmail,final String mOTP) {
       /* BodyFverifyOtp bodyFverifyOtp = new BodyFverifyOtp();
        bodyFverifyOtp.setEmail(mEmail);
        bodyFverifyOtp.setEmail(mOTP);
        services.registerVerifyOTP( bodyFverifyOtp).enqueue(new Callback<RegisterOTPVerifyResponce>() {
            @Override
            public void onResponse(Call<RegisterOTPVerifyResponce> call, Response<RegisterOTPVerifyResponce> response) {

                Log.d("verify otp",response.body().toString());
               *//* if (response.isSuccessful()){
                    Log.d("verify otp",response.body().toString());
                    int responce_code_status = response.code();
                    if(responce_code_status == 404) {
                        Toast.makeText(getApplicationContext(), "Connection Problem", Toast.LENGTH_SHORT).show();
                    }else if (responce_code_status==200){

                        String regStatus = response.body().getStatus();
                        String regMessage = response.body().getMessage();
                        if (regStatus.equals("true")) {
                            Intent intent = new Intent(VerifyForgetOTP.this,CreateNewPassword.class);
                            startActivity(intent);

                            Toast.makeText(getApplicationContext(), "yes !!", Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(getApplicationContext(), ""+regMessage, Toast.LENGTH_SHORT).show();


                        }
                    }

                }else {

                    Toast.makeText(getApplicationContext(),"Invalid User email",Toast.LENGTH_LONG).show();
                }


*//*

                Toast.makeText(getApplicationContext(),"success",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<RegisterOTPVerifyResponce> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"xxx"+t.getMessage(),Toast.LENGTH_LONG).show();


            }
        });
*/
   {
       HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.LOGIN_VERIFY_OTP,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject forget_jsonObject = new JSONObject(response);
                            String res_Status = forget_jsonObject.getString("status");
                            final String res_Message = forget_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Intent intent = new Intent(VerifyForgetOTP.this, CreateNewPassword.class);
                                intent.putExtra("mEmail",mEmail);
                                startActivity(intent);
                                finish();
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                               // JSONObject strJson = reg_jsonObject.getJSONObject("responce");

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", mEmail);
                params.put("otp", mOTP);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}}
