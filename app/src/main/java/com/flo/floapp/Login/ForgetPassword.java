package com.flo.floapp.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ForgetPassword extends AppCompatActivity {

    // implementation 'com.archit.calendar:awesome-calendar:1.1.4'
    Button btn_forgotPassword;
    ImageButton imagebutton_back;
    TextView text_myAccount, text_PasswordAssist, text_enterCredential;
    EditText editText_forgetpass, editText_Number;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_forget_password);
        init();
    }

    private void init() {
        text_myAccount = findViewById(R.id.textView_myAccount);
        text_PasswordAssist = findViewById(R.id.textView_PasswordAssistance);
        text_enterCredential = findViewById(R.id.textViewENTERCREDENTIALS);
        btn_forgotPassword = (Button) findViewById(R.id.btn_forgotPassword);
        editText_forgetpass = (EditText) findViewById(R.id.editText_forgetpass);
        editText_Number = (EditText) findViewById(R.id.editText_EMAIL_FORGEPASSWORD) ;
        imagebutton_back = findViewById(R.id.imagebutton_back);

        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Light.ttf");
        text_myAccount.setTypeface(myCustomFontBold);
        text_PasswordAssist.setTypeface(myCustomFontBold);
        text_enterCredential.setTypeface(myCustomFont);
        editText_Number.setTypeface(myCustomFont);
        editText_forgetpass.setTypeface(myCustomFont);
        btn_forgotPassword.setTypeface(myCustomFont);

        imagebutton_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBack = new Intent(ForgetPassword.this, Login.class);
                startActivity(intentBack);
            }
        });

        btn_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fogetEmailOrPassword = editText_forgetpass.getText().toString().trim();
                if (!fogetEmailOrPassword.equals("")) {
                    if (fogetEmailOrPassword.matches(emailPattern) || fogetEmailOrPassword.matches(mobilePattern)) {
                        forgetPasswordPost(fogetEmailOrPassword);

                    } else {
                        editText_forgetpass.setError("Invalid Mobile or Email");

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Enter All Fields", Toast.LENGTH_LONG).show();

                }
            }
        });

    }

            private void forgetPasswordPost(final String fogetEmailOrPassword) {
                HttpsTrustManager.allowAllSSL();
                final ProgressDialog pDialog = new ProgressDialog(this);
                pDialog.setMessage("Loading...");
                pDialog.show();
                String tag_string_req = "string_req";
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        Config.FORGET_PASSWORD,
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pDialog.dismiss();
                                try {
                                    JSONObject reg_jsonObject = new JSONObject(response);
                                    String res_Status = reg_jsonObject.getString("status");
                                    final String res_Message = reg_jsonObject.getString("message");
                                    if (res_Status.equals("true")) {

                                        goNext(fogetEmailOrPassword);

                                    } else {
                                        Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", fogetEmailOrPassword);
                        return params;
                    }
                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
            }

    private void goNext(String mEmail) {
        Intent intent = new Intent(ForgetPassword.this, VerifyForgetOTP.class);
        intent.putExtra("mEmail",mEmail);
        startActivity(intent);
        finish();
    }
}
