package com.flo.floapp.Login;

public class BodyFverifyOtp {
    String otp;
    String email;

    public BodyFverifyOtp() {
    }

    public BodyFverifyOtp(String otp, String email) {
        this.otp = otp;
        this.email = email;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
