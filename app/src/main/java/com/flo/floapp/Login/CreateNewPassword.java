package com.flo.floapp.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class CreateNewPassword extends AppCompatActivity {
    Button btn_newPassword;
    EditText et_newPassword,et_newPasswordRetype;
    String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_create_new_password);
      init();
    }

    private void init() {
        Intent intent = getIntent();
        mEmail = intent.getStringExtra("mEmail");
        et_newPassword =(EditText) findViewById(R.id.et_newPassword);
        et_newPasswordRetype = (EditText)findViewById(R.id.et_newPasswordRetype);
        btn_newPassword =(Button)findViewById(R.id.btn_newPassword);
        btn_newPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // String mEmail = PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.EMAIL,"");
                String mPassword = et_newPassword.getText().toString().trim();
                String mNewPassword = et_newPasswordRetype.getText().toString().trim();
                if (!mPassword.equals("")&& !mNewPassword.equals("")){
                    if(mPassword.equals(mNewPassword)){

                        setPassWord(mEmail,mPassword,mNewPassword);
                    }else {
                       Toast.makeText(getApplicationContext(),"Please Enter Same Password",Toast.LENGTH_LONG);
                    }

                }else {
                    Toast.makeText(getApplicationContext(),"Please Enter All Fields",Toast.LENGTH_LONG).show();

                }
               /* Intent intent = new Intent(CreateNewPassword.this, Login.class);
                startActivity(intent);*/
            }
        });
    }

    private void setPassWord(final String mEmail, final String mPassword, final String mNewPassword) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.LOGIN_NEW_PASSWORD,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(CreateNewPassword.this, Login.class);
                                intent.putExtra("REDIRECT_LOGIN","directlogin");
                                startActivity(intent);
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", mEmail);
                params.put("password", mPassword);
                params.put("newpassword", mNewPassword);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
