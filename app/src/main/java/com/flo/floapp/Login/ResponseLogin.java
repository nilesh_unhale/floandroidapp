package com.flo.floapp.Login;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResponseLogin {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private Response response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }


    public class Response {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first")
        @Expose
        private String first;
        @SerializedName("last")
        @Expose
        private String last;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("pic1")
        @Expose
        private Object pic1;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pincode")
        @Expose
        private Object pincode;
        @SerializedName("mobile")
        @Expose
        private Object mobile;
        @SerializedName("mobile_otp")
        @Expose
        private String mobileOtp;
        @SerializedName("mobile_verified")
        @Expose
        private String mobileVerified;
        @SerializedName("is_active")
        @Expose
        private String isActive;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("logo")
        @Expose
        private String logo;
        @SerializedName("occupation")
        @Expose
        private String occupation;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("photoid_type")
        @Expose
        private Object photoidType;
        @SerializedName("photoid")
        @Expose
        private Object photoid;
        @SerializedName("photoid_image")
        @Expose
        private Object photoidImage;
        @SerializedName("addressid_type")
        @Expose
        private Object addressidType;
        @SerializedName("addressid")
        @Expose
        private Object addressid;
        @SerializedName("addressid_image")
        @Expose
        private Object addressidImage;
        @SerializedName("telephone")
        @Expose
        private Object telephone;
        @SerializedName("website")
        @Expose
        private Object website;
        @SerializedName("gst_no")
        @Expose
        private String gstNo;
        @SerializedName("nationality")
        @Expose
        private String nationality;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("guestcategory")
        @Expose
        private Object guestcategory;
        @SerializedName("vip")
        @Expose
        private String vip;
        @SerializedName("countrycode")
        @Expose
        private Object countrycode;
        @SerializedName("cform")
        @Expose
        private Object cform;
        @SerializedName("ice_no")
        @Expose
        private Object iceNo;
        @SerializedName("facebook")
        @Expose
        private String facebook;
        @SerializedName("twitter")
        @Expose
        private String twitter;
        @SerializedName("linkedin")
        @Expose
        private String linkedin;
        @SerializedName("highestEducation")
        @Expose
        private String highestEducation;
        @SerializedName("joiningDate")
        @Expose
        private String joiningDate;
        @SerializedName("campanyPincode")
        @Expose
        private String campanyPincode;
        @SerializedName("campanyCountry")
        @Expose
        private String campanyCountry;
        @SerializedName("campanyState")
        @Expose
        private String campanyState;
        @SerializedName("campanyCity")
        @Expose
        private String campanyCity;
        @SerializedName("education")
        @Expose
        private Object education;
        @SerializedName("interest")
        @Expose
        private Object interest;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("email_token")
        @Expose
        private Object emailToken;
        @SerializedName("company_name")
        @Expose
        private Object companyName;
        @SerializedName("address2")
        @Expose
        private Object address2;
        @SerializedName("email_verified")
        @Expose
        private Object emailVerified;
        @SerializedName("landmark")
        @Expose
        private String landmark;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getPic1() {
            return pic1;
        }

        public void setPic1(Object pic1) {
            this.pic1 = pic1;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Object getPincode() {
            return pincode;
        }

        public void setPincode(Object pincode) {
            this.pincode = pincode;
        }

        public Object getMobile() {
            return mobile;
        }

        public void setMobile(Object mobile) {
            this.mobile = mobile;
        }

        public String getMobileOtp() {
            return mobileOtp;
        }

        public void setMobileOtp(String mobileOtp) {
            this.mobileOtp = mobileOtp;
        }

        public String getMobileVerified() {
            return mobileVerified;
        }

        public void setMobileVerified(String mobileVerified) {
            this.mobileVerified = mobileVerified;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Object getPhotoidType() {
            return photoidType;
        }

        public void setPhotoidType(Object photoidType) {
            this.photoidType = photoidType;
        }

        public Object getPhotoid() {
            return photoid;
        }

        public void setPhotoid(Object photoid) {
            this.photoid = photoid;
        }

        public Object getPhotoidImage() {
            return photoidImage;
        }

        public void setPhotoidImage(Object photoidImage) {
            this.photoidImage = photoidImage;
        }

        public Object getAddressidType() {
            return addressidType;
        }

        public void setAddressidType(Object addressidType) {
            this.addressidType = addressidType;
        }

        public Object getAddressid() {
            return addressid;
        }

        public void setAddressid(Object addressid) {
            this.addressid = addressid;
        }

        public Object getAddressidImage() {
            return addressidImage;
        }

        public void setAddressidImage(Object addressidImage) {
            this.addressidImage = addressidImage;
        }

        public Object getTelephone() {
            return telephone;
        }

        public void setTelephone(Object telephone) {
            this.telephone = telephone;
        }

        public Object getWebsite() {
            return website;
        }

        public void setWebsite(Object website) {
            this.website = website;
        }

        public String getGstNo() {
            return gstNo;
        }

        public void setGstNo(String gstNo) {
            this.gstNo = gstNo;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getGuestcategory() {
            return guestcategory;
        }

        public void setGuestcategory(Object guestcategory) {
            this.guestcategory = guestcategory;
        }

        public String getVip() {
            return vip;
        }

        public void setVip(String vip) {
            this.vip = vip;
        }

        public Object getCountrycode() {
            return countrycode;
        }

        public void setCountrycode(Object countrycode) {
            this.countrycode = countrycode;
        }

        public Object getCform() {
            return cform;
        }

        public void setCform(Object cform) {
            this.cform = cform;
        }

        public Object getIceNo() {
            return iceNo;
        }

        public void setIceNo(Object iceNo) {
            this.iceNo = iceNo;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getLinkedin() {
            return linkedin;
        }

        public void setLinkedin(String linkedin) {
            this.linkedin = linkedin;
        }

        public String getHighestEducation() {
            return highestEducation;
        }

        public void setHighestEducation(String highestEducation) {
            this.highestEducation = highestEducation;
        }

        public String getJoiningDate() {
            return joiningDate;
        }

        public void setJoiningDate(String joiningDate) {
            this.joiningDate = joiningDate;
        }

        public String getCampanyPincode() {
            return campanyPincode;
        }

        public void setCampanyPincode(String campanyPincode) {
            this.campanyPincode = campanyPincode;
        }

        public String getCampanyCountry() {
            return campanyCountry;
        }

        public void setCampanyCountry(String campanyCountry) {
            this.campanyCountry = campanyCountry;
        }

        public String getCampanyState() {
            return campanyState;
        }

        public void setCampanyState(String campanyState) {
            this.campanyState = campanyState;
        }

        public String getCampanyCity() {
            return campanyCity;
        }

        public void setCampanyCity(String campanyCity) {
            this.campanyCity = campanyCity;
        }

        public Object getEducation() {
            return education;
        }

        public void setEducation(Object education) {
            this.education = education;
        }

        public Object getInterest() {
            return interest;
        }

        public void setInterest(Object interest) {
            this.interest = interest;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getEmailToken() {
            return emailToken;
        }

        public void setEmailToken(Object emailToken) {
            this.emailToken = emailToken;
        }

        public Object getCompanyName() {
            return companyName;
        }

        public void setCompanyName(Object companyName) {
            this.companyName = companyName;
        }

        public Object getAddress2() {
            return address2;
        }

        public void setAddress2(Object address2) {
            this.address2 = address2;
        }

        public Object getEmailVerified() {
            return emailVerified;
        }

        public void setEmailVerified(Object emailVerified) {
            this.emailVerified = emailVerified;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

    }


}
