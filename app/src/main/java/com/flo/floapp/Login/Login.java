package com.flo.floapp.Login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flo.floapp.BookingDetails.EstimateBooking.EstimateBookingActivity;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.Register.Register;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SchedduleVisit.SmsListener;
import com.flo.floapp.SearchProperty.PojoPropertyDetails;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.SmsReceiver;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import bolts.Task;

public class
Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener /*implements GoogleApiClient.OnConnectionFailedListener*/ {

    FontChangeCrawler fontChanger;
    TextView tv_registerURL, tv_forgetPassword, text_KeepMeSignIn, text_OthetIssue;
    Button btn_login;
    ImageView ib_passVisibility;
    EditText et_emailorMobile, et_pass;
    boolean isVisible = false;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
    private APIService services;
    String mKeyTo_login, userEmailGoogle, userFirstNameGoogle;
    static final int SINGLE_REQUEST = 1;
    String userId,userEmail,userFirstName,userlast,mDob,mAddress,mMobile,mMobile_otp, additionalMobileNumber;
    //----google login test--
    private static final String TAG = "GPlusFragent";
    private int RC_SIGN_IN = 1;
    private GoogleApiClient mGoogleApiClient;
    private SignInButton btn_google_login;
    private LoginButton login_FB_button;
    private ImageButton imagebutton_Gplus, imagebutton_FB;
    CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private Button signOutButton;
    private Button disconnectButton;
    private LinearLayout signOutView;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;
    private ImageView imgProfilePic;
    private AppCompatCheckBox checkbox;
    Dialog getNumber;
    String MobileNo;
    Button btn_submitNumber,btn_verify_otp ;
    EditText et_number,et_passsword,et_confirmpass,et_otp;
    String mGender;
    RadioButton rb_maleIndian,rb_femaleIndian;
    LinearLayout ll_gender;
    RadioGroup radioGroupGemder;
    //----google login test--

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        setContentView(R.layout.login);
        init();
    }

    private void init() {
        text_KeepMeSignIn = (TextView) findViewById(R.id.text_KeepMeSignIn);
        btn_google_login = (SignInButton) findViewById(R.id.btn_google_login);
        login_FB_button = findViewById(R.id.login_FB_button);
        imagebutton_Gplus = findViewById(R.id.imagebutton_Gplus);
        imagebutton_FB = findViewById(R.id.imagebutton_FB);
        tv_registerURL = (TextView) findViewById(R.id.tv_registerURL);
        tv_forgetPassword = (TextView) findViewById(R.id.tv_forgetPassword);
        et_emailorMobile = (EditText) findViewById(R.id.et_emailorMobile);
        et_pass = (EditText) findViewById(R.id.et_pass);
        btn_login = (Button) findViewById(R.id.login);
        checkbox = (AppCompatCheckBox) findViewById(R.id.checkbox);
        services = ApiUtils.getAPIService();
        Intent intent = getIntent();
        mKeyTo_login = intent.getStringExtra("REDIRECT_LOGIN");

        callbackManager = CallbackManager.Factory.create();
        login_FB_button.setReadPermissions(Arrays.asList("email"));
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
          {
              if (!isChecked) {
                  et_pass.setTransformationMethod(PasswordTransformationMethod.getInstance());
              } else {
                  et_pass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
              }
          }

          });
                login_FB_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        getUserProfile(AccessToken.getCurrentAccessToken());

                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("fbError", exception.toString());
                    }
                });


        imagebutton_FB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("email","public_profile"));
            }
        });




        //----google login ---
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(Login.this)
                .enableAutoManage(Login.this,  Login.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        btn_google_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        tv_registerURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
            }
        });


        imagebutton_Gplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* getNumber = new Dialog(Login.this);
                getNumber.requestWindowFeature(Window.FEATURE_NO_TITLE);
                getNumber.setContentView(R.layout.dialog_no_of_guest_booking);
                final TextView text_titleMobNo;
                final EditText edittext_MobNo;
                final Button button_MobNo;

                text_titleMobNo = getNumber.findViewById(R.id.text_titleMobNo);
                edittext_MobNo = getNumber.findViewById(R.id.edittext_MobNo);
                button_MobNo = getNumber.findViewById(R.id.button_MobNo);
                //------------Font Applying-------------//
                Typeface myCustomFont = Typeface.createFromAsset(Login.this.getAssets(), "fonts/Raleway-Light.ttf");
                text_titleMobNo.setTypeface(myCustomFont);
                edittext_MobNo.setTypeface(myCustomFont);
                button_MobNo.setTypeface(myCustomFont);*/


                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mEmailorMobile = et_emailorMobile.getText().toString().trim();
                String mPass = et_pass.getText().toString().trim();

                if (!mEmailorMobile.equals("") && !mPass.equals("")) {
                    if (mEmailorMobile.matches(emailPattern) || mEmailorMobile.matches(mobilePattern)) {
                        loginUser(mEmailorMobile, mPass);

                    } else {
                        et_emailorMobile.setError("Invalid Mobile or Email");

                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Registered Email Or Password ", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();

                }
            }
        });

        tv_forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, ForgetPassword.class);
                startActivity(intent);
            }
        });

    }


    private void loginUser(final String mEmailorMobile, final String mPass)
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_LOGIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONObject strJson = reg_jsonObject.getJSONObject("response");
                                 userId = strJson.getString("id");
                                 userEmail = strJson.getString("email");
                                 userFirstName = strJson.getString("first");
                                 userlast = strJson.getString("last");
                                 mDob = strJson.getString("dob");
                                 mAddress = strJson.getString("address");
                                 mMobile = strJson.getString("mobile");
                                 mMobile_otp = strJson.getString("email");
                                 mGender = strJson.getString("gender");

                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.USER_ID,userId);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.EMAIL,userEmail);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.FIRST_NAME,userFirstName);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.LAST_NAME,userlast);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.DOB,mDob);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.ADDRESS,mAddress);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.MOBILE,mMobile);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.GENDER,mGender);
                                PreferenceUtil.writeBoolean(getApplicationContext(), PreferenceStrings.IS_LOGGED_IN,true);
                                callIntente();
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", mEmailorMobile);
                params.put("password", mPass);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void callIntente () {
    if(mKeyTo_login.equals("directlogin")) {
        Intent intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
       if(mKeyTo_login.equals("booking_estimate")) {

        Intent intent = new Intent();
        intent.putExtra("userId",userId);
        intent.putExtra("userEmail",userEmail);
        intent.putExtra("userFirstName",userFirstName);
        intent.putExtra("userlast",userlast);
        intent.putExtra("mMobile",mMobile);
        setResult(2,intent);
        finish();
        }

        if(mKeyTo_login.equals("booking_estimate1")) {
            Intent intent=new Intent();
            intent.putExtra("userId",userId);
            intent.putExtra("userEmail",userEmail);
            intent.putExtra("userFirstName",userFirstName);
            intent.putExtra("userlast",userlast);
            intent.putExtra("mMobile",mMobile);
            setResult(3,intent);
            finish();
        }
    else if(mKeyTo_login.equals("my_booking")){
        Intent intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    else if(mKeyTo_login.equals("") || (mKeyTo_login.equals(null))){
            Intent intent = new Intent(Login.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
    //------ google-login-------------------
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {

                GoogleSignInAccount acct = result.getSignInAccount();
                String email_google = acct.getEmail();
                String name_google = acct.getDisplayName();
                String photo_google = acct.getPhotoUrl().toString();

                String[] splitStr = name_google.split("\\s+");
                String f_name = splitStr[0];
                String l_name = splitStr[1];

                googleLogin(email_google, name_google,photo_google,f_name,l_name);
            } else {
                Toast.makeText(getApplicationContext(), "Error occured..!! Please try Again ", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(getApplicationContext(), "Connection Failed:!", Toast.LENGTH_LONG).show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }

    }
    public void onClick(View v) {
        if (v == imagebutton_Gplus) {
            btn_google_login.performClick();
        }
        if (v == imagebutton_FB) {
            login_FB_button.performClick();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void googleLogin(final String email_google, final String name_google,final String photo_google,
                             final String f_name,final String l_name)
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        pDialog.setCancelable(false);
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GOOGLE_LOGIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONObject strJson = reg_jsonObject.getJSONObject("response");
                                userId = strJson.getString("id");
                                userFirstName = strJson.getString("first");
                                userlast = strJson.getString("last");
                                userEmail = strJson.getString("email");
                                mMobile = strJson.getString("mobile");
                                Log.e("mMobil :",mMobile);


                                if(!mMobile.equalsIgnoreCase("null"))
                                {
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.USER_ID, userId);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.MOBILE, mMobile);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.EMAIL, userEmail);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.FIRST_NAME, userFirstName);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.LAST_NAME, userlast);
                                    callIntente();

                                }else {

                                    final Dialog dialogAdditionalInfo = new Dialog(Login.this);
                                    dialogAdditionalInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialogAdditionalInfo.setContentView(R.layout.dialog_mobile_collection);
                                   // dialogAdditionalInfo.setCancelable(false);
                                    fontChanger.replaceFonts((ViewGroup) dialogAdditionalInfo.findViewById(android.R.id.content));

                                    btn_submitNumber = dialogAdditionalInfo.findViewById(R.id.btn_ok);
                                    btn_verify_otp = dialogAdditionalInfo.findViewById(R.id.btn_otp_ok);
                                    et_number = dialogAdditionalInfo.findViewById(R.id.et_mobile);
                                    et_passsword = dialogAdditionalInfo.findViewById(R.id.et_pass);
                                    et_confirmpass = dialogAdditionalInfo.findViewById(R.id.et_passConfirm);
                                    et_otp = dialogAdditionalInfo.findViewById(R.id.et_otp);
                                    radioGroupGemder = dialogAdditionalInfo.findViewById(R.id.genderGuestIndian);
                                    rb_maleIndian = dialogAdditionalInfo.findViewById(R.id.login_maleIndian);
                                    ll_gender = dialogAdditionalInfo.findViewById(R.id.ll_gender);
                                    rb_femaleIndian = dialogAdditionalInfo.findViewById(R.id.login_femaleIndian);
                                 /*   SmsReceiver.bindListener(new SmsListener() {
                                        @Override
                                        public void messageReceived(String messageText) {
                                            Log.d("Text", messageText);
                                            String motp = messageText.substring(messageText.length() - 4);
                                            et_otp.setText(motp);
                                        }
                                    });
*/
                                    //-------------Select Gender RadioButton
                                    rb_maleIndian.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mGender = String.valueOf(radioGroupGemder.getCheckedRadioButtonId());
                                            mGender = "1";
                                        }
                                    });
                                    rb_femaleIndian.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mGender = String.valueOf(radioGroupGemder.getCheckedRadioButtonId());
                                            mGender = "2";
                                        }
                                    });

                                    btn_submitNumber.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            String mMobile = et_number.getText().toString().trim();
                                            String mPassword = et_passsword.getText().toString().trim();
                                            String mConfirmPassword = et_confirmpass.getText().toString().trim();
                                            if (!mMobile.equals("") && !mPassword.equals("") && !mConfirmPassword.equals("")) {
                                                if (mMobile.length() == 10) {
                                                    if (mConfirmPassword.matches(mPassword)) {
                                                        addtionalDetailsGoogle(mMobile, mPassword);
                                                    } else {
                                                        Toast.makeText(Login.this, "Confirm password must be same like Password", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    Toast toast = Toast.makeText(Login.this, "Enter valid number", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                                                    toast.show();
                                                }
                                            } else {
                                                Toast toast = Toast.makeText(Login.this, "Please enter all fields", Toast.LENGTH_SHORT);
                                                toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                                                toast.show();
                                            }
                                        }
                                    });
                                    btn_verify_otp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            String otp = et_otp.getText().toString().trim();
                                            if (!et_otp.equals("")) {
                                                if (et_otp.length() == 4) {
                                                    otpVerifyGoogle(otp, userEmail);
                                                }
                                            } else {
                                                Toast toast = Toast.makeText(Login.this, "Please enter 4 digit OTP", Toast.LENGTH_SHORT);
                                                toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                                                toast.show();
                                            }
                                        }
                                    });

                                    dialogAdditionalInfo.show();
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email_google);
                params.put("first", f_name);
                params.put("last", l_name);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String full_name = first_name+" "+last_name;
                            String email = object.getString("email");
                           // String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                            if(full_name.equals("") && email.equals("")){
                                Toast.makeText(Login.this, "Unable to access email address associated with this account", Toast.LENGTH_SHORT).show();
                            }else {
                                loginFACEBOOK(full_name,email,first_name,last_name);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void loginFACEBOOK(final String full_name, final String email, final String first_name, final String last_name) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_FACEBOOK_LOGIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONObject strJson = reg_jsonObject.getJSONObject("response");
                                userId = strJson.getString("id");
                                userFirstName = strJson.getString("first");
                                userlast = strJson.getString("last");
                                userEmail = strJson.getString("email");
                                mMobile = strJson.getString("mobile");



                                if(!mMobile.equalsIgnoreCase("null"))
                                {
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.LAST_NAME, userlast);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.MOBILE,mMobile);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.USER_ID,userId);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.EMAIL,userEmail);
                                    PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.FIRST_NAME,userFirstName);
                                    callIntente();

                                }else {
                                    final Dialog dialogAdditionalInfo = new Dialog(Login.this);
                                    dialogAdditionalInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialogAdditionalInfo.setContentView(R.layout.dialog_mobile_collection);
                                  //  dialogAdditionalInfo.setCancelable(false);
                                    fontChanger.replaceFonts((ViewGroup)dialogAdditionalInfo.findViewById(android.R.id.content));

                                    btn_submitNumber = dialogAdditionalInfo.findViewById(R.id.btn_ok);
                                    btn_verify_otp = dialogAdditionalInfo.findViewById(R.id.btn_otp_ok);
                                    et_number = dialogAdditionalInfo.findViewById(R.id.et_mobile);
                                    et_passsword = dialogAdditionalInfo.findViewById(R.id.et_pass);
                                    et_confirmpass = dialogAdditionalInfo.findViewById(R.id.et_passConfirm);
                                    et_otp = dialogAdditionalInfo.findViewById(R.id.et_otp);
                                    ll_gender = dialogAdditionalInfo.findViewById(R.id.ll_gender);
                                    radioGroupGemder = dialogAdditionalInfo.findViewById(R.id.genderGuestIndian);
                                    rb_maleIndian = dialogAdditionalInfo.findViewById(R.id.login_maleIndian);
                                    rb_femaleIndian = dialogAdditionalInfo.findViewById(R.id.login_femaleIndian);
                                   /* SmsReceiver.bindListener(new SmsListener() {
                                        @Override
                                        public void messageReceived(String messageText) {
                                            Log.d("Text",messageText);
                                            String motp = messageText.substring(messageText.length() - 4);
                                            et_otp.setText(motp);
                                        }
                                    });*/

                                    //-------------Select Gender RadioButton
                                    rb_maleIndian.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mGender = String.valueOf(radioGroupGemder.getCheckedRadioButtonId());
                                            mGender = "1";
                                        }
                                    });
                                    rb_femaleIndian.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mGender = String.valueOf(radioGroupGemder.getCheckedRadioButtonId());
                                            mGender = "2";
                                        }
                                    });

                                    btn_submitNumber.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            String mMobile = et_number.getText().toString().trim();
                                            String mPassword = et_passsword.getText().toString().trim();
                                            String mConfirmPassword = et_confirmpass.getText().toString().trim();
                                            if(!mMobile.equals("") && !mPassword.equals("") && !mConfirmPassword.equals("")){
                                                if (mMobile.length() == 10){
                                                    if (mConfirmPassword.matches(mPassword)){
                                                        addtionalDetailsFb(mMobile,mPassword,mGender);
                                                    }else {
                                                        Toast.makeText(Login.this, "Confirm password must be same like Password", Toast.LENGTH_SHORT).show();

                                                    }


                                                }else {
                                                    Toast toast = Toast.makeText(Login.this, "Enter valid number", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                                    toast.show();
                                                }
                                            }else {
                                                Toast toast = Toast.makeText(Login.this, "Please enter all fields", Toast.LENGTH_SHORT);
                                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                                toast.show();
                                            }
                                        }
                                    });

                                    btn_verify_otp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            String otp = et_otp.getText().toString().trim();
                                            if (!et_otp.equals("")){
                                                if (et_otp.length()==4){
                                                    otpVerify(otp,userEmail);
                                                }
                                            }else {
                                                Toast toast = Toast.makeText(Login.this, "Please enter 4 digit OTP", Toast.LENGTH_SHORT);
                                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                                toast.show();                                        }
                                        }
                                    });

                                    dialogAdditionalInfo.show();
                                }


                            } else {
                                Toast.makeText(getApplicationContext(), "" + "something went wrong", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("first", first_name);
                params.put("last", last_name);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    //----------------additional details ---------------------
    public void addtionalDetailsFb(final String mMobile, final String mPassword,final String gender){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_ADDITIONAL_DERTAILS,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(Login.this, res_Message, Toast.LENGTH_SHORT).show();
                                btn_submitNumber.setVisibility(View.GONE);
                                et_number.setVisibility(View.GONE);
                                et_passsword.setVisibility(View.GONE);
                                et_confirmpass.setVisibility(View.GONE);
                                ll_gender.setVisibility(View.GONE);
                                btn_verify_otp.setVisibility(View.VISIBLE);
                                et_otp.setVisibility(View.VISIBLE);


                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile", mMobile);
                params.put("password", mPassword);
                params.put("user_id", userId);
                params.put("gender", gender);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    //---------------------------otp verify-----------
    public void otpVerify(final String otp, final String useremail){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_ADDITIONAL_DERTAILS_OTP,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONObject strJson = reg_jsonObject.getJSONObject("response");
                                userId = strJson.getString("id");
                                mMobile = strJson.getString("mobile");
                                userEmail = strJson.getString("email");
                                userFirstName = strJson.getString("first");
                                userlast = strJson.getString("last");

                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.FIRST_NAME,userFirstName);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.LAST_NAME,userlast);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.MOBILE,mMobile);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.USER_ID,userId);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.EMAIL,userEmail);
                                PreferenceUtil.writeBoolean(getApplicationContext(), PreferenceStrings.LOGIN_BY_FB,true);
                                PreferenceUtil.writeBoolean(getApplicationContext(), PreferenceStrings.IS_NUMBER_TRUE,true);

                                callIntente();
                                Toast.makeText(getApplicationContext(), "" + "LOGIN SUCCESSFUL", Toast.LENGTH_LONG).show();

                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);
                params.put("email", useremail);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void addtionalDetailsGoogle(final String mMobile, final String mPassword){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_ADDITIONAL_DERTAILS,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(Login.this, res_Message, Toast.LENGTH_SHORT).show();
                                btn_submitNumber.setVisibility(View.GONE);
                                et_number.setVisibility(View.GONE);
                                et_passsword.setVisibility(View.GONE);
                                et_confirmpass.setVisibility(View.GONE);
                                ll_gender.setVisibility(View.GONE);

                                btn_verify_otp.setVisibility(View.VISIBLE);
                                et_otp.setVisibility(View.VISIBLE);


                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile", mMobile);
                params.put("password", mPassword);
                params.put("user_id", userId);
                params.put("gender", mGender);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    public void otpVerifyGoogle(final String otp, final String useremail){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_ADDITIONAL_DERTAILS_OTP,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONObject strJson = reg_jsonObject.getJSONObject("response");
                                 userId = strJson.getString("id");
                                 mMobile = strJson.getString("mobile");
                                 userEmail = strJson.getString("email");
                                 userFirstName = strJson.getString("first");
                                 userlast = strJson.getString("last");

                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.FIRST_NAME,userFirstName);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.LAST_NAME,userlast);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.MOBILE,mMobile);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.USER_ID,userId);
                                PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.EMAIL,userEmail);
                                PreferenceUtil.writeBoolean(getApplicationContext(), PreferenceStrings.LOGIN_BY_GOOGLE,true);

                                callIntente();
                                Toast.makeText(getApplicationContext(), "" + "LOGIN SUCCESSFUL", Toast.LENGTH_LONG).show();

                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);
                params.put("email", useremail);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}