package com.flo.floapp.SearchProperty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponceProList {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private Response response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }



    public class Amenity {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Category {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private Object description;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("property_id")
        @Expose
        private String propertyId;
        @SerializedName("singlepermonth")
        @Expose
        private String singlepermonth;
        @SerializedName("sharedpermonth")
        @Expose
        private String sharedpermonth;
        @SerializedName("singleperday")
        @Expose
        private String singleperday;
        @SerializedName("sharedperday")
        @Expose
        private String sharedperday;
        @SerializedName("bookingamount")
        @Expose
        private String bookingamount;
        @SerializedName("meta")
        @Expose
        private Object meta;
        @SerializedName("metad")
        @Expose
        private Object metad;
        @SerializedName("shortterm")
        @Expose
        private String shortterm;
        @SerializedName("deposit")
        @Expose
        private String deposit;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("roomdetails")
        @Expose
        private Object roomdetails;
        @SerializedName("images")
        @Expose
        private List<Response.Image> images = null;
        @SerializedName("amenities")
        @Expose
        private List<Amenity> amenities = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getDescription() {
            return description;
        }

        public void setDescription(Object description) {
            this.description = description;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getSinglepermonth() {
            return singlepermonth;
        }

        public void setSinglepermonth(String singlepermonth) {
            this.singlepermonth = singlepermonth;
        }

        public String getSharedpermonth() {
            return sharedpermonth;
        }

        public void setSharedpermonth(String sharedpermonth) {
            this.sharedpermonth = sharedpermonth;
        }

        public String getSingleperday() {
            return singleperday;
        }

        public void setSingleperday(String singleperday) {
            this.singleperday = singleperday;
        }

        public String getSharedperday() {
            return sharedperday;
        }

        public void setSharedperday(String sharedperday) {
            this.sharedperday = sharedperday;
        }

        public String getBookingamount() {
            return bookingamount;
        }

        public void setBookingamount(String bookingamount) {
            this.bookingamount = bookingamount;
        }

        public Object getMeta() {
            return meta;
        }

        public void setMeta(Object meta) {
            this.meta = meta;
        }

        public Object getMetad() {
            return metad;
        }

        public void setMetad(Object metad) {
            this.metad = metad;
        }

        public String getShortterm() {
            return shortterm;
        }

        public void setShortterm(String shortterm) {
            this.shortterm = shortterm;
        }

        public String getDeposit() {
            return deposit;
        }

        public void setDeposit(String deposit) {
            this.deposit = deposit;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getRoomdetails() {
            return roomdetails;
        }

        public void setRoomdetails(Object roomdetails) {
            this.roomdetails = roomdetails;
        }

        public List<Response.Image> getImages() {
            return images;
        }

        public void setImages(List<Response.Image> images) {
            this.images = images;
        }

        public List<Amenity> getAmenities() {
            return amenities;
        }

        public void setAmenities(List<Amenity> amenities) {
            this.amenities = amenities;
        }

    }
public class Response {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("property_code")
    @Expose
    private String propertyCode;
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("std")
    @Expose
    private String std;
    @SerializedName("tel1")
    @Expose
    private String tel1;
    @SerializedName("tel2")
    @Expose
    private String tel2;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("allimages")
    @Expose
    private List<Allimage> allimages = null;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("countrycode")
    @Expose
    private String countrycode;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("linkedin")
    @Expose
    private String linkedin;
    @SerializedName("google")
    @Expose
    private String google;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("meta")
    @Expose
    private String meta;
    @SerializedName("metad")
    @Expose
    private String metad;
    @SerializedName("whatsapp")
    @Expose
    private String whatsapp;
    @SerializedName("highlights")
    @Expose
    private String highlights;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("rules")
    @Expose
    private String rules;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("city2")
    @Expose
    private Object city2;
    @SerializedName("cityLat")
    @Expose
    private String cityLat;
    @SerializedName("cityLng")
    @Expose
    private String cityLng;
    @SerializedName("video")
    @Expose
    private Object video;
    @SerializedName("pdf")
    @Expose
    private Object pdf;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStd() {
        return std;
    }

    public void setStd(String std) {
        this.std = std;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Allimage> getAllimages() {
        return allimages;
    }

    public void setAllimages(List<Allimage> allimages) {
        this.allimages = allimages;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getMetad() {
        return metad;
    }

    public void setMetad(String metad) {
        this.metad = metad;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Object getCity2() {
        return city2;
    }

    public void setCity2(Object city2) {
        this.city2 = city2;
    }

    public String getCityLat() {
        return cityLat;
    }

    public void setCityLat(String cityLat) {
        this.cityLat = cityLat;
    }

    public String getCityLng() {
        return cityLng;
    }

    public void setCityLng(String cityLng) {
        this.cityLng = cityLng;
    }

    public Object getVideo() {
        return video;
    }

    public void setVideo(Object video) {
        this.video = video;
    }

    public Object getPdf() {
        return pdf;
    }

    public void setPdf(Object pdf) {
        this.pdf = pdf;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public class Allimage {

        @SerializedName("original_name")
        @Expose
        private String originalName;

        public String getOriginalName() {
            return originalName;
        }

        public void setOriginalName(String originalName) {
            this.originalName = originalName;
        }

    }


    public class Image {

        @SerializedName("original_name")
        @Expose
        private String originalName;

        public String getOriginalName() {
            return originalName;
        }

        public void setOriginalName(String originalName) {
            this.originalName = originalName;
        }

    }

}}