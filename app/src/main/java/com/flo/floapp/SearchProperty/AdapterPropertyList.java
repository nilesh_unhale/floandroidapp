package com.flo.floapp.SearchProperty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.flo.floapp.CategoryList.BookingDetails;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;
import com.flo.floapp.SchedduleVisit.ScheduleVisit;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;

public class AdapterPropertyList extends RecyclerView.Adapter<AdapterPropertyList.ViewHolder> {
    private ArrayList<PojoPropertyDetails> arraylist;
    private List<PojoPropertyDetails> listPojos;
    private Context context;
    NetworkDetactor networkDetactor;

    public AdapterPropertyList(Context context,List<PojoPropertyDetails> listPojos) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoPropertyDetails>();
        arraylist.addAll(listPojos);
    }

    @Override
    public AdapterPropertyList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_property_details, null);

        int height = parent.getMeasuredHeight();
        int width = parent.getMeasuredWidth();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, height));

        ViewHolder rcv = new ViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterPropertyList.ViewHolder holder, int position) {
        String[] mDistance;

        final PojoPropertyDetails propertyDetails = listPojos.get(position);
        holder.tv_PropertyName.setText(propertyDetails.getmPropertyNam());
        String[] mdistance = propertyDetails.getmPropertyDistance().split("\\.");
        holder.tv_PropertyDistance.setText("THIS PROPERTY OPTION IS "+mdistance[0]+" KM AWAY FROM PREFFERED LOCATION");
        holder.tv_propertyAddress.setText(propertyDetails.getmPropertyAddress()+"," +" "+ propertyDetails.getmPropertyLandmark());
        holder.tv_chargesSingle.setText("INR "+propertyDetails.getmCat_Chargesingleperday()+" PER DAY");
        holder.tv_chargesShare.setText("INR "+propertyDetails.getmCat_chargeSinglepermonth()+" PER MONTH");

        PreferenceUtil.writeString(context, PreferenceStrings.PROPERTY_ID,propertyDetails.getmPropertyId());
       // String[] imageEndPoints = propertyDetails.getmPropertyImages().split(" ");
        ArrayList<String> allImgs=new ArrayList<>();
        allImgs = propertyDetails.getAllImages();
        for (int i=0; i<allImgs.size(); i++) {
           /* DefaultSliderView defaultSliderView = new DefaultSliderView(context);
            defaultSliderView.image(Config.URL_IMAGE + allImgs.get(i));
            holder.sliderLayout.addSlider(defaultSliderView);*/
           // Log.d("Flo Images :", imageEndPoints[i]);

            Glide.with(this.context)
                    .load(Config.URL_IMAGE + allImgs.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_property_thumbnail)
                    .into(holder.iv_propertyImage);

        }
        networkDetactor = new NetworkDetactor(context);
        holder.buttonBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    Intent intent = new Intent(context, BookingDetails.class);
                    intent.putExtra("objpropertydetails", propertyDetails);
                    context.startActivity(intent);
                }
                else {
                    Toast.makeText(context, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.iv_propertyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    Intent intent = new Intent(context, BookingDetails.class);
                    intent.putExtra("objpropertydetails", propertyDetails);
                    context.startActivity(intent);
                }
                else {
                    Toast.makeText(context, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });
        holder.tv_PropertyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    Intent intent = new Intent(context, BookingDetails.class);
                    intent.putExtra("objpropertydetails", propertyDetails);
                    context.startActivity(intent);
                }
                else {
                    Toast.makeText(context, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });
        holder.textview_Visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkDetactor.isConnected()) {
                    Intent intent = new Intent(context, ScheduleVisit.class);
                    intent.putExtra("propertyname", propertyDetails.getmPropertyNam());
                    intent.putExtra("propertid", propertyDetails.getmPropertyId());
                    context.startActivity(intent);
                }
                else {
                    Toast.makeText(context, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });
        holder.fab_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=an+panchashil tower+pune"));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.listPojos.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_single, tv_shared, textview_Visit, tv_PropertyName,tv_PropertyDistance,
                tv_propertyAddress,tv_chargesSingle, tv_chargesShare;
        ImageView iv_propertyImage;
        private SliderLayout sliderLayout;
        private PagerIndicator pagerIndicator;
        Button buttonBook;
        FloatingActionButton fab_direction;

        public ViewHolder(View itemView) {
            super(itemView);
             iv_propertyImage =(ImageView)itemView.findViewById(R.id.iv_propertyImage);
             tv_PropertyName =(TextView)itemView.findViewById(R.id.textview_propertyName);
             tv_PropertyDistance =(TextView)itemView.findViewById(R.id.textview_distanceFrom);
             tv_propertyAddress =(TextView)itemView.findViewById(R.id.textview_propertyAddress);
             tv_chargesSingle =(TextView)itemView.findViewById(R.id.tv_chargesSingle);
             tv_chargesShare =(TextView)itemView.findViewById(R.id.tv_chargesShare);

             buttonBook =(Button)itemView.findViewById(R.id.btn_book);
            //pagerIndicator =(PagerIndicator) itemView.findViewById(R.id.slider);
             //sliderLayout =(SliderLayout) itemView.findViewById(R.id.slider);
             fab_direction =(FloatingActionButton) itemView.findViewById(R.id.fab_direction);

             tv_single =(TextView)itemView.findViewById(R.id.tv_singlePropertyList);
             tv_shared =(TextView)itemView.findViewById(R.id.tv_sharedPropertyList);
            textview_Visit =(TextView)itemView.findViewById(R.id.textview_Visit);

            Typeface myCustomFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
            tv_PropertyName.setTypeface(myCustomFontBold);
            Typeface myCustomFontRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
            tv_PropertyDistance.setTypeface(myCustomFontRegular);
            tv_propertyAddress.setTypeface(myCustomFontRegular);
            tv_chargesSingle.setTypeface(myCustomFontRegular);
            tv_chargesShare.setTypeface(myCustomFontRegular);
            buttonBook.setTypeface(myCustomFontRegular);
            tv_single.setTypeface(myCustomFontRegular);
            tv_shared.setTypeface(myCustomFontRegular);
            textview_Visit.setTypeface(myCustomFontRegular);

        }
    }
}
