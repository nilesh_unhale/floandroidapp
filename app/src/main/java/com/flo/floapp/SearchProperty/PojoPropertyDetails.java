package com.flo.floapp.SearchProperty;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;

public class PojoPropertyDetails implements Serializable {

    String mPropertyId;
    String mPropertyBrandId;
    String mPropertyCode;
    String mPropertyNam;
    String mPropertyAddress;
    String mPropertyLatitude;
    String mPropertyLongitude ;
    String mPropertyLandmark ;
    String mPropertyDistance ;
    String mPropertyImages ;
    String mCategoryId ;
    String mCategoryName ;
    String mCa_Description;
    String mCat_yBrand_id;
    String mCat_chargeSinglepermonth ;
    String mCat_ChargeSharedpermonth ;
    String mCat_Chargesingleperday;
    String mCat_chargesharedperday ;
    String mCat_bookingamount ;
    String mCat_meta;
    String mCat_metasd ;
    String mCat_shortterm ;
    String mCat_deposit ;

    public ArrayList<String> getAllImages() {
        return allImages;
    }

    public void setAllImages(ArrayList<String> allImages) {
        this.allImages = allImages;
    }

    ArrayList<String> allImages;

    public PojoPropertyDetails(String mPropertyId, String mPropertyBrandId, String mPropertyCode, String mPropertyNam,
                               String mPropertyAddress, String mPropertyLatitude, String mPropertyLongitude,
                               String mPropertyLandmark, String mPropertyDistance, String mPropertyImages,
                               String mCategoryId, String mCategoryName, String mCa_Description, String mCat_yBrand_id,
                               String mCat_chargeSinglepermonth, String mCat_ChargeSharedpermonth, String mCat_Chargesingleperday,
                               String mCat_chargesharedperday, String mCat_bookingamount, String mCat_meta, String mCat_metasd,
                               String mCat_shortterm, String mCat_deposit,ArrayList<String> allImages) {
        this.mPropertyId = mPropertyId;
        this.mPropertyBrandId = mPropertyBrandId;
        this.mPropertyCode = mPropertyCode;
        this.allImages=allImages;
        this.mPropertyNam = mPropertyNam;
        this.mPropertyAddress = mPropertyAddress;
        this.mPropertyLatitude = mPropertyLatitude;
        this.mPropertyLongitude = mPropertyLongitude;
        this.mPropertyLandmark = mPropertyLandmark;
        this.mPropertyDistance = mPropertyDistance;
        this.mPropertyImages = mPropertyImages;
        this.mCategoryId = mCategoryId;
        this.mCategoryName = mCategoryName;
        this.mCa_Description = mCa_Description;
        this.mCat_yBrand_id = mCat_yBrand_id;
        this.mCat_chargeSinglepermonth = mCat_chargeSinglepermonth;
        this.mCat_ChargeSharedpermonth = mCat_ChargeSharedpermonth;
        this.mCat_Chargesingleperday = mCat_Chargesingleperday;
        this.mCat_chargesharedperday = mCat_chargesharedperday;
        this.mCat_bookingamount = mCat_bookingamount;
        this.mCat_meta = mCat_meta;
        this.mCat_metasd = mCat_metasd;
        this.mCat_shortterm = mCat_shortterm;
        this.mCat_deposit = mCat_deposit;
    }

    public String getmPropertyId() {
        return mPropertyId;
    }

    public void setmPropertyId(String mPropertyId) {
        this.mPropertyId = mPropertyId;
    }

    public String getmPropertyBrandId() {
        return mPropertyBrandId;
    }

    public void setmPropertyBrandId(String mPropertyBrandId) {
        this.mPropertyBrandId = mPropertyBrandId;
    }

    public String getmPropertyCode() {
        return mPropertyCode;
    }

    public void setmPropertyCode(String mPropertyCode) {
        this.mPropertyCode = mPropertyCode;
    }

    public String getmPropertyNam() {
        return mPropertyNam;
    }

    public void setmPropertyNam(String mPropertyNam) {
        this.mPropertyNam = mPropertyNam;
    }

    public String getmPropertyAddress() {
        return mPropertyAddress;
    }

    public void setmPropertyAddress(String mPropertyAddress) {
        this.mPropertyAddress = mPropertyAddress;
    }

    public String getmPropertyLatitude() {
        return mPropertyLatitude;
    }

    public void setmPropertyLatitude(String mPropertyLatitude) {
        this.mPropertyLatitude = mPropertyLatitude;
    }

    public String getmPropertyLongitude() {
        return mPropertyLongitude;
    }

    public void setmPropertyLongitude(String mPropertyLongitude) {
        this.mPropertyLongitude = mPropertyLongitude;
    }

    public String getmPropertyLandmark() {
        return mPropertyLandmark;
    }

    public void setmPropertyLandmark(String mPropertyLandmark) {
        this.mPropertyLandmark = mPropertyLandmark;
    }

    public String getmPropertyDistance() {
        return mPropertyDistance;
    }

    public void setmPropertyDistance(String mPropertyDistance) {
        this.mPropertyDistance = mPropertyDistance;
    }

    public String getmPropertyImages() {
        return mPropertyImages;
    }

    public void setmPropertyImages(String mPropertyImages) {
        this.mPropertyImages = mPropertyImages;
    }

    public String getmCategoryId() {
        return mCategoryId;
    }

    public void setmCategoryId(String mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public String getmCategoryName() {
        return mCategoryName;
    }

    public void setmCategoryName(String mCategoryName) {
        this.mCategoryName = mCategoryName;
    }

    public String getmCa_Description() {
        return mCa_Description;
    }

    public void setmCa_Description(String mCa_Description) {
        this.mCa_Description = mCa_Description;
    }

    public String getmCat_yBrand_id() {
        return mCat_yBrand_id;
    }

    public void setmCat_yBrand_id(String mCat_yBrand_id) {
        this.mCat_yBrand_id = mCat_yBrand_id;
    }

    public String getmCat_chargeSinglepermonth() {
        return mCat_chargeSinglepermonth;
    }

    public void setmCat_chargeSinglepermonth(String mCat_chargeSinglepermonth) {
        this.mCat_chargeSinglepermonth = mCat_chargeSinglepermonth;
    }

    public String getmCat_ChargeSharedpermonth() {
        return mCat_ChargeSharedpermonth;
    }

    public void setmCat_ChargeSharedpermonth(String mCat_ChargeSharedpermonth) {
        this.mCat_ChargeSharedpermonth = mCat_ChargeSharedpermonth;
    }

    public String getmCat_Chargesingleperday() {
        return mCat_Chargesingleperday;
    }

    public void setmCat_Chargesingleperday(String mCat_Chargesingleperday) {
        this.mCat_Chargesingleperday = mCat_Chargesingleperday;
    }

    public String getmCat_chargesharedperday() {
        return mCat_chargesharedperday;
    }

    public void setmCat_chargesharedperday(String mCat_chargesharedperday) {
        this.mCat_chargesharedperday = mCat_chargesharedperday;
    }

    public String getmCat_bookingamount() {
        return mCat_bookingamount;
    }

    public void setmCat_bookingamount(String mCat_bookingamount) {
        this.mCat_bookingamount = mCat_bookingamount;
    }

    public String getmCat_meta() {
        return mCat_meta;
    }

    public void setmCat_meta(String mCat_meta) {
        this.mCat_meta = mCat_meta;
    }

    public String getmCat_metasd() {
        return mCat_metasd;
    }

    public void setmCat_metasd(String mCat_metasd) {
        this.mCat_metasd = mCat_metasd;
    }

    public String getmCat_shortterm() {
        return mCat_shortterm;
    }

    public void setmCat_shortterm(String mCat_shortterm) {
        this.mCat_shortterm = mCat_shortterm;
    }

    public String getmCat_deposit() {
        return mCat_deposit;
    }

    public void setmCat_deposit(String mCat_deposit) {
        this.mCat_deposit = mCat_deposit;
    }
}
