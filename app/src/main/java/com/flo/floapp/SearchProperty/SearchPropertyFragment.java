package com.flo.floapp.SearchProperty;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SearchPropertyFragment extends Fragment/* implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks*/ {
    NetworkDetactor networkDetactor;
    FontChangeCrawler fontChanger;
    private AutoCompleteTextView atv_auto_suggest;
    TextView tv_currentLocation;
    private static final int REQUEST_LOCATION = 1;
    private TextView textView_city, title_search;
    Button btn_Go;
    ImageView iv_right;
    Animation slideUp;
    LinearLayout welcomeFLO;
    private GuideView mGuideView;
    private GuideView.Builder builder;

    private static final String TAG = "Frag_ search";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    private PlaceAutoCompleteArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    String mLattitude,mLongitude;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container, false);
        //-font change
        fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)getActivity().findViewById(android.R.id.content));
        networkDetactor = new NetworkDetactor(getActivity());
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");

        /*atv_auto_suggest.setThreshold(1);

        ActivityCompat.requestPermissions(getActivity() , new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        atv_auto_suggest = (AutoCompleteTextView) view.findViewById(R.id.atv_auto_suggest);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(getActivity(), GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(Place.TYPE_COUNTRY)
                .setCountry("IN")
                .build();
        atv_auto_suggest.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceAutoCompleteArrayAdapter(getActivity(), R.layout.custom_suggestions,
                BOUNDS_MOUNTAIN_VIEW, typeFilter);
        atv_auto_suggest.setAdapter(mPlaceArrayAdapter);
        title_search= view.findViewById(R.id.title_search);
        textView_city = view.findViewById(R.id.textview_city);
        btn_Go = view.findViewById(R.id.button_searchLaandingPage);
        */iv_right = view.findViewById(R.id.iv_right);
        welcomeFLO = view.findViewById(R.id.welcomeFLO);

        slideUp = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_up);
        welcomeFLO.startAnimation(slideUp);

       // Typeface myCustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Light.ttf");
       // textView_city.setTypeface(myCustomFont);
       // atv_auto_suggest.setTypeface(myCustomFont);
       /// title_search.setTypeface(myCustomFont);
       // btn_Go.setTypeface(myCustomFont);

        iv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityPropertyList.class);
                intent.putExtra("mLattitude", "18.4897587");
                intent.putExtra("mLongitude", "73.8202962");
                getActivity().startActivity(intent);
            }
        });

        builder = new GuideView.Builder(getActivity())
                .setTitle("Click here to Explore")
                .setContentText("Booking Details,Profile,Service Request..")
                .setContentTypeFace(custom_font)//optional
                .setTitleTypeFace(custom_font)//optional
                .setDismissType(DismissType.anywhere)
                .setTargetView(MainActivity.ib_right_nevigation);


        mGuideView = builder.build();
        mGuideView.show();

       /* //------- click for Result---
        btn_Go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityPropertyList.class);
                intent.putExtra("mLattitude", "18.4897587");
                intent.putExtra("mLongitude", "73.8202962");
                getActivity().startActivity(intent);

              String mPropertyName = atv_auto_suggest.getText().toString().trim();
                if (!mPropertyName.equals("")) {
                    if (networkDetactor.isConnected()) {
                        if (mLattitude == null && mLongitude == null){
                            noDataDialog();
                        }else {
                            Intent intent = new Intent(getActivity(), ActivityPropertyList.class);
                            //intent.putExtra("mLattitude", mLattitude);
                            intent.putExtra("mLattitude", "18.4897587");
                            intent.putExtra("mLongitude", "73.8202962");
                            //intent.putExtra("mLongitude", mLongitude);
                            getActivity().startActivity(intent);
                        }
                    }
                    else {
                        Toast.makeText(getActivity(), "No Internet Connection..", Toast.LENGTH_LONG).show();
                    }
                } else {
                    //Toast.makeText(getActivity(), "Please Enter Location Address", Toast.LENGTH_LONG).show();
                }
            }
        });*/
        return view;
    }


/*    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceAutoCompleteArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(TAG, "Fetching details for ID: " + item.placeId);

            InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
            LatLng latlangObj = place.getLatLng();
            mLattitude = String.valueOf(latlangObj.latitude);
            mLongitude = String.valueOf(latlangObj.longitude);
            atv_auto_suggest.setText(Html.fromHtml(place.getName() + ""));
            atv_auto_suggest.dismissDropDown();

        }
    };

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getActivity(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }

    private void noDataDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Please select valid Location address");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }*/
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

}


