package com.flo.floapp.SearchProperty;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.flo.floapp.CategoryList.AmenitiesAdapter;
import com.flo.floapp.CategoryList.GridItemView;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import static android.view.View.VISIBLE;

public class ActivityPropertyList extends AppCompatActivity {

    RecyclerView recyclerViewPropertyDetails;
    AdapterPropertyList adapterPropertyList;
    public static ArrayList<PojoPropertyDetails> list_propertyDetails ;
    public static ArrayList<String> allImages;
    FontChangeCrawler fontChanger;
    String mLattitude,mLongitude;
    ImageButton floatinfBtn_filter;
    Dialog dialog;
    TextView seekbar_Result;
    //EditText datePreferedSelect, edittext_preferedToDate;
    private int mYear, mMonth, mDay;
    int selectedday, selectedMonth, selectedyear;
    int mGender,mOccupancy;
    ProgressDialog pDialogFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_property_list);

        recyclerViewPropertyDetails =(RecyclerView)findViewById(R.id.recyclerViewPropertyDetails);
        recyclerViewPropertyDetails.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerViewPropertyDetails.setNestedScrollingEnabled(true);
        recyclerViewPropertyDetails.setHasFixedSize(true);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewPropertyDetails);

        Intent intent = getIntent();
        mLattitude = intent.getStringExtra("mLattitude");
        mLongitude = intent.getStringExtra("mLongitude");

        if (mLattitude.equals("") && mLongitude.equals("")){


        }else {

        }
        getPropertyCards();

        floatinfBtn_filter =(ImageButton) findViewById(R.id.floatinfBtn_filter);
        floatinfBtn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(ActivityPropertyList.this, R.style.AppBaseTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_filter);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                GridView gridView = dialog.findViewById(R.id.grid_view);
                gridView.setNumColumns(4);
                final  ArrayList<String> selectedStrings= new ArrayList<>();
                final String[] numbers = new String[]{
                        "LAUNDRY", "WIFI", "LIBRARY", "SECURITY", "ROOM", "GYM", "MUSIC", "DOCTOR"};
                final ArrayList<Integer> imagelist = new ArrayList<>();
                imagelist.add(R.drawable.laundry);
                imagelist.add(R.drawable.wifi);
                imagelist.add(R.drawable.library);
                imagelist.add(R.drawable.security);
                imagelist.add(R.drawable.room);
                imagelist.add(R.drawable.gym);
                imagelist.add(R.drawable.music);
                imagelist.add(R.drawable.doctor_on_call);


                final AmenitiesAdapter adapter = new AmenitiesAdapter(numbers, ActivityPropertyList.this,imagelist);
                gridView.setAdapter(adapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        int selectedIndex = adapter.selectedPositions.indexOf(position);
                        if (selectedIndex > -1) {
                            adapter.selectedPositions.remove(selectedIndex);
                            ((GridItemView) v).display(false);
                            selectedStrings.remove((String) parent.getItemAtPosition(position));
                        } else {
                            adapter.selectedPositions.add(position);
                            ((GridItemView) v).display(true);
                            selectedStrings.add((String) parent.getItemAtPosition(position));
                        }
                    }
                });

                final SeekBar seekbar_price = dialog.findViewById(R.id.seekbar_peiceFilter);
                seekbar_price.setMax(30000);
                TextView text_TitleFilter, text_TitleOCCUPANCY, textView_PRICE_title, textView_AMENITIES_title;
                final EditText edittext_preferedDate, edittext_preferedToDate;
                final RadioButton radioButton_SINGLE, radioButton_SHARED, radioButton_MALE, radioButton_FEMALE;
                final TextView tv_toDate;
                final RadioGroup first, rg_gen;
                Button button_ClearFilter, button_ApplyFilter;

                first = dialog.findViewById(R.id.first);
                rg_gen = dialog.findViewById(R.id.rg_gen);
                edittext_preferedDate = dialog.findViewById(R.id.edittext_preferedDate);
                edittext_preferedToDate = dialog.findViewById(R.id.edittext_preferedToDate);
                radioButton_SINGLE = dialog.findViewById(R.id.radioButton_SINGLE);
                radioButton_SHARED = dialog.findViewById(R.id.radioButton_SHARED);
                radioButton_MALE = dialog.findViewById(R.id.radioButton_MALE);
                radioButton_FEMALE = dialog.findViewById(R.id.radioButton_FEMALE);
                tv_toDate = dialog.findViewById(R.id.tv_toDate);
                button_ClearFilter = dialog.findViewById(R.id.button_ClearFilter);
                button_ApplyFilter = dialog.findViewById(R.id.button_ApplyFilter);
                ImageButton imageButtonBack = dialog.findViewById(R.id.imageButtonBack);

                radioButton_MALE.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mGender = rg_gen.getCheckedRadioButtonId();
                        mGender = 1;
                    }
                });

                radioButton_FEMALE.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mGender = rg_gen.getCheckedRadioButtonId();
                        mGender = 2;
                    }
                });


                radioButton_SINGLE.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOccupancy = first.getCheckedRadioButtonId();
                        mOccupancy = 0;
                    }
                });

                radioButton_FEMALE.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOccupancy = first.getCheckedRadioButtonId();
                        mOccupancy = 1;
                    }
                });
                button_ClearFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        edittext_preferedDate.setText("");
                        edittext_preferedToDate.setText("");
                        first.clearCheck();
                        rg_gen.clearCheck();
                        seekbar_price.setProgress(1000);

                    }
                });
                button_ApplyFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (rg_gen.getCheckedRadioButtonId() == -1 && first.getCheckedRadioButtonId() == -1)
                        {
                            Toast.makeText(getApplicationContext(), "Please select option to filter", Toast.LENGTH_SHORT).show();
                        }else {
                            setfilter(mGender,mOccupancy);
                            dialog.dismiss();
                        }



                    }
                });

                seekbar_Result = dialog.findViewById(R.id.seekbar_Result);
                seekbar_price.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        seekbar_Result.setText(String.valueOf(progress));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                imageButtonBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                //datePreferedSelect = dialog.findViewById(R.id.edittext_preferedDate);

                edittext_preferedDate.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        // custom dialog
                        final Dialog dialog = new Dialog(ActivityPropertyList.this);
                        dialog.setContentView(R.layout.datepickerview);
                        final ImageButton apply = dialog.findViewById(R.id.imagebutton_dateSelect);
                        dialog.setTitle("");
                        DateRangeCalendarView datePicker = (DateRangeCalendarView) dialog.findViewById(R.id.calendarr);
                        //-font
                        Typeface typeface = Typeface.createFromAsset(ActivityPropertyList.this.getAssets(), "fonts/Raleway-Medium.ttf");
                        datePicker.setFonts(typeface);
                        datePicker.setCalendarListener(new DateRangeCalendarView.CalendarListener() {
                            @Override
                            public void onFirstDateSelected(Calendar startDate) {
                                mDay=startDate.get(Calendar.DAY_OF_MONTH);
                                mMonth=startDate.get(Calendar.MONTH)+1;
                                mYear=startDate.get(Calendar.YEAR);
                                edittext_preferedDate.setText(mDay+"-"+mMonth+"-"+mYear);
                            }
                            @Override
                            public void onDateRangeSelected(final Calendar startDate, final Calendar endDate) {
                                apply.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDay=endDate.get(Calendar.DAY_OF_MONTH);
                                        mMonth=endDate.get(Calendar.MONTH)+1;
                                        mYear=endDate.get(Calendar.YEAR);
                                        edittext_preferedToDate.setText(mDay+"-"+mMonth+"-"+mYear);
                                        tv_toDate.setVisibility(VISIBLE);
                                        if (edittext_preferedDate.getText().toString().equals(edittext_preferedToDate.getText().toString())){
                                            Toast toast = Toast.makeText(ActivityPropertyList.this, "Please select Valid dates", Toast.LENGTH_LONG);
                                            toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                                            toast.show();
                                        }else {
                                            dialog.dismiss();
                                        }
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }
                });
            }
        });
    }

    private void getPropertyCards() {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(ActivityPropertyList.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_SEARCH_PROPERTY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            list_propertyDetails = new ArrayList<PojoPropertyDetails>();
                            JSONObject reg_jsonObject = new JSONObject(response);
                            Log.d("Catrgoty OBJ :",""+reg_jsonObject);

                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                for (int i = 0; i < MainArray.length(); i++) {

                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mPropertyId = propertyOBJ.getString("id");
                                    String mPropertyBrandId = propertyOBJ.getString("brand_id");
                                    String mPropertyCode = propertyOBJ.getString("property_code");
                                    String mPropertyName = propertyOBJ.getString("property_name");
                                    String mPropertyAddress = propertyOBJ.getString("address");
                                    String mPropertyLatitude = propertyOBJ.getString("cityLat");
                                    String mPropertyLongitude = propertyOBJ.getString("cityLng");
                                    String mPropertyLandmark = propertyOBJ.getString("landmark");
                                    String mPropertyDistance = propertyOBJ.getString("distance");
                                    JSONArray arrayImages = propertyOBJ.getJSONArray("allimages");
                                    JSONObject objectCategory = propertyOBJ.getJSONObject("categories");


                                    String mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,
                                            mCat_ChargeSharedpermonth,mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount
                                            ,mCat_meta,mCat_metasd,mCat_shortterm,mCat_deposit;

                                        mCategoryId = objectCategory.getString("id");
                                        mCategoryName = objectCategory.getString("name");
                                        mCa_Description = objectCategory.getString("description");
                                        mCat_yBrand_id = objectCategory.getString("brand_id");
                                        mCat_chargeSinglepermonth = objectCategory.getString("singlepermonth");
                                        mCat_ChargeSharedpermonth = objectCategory.getString("sharedpermonth");
                                        mCat_Chargesingleperday = objectCategory.getString("singleperday");
                                        mCat_chargesharedperday = objectCategory.getString("sharedperday");
                                        mCat_bookingamount = objectCategory.getString("bookingamount");
                                        mCat_meta = objectCategory.getString("meta");
                                        mCat_metasd = objectCategory.getString("metad");
                                        mCat_shortterm = objectCategory.getString("shortterm");
                                        mCat_deposit = objectCategory.getString("deposit");

                                        String mPropertyImages = null;
                                        JSONObject objectImages =null;
                                        allImages = new ArrayList<String>();
                                        for (int j = 0; j < arrayImages.length(); j++) {


                                            objectImages = arrayImages.getJSONObject(j);
                                            mPropertyImages = objectImages.getString("original_name");
                                            allImages.add(mPropertyImages);

                                        }
                                    Log.d("img :",mPropertyImages);
                                    PojoPropertyDetails details = new PojoPropertyDetails(mPropertyId,mPropertyBrandId,mPropertyCode,mPropertyName,mPropertyAddress
                                            ,mPropertyLatitude,mPropertyLongitude,mPropertyLandmark,mPropertyDistance,mPropertyImages,
                                            mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,mCat_ChargeSharedpermonth,
                                            mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount,mCat_meta,mCat_metasd,mCat_shortterm,
                                            mCat_deposit,allImages);

                                    list_propertyDetails.add(details);
                                   // allImages.clear();


                                }
                                adapterPropertyList = new AdapterPropertyList(ActivityPropertyList.this,list_propertyDetails);
                                recyclerViewPropertyDetails.setAdapter(adapterPropertyList);

                                adapterPropertyList.notifyDataSetChanged();
                            } else {
                                noDataDialog("Property Not Avialable At This Loaction ");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.d("prop",""+error);
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cityLat", "18.4897587");
                params.put("cityLng", "73.8202962");
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void noDataDialog(String msg) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityPropertyList.this);
        alertDialogBuilder.setMessage("Property Not Avialable At This Loaction ");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        finish();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setfilter(final int gender, final int occupancy){
        HttpsTrustManager.allowAllSSL();
        pDialogFilter = new ProgressDialog(ActivityPropertyList.this);
        pDialogFilter.setMessage("Applying filter...");
        pDialogFilter.setCancelable(false);
        pDialogFilter.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_FILTER,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        pDialogFilter.dismiss();

                        try {
                            list_propertyDetails = new ArrayList<PojoPropertyDetails>();
                            JSONObject reg_jsonObject = new JSONObject(response);
                            Log.d("Catrgoty OBJ :",""+reg_jsonObject);

                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(ActivityPropertyList.this, "Filter Applied", Toast.LENGTH_SHORT).show();
                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                for (int i = 0; i < MainArray.length(); i++) {

                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mPropertyId = propertyOBJ.getString("id");
                                    String mPropertyBrandId = propertyOBJ.getString("brand_id");
                                    String mPropertyCode = propertyOBJ.getString("property_code");
                                    String mPropertyName = propertyOBJ.getString("property_name");
                                    String mPropertyAddress = propertyOBJ.getString("address");
                                    String mPropertyLatitude = propertyOBJ.getString("cityLat");
                                    String mPropertyLongitude = propertyOBJ.getString("cityLng");
                                    String mPropertyLandmark = propertyOBJ.getString("landmark");
                                    String mPropertyDistance = propertyOBJ.getString("distance");
                                    JSONArray arrayImages = propertyOBJ.getJSONArray("allimages");
                                    JSONObject objectCategory = propertyOBJ.getJSONObject("categories");


                                    String mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,
                                            mCat_ChargeSharedpermonth,mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount
                                            ,mCat_meta,mCat_metasd,mCat_shortterm,mCat_deposit;

                                    mCategoryId = objectCategory.getString("id");
                                    mCategoryName = objectCategory.getString("name");
                                    mCa_Description = objectCategory.getString("description");
                                    mCat_yBrand_id = objectCategory.getString("brand_id");
                                    mCat_chargeSinglepermonth = objectCategory.getString("singlepermonth");
                                    mCat_ChargeSharedpermonth = objectCategory.getString("sharedpermonth");
                                    mCat_Chargesingleperday = objectCategory.getString("singleperday");
                                    mCat_chargesharedperday = objectCategory.getString("sharedperday");
                                    mCat_bookingamount = objectCategory.getString("bookingamount");
                                    mCat_meta = objectCategory.getString("meta");
                                    mCat_metasd = objectCategory.getString("metad");
                                    mCat_shortterm = objectCategory.getString("shortterm");
                                    mCat_deposit = objectCategory.getString("deposit");

                                    String mPropertyImages = null;
                                    JSONObject objectImages =null;
                                    allImages = new ArrayList<String>();
                                    for (int j = 0; j < arrayImages.length(); j++) {


                                        objectImages = arrayImages.getJSONObject(j);
                                        mPropertyImages = objectImages.getString("original_name");
                                        allImages.add(mPropertyImages);

                                    }
                                    Log.d("img :",mPropertyImages);
                                    PojoPropertyDetails details = new PojoPropertyDetails(mPropertyId,mPropertyBrandId,mPropertyCode,mPropertyName,mPropertyAddress
                                            ,mPropertyLatitude,mPropertyLongitude,mPropertyLandmark,mPropertyDistance,mPropertyImages,
                                            mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,mCat_ChargeSharedpermonth,
                                            mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount,mCat_meta,mCat_metasd,mCat_shortterm,
                                            mCat_deposit,allImages);

                                    list_propertyDetails.add(details);
                                    // allImages.clear();


                                }
                                adapterPropertyList = new AdapterPropertyList(ActivityPropertyList.this,list_propertyDetails);
                                recyclerViewPropertyDetails.setAdapter(adapterPropertyList);

                                adapterPropertyList.notifyDataSetChanged();
                            } else {
                                dialog.dismiss();
                                Toast.makeText(ActivityPropertyList.this, "No result found !!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialogFilter.dismiss();
                Log.d("prop",""+error);
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("gender", String.valueOf(gender));
                params.put("occupancy", String.valueOf(occupancy));
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }
}
