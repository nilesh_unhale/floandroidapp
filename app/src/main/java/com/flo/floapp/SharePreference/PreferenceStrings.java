package com.flo.floapp.SharePreference;

import android.content.Context;

public class PreferenceStrings {
    public static final String PREF_NAME = "FLO";
    @SuppressWarnings("deprecation")
    public static final int MODE = Context.MODE_PRIVATE;

    //--OTP LOGIN
    public static final String FCM_TOKEN = "FCM_TOKEN";
    public static final String USER_ID = "USER_ID";
    public static final String LOGIN_TYPE = "LOGIN_TYPE";
    public static final String LOGIN_BY_GOOGLE = "LOGIN_BY_GOOGLE";
    public static final String IS_NUMBER_TRUE = "IS_NUMBER_TRUE";
    public static final String LOGIN_BY_FB = "LOGIN_BY_FB";
    public static final String LOGGED_IN_FIRST = "LOGGED_IN_FIRST";
    public static final String IS_LOGGED_IN= "IS_LOGGED_IN";
    public static final String LOGGED_IN_SECOND = "LOGGED_IN_SECOND";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String MIDDLE_NAME = "MIDDLE_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String EMPLOYEEID = "EMPLOYEEID";
    public static final String NAME = "NAME";
    public static final String GENDER = "GENDER";
    public static final String AGE = "AGE";
    public static final String ADDRESS = "ADDRESS";
    public static final String USER_CITY = "USER_CITY";
    public static final String USER_NATIONALITY = "USER_NATIONALITY";
    public static final String USER_COUNTRY = "USER_COUNTRY";
    public static final String USER_STATE = "USER_STATE";
    public static final String DOB = "DOB";
    public static final String MOBILE ="MOBILE";
    public static final String EMAIL ="EMAIL";
    public static final String MOBILE_OTP="MOBILE_OTP";
    public static final String MOBILE_VERIFIED ="MOBILE_VERIFIED";
    public static final String IS_USER_ACTIVE ="IS_USER_ACTIVE";
    public static final String USER_IMAGE ="USER_IMAGE";
    public static final String USER_OCCUPATION ="USER_OCCUPATION";
    public static final String WEDDING_ANNIVERSARY ="WEDDING_ANNIVERSARY";
    public static final String COMPANY_CITY ="COMPANY_CITY";
    public static final String COMAPNY_NAME ="COMAPNY_NAME";
    public static final String COMAPNY_ADDRESS ="COMAPNY_ADDRESS";
    public static final String COMAPNY_CITY ="COMAPNY_CITY";
    public static final String FACEBOOK_URL ="FACEBOOK_URL";
    public static final String TWITTER_URL ="TWITTER_URL";
    public static final String LINKEDIN_URL ="LINKEDIN_URL";
    public static final String USER_HIGHEST_EDU ="USER_HIGHEST_EDU";
    public static final String USER_INTEREST ="USER_INTEREST";
    public static final String USER_JOINING_DATE ="USER_JOINING_DATE";
    public static final String COMPANY_PINCODE ="COMPANY_PINCODE";
    public static final String COMPANY_COUNTRY ="COMPANY_COUNTRY";
    public static final String COMPANY_STATE ="COMPANY_STATE";
    public static final String IS_UPLOADED ="IS_UPLOADED";
    public static final String GUEST_ID ="GUEST_ID";
    public static final String REMAINING_GUEST ="REMAINING_GUEST";


    public static final String PROPERTY_ID ="PROPERTY_ID";
    public static final String CATEGORY_ID ="CATEGORY_ID";
    public static final String CATEGORY_NAME ="CATEGORY_NAME";
    public static final String CATEGORY_BRANDID ="CATEGORY_BRANDID";
    public static final String CAHRGE_SINGLE_PER_MONTH ="CAHRGE_SINGLE_PER_MONTH";
    public static final String CAHRGE_SINGLE_PER_DAY ="CAHRGE_SINGLE_PER_DAY";
    public static final String CAHRGE_SHARE_PER_DAY ="CAHRGE_SHARE_PER_DAY";
    public static final String CAHRGE_SHARE_PER_MONTH ="CAHRGE_SHARE_PER_MONTH";
    public static final String BOOKING_ANOUNT ="BOOKING_ANOUNT";
    public static final String SHORT_TERM ="SHORT_TERM";
    public static final String DEPOSIT ="DEPOSIT";
    public static final String PROPERTYID_ESTIMATE ="PROPERTYID_ESTIMATE";
    public static final String PROPERTY_CODE ="PROPERTY_CODE";
    public static final String PROPERTY_NAME ="PROPERTY_NAME";
    public static final String START_DATE ="START_DATE";
    public static final String END_DATE ="END_DATE";
    public static final String BOOKING_ID_FOR_NON_INDIAN ="BOOKING_ID_FOR_NON_INDIAN";




}

