package com.flo.floapp.BookingDetails.EstimateBooking;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airpay.airpaysdk_simplifiedotp.AirpayActivity;
import com.airpay.airpaysdk_simplifiedotp.ResponseMessage;
import com.airpay.airpaysdk_simplifiedotp.Transaction;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.flo.floapp.ActivityWaitingList;
import com.flo.floapp.CategoryList.BookingDetails;
import com.flo.floapp.CategoryList.PojoBookingDetails;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.ActivityGuestOptions;
import com.flo.floapp.Login.Login;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.michaelmuenzer.android.scrollablennumberpicker.ScrollableNumberPicker;
import com.michaelmuenzer.android.scrollablennumberpicker.ScrollableNumberPickerListener;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.CRC32;

import static android.view.View.VISIBLE;

public class FragmentEstimateBookShared extends Fragment implements ResponseMessage {
    TextView textview_PerMonth_single,textview_PerDay_single,et_guest_dialog_selection,
            tv_single_EstimateAmount,text_singleGSTAmount,text_singleTotalAmount,
            tv_single_EstimateAmount_left,text_singleGSTAmount_left,text_singleTotalAmount_left
            ,tv_title_left_fourth,tv_value_left_fourth_,tv_title_left_fifth,tv_value_left_fifth_, text_NA, tv_toDate;
    TextView et_bookingstartDate, et_bookingendDate;
    EditText edittext_promocode_single;
    Button button_SingleProceedandBook, button_promocodeApply_shared;
    View view, view_estimate1_single, view_estimate2_single, view_estimate3_single,view_estimate4_single;
    private int mYear, mMonth, mDay, mMonth1, mDay1;
    LinearLayout linearLayout_totalWithGst,linearLayout_GstAmount,
            linearLayout_totalWithoutGst, layout_promocode_shared;
    public  static String  mGuestCount_nd_GENDE;
    public String mMaleGuest = "0", mFemaleGuest = "0";
    String mIS_LOGIN,mUSER_ID, mBookingId;
    String oCCUPANCY = "1";
    String mPropertyName, mCatId,mFIRST_NAME,mBOOKING_AMOUNT,mPROPERTY_CODE,mLAST_NAME,mMOBILE,mEMAIL,daysDiff,mbooking_code;
    int mTotalAmount;
    Button buttonApplyGuestNumber;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    LinearLayout layoutDateShared;

    float mFinalWithGSTTotal;
    int selectedday, selectedMonth, selectedyear;
    String mChargeSharedperMonth, mChargeSharedperDay, mPROPERTY_ID,mDEPOSIT;
    NetworkDetactor networkDetactor;
    String _StartDATE,_endDATE;
    FontChangeCrawler fontChanger;
    //---gateway--
    ResponseMessage resp;
    ArrayList<Transaction>transactionList;
    private String ErrorMessage="invalid";
    public boolean ischaracter;
    public boolean boolIsError_new = true;
    private ImageView img_down;
    private LinearLayout layout_address, layout_NA_Shared;
    private Dialog dialogconf;
    private Button btyes;
    private Button btno;
    private int k=0;
    String mSHORT_TERM;
    public final int PERMISSION_REQUEST_CODE=101;
    boolean mFlgDiscount = false;
    Dialog dialogGuest, dialog_Bed_NA;
    PojoBookingDetails  categoryDetails;
    Date mDateStart,mDateEnd;
    String mBookForOther = "0";
    String mGender;
    CheckBox cb_book_other;
    public FragmentEstimateBookShared() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.frag_booking_shared, container, false);
        networkDetactor = new NetworkDetactor(getActivity());
        Bundle bundle = this.getArguments();
        if(bundle != null) {

            categoryDetails = (PojoBookingDetails)bundle.getSerializable("categoryDetails");

        }
        textview_PerDay_single = view.findViewById(R.id.textview_PerDay_single);
        textview_PerMonth_single = view.findViewById(R.id.textview_PerMonth_single);
        button_SingleProceedandBook = view.findViewById(R.id.button_SingleProceedandBook);
        view_estimate1_single = view.findViewById(R.id.view_estimate1_single);
        view_estimate2_single = view.findViewById(R.id.view_estimate2_single);
        view_estimate3_single = view.findViewById(R.id.view_estimate3_single);
        view_estimate4_single = view.findViewById(R.id.view_estimate4_single);
        tv_title_left_fourth = view.findViewById(R.id.tv_title_left_fourth);
        tv_value_left_fourth_ = view.findViewById(R.id.tv_value_left_fourth);
        tv_title_left_fifth = view.findViewById(R.id.tv_title_left_fifth);
        tv_value_left_fifth_ = view.findViewById(R.id.tv_value_left_fifth);
        linearLayout_totalWithGst = view.findViewById(R.id.linearLayout_totalWithGst);
        linearLayout_GstAmount = view.findViewById(R.id.linearLayout_GstAmount);
        linearLayout_totalWithoutGst = view.findViewById(R.id.linearLayout_totalWithoutGst);
        tv_single_EstimateAmount = view.findViewById(R.id.tv_single_EstimateAmount);
        tv_single_EstimateAmount_left = view.findViewById(R.id.tv_single_EstimateAmount_left);
        text_singleGSTAmount = view.findViewById(R.id.text_singleGSTAmount);
        text_singleGSTAmount_left = view.findViewById(R.id.text_singleGSTAmount_left);
        text_singleTotalAmount = view.findViewById(R.id.text_singleTotalAmount);
        text_singleTotalAmount_left = view.findViewById(R.id.text_singleTotalAmount_left);
        et_guest_dialog_selection = view.findViewById(R.id.et_guest_dialog_selection);
        et_bookingstartDate = view.findViewById(R.id.et_bookingstartDateShared);
        et_bookingendDate = view.findViewById(R.id.et_bookingendDateShared);
        button_promocodeApply_shared = view.findViewById(R.id.button_promocodeApply_single);
        edittext_promocode_single = view.findViewById(R.id.edittext_promocode_single);
        layout_promocode_shared = view.findViewById(R.id.layout_promocode_single);
        layout_NA_Shared = view.findViewById(R.id.layout_NA_Shared);
        text_NA = view.findViewById(R.id.text_NA);
        cb_book_other = view.findViewById(R.id.cb_book_other);
        tv_toDate = view.findViewById(R.id.tv_toDate);
        layoutDateShared = view.findViewById(R.id.layoutDateShared);

        mCatId = categoryDetails.getmCategoryId();
        mChargeSharedperMonth = categoryDetails.getmCat_ChargeSharedpermonth();
        mChargeSharedperDay = categoryDetails.getmCat_chargesharedperday();
        mBOOKING_AMOUNT = categoryDetails.getmCat_bookingamount();
        mSHORT_TERM = categoryDetails.getmCat_shortterm();
        mDEPOSIT = categoryDetails.getmCat_deposit();
        mPropertyName = categoryDetails.getmPropertyNam();
        mPROPERTY_ID = categoryDetails.getmPropertyId();
        mPROPERTY_CODE = categoryDetails.getmPropertyCode();

        mUSER_ID = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");
        mFIRST_NAME = PreferenceUtil.readString(getActivity(), PreferenceStrings.FIRST_NAME,"");
        mLAST_NAME  = PreferenceUtil.readString(getActivity(), PreferenceStrings.LAST_NAME,"");
        mMOBILE = PreferenceUtil.readString(getActivity(), PreferenceStrings.MOBILE,"");
        mEMAIL = PreferenceUtil.readString(getActivity(), PreferenceStrings.EMAIL,"");
        mGender = PreferenceUtil.readString(getActivity(), PreferenceStrings.GENDER,"");

        if (mChargeSharedperDay.matches("0.00")&& mChargeSharedperMonth.matches("0.00")){
            /*textview_PerDay_single.setText("NA");
            textview_PerMonth_single.setText("NA");*/
            text_NA.setVisibility(VISIBLE);
        }else {
            layout_NA_Shared.setVisibility(VISIBLE);
            button_SingleProceedandBook.setVisibility(VISIBLE);
            textview_PerDay_single.setText(mChargeSharedperDay+" "+"PER DAY");
            textview_PerMonth_single.setText(mChargeSharedperMonth+" "+"PER MONTH");
        }

        button_promocodeApply_shared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mPromoCode = edittext_promocode_single.getText().toString().trim();
                if(!mPromoCode.equals("")){
                    applyPROMOCODE(mPromoCode);

                }
            }
        });

        cb_book_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CheckBox)view).isChecked()){
                    mBookForOther = "1";
                }
                else{
                    mBookForOther = "0";
                }
            }
        });

        button_SingleProceedandBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    if (et_bookingstartDate.getText().toString().trim().equals("")){
                        Toast toast = Toast.makeText(getActivity(), "Please select Date and Guest", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    }else if (et_bookingendDate.getText().toString().trim().equals("")){
                        Toast toast = Toast.makeText(getActivity(), "Please select Date and Guest", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    }else if (et_guest_dialog_selection.getText().toString().trim().equals("")){
                        Toast toast = Toast.makeText(getActivity(), "Please select Guest", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                    else if (mUSER_ID.equals("")){
                        Intent intent = new Intent(getActivity(),Login.class);
                        intent.putExtra("REDIRECT_LOGIN","booking_estimate1");
                        startActivityForResult(intent, 3);
                    }
                    else {
                        if (et_bookingstartDate.getText().toString().trim().equals("")&&
                                et_bookingendDate.getText().toString().trim().equals("")){
                            Toast toast = Toast.makeText(getActivity(), "Please Select DATES ", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                            toast.show();

                        }else {
                            if (mChargeSharedperDay.matches("0.00")&& mChargeSharedperMonth.matches("0.00")){
                                bedNotAvialable();
                            }else {
                                if (mFlgDiscount ==true){
                                    bookWithDiscount();
                                }
                                else {
                                    bookProperty();
                                }
                            }
                        }
                    }
                }else {
                    Toast toast = Toast.makeText(getActivity(), "No Internet Connection..", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        et_guest_dialog_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_single_EstimateAmount_left.setText("");
                tv_single_EstimateAmount.setText("");
                text_singleGSTAmount.setText("");
                text_singleGSTAmount_left.setText("");
                text_singleTotalAmount.setText("");
                text_singleTotalAmount_left.setText("");
                tv_title_left_fourth.setText("");
                tv_value_left_fourth_.setText("");
                tv_value_left_fifth_.setText("");
                tv_title_left_fifth.setText("");
                view_estimate1_single.setVisibility(View.INVISIBLE);
                view_estimate2_single.setVisibility(View.INVISIBLE);
                view_estimate3_single.setVisibility(View.INVISIBLE);
                view_estimate4_single.setVisibility(View.INVISIBLE);
                layout_promocode_shared.setVisibility(View.INVISIBLE);
                tv_single_EstimateAmount.setVisibility(View.GONE);
                text_singleGSTAmount.setVisibility(View.GONE);
                text_singleTotalAmount.setVisibility(View.GONE);
                tv_value_left_fourth_.setVisibility(View.GONE);
                tv_value_left_fifth_.setVisibility(View.GONE);
                mMaleGuest = "0";
                mFemaleGuest = "0";

                if (!et_bookingstartDate.getText().toString().trim().equals("") &&
                        !et_bookingendDate.getText().toString().trim().equals("")) {

                    if (et_bookingstartDate.getText().toString().equals(et_bookingendDate.getText().toString())){

                        Toast toast = Toast.makeText(getActivity(), "Please select Valid dates", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                        toast.show();
                    }else {

                        guestSelectiondialog();
                    }

                } else {
                    Toast toast = Toast.makeText(getActivity(), "Please select date", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

        });

        et_guest_dialog_selection.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //doSomething();
                if (!et_bookingstartDate.getText().toString().equals("") &&
                        !et_bookingendDate.getText().toString().equals("") &&
                        !et_guest_dialog_selection.getText().toString().equals("")){

                    daysDiff = getCountOfDays(et_bookingstartDate.getText().toString(),et_bookingendDate.getText().toString());
                    int piasa=Integer.parseInt(daysDiff.replaceAll("[\\D]",""))*Integer.parseInt(s.toString().replaceAll("[\\D]",""));
                    Log.d("Totol",+ piasa + " Count");
                    mTotalAmount = piasa*Integer.parseInt(mChargeSharedperDay);

                    //---days > short term
                    if((Integer.parseInt(daysDiff)) >= (Integer.parseInt(mSHORT_TERM))){
                        //                        //--1st
                        tv_single_EstimateAmount.setText(" "+mBOOKING_AMOUNT);
                        tv_single_EstimateAmount_left.setText("INITIAL DEPOSIT PAYABLE");
                        tv_single_EstimateAmount.setVisibility(VISIBLE);

                        int deposit = Integer.parseInt(mDEPOSIT)-Integer.parseInt(mBOOKING_AMOUNT);
                        //--2nd
                       // text_singleGSTAmount.setText(":   "+" Rs "+mDEPOSIT);
                        text_singleGSTAmount.setText("  "+deposit);
                        text_singleGSTAmount_left.setText("BALANCE DEPOSIT");
                        text_singleGSTAmount.setVisibility(VISIBLE);
                        //--3rd
                        text_singleTotalAmount.setText("  "+mChargeSharedperMonth);
                        text_singleTotalAmount_left.setText("MONTHLY RENT");
                        text_singleTotalAmount.setVisibility(VISIBLE);
                        //--4th
                        tv_title_left_fourth.setText("YOUR DUE AT THE TIME OF CHECK IN");
                        int final_total_long_stay = Integer.parseInt(mDEPOSIT)-Integer.parseInt(mBOOKING_AMOUNT)+Integer.parseInt(mChargeSharedperMonth);
                        tv_value_left_fourth_.setText("  "+final_total_long_stay);
                        tv_value_left_fourth_.setVisibility(VISIBLE);

                        tv_title_left_fifth.setText("GRAND TOTAL");
                        int sharePerMonthchange = Integer.parseInt(mChargeSharedperMonth);
                        int grandTotal = Integer.parseInt(String.valueOf(((sharePerMonthchange/30)*Integer.parseInt(daysDiff)*(Integer.parseInt(mMaleGuest)+Integer.parseInt(mFemaleGuest)))+(Integer.parseInt(mDEPOSIT))));
                        Log.d("grnadT : ",""+grandTotal);
                        tv_value_left_fifth_.setText("  "+grandTotal);
                        tv_value_left_fifth_.setVisibility(VISIBLE);


                    }
                    else{
                        tv_single_EstimateAmount.setText("  "+mTotalAmount);
                        tv_single_EstimateAmount_left.setText("ESTIMATED SPEND FOR YOUR STAY");
                        tv_single_EstimateAmount.setVisibility(VISIBLE);
                        view_estimate4_single.setVisibility(View.INVISIBLE);

                        amountWithTaxesDetails(mTotalAmount,daysDiff);
                    }
                    Date sd,ed;
                }else {
                    /*Toast toast = Toast.makeText(getActivity(), "Please Select All fieds ", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();*/
                }
            }
        });

        layoutDateShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    tv_single_EstimateAmount_left.setText("");
                    tv_single_EstimateAmount.setText("");
                    text_singleGSTAmount.setText("");
                    text_singleGSTAmount_left.setText("");
                    text_singleTotalAmount.setText("");
                    text_singleTotalAmount_left.setText("");
                    tv_title_left_fourth.setText("");
                    tv_value_left_fourth_.setText("");
                    et_guest_dialog_selection.setText("");
                    view_estimate1_single.setVisibility(View.INVISIBLE);
                    view_estimate2_single.setVisibility(View.INVISIBLE);
                    view_estimate3_single.setVisibility(View.INVISIBLE);
                    layout_promocode_shared.setVisibility(View.INVISIBLE);
                    tv_single_EstimateAmount.setVisibility(View.GONE);
                    text_singleGSTAmount.setVisibility(View.GONE);
                    text_singleTotalAmount.setVisibility(View.GONE);
                    tv_value_left_fourth_.setVisibility(View.GONE);
                    // custom dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.datepickerview);
                    final ImageButton apply = dialog.findViewById(R.id.imagebutton_dateSelect);
                    dialog.setTitle("");
                    DateRangeCalendarView datePicker = (DateRangeCalendarView) dialog.findViewById(R.id.calendarr);
                    //-font
                    Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
                    datePicker.setFonts(typeface);
                    datePicker.setCalendarListener(new DateRangeCalendarView.CalendarListener() {
                        @Override
                        public void onFirstDateSelected(Calendar startDate) {
                            mDay=startDate.get(Calendar.DAY_OF_MONTH);
                            mMonth=startDate.get(Calendar.MONTH)+1;
                            mYear=startDate.get(Calendar.YEAR);
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(mYear, mMonth, mDay);
                            String stDate = simpleDateFormat.format(calendar.getTime());
                            //et_bookingstartDate.setText(mDay+"-"+mMonth+"-"+mYear);
                            et_bookingstartDate.setText(stDate);
                        }
                        @Override
                        public void onDateRangeSelected(final Calendar startDate, final Calendar endDate) {
                            apply.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDay=endDate.get(Calendar.DAY_OF_MONTH);
                                    mMonth=endDate.get(Calendar.MONTH)+1;
                                    mYear=endDate.get(Calendar.YEAR);

                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(mYear, mMonth, mDay);
                                    String edDate = simpleDateFormat.format(calendar.getTime());
                                    //et_bookingendDate.setText(mDay+"-"+mMonth+"-"+mYear);

                                    et_bookingendDate.setText(edDate);
                                    tv_toDate.setVisibility(VISIBLE);
                                    if (et_bookingstartDate.getText().toString().equals(et_bookingendDate.getText().toString())){
                                        Toast toast = Toast.makeText(getActivity(), "Please select Valid dates ", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                                        toast.show();
                                    }else {
                                        dialog.dismiss();
                                        if(!mMaleGuest.equals("0") || !mFemaleGuest.equals("0")){
                                            checkGenderAvailability(mCatId, oCCUPANCY, mPROPERTY_ID, mMaleGuest, mFemaleGuest, et_bookingstartDate.getText().toString()
                                                    , et_bookingendDate.getText().toString());
                                        }else {
                                            Toast toast = Toast.makeText(getActivity(), "Please select number of guest", Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                                            toast.show();                                        }
                                    }
                                }
                            });
                        }
                    });
                    dialog.show();
                }else {
                    Toast toast = Toast.makeText(getActivity(), "No Internet Connection..", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
        return  view;
    }

    private void _PaymentProcess(String amount, String mFIRST_NAME, String mLAST_NAME, String mMOBILE, String mEMAIL, String mbooking_code, String mBookingId) {

        Intent myIntent = new Intent(getActivity(), AirpayActivity.class);
        Bundle b = new Bundle();
        // Please enter Merchant configuration value
        b.putString("USERNAME", "5950385");
        b.putString("PASSWORD", "9eTwht9V");
        b.putString("SECRET", "6UppB5h97PwFnJF9");
        b.putString("MERCHANT_ID", "26253");
        b.putString("EMAIL", mEMAIL);
        // This is for dynamic phone no value code - Uncomment it
        b.putString("PHONE", mMOBILE);
					/*//  Please enter phone no value
					b.putString("PHONE", "");*/
        b.putString("FIRSTNAME", mFIRST_NAME);
        b.putString("LASTNAME", mLAST_NAME);
        b.putString("ADDRESS", "");
        b.putString("CITY", "");
       /* b.putString("STATE", state.getEditableText().toString().trim());
        b.putString("COUNTRY", country.getEditableText().toString().trim());
        b.putString("PIN_CODE", pincode.getEditableText().toString().trim());*/
        b.putString("ORDER_ID", mbooking_code);
        b.putString("AMOUNT", amount);
        b.putString("CURRENCY", "356");
        b.putString("ISOCURRENCY", "INR");
        b.putString("CHMOD", "");
        b.putString("CUSTOMVAR", "");
        b.putString("TXNSUBTYPE", "");
        b.putString("WALLET", "0");
        // Success URL -
        b.putString("SUCCESS_URL", "https://flocolive.com/airpaymentsuccess");
        b.putParcelable("RESPONSEMESSAGE", (Parcelable) resp);
        myIntent.putExtras(b);
        startActivityForResult(myIntent, 120);
    }

    //--------------------------
    private void amountWithTaxesDetails(final int amount, final String daysDiff)
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading..");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GST,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                float mGSTDetails  = Float.valueOf(reg_jsonObject.getString("response"));

                                text_singleGSTAmount.setText("  "+mGSTDetails);
                                text_singleGSTAmount_left.setText("GST");
                                text_singleGSTAmount.setVisibility(VISIBLE);

                                float add = ((float)amount)+(mGSTDetails);
                                mFinalWithGSTTotal = add;

                                text_singleTotalAmount.setText("  "+mFinalWithGSTTotal);
                                text_singleTotalAmount_left.setText("TOTAL SPEND FOR YOUR STAY");
                                text_singleTotalAmount.setVisibility(VISIBLE);
                                view_estimate4_single.setVisibility(View.INVISIBLE);

                            }
                            else{
                                showAlertDialog(res_Message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("amount", String.valueOf(amount));
                params.put("diff", daysDiff);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void bookingConfirmationDialog(final String mbooking_code)
    {
        final Dialog dialogConfirmationDialog = new Dialog(getActivity());
        dialogConfirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogConfirmationDialog.setContentView(R.layout.dialog_for_upload_document);

        TextView tv_confirmationMSG = dialogConfirmationDialog.findViewById(R.id.tv_confirmationMSG);
        TextView tv_userName = dialogConfirmationDialog.findViewById(R.id.tv_userName);
        Button btn_upload_doc =dialogConfirmationDialog.findViewById(R.id.btn_upload_doc);
        Button btn_upload_later =dialogConfirmationDialog.findViewById(R.id.btn_upload_later);
        tv_userName.setText("HI, "+mFIRST_NAME);
        tv_confirmationMSG.setText("Your booking has been confirmed with us please refer your booking ID :"+mbooking_code+" for all your future communications.");
        btn_upload_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        btn_upload_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast toast = Toast.makeText(getActivity(), "Under Development", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                toast.show();
               /* startActivity(ActivityGuestOptions.getActivityGuestOptions(getActivity(),Integer.parseInt(mMaleGuest),Integer.parseInt(mFemaleGuest),
                        et_bookingstartDate.getText().toString(),et_bookingendDate.getText().toString(),mLAST_NAME, mFIRST_NAME,mbooking_code));*/
            }
        });
        dialogConfirmationDialog.show();
        dialogConfirmationDialog.setCancelable(false);

    }



    //--dialog booking confirm---



    private void checkBedAvailability(final String mcat_Id, final String mOccupancy, final String mProperty_Id,
                                      final String _endDATE,final String _StartDATE) {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Checking Avilability Details...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_DATE_AVILABILITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                            }
                            else{
                                showAlertDialog(res_Message);                      }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category", mcat_Id);
                params.put("occupancy", mOccupancy);
                params.put("property", mProperty_Id);
                params.put("date", _StartDATE);
                params.put("dateto", _endDATE);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void checkGenderAvailability(final String mCatId, final String oCCUPANCY, final String mPROPERTY_ID,
                                         final String mMaleGuest, final String mFemaleGuest, final String _StartDATE, final String _endDATE) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Checking Avilability Details...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_DATE_AVILABILITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            final String waitlist = reg_jsonObject.getString("waitlist");
                            if (res_Status.equals("true")) {

                                et_guest_dialog_selection.setText("Total Guest : "+(Integer.parseInt(mMaleGuest) + Integer.parseInt(mFemaleGuest)));
                                mGuestCount_nd_GENDE = String.valueOf((Integer.parseInt(mMaleGuest) + Integer.parseInt(mFemaleGuest)));

                                dialogGuest.dismiss();
                                view_estimate1_single.setVisibility(VISIBLE);
                                view_estimate2_single.setVisibility(VISIBLE);
                                view_estimate3_single.setVisibility(VISIBLE);
                                view_estimate4_single.setVisibility(VISIBLE);
                                layout_promocode_shared.setVisibility(VISIBLE);
                            }
                            else{
                                if (waitlist.equals("1")){

                                    int guestCount = Integer.parseInt(mMaleGuest) + Integer.parseInt(mFemaleGuest);
                                    waitlistDialog(res_Message,mCatId,oCCUPANCY,mPROPERTY_ID,_StartDATE,_endDATE,guestCount);
                                }else {
                                    showAlertDialog(res_Message);
                                   // mGuestCount_nd_GENDE = "";
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category", mCatId);
                params.put("occupancy", oCCUPANCY);
                params.put("property", mPROPERTY_ID);
                params.put("males",mMaleGuest);
                params.put("females", mFemaleGuest);
                params.put("date", _StartDATE);
                params.put("dateto", _endDATE);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showAlertDialog(String res_message) {
        dialog_Bed_NA = new Dialog(getActivity());
        dialog_Bed_NA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_Bed_NA.setContentView(R.layout.dialog_not_available);
        TextView tv_message = dialog_Bed_NA.findViewById(R.id.tv_message);
        Button btn_NA = dialog_Bed_NA.findViewById(R.id.btn_NA);
        fontChanger.replaceFonts((ViewGroup)dialog_Bed_NA.findViewById(android.R.id.content));
        tv_message.setText(res_message);
        btn_NA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Bed_NA.dismiss();
            }
        });
        dialog_Bed_NA.show();
    }

    private void waitlistDialog(String res_Message, final String mCatId, final String oCCUPANCY, final String mPROPERTY_ID, final String startD, final String endDATE, final int guestCount) {
        final Dialog dialog_waitlist = new Dialog(getActivity());
        dialog_waitlist.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_waitlist.setContentView(R.layout.dialog_waitlist);
        TextView tv_message = dialog_waitlist.findViewById(R.id.tv_message);
        Button btn_wait_no = dialog_waitlist.findViewById(R.id.btn_wait_no);
        Button btn_wait_yes = dialog_waitlist.findViewById(R.id.btn_wait_yes);
        fontChanger.replaceFonts((ViewGroup)dialog_waitlist.findViewById(android.R.id.content));
        tv_message.setText(res_Message);
        btn_wait_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_waitlist.dismiss();
            }
        });
        btn_wait_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityWaitingList.class);

                intent.putExtra("mCatId",mCatId);
                intent.putExtra("oCCUPANCY",oCCUPANCY);
                intent.putExtra("mPROPERTY_ID",mPROPERTY_ID);
                intent.putExtra("_StartDATE",startD);
                intent.putExtra("_endDATE",endDATE);
                intent.putExtra("guestCount",guestCount);
                startActivity(intent);
            }
        });
        dialog_waitlist.show();
    }

    private void guestSelectiondialog(){

        dialogGuest = new Dialog(getActivity());
        dialogGuest.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGuest.setContentView(R.layout.dialog_no_of_guest_booking);
        fontChanger.replaceFonts((ViewGroup)dialogGuest.findViewById(android.R.id.content));
        Button buttonApplyGuestNumber =dialogGuest.findViewById(R.id.buttonApplyGuestNumber);
        ScrollableNumberPicker npMale = dialogGuest.findViewById(R.id.npMale);
        ScrollableNumberPicker np_Female = dialogGuest.findViewById(R.id.np_Female);

        npMale.setListener(new ScrollableNumberPickerListener() {
            @Override
            public void onNumberPicked(int valueMale) {
                if (!cb_book_other.isChecked()){
                    if (mGender.equals("2")){
                        String mText = "You are booking for male occupant wanted to continue !!";
                        bookForOtherDialog(mText);
                    }
                }
                mMaleGuest = String.valueOf(valueMale);
            }
        });
        np_Female.setListener(new ScrollableNumberPickerListener() {
            @Override
            public void onNumberPicked(int valueFemale) {
                if (!cb_book_other.isChecked()){
                    if (mGender.equals("1")){
                        String mText = "You are booking for female occupant wanted to continue !!";
                        bookForOtherDialog(mText);
                    }
                }
                mFemaleGuest = String.valueOf(valueFemale);
            }
        });


        buttonApplyGuestNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mMaleGuest.matches("0") && mFemaleGuest.matches("0")){
                    Toast toast = Toast.makeText(getActivity(), "Please select number of guest", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else {
                    checkGenderAvailability(mCatId, oCCUPANCY, mPROPERTY_ID, mMaleGuest, mFemaleGuest, et_bookingstartDate.getText().toString()
                            , et_bookingendDate.getText().toString());
                }
            }
        });
        dialogGuest.show();
    }

    public String getCountOfDays(String createdDateString, String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }


        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
        Log.d("xxxx::",+ (int) dayCount + " Days");
        return String.valueOf(( (int) dayCount));
    }

    @Override
    public void callback(ArrayList<Transaction> arrayList, boolean b) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!= null){

        if(requestCode==3)
        {
            mUSER_ID =data.getStringExtra("userId");
            mEMAIL =data.getStringExtra("userEmail");
            mFIRST_NAME =data.getStringExtra("userFirstName");
            mLAST_NAME=data.getStringExtra("userlast");
            mMOBILE =data.getStringExtra("mMobile");

            if (et_bookingstartDate.getText().toString().trim().equals("")&&
                    et_bookingendDate.getText().toString().trim().equals("")){
                Toast toast = Toast.makeText(getActivity(), "Please Select DATES ", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                toast.show();

            }else {
                if (mChargeSharedperDay.matches("0.00")&& mChargeSharedperMonth.matches("0.00")){
                    bedNotAvialable();
                }else {
                    if (mFlgDiscount == true){
                        bookWithDiscount();
                    }
                    else {
                        bookProperty();
                    }
                }
            }
        }else {

            try {
                Bundle bundle = data.getExtras();
                transactionList = new ArrayList<Transaction>();
                transactionList = (ArrayList<Transaction>) bundle.getSerializable("DATA");
                if (transactionList != null) {
                    //    Toast.makeText(getActivity(), transactionList.get(0).getSTATUS()+"\n"+transactionList.get(0).getSTATUSMSG(), Toast.LENGTH_LONG).show();;

                    Log.e("RESPONSE URL-->>", "" + transactionList);
                    System.out.println("A=" + transactionList.get(0).getSTATUS()); // 200= success
                    System.out.println("B=" + transactionList.get(0).getMERCHANTKEY());
                    System.out.println("C=" + transactionList.get(0).getMERCHANTPOSTTYPE());
                    System.out.println("D=" + transactionList.get(0).getSTATUSMSG()); //  success or fail
                    System.out.println("E=" + transactionList.get(0).getTRANSACTIONAMT());
                    System.out.println("F=" + transactionList.get(0).getTXN_MODE());
                    System.out.println("G=" + transactionList.get(0).getMERCHANTTRANSACTIONID()); // order id
                    System.out.println("H=" + transactionList.get(0).getSECUREHASH());
                    System.out.println("I=" + transactionList.get(0).getCUSTOMVAR());
                    System.out.println("J=" + transactionList.get(0).getTRANSACTIONID());
                    System.out.println("K=" + transactionList.get(0).getTRANSACTIONSTATUS());

                    Log.e("11111111111 ", "A=" + transactionList.get(0).getSTATUS()); // 200= success
                    Log.e("22222222222 ", "B=" + transactionList.get(0).getMERCHANTKEY());
                    Log.e("33333333333 ", "C=" + transactionList.get(0).getMERCHANTPOSTTYPE());
                    Log.e("44444444444 ", "D=" + transactionList.get(0).getSTATUSMSG()); //  success or fail
                    Log.e("55555555555 ", "E=" + transactionList.get(0).getTRANSACTIONAMT());
                    Log.e("66666666666 ", "F=" + transactionList.get(0).getTXN_MODE());
                    Log.e("77777777777 ", "G=" + transactionList.get(0).getMERCHANTTRANSACTIONID()); // order id
                    Log.e("88888888888 ", "H=" + transactionList.get(0).getSECUREHASH());
                    Log.e("99999999999 ", "I=" + transactionList.get(0).getCUSTOMVAR());
                    Log.e("00000000000 ", "J=" + transactionList.get(0).getTRANSACTIONID());
                    Log.e("45644646646 ", "K=" + transactionList.get(0).getTRANSACTIONSTATUS());


                    String mSTATUS = transactionList.get(0).getSTATUS();
                    String mSTATUS_MSG = transactionList.get(0).getSTATUSMSG();
                    String merchentTransid = transactionList.get(0).getMERCHANTTRANSACTIONID();
                    String apTransactionID = transactionList.get(0).getTRANSACTIONID();
                    String amount = transactionList.get(0).getTRANSACTIONAMT();
                    String transtatus = transactionList.get(0).getTRANSACTIONSTATUS();
                    String message = transactionList.get(0).getSTATUSMSG();
                    String mCUSTOMVAR = transactionList.get(0).getCUSTOMVAR();

                    String cardNumber = "na";
                    String cARDISSUER = "na";
                    String mERCHANT_NAME = "na";
                    String bILLEDAMOUNT = transactionList.get(0).getTRANSACTIONAMT();
                    String cUSTOMEREMAIL = PreferenceUtil.readString(getActivity(), PreferenceStrings.EMAIL, "");
                    String cUSTOMERPHONE = PreferenceUtil.readString(getActivity(), PreferenceStrings.MOBILE, "");
                    String sECUREHASH = transactionList.get(0).getSECUREHASH();

                    if (mSTATUS.equals("200")) {
                        savePaymentDetails(mCUSTOMVAR, apTransactionID, merchentTransid, mBookingId, mUSER_ID, amount,
                                transtatus, message, cardNumber, cARDISSUER, mERCHANT_NAME, bILLEDAMOUNT, cUSTOMEREMAIL,
                                cUSTOMERPHONE, sECUREHASH);

                    }

                    String merchantid = ""; //provided by Airpay
                    String username = "";        //provided by Airpay
                    String sParam = merchentTransid + ":" + apTransactionID + ":" + amount + ":" + transtatus + ":" + message + ":" + merchantid + ":" + username;
                    Log.e("SECURE HASH PARAMS==>", "" + sParam);
                    CRC32 crc = new CRC32();
                    crc.update(sParam.getBytes());
                    String sCRC = "" + crc.getValue();
                    System.out.println("Verified Hash= " + sCRC);
                    Log.e("Verified Hash ==", "Verified Hash= " + sCRC);

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("", "Error Message --- >>> " + e.getMessage());
                dialogWithMessage(e.getMessage());
            }

        }
        }
    }

    private void savePaymentDetails(final String mCUSTOMVAR, final String apTransactionID, final String merchentTransid,
                                    final String mBookingId, final String mUSER_ID, final String amount,
                                    final String transtatus, final String message, final String cardNumber,
                                    final String cARDISSUER, final String mERCHANT_NAME, final String bILLEDAMOUNT,
                                    final String cUSTOMEREMAIL, final String cUSTOMERPHONE, final String sECUREHASH) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading..");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_SAVE_PAYMENT_DETAILS,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                startActivity(ActivityGuestOptions.getActivityGuestOptions(getActivity(),
                                        Integer.parseInt(mMaleGuest),Integer.parseInt(mFemaleGuest),
                                        et_bookingstartDate.getText().toString(),et_bookingendDate.getText().toString(),
                                        mLAST_NAME, mFIRST_NAME,mbooking_code,mBookingId,mPropertyName));
                                getActivity().finish();
                            }
                            else{
                                showAlertDialog(res_Message);
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast toast = Toast.makeText(getActivity(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                toast.show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("CUSTOMVAR",mCUSTOMVAR);
                params.put("booking_id",mBookingId);
                params.put("user_id", mUSER_ID);
                params.put("TRANSACTIONID", apTransactionID);
                params.put("APTRANSACTIONID", merchentTransid);
                params.put("TRANSACTIONAMT", amount);
                params.put("TRANSACTIONSTATUS", transtatus);
                params.put("MESSAGE", message);
                params.put("CARD_NUMBER", cardNumber);
                params.put("CARDISSUER", cARDISSUER);
                params.put("MERCHANT_NAME", mERCHANT_NAME);
                params.put("BILLEDAMOUNT", bILLEDAMOUNT);
                params.put("CUSTOMEREMAIL", cUSTOMEREMAIL);
                params.put("CUSTOMERPHONE", cUSTOMERPHONE);
                params.put("SECUREHASH", sECUREHASH);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //------------booking Property-------------
    private void bookProperty()
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Redirecting to payment...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_BOOK,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                JSONObject mOBJ_BOOKING_DETAILS  = reg_jsonObject.getJSONObject("response");
                                mbooking_code = mOBJ_BOOKING_DETAILS.getString("booking_code");
                                mBookingId = mOBJ_BOOKING_DETAILS.getString("id");
                                /* String muser_id = mOBJ_BOOKING_DETAILS.getString("user_id");
                                String mproperty_id = mOBJ_BOOKING_DETAILS.getString("property_id");
                                String mGuest_males = mOBJ_BOOKING_DETAILS.getString("males");
                                String mGuest_female = mOBJ_BOOKING_DETAILS.getString("females");
                                String moccupancy = mOBJ_BOOKING_DETAILS.getString("occupancy");
                                String mnoofdays = mOBJ_BOOKING_DETAILS.getString("noofdays");
                                String mTOTAL_Guest = mOBJ_BOOKING_DETAILS.getString("number_guest");
                                String mROOM_CAT_ID = mOBJ_BOOKING_DETAILS.getString("roomcat_id");
                                String mBookingSartDate = mOBJ_BOOKING_DETAILS.getString("from");
                                String mBookingEndDate = mOBJ_BOOKING_DETAILS.getString("to");
                                String mTotalAmount = mOBJ_BOOKING_DETAILS.getString("total");
                                String mBookingamount = mOBJ_BOOKING_DETAILS.getString("bookingamount");
                                String mCheckinamount = mOBJ_BOOKING_DETAILS.getString("checkinamount");
                                String mBED_ID = mOBJ_BOOKING_DETAILS.getString("bed_id");
                                String mID = mOBJ_BOOKING_DETAILS.getString("id");
                                String mStay_Tyape = mOBJ_BOOKING_DETAILS.getString("stay_type");
                                bookingConfirmationDialog(mbooking_code);*/

                                if((Integer.parseInt(daysDiff)) >= (Integer.parseInt(mSHORT_TERM))){

                                    _PaymentProcess(mBOOKING_AMOUNT,mFIRST_NAME,mLAST_NAME,mMOBILE,mEMAIL,mbooking_code,mBookingId);

                                }else {
                                    _PaymentProcess(String.valueOf(mFinalWithGSTTotal),mFIRST_NAME,mLAST_NAME,mMOBILE,mEMAIL,mbooking_code,mBookingId);
                                }
                            }
                            else{

                                Toast toast = Toast.makeText(getActivity(),""+res_Message,Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("roomcat", mCatId);
                params.put("males", mMaleGuest);
                params.put("females", mFemaleGuest);
                params.put("number_guest", mGuestCount_nd_GENDE);
                params.put("fromdate", et_bookingstartDate.getText().toString());
                params.put("todate", et_bookingendDate.getText().toString());
                params.put("occupancy", oCCUPANCY);
                params.put("user_id",PreferenceUtil.readString(getActivity(),PreferenceStrings.USER_ID,""));
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void bedNotAvialable() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("This Category Not Avialable");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


       /* dialog_Bed_NA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_Bed_NA.setContentView(R.layout.dialog_not_available);*/
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    //----Apply promo code -----------------
    private void applyPROMOCODE(final String mPromoCode) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Applying Promo-code..");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_PROMOCODE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast toast = Toast.makeText(getActivity(), res_Message , Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();

                                mFlgDiscount = true;
                                tv_single_EstimateAmount.setVisibility(View.GONE);
                                tv_single_EstimateAmount_left.setText("You Will get 100% Discount");

                                //--2nd
                                text_singleGSTAmount.setVisibility(View.GONE);
                                text_singleGSTAmount_left.setVisibility(View.GONE);

                                //--3rd
                                text_singleTotalAmount.setVisibility(View.GONE);
                                text_singleTotalAmount_left.setVisibility(View.GONE);

                                //--4th
                                tv_title_left_fourth.setVisibility(View.GONE);
                                tv_value_left_fourth_.setVisibility(View.GONE);

                                tv_single_EstimateAmount.setVisibility(View.GONE);
                                view_estimate1_single.setVisibility(View.GONE);
                                view_estimate2_single.setVisibility(View.GONE);
                                view_estimate3_single.setVisibility(View.GONE);
                                view_estimate4_single.setVisibility(View.GONE);

/*
                                text_singleGSTAmount.setText(":   "+" Rs "+mGSTDetails);
                                text_singleGSTAmount_left.setText("GST");

                                text_singleTotalAmount.setText(":   "+" Rs "+mFinalWithGSTTotal);
                                text_singleTotalAmount_left.setText("TOTAL SPEND FOR YOUR STAY");*/

                            }
                            else{
                                showAlertDialog(res_Message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast toast = Toast.makeText(getActivity(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                toast.show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("promocode",mPromoCode);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void bookWithDiscount()
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_BOOK,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                JSONObject mOBJ_BOOKING_DETAILS  = reg_jsonObject.getJSONObject("response");
                                mbooking_code = mOBJ_BOOKING_DETAILS.getString("booking_code");
                                String booking_id = mOBJ_BOOKING_DETAILS.getString("id");
                                //------next to upload activity activity
                                startActivity(ActivityGuestOptions.getActivityGuestOptions(getActivity(),
                                        Integer.parseInt(mMaleGuest),Integer.parseInt(mFemaleGuest),
                                        et_bookingstartDate.getText().toString(),et_bookingendDate.getText().toString(),
                                        mLAST_NAME, mFIRST_NAME,mbooking_code,booking_id,mPropertyName));
                            }
                            else{
                                showAlertDialog(res_Message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();

                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("roomcat", mCatId);
                params.put("males", mMaleGuest);
                params.put("females", mFemaleGuest);
                params.put("number_guest", mGuestCount_nd_GENDE);
                params.put("fromdate", et_bookingstartDate.getText().toString());
                params.put("todate", et_bookingendDate.getText().toString());
                params.put("occupancy", oCCUPANCY);
                params.put("user_id",PreferenceUtil.readString(getActivity(),PreferenceStrings.USER_ID,""));
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void dialogWithMessage(String message){
        dialog_Bed_NA = new Dialog(getActivity());
        dialog_Bed_NA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_Bed_NA.setContentView(R.layout.dialog_not_available);
        TextView tv_message = dialog_Bed_NA.findViewById(R.id.tv_message);
        Button btn_NA = dialog_Bed_NA.findViewById(R.id.btn_NA);
        fontChanger.replaceFonts((ViewGroup)dialog_Bed_NA.findViewById(android.R.id.content));
        tv_message.setText(message);
        btn_NA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Bed_NA.dismiss();
            }
        });
        dialog_Bed_NA.show();
    }

    public void bookForOtherDialog(String msg){
        final Dialog dialog_book_other = new Dialog(getActivity());
        dialog_book_other.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_book_other.setContentView(R.layout.dialog_book_for_other);
        TextView tv_message = dialog_book_other.findViewById(R.id.tv_message);
        Button btn_no = dialog_book_other.findViewById(R.id.btn_other_no);
        Button btn_yes = dialog_book_other.findViewById(R.id.btn_other_yes);
        fontChanger.replaceFonts((ViewGroup)dialog_book_other.findViewById(android.R.id.content));
        tv_message.setText(msg);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_book_other.dismiss();
            }
        });
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_book_other.dismiss();
                cb_book_other.setChecked(true);
                mBookForOther = "1";
            }
        });
        dialog_book_other.show();
    }

}
