package com.flo.floapp.BookingDetails.UploadDocument;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.Login.ForgetPassword;
import com.flo.floapp.Login.VerifyForgetOTP;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SearchProperty.ActivityPropertyList;
import com.flo.floapp.SearchProperty.ResponceProList;
import com.flo.floapp.Volley.MyApplication;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityUploadDocument extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    List<String> list_child;
    LinearLayout container;
    int maleGuestCount, femaleGuestCount;
    private ArrayList<GuestDetailsModel> guestDetailsList = new ArrayList<GuestDetailsModel>();

    HashMap<String, String> meMap;
    private static String KEY_MALE_COUNT = "KEY_MALE_COUNT";
    private static String KEY_FEMALE_COUNT = "KEY_FEMALE_COUNT";
    Button btn_submit,btn_Clear;
    RadioGroup radioGroup_idType;
    RadioButton rb_pancard,rb_aadharcard,rb_drivinglicence;
    Spinner spinner_salutation;
    EditText fName,et_guestLast,et_guest_email,et_guest_number,et_document_no;
    Button btn_selectGuestImage;
    private static final int PICK_IMAGE_REQUEST = 1;
    Bitmap bitmap;
    View convertview;
    int selectedId;
    private APIService services;

    String mDocType;

    private ArrayList<HashMap<String, String>> guestDetailsListMap = new ArrayList<HashMap<String, String>>();
     ImageView myImage;


    public static Intent getActivityUploadDocument(Context context, int maleCount, int femaleCount){
        Intent intent = new Intent(context, ActivityUploadDocument.class);
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_MALE_COUNT,maleCount);
        bundle.putInt(KEY_FEMALE_COUNT,femaleCount);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_upload_document);
        expListView = (ExpandableListView) findViewById(R.id.expandableListView);
        list_child = new ArrayList<String>();
        container = (LinearLayout) findViewById(R.id.container);

        Bundle bundle = getIntent().getExtras();
        maleGuestCount = bundle.getInt(KEY_MALE_COUNT);
        femaleGuestCount = bundle.getInt(KEY_FEMALE_COUNT);
        services = ApiUtils.getAPIService();

        prepareListData();

        btn_submit = (Button)findViewById(R.id.btn_guest_details);
        btn_Clear = (Button)findViewById(R.id.btn_Clear);
        btn_Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                fName = convertview.findViewById(R.id.et_guest_fname);
                et_guestLast = convertview.findViewById(R.id.et_guestLast);
                et_guest_email = convertview.findViewById(R.id.et_guest_email);
                et_guest_number = convertview.findViewById(R.id.et_guest_number);
                et_document_no = convertview.findViewById(R.id.et_document_no);

                fName.setText("");
                et_guestLast.setText("");
                et_guest_email.setText("");
                et_document_no.setText("");
                et_guest_number.setText("");
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                for (int i = 1; i<= maleGuestCount;i++){

                     convertview = (View)container.findViewById(10+i);
                     GuestDetailsModel singleGuestDetails = new GuestDetailsModel();
                     radioGroup_idType =convertview.findViewById(R.id.radioGroup_idType);
                     rb_pancard =convertview.findViewById(R.id.rb_pancard);
                     rb_aadharcard =convertview.findViewById(R.id.rb_aadharcard);
                     rb_drivinglicence =convertview.findViewById(R.id.rb_drivinglicence);
                     spinner_salutation = convertview.findViewById(R.id.spinner_salutation);
                     fName = convertview.findViewById(R.id.et_guest_fname);
                     et_guestLast = convertview.findViewById(R.id.et_guestLast);
                     et_guest_email = convertview.findViewById(R.id.et_guest_email);
                     et_guest_number = convertview.findViewById(R.id.et_guest_number);
                     et_document_no = convertview.findViewById(R.id.et_document_no);
                     btn_selectGuestImage = convertview.findViewById(R.id.btn_selectGuestImage);

                    rb_aadharcard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedId = radioGroup_idType.getCheckedRadioButtonId();
                            mDocType = "AADHARCARD";

                        }
                    });
                    rb_pancard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedId = radioGroup_idType.getCheckedRadioButtonId();
                            mDocType = "PANCARD";

                        }
                    });
                    rb_drivinglicence.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedId = radioGroup_idType.getCheckedRadioButtonId();
                            mDocType = "DRIVINGLICENCE";

                        }
                    });
                     btn_selectGuestImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getApplicationContext(),"hi",Toast.LENGTH_LONG).show();
                            //_selectImage();
                        }
                    });
                   // image_PreviewGuestImage = convertview.findViewById(R.id.image_PreviewGuestImage);

                     if(!fName.getText().toString().equals("") && !et_document_no.getText().toString().equals("") && !et_guestLast.getText().toString().equals("")
                            && !et_guest_email.getText().toString().equals("") && !et_guest_number.getText().toString().equals("")) {

                        singleGuestDetails.setFirstName(fName.getText().toString());
                        singleGuestDetails.setLastName(et_guestLast.getText().toString());
                        singleGuestDetails.setEmailId(et_guest_email.getText().toString());
                        singleGuestDetails.setMobileNumber(et_guest_number.getText().toString());
                        singleGuestDetails.setPhotoIdNumber(et_document_no.getText().toString());
                        singleGuestDetails.setPhotoIdType("gg");
                        singleGuestDetails.setSalutation("Mr.");
                        singleGuestDetails.setGender("1");
                        singleGuestDetails.setUserId("1");

                        meMap=new HashMap<String, String>();
                         meMap.put("fname",singleGuestDetails.getFirstName());
                         meMap.put("lname",singleGuestDetails.getLastName());
                         meMap.put("email",singleGuestDetails.getEmailId());
                         meMap.put("gender",singleGuestDetails.getGender());
                         meMap.put("mobile",singleGuestDetails.getMobileNumber());
                         meMap.put("salutation",singleGuestDetails.getSalutation());
                         meMap.put("photoid_type","hhh");
                         meMap.put("photoid_image","www");
                         meMap.put("photoid",singleGuestDetails.getPhotoIdNumber());

                     }else {
                         Toast.makeText(getApplicationContext(),"Please Enter All Guest Details",Toast.LENGTH_LONG).show();
                    }
                    guestDetailsListMap.add(meMap);
                   // meMap.put("Color1","Red");
                }
                for (int i =1; i<=femaleGuestCount;i++){
                     convertview = (View)container.findViewById(20+i);

                    GuestDetailsModel singleGuestDetails = new GuestDetailsModel();
                    singleGuestDetails.setSalutation("Miss");
                    radioGroup_idType =convertview.findViewById(R.id.radioGroup_idType);
                    rb_pancard =convertview.findViewById(R.id.rb_pancard);
                    rb_aadharcard =convertview.findViewById(R.id.rb_aadharcard);
                    rb_drivinglicence =convertview.findViewById(R.id.rb_drivinglicence);
                    spinner_salutation = convertview.findViewById(R.id.spinner_salutation);
                    fName = convertview.findViewById(R.id.et_guest_fname);
                    et_guestLast = convertview.findViewById(R.id.et_guestLast);
                    et_guest_email = convertview.findViewById(R.id.et_guest_email);
                    et_guest_number = convertview.findViewById(R.id.et_guest_number);
                    et_document_no = convertview.findViewById(R.id.et_document_no);

                    rb_aadharcard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedId = radioGroup_idType.getCheckedRadioButtonId();
                            mDocType = "AADHARCARD";

                        }
                    });
                    rb_pancard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedId = radioGroup_idType.getCheckedRadioButtonId();
                            mDocType = "PANCARD";

                        }
                    });
                    rb_drivinglicence.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedId = radioGroup_idType.getCheckedRadioButtonId();
                            mDocType = "DRIVINGLICENCE";

                        }
                    });

                    if(!fName.getText().toString().equals("") && !et_document_no.getText().toString().equals("") && !et_guestLast.getText().toString().equals("")
                            && !et_guest_email.getText().toString().equals("") && !et_guest_number.getText().toString().equals("")) {

                        singleGuestDetails.setFirstName(fName.getText().toString());
                        singleGuestDetails.setLastName(et_guestLast.getText().toString());
                        singleGuestDetails.setEmailId(et_guest_email.getText().toString());
                        singleGuestDetails.setMobileNumber(et_guest_number.getText().toString());
                        singleGuestDetails.setPhotoIdNumber(et_document_no.getText().toString());
                        singleGuestDetails.setPhotoIdType(mDocType);
                        singleGuestDetails.setSalutation("Miss");
                        singleGuestDetails.setGender("2");
                       // singleGuestDetails.setUserId("1");

                        meMap=new HashMap<String, String>();
                        meMap.put("fname",singleGuestDetails.getFirstName());
                        meMap.put("lname",singleGuestDetails.getLastName());
                        meMap.put("email",singleGuestDetails.getEmailId());
                        meMap.put("gender",singleGuestDetails.getGender());
                        meMap.put("mobile",singleGuestDetails.getMobileNumber());
                        meMap.put("salutation",singleGuestDetails.getSalutation());
                        meMap.put("photoid_type","ff");
                        meMap.put("photoid_image","PAN");
                        meMap.put("photoid","123");


                    }else {
                        Toast.makeText(getApplicationContext(),"Please Enter All the Fields",Toast.LENGTH_LONG).show();
                    }

                  guestDetailsListMap.add(meMap);
                }
                                
  //-----------------api call--
               // Toast.makeText(getApplicationContext(),"API call",Toast.LENGTH_LONG).show();

                Log.e("MAP", guestDetailsListMap.toString());
                postGuestAPI();

            }
        });

       // Toast.makeText(getApplicationContext(),""+ FragmentEstimateBookSingle.mGuestCount_nd_GENDE,Toast.LENGTH_LONG).show();
    }

    private void postGuestAPI(){
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait Uploading Guest Details...");
        pDialog.show();
try{


        String json = new Gson().toJson(guestDetailsListMap);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("booking_id", "2");
        params.put("user_id", "1");
        params.put("guest_array",json);

    //    "[{"fname":"raJU","lname":"baburao","email":"nee@gmail.com","gender":"2","mobile":"9545","salutation":"mr","photoid_type":"pan","photoid_image":"pan","photoid":"11122" },	{"fname":"raJU", "lname":"baburao","email":"nee@gmail1.com","gender":"2","mobile":"9545","salutation":"mr","photoid_type":"pan","photoid_image":"pan","photoid":"11122" }]"

        String data = new Gson().toJson(params);

        JSONObject jsonobj = new JSONObject(params);

        GuestDetailsRequestModel paramsData = new GuestDetailsRequestModel();
        paramsData.setBooking_id("2");
        paramsData.setUser_id("1");
        paramsData.setGuest_data(guestDetailsListMap);

        services.postGuest(paramsData/*,"1",json*/).enqueue(new Callback<GuestResponce>() {
            @Override
            public void onResponse(Call<GuestResponce> call, Response<GuestResponce> response) {
                pDialog.dismiss();
                alerUploadGuestDialog();
               // Toast.makeText(getApplicationContext(),"xxx"+response.body().getMessage().toString(),Toast.LENGTH_LONG).show();

        }

        @Override
        public void onFailure(Call<GuestResponce> call, Throwable t) {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();

        }
    });
}catch (Exception e){
    e.printStackTrace();
}
    }

    private void alerUploadGuestDialog() {

            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Guest details Successfully Uploaded...");
            alertDialogBuilder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.cancel();
                            Intent intent = new Intent(ActivityUploadDocument.this, MainActivity.class);
                            startActivity(intent);
                            arg0.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    /*{
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait..Uploading Guest details..");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GUEST_DETAILS,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "" + response.toString(), Toast.LENGTH_LONG).show();
                        Log.d("booking responce :",response.toString());


                        pDialog.dismiss();
                        try {

                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                *//*Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(ActivityUploadDocument.this, ActivityPropertyList.class);
                                startActivity(intent);
                                finish();*//*

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
                Log.d("booking :",error.toString());


            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("booking_id", "2");
                params.put("user_id", "1");
                params.put("guest_array",guestDetailsListMap.toString());
                Log.e("MAP_param", params.toString());
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }*/

    private void prepareListData() {

        LayoutInflater infalInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //For male entries ID's for View we are starting from (10+i) and for female entries starting from (20+i)
        for (int i=1 ;i<= maleGuestCount;i++){

            View convertView = infalInflater.inflate(R.layout.temp_linear_data_holder, null);
            convertView.setId(10+i);
            final LinearLayout l1 = (LinearLayout)convertView.findViewById(R.id.maincontainer);
            TextView text_guestCountAndGender = convertView.findViewById(R.id.text_guestCountAndGender);
            text_guestCountAndGender.setText("GUEST (MALE) "+(i+1));
            RelativeLayout guest_Layout = convertView.findViewById(R.id.guest_Layout);
            guest_Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(l1.getVisibility()== View.VISIBLE){
                        l1.setVisibility(View.GONE);
                    }else{
                        l1.setVisibility(View.VISIBLE);
                    }
                }
            });
            final ImageView image_PreviewGuestImage = convertView.findViewById(R.id.image_PreviewGuestImage);

            Button btn_selectGuestImage = convertView.findViewById(R.id.btn_selectGuestImage);
            btn_selectGuestImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    _selectImage(image_PreviewGuestImage);
                }
            });
            container.addView(convertView);
        }

        for (int i=1 ;i<=femaleGuestCount;i++){
            final View convertView = infalInflater.inflate(R.layout.temp_linear_data_holder, null);
            convertView.setId(20+i);
            final LinearLayout l1 = (LinearLayout)convertView.findViewById(R.id.maincontainer);
            TextView text_guestCountAndGender = convertView.findViewById(R.id.text_guestCountAndGender);
            text_guestCountAndGender.setText("GUEST (FEMALE) "+(i+1));
            RelativeLayout guest_Layout = convertView.findViewById(R.id.guest_Layout);
            guest_Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(l1.getVisibility()== View.VISIBLE){
                        l1.setVisibility(View.GONE);
                    }else{
                        l1.setVisibility(View.VISIBLE);
                    }
                }
            });
            final ImageView image_PreviewGuestImage = convertView.findViewById(R.id.image_PreviewGuestImage);

            Button btn_selectGuestImage = convertView.findViewById(R.id.btn_selectGuestImage);
            btn_selectGuestImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    _selectImage(image_PreviewGuestImage);
                }
            });
            container.addView(convertView);
        }

    }
//--------------------------------img select img-------------------
    public void _selectImage(ImageView image_PreviewGuestImage){
        final int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                selectImage(image_PreviewGuestImage);
            }
        }
    }

    private void selectImage(ImageView image_PreviewGuestImage) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, PICK_IMAGE_REQUEST);
        myImage = image_PreviewGuestImage;
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //selectImage();

                } else {
                    Toast.makeText(this, "Please give your permission.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_REQUEST:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String[] projection = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    String filepath = cursor.getString(columnIndex);
                    cursor.close();

                    bitmap = BitmapFactory.decodeFile(filepath);
                    Drawable drawable = new BitmapDrawable(bitmap);
                    myImage.setImageBitmap(bitmap);
                }
                break;
            default:
                break;
        }
    }
//-----------------------------------------end select img

}
