package com.flo.floapp.BookingDetails.EstimateBooking;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterEstimateBooking extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AdapterEstimateBooking(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentEstimateBookSingle tab1 = new FragmentEstimateBookSingle();
                return tab1;
            case 1:
                FragmentEstimateBookShared tab2 = new FragmentEstimateBookShared();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
