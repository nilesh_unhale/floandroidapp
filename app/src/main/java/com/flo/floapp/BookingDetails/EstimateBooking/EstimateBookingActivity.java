package com.flo.floapp.BookingDetails.EstimateBooking;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.flo.floapp.CategoryList.PojoBookingDetails;
import com.flo.floapp.R;

import java.util.ArrayList;
import java.util.List;

public class EstimateBookingActivity extends AppCompatActivity {
    TextView text_SELECTYOURSTAY;
    TabLayout tabLayout;
    ViewPager viewPager;
    PojoBookingDetails categoryDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_estimate_booking);
        Intent intent = getIntent();
        categoryDetails = (PojoBookingDetails)intent.getSerializableExtra("categorydetails");
        text_SELECTYOURSTAY = findViewById(R.id.text_SELECTYOURSTAY);
        viewPager = (ViewPager) findViewById(R.id.pager_EstimateBooking);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tab_layout_EstimateBooking);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(intent.getIntExtra("tabNo",0));

        setupTabIcons();
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("SINGLE");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_person_black_24dp, 0, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("SHARED");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shared_black_24dp, 0, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Light.ttf");
        tabOne.setTypeface(myCustomFontBold);
        tabTwo.setTypeface(myCustomFontBold);
        text_SELECTYOURSTAY.setTypeface(myCustomFontBold);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putSerializable("categoryDetails", categoryDetails);
        FragmentEstimateBookSingle fragSingle = new FragmentEstimateBookSingle();
        fragSingle.setArguments(bundle);
        adapter.addFrag(fragSingle, "SINGLE");


        FragmentEstimateBookShared fragShare = new FragmentEstimateBookShared();
        fragShare.setArguments(bundle);
        adapter.addFrag(fragShare, "SHARED");

        viewPager.setAdapter(adapter);
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);

        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
