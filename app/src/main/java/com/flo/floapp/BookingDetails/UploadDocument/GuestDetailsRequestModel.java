package com.flo.floapp.BookingDetails.UploadDocument;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class GuestDetailsRequestModel {

    @SerializedName("user_id")
    private String user_id;
    @SerializedName("booking_id")
    private String booking_id;
    @SerializedName("guest_array")
    private ArrayList<HashMap<String, String>> guest_data;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public ArrayList<HashMap<String, String>> getGuest_data() {
        return guest_data;
    }

    public void setGuest_data(ArrayList<HashMap<String, String>> guest_data) {
        this.guest_data = guest_data;
    }


}
