package com.flo.floapp.MyPreferences;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import com.flo.floapp.R;
import java.util.ArrayList;

public class AdapterPreference extends RecyclerView.Adapter<AdapterPreference.PreHolder>   {
    Context _ConText;
    ArrayList<SinglePreference> preList;
    public static ArrayList<String> _arrayPreference = new ArrayList<String>();

    public AdapterPreference(Context _ConText, ArrayList<SinglePreference> preList) {
        this._ConText = _ConText;
        this.preList = preList;
    }

    @Override
    public AdapterPreference.PreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(_ConText).inflate(R.layout.single_preference,parent, false);
        AdapterPreference.PreHolder rcv = new AdapterPreference.PreHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final AdapterPreference.PreHolder holder, final int position) {
        final SinglePreference singlePreference = preList.get(position);
        holder.textView_name.setText(singlePreference.get_preferenceDiscription());
        if(singlePreference.get_preferenceIS_SELECTED().equals("1")){
            holder.checkBox.setChecked(true);
        }
        else {
            holder.checkBox.setChecked(false);
        }

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(((CheckBox)view).isChecked()){
                    _arrayPreference.add(String.valueOf(position+1));
                }
                else{
                    _arrayPreference.remove(String.valueOf(position+1));
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return this.preList.size();
    }

    public class PreHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;
        TextView textView_name;

        public PreHolder(View itemView) {
            super(itemView);
            textView_name = itemView.findViewById(R.id.tv_prefeName);
            checkBox = itemView.findViewById(R.id.cb_pre);

            //Typeface myCustomFontBold = Typeface.createFromAsset(_ConText.getAssets(), "fonts/Raleway-Medium.ttf");
            Typeface myCustomFontRegular = Typeface.createFromAsset(_ConText.getAssets(), "fonts/Raleway-Light.ttf");
            textView_name.setTypeface(myCustomFontRegular);

        }
    }

}
