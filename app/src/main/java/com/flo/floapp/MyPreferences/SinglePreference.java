package com.flo.floapp.MyPreferences;

public class SinglePreference {
    String _preferenceId;
    String _preferenceName;
    String _preferenceDiscription;
    String _preferenceIS_SELECTED;

    public SinglePreference(String _preferenceId, String _preferenceName, String _preferenceDiscription, String _preferenceIS_SELECTED) {
        this._preferenceId = _preferenceId;
        this._preferenceName = _preferenceName;
        this._preferenceDiscription = _preferenceDiscription;
        this._preferenceIS_SELECTED = _preferenceIS_SELECTED;
    }

    public SinglePreference() {
    }

    public String get_preferenceId() {
        return _preferenceId;
    }

    public void set_preferenceId(String _preferenceId) {
        this._preferenceId = _preferenceId;
    }

    public String get_preferenceName() {
        return _preferenceName;
    }

    public void set_preferenceName(String _preferenceName) {
        this._preferenceName = _preferenceName;
    }

    public String get_preferenceDiscription() {
        return _preferenceDiscription;
    }

    public void set_preferenceDiscription(String _preferenceDiscription) {
        this._preferenceDiscription = _preferenceDiscription;
    }

    public String get_preferenceIS_SELECTED() {
        return _preferenceIS_SELECTED;
    }

    public void set_preferenceIS_SELECTED(String _preferenceIS_SELECTED) {
        this._preferenceIS_SELECTED = _preferenceIS_SELECTED;
    }
}
