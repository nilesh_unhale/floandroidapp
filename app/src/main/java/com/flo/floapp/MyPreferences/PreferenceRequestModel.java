package com.flo.floapp.MyPreferences;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreferenceRequestModel {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("preference")
    @Expose
    private List<String> preference = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getPreference() {
        return preference;
    }

    public void setPreference(ArrayList<String> preference) {
        this.preference = preference;
    }
}
