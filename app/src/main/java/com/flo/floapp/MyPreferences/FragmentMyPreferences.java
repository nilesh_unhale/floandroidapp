package com.flo.floapp.MyPreferences;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyPreferences extends Fragment {
    View view;
    RecyclerView _rv_preference;
    String mUSER_ID;
    ProgressBar mProgressBar;
    ArrayList<SinglePreference> mList;
    AdapterPreference _Adapter;
    Button buttonSave;
    JSONArray jsArray;
    TextView text_namePreference;
    private APIService services;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_preferences, container, false);

        _rv_preference = view.findViewById(R.id.re_preference);
         buttonSave = view.findViewById(R.id.buttonSave);
        text_namePreference = view.findViewById(R.id.text_namePreference);
        _rv_preference.setLayoutManager(new LinearLayoutManager(getActivity()));
        _rv_preference.setNestedScrollingEnabled(true);
        _rv_preference.setHasFixedSize(true);
        services = ApiUtils.getAPIService();
        mList = new ArrayList<SinglePreference>();
        mUSER_ID = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");
        getAllPreferences();

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                     jsArray = new JSONArray(AdapterPreference._arrayPreference.toString());
                    Log.e("MAP", jsArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                saveMyPreference();
            }
        });
        return view;
    }

    private void saveMyPreference(){
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait ...");
        pDialog.show();
        try{
            PreferenceRequestModel paramsData = new PreferenceRequestModel();
            paramsData.setUserId(mUSER_ID);
            paramsData.setPreference(AdapterPreference._arrayPreference);

            services.savePreference(paramsData).enqueue(new Callback<PreferenceResponceModel>() {
                @Override
                public void onResponse(Call<PreferenceResponceModel> call, Response<PreferenceResponceModel> response) {
                    pDialog.dismiss();
                     Toast.makeText(getActivity(),""+response.body().getMessage().toString(),Toast.LENGTH_LONG).show();
                     Log.d("pr : ",response.body().toString());
                }

                @Override
                public void onFailure(Call<PreferenceResponceModel> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getAllPreferences()
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GET_PREFRENCE,
                new com.android.volley.Response.Listener<String>() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray array_preference = reg_jsonObject.getJSONArray("response");

                                for (int i=0;i<array_preference.length();i++){

                                    JSONObject mObj = array_preference.getJSONObject(i);
                                    String mPRE_ID = mObj.getString("id");
                                    String mPRE_NAME = mObj.getString("name");
                                    String mPRE_DISCRIPTION = mObj.getString("description");
                                    String mIs_selected = mObj.getString("selected");
                                    SinglePreference sp = new SinglePreference();

                                    sp.set_preferenceId(mPRE_ID);
                                    sp.set_preferenceName(mPRE_NAME);
                                    sp.set_preferenceDiscription(mPRE_DISCRIPTION);
                                    sp.set_preferenceIS_SELECTED(mIs_selected);
                                    mList.add(sp);
                                }
                                _Adapter = new AdapterPreference(getActivity(), mList);
                                _rv_preference.setAdapter(_Adapter);


                            } else {
                                Toast.makeText(getActivity(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mUSER_ID);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }
}
