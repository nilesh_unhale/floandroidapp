package com.flo.floapp.Refferal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FragmentRefferal extends Fragment {
    View view;
    Button button_ADD_REFFERAL;
    EditText text_NAMEinput, text_MOBILEinput, text_EMAILinput;
    CountryCodePicker codePicker;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mUSERiD;

//implementation 'com.github.joielechong:countrycodepicker:2.1.8'
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_referal, container, false);

        button_ADD_REFFERAL = view.findViewById(R.id.button_ADD_REFFERAL);
        text_NAMEinput = view.findViewById(R.id.text_NAMEinput);
        text_MOBILEinput = view.findViewById(R.id.text_MOBILEinput);
        text_EMAILinput = view.findViewById(R.id.text_EMAILinput);
        codePicker = (CountryCodePicker) view.findViewById(R.id.ccp);

        mUSERiD = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");


        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Light.ttf");

        button_ADD_REFFERAL.setTypeface(myCustomFont);
        text_NAMEinput.setTypeface(myCustomFont);
        text_MOBILEinput.setTypeface(myCustomFont);
        text_EMAILinput.setTypeface(myCustomFont);

        codePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
            }
        });

        button_ADD_REFFERAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mNAME = text_NAMEinput.getText().toString().trim();
                String mEMAIL = text_EMAILinput.getText().toString().trim();
                String mNUMBER = text_MOBILEinput.getText().toString().trim();

                if (mEMAIL.equals("") && mNAME.equals("") && mNUMBER.equals("")){

                    Toast.makeText(getActivity(),"Please Enter All Value",Toast.LENGTH_LONG).show();
                }
                else {

                    if (mEMAIL.matches(emailPattern)) {

                        _referFriend(mNAME,mEMAIL,mNUMBER);

                    } else {
                        text_EMAILinput.setError("Invalid email address");

                    }
                }
            }
        });

        return view;


    }

    private void _referFriend(final String mNAME, final String mEMAIL, final String mNUMBER) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_REFFERAL,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                dialogSuccess(res_Message);
                            }
                            else {
                                Toast.makeText(getActivity(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", mEMAIL);
                params.put("name", mNAME);
                params.put("mobile", mNUMBER);
                params.put("user_id", mUSERiD);
                params.put("stdcode", "91");
                //params.put("stdcode", codePicker.getSelectedCountryCode());

                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void dialogSuccess(String res_message) {
        final Dialog dialog_refere = new Dialog(getActivity());
        dialog_refere.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_refere.setContentView(R.layout.dialog_success);
        dialog_refere.show();
        TextView tv_msg = dialog_refere.findViewById(R.id.tv_messageSuccess);
        tv_msg.setText(res_message);
        Button button_service = dialog_refere.findViewById(R.id.btn_Success);
        button_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    dialog_refere.dismiss();
            }
        });
    }

}


