package com.flo.floapp.Register;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

    public class RegisterResponce {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("response")
        @Expose
        private Response response;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Response getResponse() {
            return response;
        }

        public void setResponse(Response response) {
            this.response = response;
        }
        public class Response {

            @SerializedName("first")
            @Expose
            private String first;
            @SerializedName("last")
            @Expose
            private String last;
            @SerializedName("mobile")
            @Expose
            private String mobile;
            @SerializedName("gender")
            @Expose
            private String gender;
            @SerializedName("mobile_otp")
            @Expose
            private Integer mobileOtp;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("id")
            @Expose
            private Integer id;

            public String getFirst() {
                return first;
            }

            public void setFirst(String first) {
                this.first = first;
            }

            public String getLast() {
                return last;
            }

            public void setLast(String last) {
                this.last = last;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Integer getMobileOtp() {
                return mobileOtp;
            }

            public void setMobileOtp(Integer mobileOtp) {
                this.mobileOtp = mobileOtp;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

        }
    }


