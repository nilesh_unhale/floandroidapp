package com.flo.floapp.Register;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.ActivityGuestOptions;
import com.flo.floapp.Login.ForgetPassword;
import com.flo.floapp.Login.Login;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SchedduleVisit.ActivityScheduleVisitVerify;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.google.gson.JsonObject;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
    private APIService services;
    private int mYear, mMonth, mDay;
    TextView text_AlreadyHaveAccount, tv_registerURL;
    EditText et_firstName, et_lastName, et_mobilet, et_confirm_password, et_email, et_password;
    Button btn_register;
    Spinner spinner_GENDER;
    CountryCodePicker codePicker;
    Dialog dialog_passValid;
    FontChangeCrawler fontChanger;
    NetworkDetactor networkDetactor;
    private static final int REQUEST_READ_SMS= 1;
    RadioGroup rGRegister;
    RadioButton rb_maleRegister, rb_femaleRegister;
    int mGender = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_register);
       // ActivityCompat.requestPermissions(Register.this , new String[]{Manifest.permission.READ_SMS}, REQUEST_READ_SMS);
        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
        networkDetactor = new NetworkDetactor(Register.this);
        init();
    }

    public void init() {
        text_AlreadyHaveAccount = findViewById(R.id.text_AlreadyHaveAccount);
        tv_registerURL = findViewById(R.id.tv_registerURL);
        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_mobilet = findViewById(R.id.et_mobilet);
        et_confirm_password = findViewById(R.id.et_confirm_password);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        btn_register = findViewById(R.id.btn_register);
        rGRegister = findViewById(R.id.genderRegister);

        rb_maleRegister = findViewById(R.id.rb_maleRegister);
        rb_femaleRegister = findViewById(R.id.rb_femaleRegister);
        codePicker = (CountryCodePicker) findViewById(R.id.registerccp);


        rb_maleRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = rGRegister.getCheckedRadioButtonId();
                mGender = 1;
            }
        });
        rb_femaleRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = rGRegister.getCheckedRadioButtonId();
                mGender = 2;
            }
        });

        codePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
            }
        });


        /*services = ApiUtils.getAPIService();
        //buttonSignUp.setOnClickListener(this);
*/
       /* imagebutton_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBack = new Intent(Register.this, Login.class);
                startActivity(intentBack);
                finish();
            }
        });*/
        tv_registerURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBack = new Intent(Register.this, Login.class);
                startActivity(intentBack);
                finish();

            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (networkDetactor.isConnected()){
                String mFirstName = et_firstName.getText().toString().trim();
                String mLastName = et_lastName.getText().toString().trim();
                String mEmail = et_email.getText().toString().trim();
                String mPassword = et_password.getText().toString().trim();
                String mMobile = et_mobilet.getText().toString().trim();
                String mConfirmPassword = et_confirm_password.getText().toString().trim();

                if (!mFirstName.equals("") &&
                        !mLastName.equals("") &&
                        !mEmail.equals("") &&
                        !mMobile.equals("") &&
                        !mConfirmPassword.equals("") &&
                        !mPassword.equals("")) {
                    if (mEmail.matches(emailPattern)) {

                         if (mMobile.length()<10){
                            Toast toast1 = Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number", Toast.LENGTH_LONG);
                            toast1.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                            toast1.show();
                        }

                        else if (mPassword.length() < 8 && !isValidPassword(mPassword)) {
                            dialog_passValid = new Dialog(Register.this);
                            dialog_passValid.setTitle("PASSWORD REQUIRMENTS");
                            dialog_passValid.setContentView(R.layout.dialog_password_validations);
                            Button btn_validations = dialog_passValid.findViewById(R.id.btn_validations);
                            btn_validations.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog_passValid.dismiss();
                                }
                            });
                            dialog_passValid.show();
                        } else{
                            if (mPassword.equals(mConfirmPassword)) {
                                registerUser(mFirstName, mLastName, mMobile, mEmail, mPassword,mGender);
                            } else {
                                et_confirm_password.setError("Password not Match");
                            }
                        }
                    } else {
                        et_email.setError("Invalid Email Address");
                    }
                } else if (mFirstName.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter First Name", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }else if (mLastName.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Last Name", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }else if (mMobile.equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Mobile Number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }else if (mEmail.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Email", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }else if (mPassword.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Password", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }else if (mConfirmPassword.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Confirm Password", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
                }else {
                    Toast toast = Toast.makeText(Register.this, "No Internet Connection..", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }


    private void registerUser(final String mFirstName, final String mLastName, final String mMobile,
                              final String mEmail, final String mPassword, final int mGender) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_REISTER,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                JSONObject strJson = reg_jsonObject.getJSONObject("response");
                                String mobile_otp = strJson.getString("mobile_otp");
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();

                                Intent reg_intent = new Intent(Register.this, OTPVerify.class);
                                reg_intent.putExtra("mMobile", mMobile);
                                reg_intent.putExtra("mobile_otp", mobile_otp);
                                startActivity(reg_intent);
                                finish();
                            } else {
                                Toast toast =  Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast =Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("first", mFirstName);
                params.put("last", mLastName);
                params.put("mobile", mMobile);
                params.put("email", mEmail);
                params.put("password", mPassword);
                params.put("fcmtoken", PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.FCM_TOKEN,""));
                params.put("fcmtoken", "aaa");
                params.put("stdcode", codePicker.getSelectedCountryCode());
                params.put("gender", String.valueOf(mGender));
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

}

