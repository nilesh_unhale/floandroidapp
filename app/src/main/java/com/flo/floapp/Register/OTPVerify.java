package com.flo.floapp.Register;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.Login.Login;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.R;
import com.flo.floapp.SchedduleVisit.SmsListener;
import com.flo.floapp.SmsReceiver;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OTPVerify extends AppCompatActivity {
    Button btnVerifyNuber;
    EditText editTextVerifyOTP;
    FontChangeCrawler fontChanger;
    private static final int REQUEST_READ_SMS= 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_otpverify);
       // ActivityCompat.requestPermissions(OTPVerify.this , new String[]{Manifest.permission.READ_SMS}, REQUEST_READ_SMS);

      /*  SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.d("Text",messageText);
                String motp = messageText.substring(messageText.length() - 4);
                editTextVerifyOTP.setText(motp);
            }
        });*/

        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        init();
    }

    private void init() {
                Intent reg_intent = getIntent();
                final String mMobile = reg_intent.getStringExtra("mMobile");
                String mobile_otp = reg_intent.getStringExtra("mobile_otp");
                //Toast.makeText(getApplicationContext(),"Use OTP for Verification :- "+mobile_otp,Toast.LENGTH_LONG).show();

                 btnVerifyNuber = (Button) findViewById(R.id.btn_verify_number);
                 editTextVerifyOTP = (EditText) findViewById(R.id.editText_verifyotp);

                 btnVerifyNuber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String mOTP = editTextVerifyOTP.getText().toString().trim();
                        if (!mOTP.equals(""))
                        {
                            verifyOTP(mMobile,mOTP);
                        }
                        else
                            {
                                Toast toast = Toast.makeText(getApplicationContext(),"Please Enter 4 digit OTP  ",Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                    }
                });
    }

    private void verifyOTP(final String mMobile, final String mOTP) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.REISTER_VERIFY_OTP,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Intent intent = new Intent(OTPVerify.this, Login.class);
                                intent.putExtra("REDIRECT_LOGIN","directlogin");
                                startActivity(intent);
                                finish();
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                                // JSONObject strJson = reg_jsonObject.getJSONObject("response");

                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", mMobile);
                params.put("otp", mOTP);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    }
