package com.flo.floapp.Retrofit;

import com.flo.floapp.BookingDetails.UploadDocument.GuestDetailsModel;
import com.flo.floapp.BookingDetails.UploadDocument.GuestDetailsRequestModel;
import com.flo.floapp.BookingDetails.UploadDocument.GuestResponce;
import com.flo.floapp.CategoryList.CategoryRequest;
import com.flo.floapp.CategoryList.CategoryResponce;
import com.flo.floapp.Login.BodyFverifyOtp;
import com.flo.floapp.Login.ResponseLogin;
import com.flo.floapp.MyPreferences.PreferenceRequestModel;
import com.flo.floapp.MyPreferences.PreferenceResponceModel;
import com.flo.floapp.Register.RegisterOTPVerifyResponce;
import com.flo.floapp.Register.RegisterResponce;
import com.flo.floapp.SearchProperty.ResponceProList;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIService
{
    @FormUrlEncoded
    @POST("register")
    Call<RegisterResponce> registerPost(@Field("first") String firstName, @Field("last") String lastName, @Field("mobile") String mobile,
                                    @Field("gender") String gender, @Field("smsotp") String mobile_otp, @Field("emailotp") String emailotp,
                                    @Field("email") String email,
                                    @Field("password") String password);

    @FormUrlEncoded
    @POST("login")
    Call<ResponseLogin> loginUser(@Field("email") String firstName, @Field("password") String lastName);

    @FormUrlEncoded
    @POST("/forgetPasswordOTP")
    Call<RegisterOTPVerifyResponce> registerVerifyOTP(@Body BodyFverifyOtp bodyFverifyOtp);


    /*@GET("currentBooking/{id}")
    Call<ResponceCurrentBooking> getCurrentBooking(@Path("id") String id);*/


    @FormUrlEncoded
    @POST("/forgetPasswordOTP")
    Call<ResponceProList> ProList(@Field("id") String mId);

    //http://inubex.in/output/FLO/uat/api/useridentity
    @POST("useridentity")
    Call<GuestResponce> postGuest(@Body GuestDetailsRequestModel params/*, @Field("user_id") String user_id, @Field("guest_array") String mapData*/);

    @POST("uploadpreferences")
    Call<PreferenceResponceModel> savePreference(@Body PreferenceRequestModel params);

    @POST("getProperty")
    Call<CategoryResponce> categotyGet(@Body CategoryRequest paramsData);

}



