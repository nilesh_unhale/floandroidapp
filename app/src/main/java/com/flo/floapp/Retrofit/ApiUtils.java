package com.flo.floapp.Retrofit;

/**
 * Created by neel.
 */
public class ApiUtils {
    private ApiUtils() {}

    /*public static final String DOMAIN_NAME = "https://flocolive.com/stage1/api/";
    public static final String URL_IMAGE = "https://flocolive.com/stage1/public/uploads/full_size/";
    public static final String URL_USER_IMAGE = "https://flocolive.com/stage1/public/uploads/users/profile/";
    public static final String URL_VDO = "https://flocolive.com/stage1/public/uploads/properties/video/";
    public static final String URL_PDF = "https://flocolive.com/stage1/public/uploads/properties/pdf/";*/

    public static final String DOMAIN_NAME = "https://www.flocolive.com/api/";
    public static final String URL_IMAGE = "https://www.flocolive.com/public/uploads/full_size/";
    public static final String URL_USER_IMAGE = "https://www.flocolive.com/public/uploads/users/profile/";
    public static final String URL_VDO = "https://www.flocolive.com/public/uploads/properties/video/";
    public static final String URL_PDF = "https://www.flocolive.com/public/uploads/properties/pdf/";


    public static APIService getAPIService() {

        return RetrofitClient.getClient(DOMAIN_NAME).create(APIService.class);
    }
}


/*

package com.vishalkumar.android.icareer.Activity;

        import android.Manifest;
        import android.app.DatePickerDialog;
        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.graphics.Bitmap;
        import android.net.Uri;
        import android.os.Build;
        import android.provider.MediaStore;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.content.ContextCompat;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Base64;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.RadioButton;
        import android.widget.RadioGroup;
        import android.widget.Spinner;
        import android.widget.Toast;

        import com.android.volley.DefaultRetryPolicy;
        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.rilixtech.Country;
        import com.rilixtech.CountryCodePicker;
        import com.vishalkumar.android.icareer.R;
        import com.vishalkumar.android.icareer.SharePreference.PreferenceStrings;
        import com.vishalkumar.android.icareer.SharePreference.PreferenceUtils;
        import com.vishalkumar.android.icareer.Utilities.AppConstant;
        import com.vishalkumar.android.icareer.Volley.VolleySingleton;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.ByteArrayOutputStream;
        import java.text.SimpleDateFormat;
        import java.util.Calendar;
        import java.util.HashMap;
        import java.util.Map;

        import de.hdodenhof.circleimageview.CircleImageView;

        import static android.Manifest.permission.CAMERA;
        import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
        import static android.Manifest.permission.RECORD_AUDIO;
        import static java.security.AccessController.getContext;

public class AdditionalInfo extends AppCompatActivity {
    EditText et_fname,et_lname,et_dob,et_strength,et_mob;
    RadioGroup radioGroup;
    RadioButton male,female;
    Bitmap bitmap;
    public static final int RequestPermissionCode = 1;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    String formattedDate,mMobile,mStrength;
    private int mYear,mMonth,mDay;
    ProgressDialog prgDialog2;
    int selectedId,i=0;
    int PICK_IMAGE_REQUEST = 111;
    String imageString;
    Button btn_done,buttonLoadPicture;
    String gender,mFname,mLname,mdob,cc;
    String first_name,middleName,lastName;
    CircleImageView circleImageView;
    Context context;
    String token;
    CountryCodePicker ccp;
    String[] country_namelist,country_nameIdlist,state_namelist,state_nameIdlist,city_namelist,city_nameIdlist;
    Spinner spinner_country,spinner_state,spinner_city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_info);
        context = AdditionalInfo.this;
        et_fname = (EditText) findViewById(R.id.et_fname);
        et_lname = (EditText) findViewById(R.id.et_lastname);
        et_mob = (EditText) findViewById(R.id.et_mob);
        et_strength = (EditText) findViewById(R.id.strength);

        spinner_country = (Spinner) findViewById(R.id.countrySpinner);
        spinner_state = (Spinner) findViewById(R.id.stateSpinner);
        spinner_city = (Spinner) findViewById(R.id.citySpinner);

        et_strength = (EditText) findViewById(R.id.strength);
        et_strength = (EditText) findViewById(R.id.strength);
        et_dob = (EditText) findViewById(R.id.et_dob);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        btn_done = (Button) findViewById(R.id.btn_done);
        buttonLoadPicture = (Button) findViewById(R.id.buttonLoadPicture);
        circleImageView = (CircleImageView) findViewById(R.id.userPic);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        male = (RadioButton) findViewById(R.id.radio_male);
        female = (RadioButton) findViewById(R.id.radio_female);
        prgDialog2 = new ProgressDialog(AdditionalInfo.this);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = simpleDateFormat.format(calendar.getTime());
        token = PreferenceUtils.readString(context, PreferenceStrings.LOGIN_TOKEN, "");
        getCountry();
        if(checkPermission()){
            //  Toast.makeText(AdditionalInfo.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
        }
        else {

            requestPermission();
        }
        buttonLoadPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedId = radioGroup.getCheckedRadioButtonId();
                gender = "1";

            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedId = radioGroup.getCheckedRadioButtonId();
                //   gender = female.getText().toString();
                gender = "2";

            }
        });
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                cc = selectedCountry.getPhoneCode();
                // Toast.makeText(context, "Updated " +ccp.getSelectedCountryCode(), Toast.LENGTH_SHORT).show();
            }
        });

        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AdditionalInfo.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                et_dob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();


            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mFname = et_fname.getText().toString().trim();
                mLname = et_lname.getText().toString().trim();
                mdob = et_dob.getText().toString().trim();
                mMobile = et_mob.getText().toString().trim();
                mStrength = et_strength.getText().toString().trim();

                if (!mFname.equals("") && !mLname.equals("") && !mdob.equals("")
                        && !mStrength.equals("") && !mMobile.equals("")) {
                    additionalInfo();

                } else {
                    Toast.makeText(getApplicationContext(), "All fields are mandatory", Toast.LENGTH_LONG).show();

                }
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();

            try {
                //getting image from gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

              */
/*  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);*//*

                //Setting image to ImageView
                circleImageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    //       ----- function call -----
    public void additionalInfo(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.ADDTIONAL_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Additionalifo--->",response.toString());
                        //   prgDialog2.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (status.equals("1"))
                            {
                                Toast.makeText(context,"Successfully Updated All details",Toast.LENGTH_LONG).show();
                                //  Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                                PreferenceUtils.writeBoolean(getApplicationContext(), PreferenceStrings.IS_LOGIN1, true);
                                Intent i = new Intent(AdditionalInfo.this, NevigationActivity.class);
                                startActivity(i);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Somethink Went Wrong ", Toast.LENGTH_LONG).show();

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("token",token);
                params.put("firstname", mFname);
                params.put("lastname", mLname);
                params.put("country",spinner_country.getSelectedItem().toString());
                params.put("state",spinner_state.getSelectedItem().toString());
                params.put("city",spinner_city.getSelectedItem().toString());
                params.put("gender","1");
                params.put("date_of_birth",et_dob.getText().toString());
                params.put("country_code",ccp.getSelectedCountryCode());
                params.put("mobile",mMobile);
                params.put("strength",mStrength);
                // params.put("profile_image_","");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(stringRequest);
    }
    //-----------permission
    private void requestPermission() {

        ActivityCompat.requestPermissions(AdditionalInfo.this, new String[]
                {
                        CAMERA,
                        READ_EXTERNAL_STORAGE,
                        RECORD_AUDIO
                }, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadContactsPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadPhoneStatePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (CameraPermission && ReadContactsPermission && ReadPhoneStatePermission) {

                        Toast.makeText(AdditionalInfo.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(AdditionalInfo.this,"Permission Denied Application Won't Run Without Camera",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    public boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    //---get country--
    public void getCountry(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.URL_COUNTRY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("COUNTRies--->",response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray_LOC = jsonObject.getJSONArray("data");
                            country_namelist = new String[jsonArray_LOC.length()];
                            country_nameIdlist = new String[jsonArray_LOC.length()];
                            for (int i = 0; i < jsonArray_LOC.length(); i++) {
                                JSONObject jobject = jsonArray_LOC.getJSONObject(i);
                                String country_id = jobject.getString("id");
                                String country_name = jobject.getString("name");
                                country_namelist[i] = country_name;
                                country_nameIdlist[i] = country_id;
                            }
                            ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(AdditionalInfo.this,
                                    R.layout.support_simple_spinner_dropdown_item, country_namelist);
                            spinner_country.setAdapter(Adapter1);
                            spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    getStates(country_nameIdlist[position].toString().trim());
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Somethink Went Wrong ", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("token",token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(stringRequest);
    }
    //--------   ----------------states----------------     --------------   ---------
    public void getStates(final String id){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.URL_STATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getStetes-->",response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray_LOC = jsonObject.getJSONArray("data");
                            state_namelist = new String[jsonArray_LOC.length()];
                            state_nameIdlist = new String[jsonArray_LOC.length()];
                            for (int i = 0; i < jsonArray_LOC.length(); i++) {
                                JSONObject jobject = jsonArray_LOC.getJSONObject(i);
                                String state_id = jobject.getString("id");
                                String state_name = jobject.getString("name");
                                state_namelist[i] = state_name;
                                state_nameIdlist[i] = state_id;
                            }
                            ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(AdditionalInfo.this,
                                    R.layout.support_simple_spinner_dropdown_item, state_namelist);
                            spinner_state.setAdapter(Adapter2);
                            spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    getCity1(state_nameIdlist[position].toString().trim());
                                    //Toast.makeText(getApplicationContext(), "Somethink Went Wrong "+state_nameIdlist[position].toString().trim(), Toast.LENGTH_LONG).show();

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Somethink Went Wrong ", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("country_id",id);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(stringRequest);
    }
    private void getCity1(final String trim) {
        {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.URL_CITY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("cities--->", response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray_LOC = jsonObject.getJSONArray("data");
                                city_namelist = new String[jsonArray_LOC.length()];
                                city_nameIdlist = new String[jsonArray_LOC.length()];
                                for (int i = 0; i < jsonArray_LOC.length(); i++) {
                                    JSONObject jobject = jsonArray_LOC.getJSONObject(i);
                                    String city_id = jobject.getString("id");
                                    String city_name = jobject.getString("name");
                                    city_namelist[i] = city_name;
                                    city_nameIdlist[i] = city_id;
                                }
                                ArrayAdapter<String> Adapter3 = new ArrayAdapter<String>(AdditionalInfo.this,
                                        R.layout.support_simple_spinner_dropdown_item, city_namelist);
                                spinner_city.setAdapter(Adapter3);
                                spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        //  Toast.makeText(getApplicationContext(), ""+ insurance_Id[position], Toast.LENGTH_LONG).show();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Somethink Went Wrong ", Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("state_id", trim);
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
            requestQueue.add(stringRequest);
        }
    }
}

*/
