package com.flo.floapp.ContactUs;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.Login.Login;
import com.flo.floapp.MyBooking.UpdateBooking.UpdateBooking;
import com.flo.floapp.NeviDrawer.MainActivity;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;
import com.flo.floapp.Register.OTPVerify;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FragmentContactUs extends Fragment implements OnMapReadyCallback {
    View view;
    EditText etName,etEmail,etPhone,etMasssage;
    Button btnSubmitContactUs;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
    NetworkDetactor networkDetactor;
    String mUserId;
    Dialog success, error;
    TextView textView_contactUs;
    String phone = "+917558474444";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contactus, container, false);

        networkDetactor = new NetworkDetactor(getActivity());
        mUserId = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");
        btnSubmitContactUs = view.findViewById(R.id.button_contactUS_Send);

        etName =  view.findViewById(R.id.editText_Name);
        etEmail =  view.findViewById(R.id.editText_email);
        etPhone =  view.findViewById(R.id.editText_phone);
        etMasssage =  view.findViewById(R.id.editText_Message);
        textView_contactUs =  view.findViewById(R.id.textView_contactUs);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        textView_contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(callIntent);
            }
        });

        btnSubmitContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (networkDetactor.isConnected()){
                String mName = etName.getText().toString().trim();
                String mEmail = etEmail.getText().toString().trim();
                String mPhone = etPhone.getText().toString().trim();
                String mMassage = etMasssage.getText().toString().trim();

                if (!mName.equals("") &&!mEmail.equals("")
                        &&!mPhone.equals("")&&!mMassage.equals("")){

                    if (mEmail.matches(emailPattern)) {
                        uploadContUsDetails(mName,mEmail,mPhone,mMassage);

                    }else {
                        etEmail.setError("Enter Valid Email Address");
                    }

                }else {
                    Toast.makeText(getActivity(), "All Fields Are Mandatory",
                            Toast.LENGTH_SHORT).show();
                }
                }else {
                    Toast.makeText(getActivity(), "No Internet Connection..", Toast.LENGTH_LONG).show();
                }

            }
        });
        return view;


    }

    private void uploadContUsDetails(final String mName, final String mEmail,
                                     final String mPhone, final String mMassage) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        pDialog.setCancelable(false);
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CONTACT_US,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                //Toast.makeText(getActivity(), "" + res_Message, Toast.LENGTH_LONG).show();
                                success = new Dialog(getActivity());
                                success.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                success.setContentView(R.layout.dialog_success);
                                TextView tv_messageSuccess = success.findViewById(R.id.tv_messageSuccess);
                                Button btn_Success = success.findViewById(R.id.btn_Success);
                                tv_messageSuccess.setText(res_Message);
                                btn_Success.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                       Intent intent = new Intent(getActivity(),MainActivity.class);
                                       startActivity(intent);
                                       getActivity().finish();
                                    }
                                });
                                success.show();
                                etName.setText("");
                                etEmail.setText("");
                                etPhone.setText("");
                                etMasssage.setText("");

                            } else {
                                //Toast.makeText(getActivity(), "" + res_Message, Toast.LENGTH_LONG).show();
                                error.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                error.setContentView(R.layout.dialog_not_available);
                                TextView tv_message = error.findViewById(R.id.tv_message);
                                Button btn_NA = error.findViewById(R.id.btn_NA);
                                tv_message.setText(res_Message);
                                btn_NA.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        error.dismiss();
                                    }
                                });
                                etName.setText("");
                                etEmail.setText("");
                                etPhone.setText("");
                                etMasssage.setText("");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                }


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", mName);
                params.put("email", mEmail);
                params.put("mobile", mPhone);
                params.put("message", mMassage);
                return params;
                }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }
    public void successDialog(){

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng mlat = new LatLng(18.55570, 73.9302);
        googleMap.addMarker(new MarkerOptions().position(mlat)
                .title("EON WATERFRONT PHASE-II"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(mlat));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mlat, 17));
    }

}
