package com.flo.floapp.SchedduleVisit;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ScheduleVisit extends AppCompatActivity {
    FontChangeCrawler fontChanger;
    EditText edittext_firstName,edittext_LastName,et_mobileNum,
            et_email,et_comment;
    ImageButton imageButtonBack;
    private int mYear, mMonth, mDay;
    CountryCodePicker CountryCode;
    TextView tv_cityName,tv_ProprtyName, textview_Date, textview_Time;
    Button btn_submit;
    String mPropertyName,mPropertid;
    Spinner spinner_salutation;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_schedule_visit);

        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

        Intent intent = getIntent();
        mPropertyName = intent.getStringExtra("propertyname");
        mPropertid = intent.getStringExtra("propertid");


        initUI();
    }

    public void initUI(){

        spinner_salutation = findViewById(R.id.spinner_salutation);
        tv_cityName = findViewById(R.id.tv_cityName);
        tv_ProprtyName = findViewById(R.id.tv_property);
        textview_Date = findViewById(R.id.textview_Date);
        textview_Time = findViewById(R.id.textview_Time);
        CountryCode = findViewById(R.id.CountryCode);
        edittext_firstName = findViewById(R.id.et_firstname);
        edittext_LastName = findViewById(R.id.et_lastname);
        et_mobileNum = findViewById(R.id.et_mobileNum);
        et_email = findViewById(R.id.et_email);
        et_comment = findViewById(R.id.et_comment);
        btn_submit = findViewById(R.id.btn_comment);
        imageButtonBack = findViewById(R.id.imageButtonBack);

        tv_ProprtyName.setText(mPropertyName);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textview_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                Log.e("", mYear+"-"+mMonth+"-"+mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
                DatePickerDialog datePickerDialog = new DatePickerDialog(ScheduleVisit.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                textview_Date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                c1.add(Calendar.DAY_OF_MONTH,15);
                datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        textview_Time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ScheduleVisit.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String AM_PM ;
                        if(selectedHour < 11) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }

                        if (selectedHour > 8 && selectedHour < 19) {

                            textview_Time.setText(selectedHour + " : " + selectedMinute + " " + AM_PM);
                        }
                        else {
                            textview_Time.setText("");
                            Toast.makeText(ScheduleVisit.this, "Please select time between 9 Am - 7 PM", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");

                mTimePicker.show();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mDate = textview_Date.getText().toString().trim();
                String mTime = textview_Time.getText().toString().trim();
                String mFirstName = edittext_firstName.getText().toString().trim();
                String mLastName = edittext_LastName.getText().toString().trim();
                String mNumber = et_mobileNum.getText().toString().trim();
                String mComment = et_comment.getText().toString().trim();
                String mEmail = et_email.getText().toString().trim();

                if (mEmail.equals("")){
                    mEmail = "Na";
                }else if(mComment.equals("")){
                    mComment = "Na";
                }

                if (!mDate.equals("") && !mTime.equals("") && !mFirstName.equals("") &&
                        !mLastName.equals("") && !mNumber.equals("")){

                    /* if (mEmail.matches(emailPattern)) {*/
                    if (mNumber.length() == 10){
                        postCallScheduleVisit(mPropertyName,mDate,mTime,mFirstName,mLastName,mNumber,mComment,mEmail);
                    }else{
                        Toast toast =  Toast.makeText(getApplicationContext(),"Invalid Mobile Number",Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                    /* }else{
                         Toast toast =  Toast.makeText(getApplicationContext(),"Invalid Email",Toast.LENGTH_LONG);
                         toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                         toast.show();
                     }*/
                }else if(mDate.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Select Date",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }else if(mTime.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Select Time",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }else if (mFirstName.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Enter First Name",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }else if(mLastName.equals("")){
                    Toast toast1 = Toast.makeText(getApplicationContext(),"Please Enter Last Name",Toast.LENGTH_LONG);
                    toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast1.show();
                }else if (mNumber.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Enter Mobile Number",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }/*else if (mEmail.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Enter Email Id",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }*//*else if (mComment.equals("")){
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Enter Comment",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                    toast.show();
                }*/
            }
        });
    }

    private void postCallScheduleVisit(String mPropertyName, final String mDate, final String mTime,
                                       final String mFirstName, final String mLastName,
                                       final String mNumber, final String mComment, final String mEmail)
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(ScheduleVisit.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_SCHEDULE_VISIT,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Intent intent = new Intent(ScheduleVisit.this,ActivityScheduleVisitVerify.class);
                                intent.putExtra("mobileNumber",mNumber);
                                intent.putExtra("time",mTime);
                                intent.putExtra("date",mDate);
                                intent.putExtra("FirstName",mFirstName);
                                startActivity(intent);
                                finish();

                            } else
                            {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                //  Log.e("error", error.getMessage());
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }                }
        }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", mFirstName+" "+mLastName);
                params.put("city", "Pune");
                params.put("property_id", mPropertid);
                params.put("stdCode", CountryCode.getSelectedCountryCode());
                params.put("email", mEmail);
                params.put("mobile", mNumber);
                params.put("time", mTime);
                params.put("date", mDate);
                params.put("comments", mComment);
                params.put("salutation", "Mr.");
                return params;

            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);



    }
    public interface CalendarListener {
        void onFirstDateSelected(Calendar startDate);
        void onDateRangeSelected(Calendar startDate, Calendar endDate);
    }
}
