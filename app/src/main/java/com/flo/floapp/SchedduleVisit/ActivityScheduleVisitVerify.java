package com.flo.floapp.SchedduleVisit;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.Profile.ActivityUpdateProfile;
import com.flo.floapp.R;
import com.flo.floapp.SearchProperty.ActivityPropertyList;
import com.flo.floapp.SmsReceiver;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityScheduleVisitVerify extends AppCompatActivity {

    Button buttonVerifyNuber;
    EditText editTextVerifyOTP;
    Dialog dialog_visitSuccess;
    String mFirstName;
    TextView tv_time_left,tv_sv_resend;
  //  SmsReceiver smsReceiver;
    private static final int REQUEST_READ_SMS= 1;
    private static final int REQUEST_RECEIVE_SMS= 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_schedule_visit_verify);

       /* ActivityCompat.requestPermissions(ActivityScheduleVisitVerify.this , new String[]{Manifest.permission.READ_SMS}, REQUEST_READ_SMS);
        ActivityCompat.requestPermissions(ActivityScheduleVisitVerify.this , new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_RECEIVE_SMS);



        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.d("Text",messageText);
                String motp = messageText.substring(messageText.length() - 4);
                editTextVerifyOTP.setText(motp);
            }
        });
*/
        buttonVerifyNuber = (Button) findViewById(R.id.btn_visit_verify_number);
        editTextVerifyOTP = (EditText) findViewById(R.id.editText_visit_verifyotp);
        tv_time_left = (TextView) findViewById(R.id.tv_time_left);
        tv_sv_resend = (TextView) findViewById(R.id.tv_sv_resend);

        Intent intent = getIntent();
        final String mMobile = intent.getStringExtra("mobileNumber");
        final String mTime = intent.getStringExtra("time");
        final String mDate = intent.getStringExtra("date");
        mFirstName = intent.getStringExtra("FirstName");

        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                tv_time_left.setText("Time Left : " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_time_left.setText("Expired");
            }

        }.start();

        tv_sv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CountDownTimer(30000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        tv_time_left.setText("Time Left : " + millisUntilFinished / 1000);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        tv_time_left.setText("00:00");
                    }

                }.start();
            }
        });
        buttonVerifyNuber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mOTP = editTextVerifyOTP.getText().toString().trim();
                if (!mOTP.equals(""))
                {
                    verifyOTP(mMobile,mOTP,mTime,mDate);
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(),"Please Enter 4 digit OTP  ",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private void verifyOTP(final String mMobile, final String mOTP, final String mTime, final String mDate) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_SCHEDULE_VISIT_VERIFY_OTP,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                dialog_visitSuccess = new Dialog(ActivityScheduleVisitVerify.this);
                                dialog_visitSuccess.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog_visitSuccess.setContentView(R.layout.dialog_verify_successful);
                                dialog_visitSuccess.show();
                                TextView text_VisitorName = dialog_visitSuccess.findViewById(R.id.text_VisitorName);
                                text_VisitorName.setText(mFirstName);
                                Button button_service = dialog_visitSuccess.findViewById(R.id.button_service);

                                button_service.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(ActivityScheduleVisitVerify.this, ActivityPropertyList.class);
                                        intent.putExtra("mLattitude", "18.4897587");
                                        intent.putExtra("mLongitude", "73.8202962");

                                        startActivity(intent);
                                        finish();
                                    }
                                });


                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast toast = Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile", mMobile);
                params.put("sendotp", mOTP);
                params.put("time", mTime);
                params.put("date", mDate);
                return params;

            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
