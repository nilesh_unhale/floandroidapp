package com.flo.floapp.Setting;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class FragmentAccountSetting extends Fragment {
    View view;
    CheckBox sb_NAME, sb_contactNumber, sb_email, sb_dob, sb_Occupation, sb_CompanyName,
            sb_SOCIAL_DETAILS, sb_edu_deatails, sb_interest;
    Button btn_Acc_setting_update;
    int mName, mNumber, mEmail, mDOB, mOccupation, mCompanyName, mSocialDetail, mEduDetails,
            mIntrest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account_setting, container, false);
        initView();
        return view;
    }

    public void initView(){
        sb_NAME = view.findViewById(R.id.sb_NAME);
        sb_contactNumber = view.findViewById(R.id.sb_contactNumber);
        sb_email = view.findViewById(R.id.sb_email);
        sb_dob = view.findViewById(R.id.sb_dob);
        sb_Occupation = view.findViewById(R.id.sb_Occupation);
        sb_CompanyName = view.findViewById(R.id.sb_CompanyName);
        sb_SOCIAL_DETAILS = view.findViewById(R.id.sb_SOCIAL_DETAILS);
        sb_edu_deatails = view.findViewById(R.id.sb_edu_deatails);
        sb_interest = view.findViewById(R.id.sb_interest);
        btn_Acc_setting_update = view.findViewById(R.id.btn_Acc_setting_update);
        String mUserId = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");
        getacc(mUserId);

        sb_NAME.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_NAME.isChecked()){
                    mName = 1;
                }
                else {
                    mName = 0;
                }
            }
        });
        sb_contactNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_contactNumber.isChecked()){
                    mNumber = 1;
                }
                else {
                    mNumber = 0;
                }
            }
        });
        sb_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_email.isChecked()){
                    mEmail = 1;
                }
                else {
                    mEmail = 0;
                }
            }
        });
        sb_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_dob.isChecked()){
                    mDOB = 1;
                }
                else {
                    mDOB = 0;
                }
            }
        });
        sb_Occupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_Occupation.isChecked()){
                    mOccupation = 1;
                }
                else {
                    mOccupation = 0;
                }
            }
        });
        sb_CompanyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_CompanyName.isChecked()){
                    mCompanyName = 1;
                }
                else {
                    mCompanyName = 0;
                }
            }
        });
        sb_SOCIAL_DETAILS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_SOCIAL_DETAILS.isChecked()){
                    mSocialDetail = 1;
                }
                else {
                    mSocialDetail = 0;
                }
            }
        });
        sb_edu_deatails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_edu_deatails.isChecked()){
                    mEduDetails = 1;
                }
                else {
                    mEduDetails = 0;
                }
            }
        });
        sb_interest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sb_interest.isChecked()) {
                    mIntrest = 1;
                } else {
                    mIntrest = 0;
                }
            }
        });

        btn_Acc_setting_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(getActivity(), ""+mName+""+mNumber+""+mEmail+""+mDOB+""+
                        mOccupation+""+mCompanyName+mCompanyName+""+mSocialDetail+""+mEduDetails+""+
                        mIntrest, Toast.LENGTH_LONG).show();*/
                {
                    HttpsTrustManager.allowAllSSL();
                    final ProgressDialog pDialog = new ProgressDialog(getActivity());
                    pDialog.setMessage("Loading...");
                    pDialog.show();
                    String tag_string_req = "string_req";
                    StringRequest strReq = new StringRequest(Request.Method.POST,
                            Config.URL_POST_ACCOUNT_SETTING,
                            new com.android.volley.Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    pDialog.dismiss();
                                    try {
                                        final JSONObject reg_jsonObject = new JSONObject(response);
                                        String res_Status = reg_jsonObject.getString("status");
                                        final String res_Message = reg_jsonObject.getString("message");

                                        if (res_Status.equals("true")) {
                                            Toast.makeText(getActivity(), "success"+res_Message , Toast.LENGTH_LONG).show();

                                        }
                                        else {
                                            Toast.makeText(getActivity(), "Error"+res_Message , Toast.LENGTH_LONG).show();

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pDialog.dismiss();
                            Toast.makeText(getActivity(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", "74");
                            params.put("name", String.valueOf(mName));
                            params.put("contact_number", String.valueOf(mNumber));
                            params.put("email", String.valueOf(mEmail));
                            params.put("date_of_birth", String.valueOf(mDOB));
                            params.put("occupation", String.valueOf(mOccupation));
                            params.put("company_name", String.valueOf(mCompanyName));
                            params.put("social_details", String.valueOf(mSocialDetail));
                            params.put("educaton_details", String.valueOf(mEduDetails));
                            params.put("interest", String.valueOf(mIntrest));

                            return params;
                        }
                    };
                    strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
                }
            }
        });
    }

    private void getacc(final String mUserId)
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GET_ACCOUNT_SETTING,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");

                            if (res_Status.equals("true")) {
                                JSONArray jsonArray = reg_jsonObject.getJSONArray("response");
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String _name = jsonObject.getString("name");
                                String _contactNo = jsonObject.getString("contact_number");
                                String _email = jsonObject.getString("email");
                                String _dob = jsonObject.getString("date_of_birth");
                                String _occupation = jsonObject.getString("occupation");
                                String _companyName = jsonObject.getString("company_name");
                                String _socialDetails = jsonObject.getString("social_details");
                                String _interest = jsonObject.getString("interest");
                                String _educationalDetails = jsonObject.getString("educaton_details");

                                // CHeckbox set status
                                if (_name.equals("1")){
                                    sb_NAME.setChecked(true);
                                }
                                if (_contactNo.equals("1")){
                                    sb_contactNumber.setChecked(true);
                                }
                                if (_email.equals("1")){
                                    sb_email.setChecked(true);
                                }
                                if (_dob.equals("1")){
                                    sb_dob.setChecked(true);
                                }
                                if (_occupation.equals("1")){
                                    sb_Occupation.setChecked(true);
                                }
                                if (_companyName.equals("1")){
                                    sb_CompanyName.setChecked(true);
                                }
                                if (_socialDetails.equals("1")){
                                    sb_SOCIAL_DETAILS.setChecked(true);
                                }
                                if (_interest.equals("1")){
                                    sb_interest.setChecked(true);
                                }
                                if (_educationalDetails.equals("1")){
                                    sb_edu_deatails.setChecked(true);
                                }

                            }
                            else {
                                Toast.makeText(getActivity(), "Error" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getActivity(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "74");
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
