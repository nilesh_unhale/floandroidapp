package com.flo.floapp.MyNotification;

public class PojoNotification {
    String _Id;
    String _NoticationMSG;

    public PojoNotification(String _Id, String _NoticationMSG) {
        this._Id = _Id;
        this._NoticationMSG = _NoticationMSG;
    }

    public String get_Id() {
        return _Id;
    }

    public void set_Id(String _Id) {
        this._Id = _Id;
    }

    public String get_NoticationMSG() {
        return _NoticationMSG;
    }

    public void set_NoticationMSG(String _NoticationMSG) {
        this._NoticationMSG = _NoticationMSG;
    }
}
