package com.flo.floapp.MyNotification;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.flo.floapp.R;
import java.util.ArrayList;

public class AdapterNotification extends RecyclerView.Adapter<AdapterNotification.PreHolder>   {
    Context _ConText;
    ArrayList<PojoNotification> preList;


    public AdapterNotification(Context _ConText, ArrayList<PojoNotification> preList){
        this._ConText = _ConText;
        this.preList = preList;
    }

    @Override
    public AdapterNotification.PreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(_ConText).inflate(R.layout.single_notification,parent, false);
        AdapterNotification.PreHolder rcv = new AdapterNotification.PreHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final AdapterNotification.PreHolder holder, final int position) {
        final PojoNotification pojoNotification = preList.get(position);
        holder.textView_name.setText(pojoNotification.get_NoticationMSG());
    }

    @Override
    public int getItemCount() {
        return this.preList.size();
    }

    public class PreHolder extends RecyclerView.ViewHolder {
        TextView textView_name;

        public PreHolder(View itemView) {
            super(itemView);
            textView_name = itemView.findViewById(R.id.tv_notifcnText);
            //Typeface myCustomFontBold = Typeface.createFromAsset(_ConText.getAssets(), "fonts/Raleway-Medium.ttf");
            Typeface myCustomFontRegular = Typeface.createFromAsset(_ConText.getAssets(), "fonts/Raleway-Light.ttf");
            textView_name.setTypeface(myCustomFontRegular);
        }
    }




}
