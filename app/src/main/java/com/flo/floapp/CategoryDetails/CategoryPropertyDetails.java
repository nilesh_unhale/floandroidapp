package com.flo.floapp.CategoryDetails;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flo.floapp.BookingDetails.EstimateBooking.EstimateBookingActivity;
import com.flo.floapp.CategoryList.AmenitiesAdapter;
import com.flo.floapp.CategoryList.GridItemView;
import com.flo.floapp.CategoryList.PojoBookingDetails;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.NearByPlaces.ActivityNearBy;
import com.flo.floapp.Other.DownloadTask;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;
import com.flo.floapp.VirtualTour.ActivityVirtaulTour;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

public class CategoryPropertyDetails extends AppCompatActivity implements OnMapReadyCallback {

    FontChangeCrawler fontChanger;
    TextView text_titleCategoryName, text_titleNEIGHBORHOOD,text_onMap,textView_virtualTour,
            textview_FactFile, text_more, text_moreRules, text_less, text_lessRules, tv_moreAmenities, tv_lessAmenities;
    HtmlTextView textView_DetailsMatter, textView_RulesMatter;

    Fragment map;
    Button btn_book_catagory;
    ImageView iv_catImage;
    PojoBookingDetails categoryDetails;
    RecyclerView rv_viewAmenities;
    NetworkDetactor networkDetactor;
    AdapterAmenity adapterAmenity;
    private static final int REQUEST_WRITE_STORAGE= 1;
    List<String> imagelist_sub = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_category_property_details);
        ActivityCompat.requestPermissions(CategoryPropertyDetails.this , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);

        Intent intent = getIntent();
        categoryDetails = (PojoBookingDetails)intent.getSerializableExtra("categorydetails");

        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
        initUI();

    }
    public void initUI(){
        textView_virtualTour = findViewById(R.id.textView_virtualTour);
        textview_FactFile = findViewById(R.id.textview_FactFile);
        text_titleCategoryName = findViewById(R.id.text_titleCategoryName);
        iv_catImage = findViewById(R.id.iv_cat);
        textView_DetailsMatter = findViewById(R.id.textView_DetailsMatter);
        textView_RulesMatter = findViewById(R.id.textView_RulesMatter);
        text_titleNEIGHBORHOOD = findViewById(R.id.text_titleNEIGHBORHOOD);
        text_onMap = findViewById(R.id.text_onMap);
        btn_book_catagory = findViewById(R.id.btn_book_catagory);
        text_more = findViewById(R.id.text_more);
        text_less = findViewById(R.id.text_less);
        text_moreRules = findViewById(R.id.text_moreRules);
        text_lessRules = findViewById(R.id.text_lessRules);
        rv_viewAmenities = findViewById(R.id.rv_viewAmenities);
        tv_moreAmenities = findViewById(R.id.tv_moreAmenities);
        tv_lessAmenities = findViewById(R.id.tv_lessAmenities);
        GridLayoutManager manager = new GridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false);
        rv_viewAmenities.setLayoutManager(manager);
        rv_viewAmenities.setNestedScrollingEnabled(true);
        rv_viewAmenities.setHasFixedSize(true);


        networkDetactor = new NetworkDetactor(CategoryPropertyDetails.this);

        textview_FactFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DownloadTask(CategoryPropertyDetails.this, Config.URL_PDF+categoryDetails.getmFactFile());
            }
        });
        textView_virtualTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(CategoryPropertyDetails.this, ActivityVirtaulTour.class);
                intent.putExtra("virtualtour",categoryDetails.getmVirtualTourVdo());
                startActivity(intent);*/
                Intent virtualTour = new Intent(Intent.ACTION_VIEW);
                virtualTour.setData(Uri.parse("https://www.youtube.com/watch?v=kbG5d7TO7I0"));
                startActivity(virtualTour);
            }
        });

        ImageButton imageButtonBack = findViewById(R.id.imageButtonBack);
        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ArrayList<String> allImgs=new ArrayList<>();
        allImgs = categoryDetails.getmCategoryImages();
        for (int i=0; i<allImgs.size(); i++) {

            Glide.with(CategoryPropertyDetails.this)
                    .load(Config.URL_IMAGE + allImgs.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_catImage);
        }

        text_titleCategoryName.setText(Html.fromHtml(categoryDetails.getmCategoryName()));
        textView_DetailsMatter.setText(Html.fromHtml(categoryDetails.getmProp_rules()));
        textView_RulesMatter.setText(Html.fromHtml(categoryDetails.getmProp_details()));
        text_titleCategoryName.setText(Html.fromHtml(categoryDetails.getmCategoryName()));
        text_titleCategoryName.setText(Html.fromHtml(categoryDetails.getmCategoryName()));

        /* categoryDetails.getAmenitiesIMG();*/
        // Toast.makeText(this, "AmeNITIES" + categoryDetails.getmCategoryAmenitiName()+" Id :"+categoryDetails.getmAmenitiesId(), Toast.LENGTH_SHORT).show();

        text_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_more.setVisibility(View.GONE);
                text_less.setVisibility(View.VISIBLE);
                textView_DetailsMatter.setMaxLines(Integer.MAX_VALUE);
            }
        });
        text_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_less.setVisibility(View.GONE);
                text_more.setVisibility(View.VISIBLE);
                textView_DetailsMatter.setMaxLines(5);
            }
        });
        text_moreRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_moreRules.setVisibility(View.GONE);
                text_lessRules.setVisibility(View.VISIBLE);
                textView_RulesMatter.setMaxLines(Integer.MAX_VALUE);
            }
        });
        text_lessRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_lessRules.setVisibility(View.GONE);
                text_moreRules.setVisibility(View.VISIBLE);
                textView_RulesMatter.setMaxLines(5);
            }
        });

        text_titleNEIGHBORHOOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryPropertyDetails.this, ActivityNearBy.class);
                startActivity(intent);
            }
        });

        text_onMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryPropertyDetails.this, ActivityNearBy.class);
                startActivity(intent);
            }
        });

        ArrayList<String> amenitiesImage=new ArrayList<>();
        final ArrayList<String> imagelist = new ArrayList<>();

        amenitiesImage = categoryDetails.getAi();

        for (int i=0; i<amenitiesImage.size(); i++) {

            imagelist.add(amenitiesImage.get(i));

        }

        imagelist_sub = imagelist.subList(0,8);
        adapterAmenity = new AdapterAmenity(CategoryPropertyDetails.this, imagelist_sub);
        rv_viewAmenities.setAdapter(adapterAmenity);

        tv_moreAmenities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterAmenity = new AdapterAmenity(CategoryPropertyDetails.this, imagelist);
                rv_viewAmenities.setAdapter(adapterAmenity);
                tv_lessAmenities.setVisibility(View.VISIBLE);
                tv_moreAmenities.setVisibility(View.GONE);
            }
        });
        tv_lessAmenities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagelist_sub = imagelist.subList(0,8);
                adapterAmenity = new AdapterAmenity(CategoryPropertyDetails.this, imagelist_sub);
                rv_viewAmenities.setAdapter(adapterAmenity);
                tv_lessAmenities.setVisibility(View.GONE);
                tv_moreAmenities.setVisibility(View.VISIBLE);
            }
        });

       /* final AmenitiesAdapter adapter = new AmenitiesAdapter(numbers, CategoryPropertyDetails.this,imagelist);
        gridView.setAdapter(adapter);
*/
        /*gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                int selectedIndex = adapter.selectedPositions.indexOf(position);
                if (selectedIndex > -1) {
                    adapter.selectedPositions.remove(selectedIndex);
                    ((GridItemView) v).display(false);
                    selectedStrings.remove((String) parent.getItemAtPosition(position));
                } else {
                    adapter.selectedPositions.add(position);
                    ((GridItemView) v).display(true);
                    selectedStrings.add((String) parent.getItemAtPosition(position));
                }
            }
        });*/


        btn_book_catagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    Intent intent = new Intent(CategoryPropertyDetails.this, EstimateBookingActivity.class);
                    intent.putExtra("categorydetails", categoryDetails);
                    startActivity(intent);
                } else {
                    Toast.makeText(CategoryPropertyDetails.this, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) CategoryPropertyDetails.this.getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(18.5476, 73.9408);
        googleMap.addMarker(new MarkerOptions().position(sydney)
                .title("EON WATER FRONT PHASE 2 "));
        //  googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney,15));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
    }
}
