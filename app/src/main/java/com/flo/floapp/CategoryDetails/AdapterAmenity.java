package com.flo.floapp.CategoryDetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flo.floapp.CategoryList.PojoBookingDetails;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import java.util.ArrayList;
import java.util.List;

public class AdapterAmenity extends RecyclerView.Adapter<AdapterAmenity.ViewHolder>{
    Context context;
    List<String> list;
    FontChangeCrawler fontChanger;

    public AdapterAmenity(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public AdapterAmenity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_amenities, null);
        fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)layoutView);

        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(AdapterAmenity.ViewHolder holder, int position) {
        String amenity = list.get(position);
        holder.tv_amenity_name.setText(amenity);
        Glide.with(context).load("https://flocolive.com/stage1/public/uploads/amenity/"+amenity).into(holder.iv_amenityImage);
        //Toast.makeText(context, ""+amenity, Toast.LENGTH_SHORT).show();
        //Glide.with(context).load(amenity).into(holder.iv_amenityImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_amenityImage;
        TextView tv_amenity_name;
        public ViewHolder(View itemView) {
            super(itemView);
            iv_amenityImage = itemView.findViewById(R.id.iv_amenityImage);
            tv_amenity_name = itemView.findViewById(R.id.tv_amenity_name);
        }
    }
}
