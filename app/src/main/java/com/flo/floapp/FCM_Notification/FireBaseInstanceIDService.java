package com.flo.floapp.FCM_Notification;

import android.annotation.SuppressLint;
import android.util.Log;

import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FireBaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String Tag = "FireBaseInstanceIDService";

    @SuppressLint("LongLogTag")
    @Override
    public void onTokenRefresh() {

        //For registration of token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        PreferenceUtil.writeString(getApplicationContext(), PreferenceStrings.FCM_TOKEN,refreshedToken);

        //To displaying token on logcat
        Log.d(Tag,"TOKEN: "+ refreshedToken);

    }

}