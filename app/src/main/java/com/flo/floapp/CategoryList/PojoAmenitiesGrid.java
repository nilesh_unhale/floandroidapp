package com.flo.floapp.CategoryList;

public class PojoAmenitiesGrid {

    String image;
    String title_amenities;

    public PojoAmenitiesGrid(String image, String title_amenities) {
        this.image = image;
        this.title_amenities = title_amenities;
    }

    public PojoAmenitiesGrid() {

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle_amenities() {
        return title_amenities;
    }

    public void setTitle_amenities(String title_amenities) {
        this.title_amenities = title_amenities;
    }
}
