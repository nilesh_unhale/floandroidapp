package com.flo.floapp.CategoryList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flo.floapp.BookingDetails.EstimateBooking.EstimateBookingActivity;
import com.flo.floapp.CategoryDetails.CategoryPropertyDetails;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.Other.NetworkDetactor;
import com.flo.floapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterCategoryList extends RecyclerView.Adapter<AdapterCategoryList.CatListHolderViewHolder> {

    private ArrayList<PojoBookingDetails> arraylist;
    private List<PojoBookingDetails> listPojos;
    FontChangeCrawler fontChanger;
    NetworkDetactor networkDetactor;
    private Context context;

    public AdapterCategoryList(List<PojoBookingDetails> listPojos, Context context) {
        this.context = context;
        this.listPojos = listPojos;
        arraylist = new ArrayList<PojoBookingDetails>();
        arraylist.addAll(listPojos);
    }


    @Override
    public AdapterCategoryList.CatListHolderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_category_property, null);
        fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)layoutView);
        AdapterCategoryList.CatListHolderViewHolder rcv = new AdapterCategoryList.CatListHolderViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterCategoryList.CatListHolderViewHolder holder, int position) {
        final PojoBookingDetails _cList = listPojos.get(position);
         holder.tv_catName.setText(_cList.getmCategoryName());

         if (_cList.getmCat_chargesharedperday().matches("0.00")){
             holder.tv_perDayChaarge.setText("INR "+_cList.getmCat_Chargesingleperday()+" PER DAY");
             holder.tv_perMonthChaarge.setText("INR "+_cList.getmCat_chargeSinglepermonth()+" PER MONTH");
             holder.tv_singleicon.setVisibility(View.VISIBLE);
         }
         else {
             holder.tv_perDayChaarge.setText("INR "+_cList.getmCat_chargesharedperday()+" PER DAY");
             holder.tv_perMonthChaarge.setText("INR "+_cList.getmCat_ChargeSharedpermonth()+" PER MONTH");
             holder.tv_sharedicon.setVisibility(View.VISIBLE);
         }

        ArrayList<String> allImgs=new ArrayList<>();
        allImgs = _cList.getmCategoryImages();
        for (int i=0; i<allImgs.size(); i++) {

            Glide.with(this.context)
                    .load(Config.URL_IMAGE + allImgs.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_property_thumbnail)
                    .into(holder.imageView_catImg);

        }

        //--btn click ------
        networkDetactor = new NetworkDetactor(context);
        holder.btn_Book_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()){
                    if (_cList.getmCat_chargesharedperday().matches("0.00")){
                        Intent intent = new Intent(context, EstimateBookingActivity.class);
                        intent.putExtra("tabNo",0);
                        intent.putExtra("categorydetails",_cList);
                        context.startActivity(intent);
                       // ((Activity)context).finish();
                    }else {
                        Intent intent = new Intent(context, EstimateBookingActivity.class);
                        intent.putExtra("tabNo",1);
                        intent.putExtra("categorydetails",_cList);
                        context.startActivity(intent);
                       // ((Activity)context).finish();
                    }


                }else {
                    Toast.makeText(context, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.tv_details_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkDetactor.isConnected()) {
                    Intent intent = new Intent(context, CategoryPropertyDetails.class);
                    intent.putExtra("categorydetails", _cList);
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }
                else {
                    Toast.makeText(context, "No Internet Connection..", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPojos.size();
    }

    public class CatListHolderViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView_catImg;
        TextView tv_catName,tv_perDayChaarge,tv_perMonthChaarge,tv_details_category,
                tv_singleicon, tv_sharedicon;
        Button btn_Book_category;

        public CatListHolderViewHolder(View itemView) {
            super(itemView);

            imageView_catImg = itemView.findViewById(R.id.image_Category);
            tv_catName = itemView.findViewById(R.id.tv_catName);
            tv_perDayChaarge = itemView.findViewById(R.id.tv_chargesSingleCategory);
            tv_perMonthChaarge = itemView.findViewById(R.id.tv_chargesShareCategory);
            btn_Book_category = itemView.findViewById(R.id.btn_bookCategory);
            tv_details_category = itemView.findViewById(R.id.textview_details_Category);
            tv_singleicon = itemView.findViewById(R.id.tv_singlePropertyListCategory);
            tv_sharedicon = itemView.findViewById(R.id.tv_sharedPropertyListCategory);

        }
    }
}
