package com.flo.floapp.CategoryList;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class AmenitiesAdapter extends BaseAdapter {

    private Activity activity;
    private String[] strings;
    public List selectedPositions;
    private ArrayList<Integer> imagelist;

    public AmenitiesAdapter(String[] strings, Activity activity, ArrayList<Integer> imagelist) {
        this.strings = strings;
        this.activity = activity;
        selectedPositions = new ArrayList<>();
        this.imagelist = imagelist;
    }

    @Override
    public int getCount() {
        return strings.length;
    }

    @Override
    public Object getItem(int position) {
        return strings[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridItemView customView = (convertView == null) ? new GridItemView(activity) : (GridItemView) convertView;
        customView.display(strings[position], selectedPositions.contains(position), imagelist.get(position));

        return customView;
    }

}
