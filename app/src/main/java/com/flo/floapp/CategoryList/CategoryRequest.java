package com.flo.floapp.CategoryList;

import com.google.gson.annotations.SerializedName;

public class CategoryRequest {

    @SerializedName("id")
    private String _PropertyId;

    public CategoryRequest() {
    }

    public CategoryRequest(String _PropertyId) {
        this._PropertyId = _PropertyId;
    }

    public String get_PropertyId() {
        return _PropertyId;
    }

    public void set_PropertyId(String _PropertyId) {
        this._PropertyId = _PropertyId;
    }
}
