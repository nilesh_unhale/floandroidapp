package com.flo.floapp.CategoryList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.MyPreferences.AdapterPreference;
import com.flo.floapp.MyPreferences.PreferenceRequestModel;
import com.flo.floapp.MyPreferences.PreferenceResponceModel;
import com.flo.floapp.PropertyDetails.PropertyDetails;
import com.flo.floapp.PropertyList.PropertyListFragment;
import com.flo.floapp.R;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SearchProperty.PojoPropertyDetails;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingDetails extends AppCompatActivity {
    SliderLayout sliderLayout;
    AppBarLayout appBarLayout;
    String propertyId;
    public final List<Fragment> mFragmentList = new ArrayList<>();
    public final List<String> mFragmentTitleList = new ArrayList<>();
    public static ArrayList<PojoBookingDetails> mCatDetailsList ;
    public static ArrayList<String> allImages;
    public static ArrayList<String> ai;
    private ViewPagerAdapter adapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    TextView text_titlePropertyName;
    public static int fragmentNumber;
    public static String  mProp_highlights, mProp_details,mProp_rules;


    private APIService services;
    RecyclerView rv_catList;
    AdapterCategoryList adapterCategoryList;
    public static ArrayList<CategoryResponce> cat ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_booking_details);

        Intent intent = getIntent();
        PojoPropertyDetails propertyDetails = (PojoPropertyDetails)intent.getSerializableExtra("objpropertydetails");

        propertyId = propertyDetails.getmPropertyId();
        rv_catList = findViewById(R.id.rc_catList);
        rv_catList.setLayoutManager(new LinearLayoutManager(BookingDetails.this));
        rv_catList.setNestedScrollingEnabled(true);
        rv_catList.setHasFixedSize(true);


        services = ApiUtils.getAPIService();

        ImageButton imageButtonBack = findViewById(R.id.imageButtonBack);
        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        text_titlePropertyName = findViewById(R.id.text_titlePropertyName);
        text_titlePropertyName.setText(propertyDetails.getmPropertyNam());


      /*  appBarLayout  = (AppBarLayout) findViewById(R.id.abar);
        sliderLayout =(SliderLayout)findViewById(R.id.slider_details);

        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/Raleway-Light.ttf");

        DefaultSliderView defaultSliderView = new DefaultSliderView(BookingDetails.this);
        defaultSliderView.image(Config.URL_IMAGE + "gallery5.jpg");
        sliderLayout.addSlider(defaultSliderView);

        viewPager = (ViewPager) findViewById(R.id.pager_bookingDetails);
        tabLayout = (TabLayout)findViewById(R.id.tab_layout_bookingDetails);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(mFragmentList.size());
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
*/
        // getPropertyDetails1();
        getPropertyDetails();


    }

    private void getPropertyDetails1() {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(BookingDetails.this);
        pDialog.setMessage("Please wait ...");
        pDialog.show();
        try{
            CategoryRequest paramsData = new CategoryRequest();
            paramsData.set_PropertyId(propertyId);
            services.categotyGet(paramsData).enqueue(new Callback<CategoryResponce>() {
                @Override
                public void onResponse(Call<CategoryResponce> call, Response<CategoryResponce> response) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),""+response.body().getStatus().toString(),Toast.LENGTH_LONG).show();
                    if (response.body().getStatus().toString().equals("true")){

                        //  ArrayList<CategoryResponce> pCat = new ArrayList();
                        CategoryResponce pCat = response.body();
                        CategoryResponce.Response response1=pCat.getResponse();
                        // CategoryResponce.Response.Category category=response1.getCategories();
                     /*   ArrayList<CategoryResponce.Category> p = new ArrayList();
                        AdapterCategoryList editadapter = new AdapterCategoryList(p,BookingDetails.this);*/
                        // rv_catList.setAdapter(editadapter);
                    }


                }

                @Override
                public void onFailure(Call<CategoryResponce> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void getPropertyDetails() {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(BookingDetails.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GET_PROPERTY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            mCatDetailsList = new ArrayList<PojoBookingDetails>();
                            String mCategoryId, mCategoryName, mCa_Description, mCat_yBrand_id,
                                    mCat_chargeSinglepermonth,mCat_roomDaetails,
                                    mCat_ChargeSharedpermonth, mCat_Chargesingleperday, mCat_chargesharedperday,
                                    mCat_bookingamount, mCat_meta, mCat_metasd, mCat_shortterm, mCat_deposit;
                            String amenityId =null,amenityName = null;
                            String mPropertyImages = null;
                            String imgAmenity = null;
                            ArrayList<String> _Category_Images = null;
                            JSONObject objectImages =null;
                            JSONObject objectCategory = null;
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            Log.d("Booking Responce : ",""+reg_jsonObject);
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONObject objectPropertyDetails = reg_jsonObject.getJSONObject("response");
                                //  for (int i = 0; i < MainArray.length(); i++) {
                                String mPropertyId = objectPropertyDetails.getString("id");
                                String mProp_BrandId = objectPropertyDetails.getString("brand_id");
                                String mProp_Code = objectPropertyDetails.getString("property_code");
                                String mProp_Name = objectPropertyDetails.getString("property_name");
                                String mProp_Address = objectPropertyDetails.getString("address");
                                String mProp_Landmark = objectPropertyDetails.getString("landmark");
                                String mProp_city = objectPropertyDetails.getString("city");
                                String mProp_Zipcode = objectPropertyDetails.getString("zip");
                                String mProp_state = objectPropertyDetails.getString("state");
                                String mProp_country = objectPropertyDetails.getString("country");
                                String mProp_std = objectPropertyDetails.getString("std");
                                String mProp_ContactNumber1 = objectPropertyDetails.getString("tel1");
                                String mProp_ContactNumber2 = objectPropertyDetails.getString("tel2");
                                String mProp_email = objectPropertyDetails.getString("email");
                                String mProp_website = objectPropertyDetails.getString("website");
                                String mProp_countrycode = objectPropertyDetails.getString("countrycode");
                                String mProp_facebook = objectPropertyDetails.getString("facebook");
                                String mProp_twitter = objectPropertyDetails.getString("twitter");
                                String mProp_linkedin = objectPropertyDetails.getString("linkedin");
                                String mProp_google = objectPropertyDetails.getString("google");
                                String mProp_status = objectPropertyDetails.getString("status");
                                String mProp_meta = objectPropertyDetails.getString("meta");
                                String mProp_metad = objectPropertyDetails.getString("metad");
                                String mProp_whatsapp = objectPropertyDetails.getString("whatsapp");
                                mProp_highlights = objectPropertyDetails.getString("highlights");
                                mProp_details = objectPropertyDetails.getString("details");
                                mProp_rules = objectPropertyDetails.getString("rules");
                                String mProp_location = objectPropertyDetails.getString("location");
                                String mProp_cityLat = objectPropertyDetails.getString("cityLat");
                                String mProp_cityLng = objectPropertyDetails.getString("cityLng");
                                String mFactFile = objectPropertyDetails.getString("pdf");
                                String mVirtualTour = objectPropertyDetails.getString("video");

                                JSONArray arrayCategory = objectPropertyDetails.getJSONArray("categories");

                                for (int i = 0; i < arrayCategory.length(); i++){
                                    objectCategory = arrayCategory.getJSONObject(i);
                                    mCategoryId = objectCategory.getString("id");
                                    mCategoryName = objectCategory.getString("name");
                                    mCa_Description = objectCategory.getString("description");
                                    mCat_yBrand_id = objectCategory.getString("brand_id");
                                    mCat_chargeSinglepermonth = objectCategory.getString("singlepermonth");
                                    mCat_ChargeSharedpermonth = objectCategory.getString("sharedpermonth");
                                    mCat_Chargesingleperday = objectCategory.getString("singleperday");
                                    mCat_chargesharedperday = objectCategory.getString("sharedperday");
                                    mCat_bookingamount = objectCategory.getString("bookingamount");
                                    mCat_meta = objectCategory.getString("meta");
                                    mCat_metasd = objectCategory.getString("metad");
                                    mCat_shortterm = objectCategory.getString("shortterm");
                                    mCat_deposit = objectCategory.getString("deposit");
                                    mCat_roomDaetails = objectCategory.getString("roomdetails");
                                    mFragmentTitleList.add(mCategoryName);

                                    JSONArray _ArrayCatImages = objectCategory.getJSONArray("images");
                                    for (int j = 0; j < _ArrayCatImages.length(); j++) {
                                        _Category_Images =new  ArrayList<String>();
                                        JSONObject jsonObject_Amenities = _ArrayCatImages.getJSONObject(j);
                                        _Category_Images.add(jsonObject_Amenities.getString("original_name"));

                                    }
                                    JSONArray jsonArray_Aminities = objectCategory.getJSONArray("amenities");
                                    allImages = new ArrayList<String>();
                                    ai = new ArrayList<String>();
                                    for (int j = 0; j < jsonArray_Aminities.length(); j++) {
                                        JSONObject jsonObject_Amenities = jsonArray_Aminities.getJSONObject(j);
                                        amenityId = jsonObject_Amenities.getString("id");
                                        amenityName = jsonObject_Amenities.getString("name");
                                        imgAmenity = jsonObject_Amenities.getString("mobileimage");
                                        allImages.add(mPropertyImages);
                                        ai.add(imgAmenity);

                                    }

                                    PojoBookingDetails details = new PojoBookingDetails(mPropertyId,mProp_BrandId,mProp_Code,mProp_Name,mProp_Address
                                            ,mProp_cityLat,mProp_cityLng,mProp_Landmark,mProp_Landmark,mPropertyImages,
                                            mCategoryId,mCategoryName,mCa_Description,mCat_yBrand_id,mCat_chargeSinglepermonth,mCat_ChargeSharedpermonth,
                                            mCat_Chargesingleperday,mCat_chargesharedperday,mCat_bookingamount,mCat_meta,mCat_metasd,mCat_shortterm,
                                            mCat_deposit,mProp_highlights,mProp_rules,mProp_details,_Category_Images,amenityId,amenityName,mFactFile,mVirtualTour, allImages,ai);
                                    mCatDetailsList.add(details);

                                    AdapterCategoryList editadapter = new AdapterCategoryList(mCatDetailsList,BookingDetails.this);
                                    rv_catList.setAdapter(editadapter);

                                 /*   for (int m=0; m<allImages.size(); m++) {
                                        DefaultSliderView defaultSliderView = new DefaultSliderView(BookingDetails.this);
                                        defaultSliderView.image(Config.URL_IMAGE + details.getmPropertyImages());
                                        sliderLayout.addSlider(defaultSliderView);
                                    }*/
                                }

                                /*for(int i = 0; i < mFragmentTitleList.size(); i++) {
                                    mFragmentList.add(new FragmentCategory1());

                                }
                                setupViewPager(viewPager);
                                tabLayout.setupWithViewPager(viewPager);
                                viewPager.setOffscreenPageLimit(mFragmentList.size());
                                tabLayout.setupWithViewPager(viewPager);

                                JSONArray arrayImages = objectPropertyDetails.getJSONArray("allimages");

                                    for (int j = 0; j < arrayImages.length(); j++) {

                                        objectImages = arrayImages.getJSONObject(j);
                                        mPropertyImages = objectImages.getString("original_name");
                                        Log.d("mPropertyImages :",mPropertyImages);

                                    }*/

                                //  }

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "OOPS!! Server Not Responding" , Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", propertyId);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void setupViewPager(final ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), mFragmentList, mFragmentTitleList);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragmentNumber=position;
                //  fragmentNumber=position;
                FragmentCategory1.refreshFragment();
           /*     FragmentCategory1 comP1 = (FragmentCategory1) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
               // comP1.refreshpage();
                viewPager.setCurrentItem(position);
                viewPager.getAdapter().notifyDataSetChanged();*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragmentList = new ArrayList<>();
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String> titleLists) {
            super(fm);
            this.mFragmentList = fragments;
            this.mFragmentTitleList = titleLists;
        }


        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList == null ? 0 : mFragmentList.size();

        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

/*
        @Override public int getItemPosition(Object object)
        {
            return PagerAdapter.POSITION_NONE;
        }*/

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }}
