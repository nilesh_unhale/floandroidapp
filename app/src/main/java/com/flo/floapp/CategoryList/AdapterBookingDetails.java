package com.flo.floapp.CategoryList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterBookingDetails extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AdapterBookingDetails(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentCategory1 tab1 = new FragmentCategory1();
                return tab1;
            case 1:
                FragmentCategory2 tab2 = new FragmentCategory2();
                return tab2;
            case 2:
                FragmentCategory3 tab3 = new FragmentCategory3();
                return tab3;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
