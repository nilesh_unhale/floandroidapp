package com.flo.floapp.CategoryList;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import com.flo.floapp.BookingDetails.EstimateBooking.EstimateBookingActivity;
import com.flo.floapp.NearByPlaces.ActivityNearBy;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;

public class FragmentCategory1 extends Fragment implements OnMapReadyCallback {
    View view;
    public static TextView textview_BookingAmount_proceedandPay, text_titleAMENITIES,
            tv_highlights, text_titleDETAILS,text_onMap,
            text_titleRULESANDETIQUETTES, text_titleREVIEW, text_titleNEIGHBORHOOD;
    HtmlTextView textViewHighlights, textViewCat_Details, textView_RulesMatter;
    Button btn_book_cat;
    private int mYear, mMonth, mDay,selectedId;
    EditText textview_NoOfGuest;
    String mCatId,mPropertyId,mOccupancy;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static  void refreshFragment(){

        // textViewHighlights.setText(BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_ChargeSharedpermonth());
        //Log.d("Cat_Fra :",BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_chargesharedperday());

        }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category1_boking_details, container, false);
        textViewHighlights = view.findViewById(R.id.textView_HighLightMatter);
        textViewCat_Details = view.findViewById(R.id.textView_DetailsMatter);
        textView_RulesMatter = view.findViewById(R.id.textView_RulesMatter);

        textViewHighlights.setHtml(BookingDetails.mProp_highlights); //BookingDetails.mProp_highlights
        textViewCat_Details.setHtml(BookingDetails.mProp_details);
        textView_RulesMatter.setHtml(BookingDetails.mProp_rules);

        btn_book_cat = view.findViewById(R.id.btn_book_cat);
        text_titleDETAILS = view.findViewById(R.id.text_titleDETAILS);
        text_titleRULESANDETIQUETTES = view.findViewById(R.id.text_titleRULESANDETIQUETTES);
        text_titleREVIEW = view.findViewById(R.id.text_titleREVIEW);
        text_titleNEIGHBORHOOD = view.findViewById(R.id.text_titleNEIGHBORHOOD);
        text_onMap = view.findViewById(R.id.text_onMap);
        //------------Font Applying-------------//
        Typeface myCustomFontBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        Typeface myCustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Light.ttf");
        textViewHighlights.setTypeface(myCustomFont);
        textViewCat_Details.setTypeface(myCustomFont);
        textView_RulesMatter.setTypeface(myCustomFont);
        btn_book_cat.setTypeface(myCustomFont);
        text_titleDETAILS.setTypeface(myCustomFontBold);
        text_titleRULESANDETIQUETTES.setTypeface(myCustomFontBold);
        text_titleREVIEW.setTypeface(myCustomFontBold);
        text_onMap.setTypeface(myCustomFontBold);

        text_titleNEIGHBORHOOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityNearBy.class);
                getActivity().startActivity(intent);
            }
        });

        text_onMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityNearBy.class);
                getActivity().startActivity(intent);
            }
        });

        GridView gridView = view.findViewById(R.id.grid_viewAmenities);
        gridView.setNumColumns(4);
        final  ArrayList<String> selectedStrings= new ArrayList<>();
        final String[] numbers = new String[]{
                "LAUNDRY", "WIFI", "LIBRARY", "SECURITY", "ROOM", "GYM", "MUSIC", "DOCTOR"};
        final ArrayList<Integer> imagelist = new ArrayList<>();
        imagelist.add(R.drawable.laundry);
        imagelist.add(R.drawable.wifi);
        imagelist.add(R.drawable.library);
        imagelist.add(R.drawable.security);
        imagelist.add(R.drawable.room);
        imagelist.add(R.drawable.gym);
        imagelist.add(R.drawable.music);
        imagelist.add(R.drawable.doctor_on_call);

        final AmenitiesAdapter adapter = new AmenitiesAdapter(numbers, getActivity(),imagelist);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                int selectedIndex = adapter.selectedPositions.indexOf(position);
                if (selectedIndex > -1) {
                    adapter.selectedPositions.remove(selectedIndex);
                    ((GridItemView) v).display(false);
                    selectedStrings.remove((String) parent.getItemAtPosition(position));
                } else {
                    adapter.selectedPositions.add(position);
                    ((GridItemView) v).display(true);
                    selectedStrings.add((String) parent.getItemAtPosition(position));
                }
            }
        });

        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CATEGORY_ID,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCategoryId());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CATEGORY_NAME,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCategoryName());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CATEGORY_BRANDID,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_yBrand_id());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.PROPERTY_ID,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCategoryId());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CAHRGE_SINGLE_PER_DAY,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_Chargesingleperday());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CAHRGE_SINGLE_PER_MONTH,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_chargeSinglepermonth());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CAHRGE_SHARE_PER_DAY,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_chargesharedperday());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.CAHRGE_SHARE_PER_MONTH,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_ChargeSharedpermonth());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.BOOKING_ANOUNT,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_bookingamount());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.SHORT_TERM,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_shortterm());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.DEPOSIT,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_deposit());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.PROPERTYID_ESTIMATE,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmPropertyId());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.PROPERTY_CODE,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmPropertyCode());
        PreferenceUtil.writeString(getActivity(), PreferenceStrings.PROPERTY_NAME,BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmPropertyNam());

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btn_book_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent estimateBooking = new Intent(getActivity(), EstimateBookingActivity.class);
                startActivity(estimateBooking);

            }
        });

        return view;
    }

    /*private void dialogEstimate() throws ParseException {
        final Dialog dialogEstimate = new Dialog(getActivity());
        dialogEstimate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEstimate.setContentView(R.layout.dialog_proceed_and_booking);

        ImageButton imageButtonClose = dialogEstimate.findViewById(R.id.dialog_close_proceedAndPay);
        RadioButton rb_single_proceedandPay = dialogEstimate.findViewById(R.id.textview_single_proceedandPay);
        final RadioGroup rg_dialog_book = dialogEstimate.findViewById(R.id.rg_dialog_book);
        RadioButton rb_shared_proceedandPay = dialogEstimate.findViewById(R.id.textview_shared_proceedandPay);
        final EditText textview_StartDate = dialogEstimate.findViewById(R.id.textview_StartDate_proceedandPay);
        final EditText textview_EndDate = dialogEstimate.findViewById(R.id.textview_End_proceedandPay);
        textview_NoOfGuest = dialogEstimate.findViewById(R.id.textview_NoOfGuest);
        textview_BookingAmount_proceedandPay = dialogEstimate.findViewById(R.id.textview_BookingAmount_proceedandPay);
        Button button_dialogProceedandBook = dialogEstimate.findViewById(R.id.button_dialogProceedandBook);


        *//*textview_NoOfGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGuestDialog();
            }
        });*//*
        rb_single_proceedandPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedId = rg_dialog_book.getCheckedRadioButtonId();
                mOccupancy = "0";
            }
        });

        textview_NoOfGuest.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //doSomething();
                if (!textview_StartDate.getText().toString().equals("") &&
                        !textview_EndDate.getText().toString().equals("") &&
                        !textview_NoOfGuest.getText().toString().equals("")){

                    String daysDiff=getCountOfDays(textview_StartDate.getText().toString(),textview_EndDate.getText().toString());
                    int piasa=Integer.parseInt(daysDiff.replaceAll("[\\D]",""))*Integer.parseInt(s.toString().replaceAll("[\\D]",""));
                    Log.d("Totol",+ piasa + " Count");
                    textview_BookingAmount_proceedandPay.setText(""+piasa*Integer.parseInt(BookingDetails.mCatDetailsList.get(BookingDetails.fragmentNumber).getmCat_Chargesingleperday())+" Rs");
                    Date sd,ed;
                  *//* sd = new Date(textview_StartDate.getText().toString());
                   ed = new Date(textview_EndDate.getText().toString());
                   long diff = ed.getTime() - sd.getTime();
                   long seconds = diff / 1000;
                   long minutes = seconds / 60;
                   long hours = minutes / 60;
                   long days = (hours / 24) + 1;
                   Log.d("days", "" + days);
                  Log.d("Days: " ,""+ TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
*//*
                }else {
                    Toast.makeText(getActivity(), "Please Select All fieds ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rb_shared_proceedandPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedId = rg_dialog_book.getCheckedRadioButtonId();
                mOccupancy = "1";

            }
        });
        textview_EndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                textview_EndDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


 //--------final book click ---------------
        button_dialogProceedandBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (!textview_StartDate.getText().toString().equals("") &&
                       !textview_EndDate.getText().toString().equals("") &&
                       !textview_NoOfGuest.getText().toString().equals("")){

                   getCountOfDays(textview_StartDate.getText().toString(),textview_EndDate.getText().toString());
                    Date sd,ed;
                  *//* sd = new Date(textview_StartDate.getText().toString());
                   ed = new Date(textview_EndDate.getText().toString());
                   long diff = ed.getTime() - sd.getTime();
                   long seconds = diff / 1000;
                   long minutes = seconds / 60;
                   long hours = minutes / 60;
                   long days = (hours / 24) + 1;
                   Log.d("days", "" + days);
                  Log.d("Days: " ,""+ TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
*//*
               }else {
                   Toast.makeText(getActivity(), "Please Select All fieds ", Toast.LENGTH_SHORT).show();
               }
            }
        });
 //-----------------------------------------
        rb_single_proceedandPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        imageButtonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogEstimate.dismiss();
            }
        });
        dialogEstimate.show();
    }*/


   /* public String getCountOfDays(String createdDateString, String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
        Log.d("xxxx::",+ (int) dayCount + " Days");
        return String.valueOf(( (int) dayCount));
    }*/


    /*private void showAlertDialog(String res_message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(res_message);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }*/

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(18.5204, 73.8567);
        googleMap.addMarker(new MarkerOptions().position(sydney)
                .title("Marker in Pune"));
      //  googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney,15));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));

    }
}
