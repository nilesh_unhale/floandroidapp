package com.flo.floapp.CategoryList;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flo.floapp.R;

public class GridItemView extends FrameLayout {

    private TextView textView;
    private ImageView imageview_amenities;
    private LinearLayout linear_layoutAmenities;

    public GridItemView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.single_grid_amenities, this);
        textView = (TextView) getRootView().findViewById(R.id.textView_amenitiesTitle);
        imageview_amenities = (ImageView) getRootView().findViewById(R.id.imageview_amenities);
        linear_layoutAmenities = getRootView().findViewById(R.id.linear_layoutAmenities);
    }

    public void display(String text, boolean isSelected, int imageid) {
        textView.setText(text);
        imageview_amenities.setImageResource(imageid);
        display(isSelected);
    }

    public void display(boolean isSelected) {
        linear_layoutAmenities.setBackgroundColor(isSelected ? getResources().getColor(R.color.color_GrayTransperent) : getResources().getColor(R.color.color_white));
    }
}