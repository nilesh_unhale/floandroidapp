package com.flo.floapp.Profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class UpdateProfileResponce {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private Response response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }



    public class Response {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first")
        @Expose
        private String first;
        @SerializedName("salutation")
        @Expose
        private Object salutation;
        @SerializedName("last")
        @Expose
        private String last;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userimage")
        @Expose
        private String userimage;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("gst")
        @Expose
        private Object gst;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("photoid_verified")
        @Expose
        private Object photoidVerified;
        @SerializedName("sameasphotoid")
        @Expose
        private Object sameasphotoid;
        @SerializedName("addressid_verified")
        @Expose
        private String addressidVerified;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("mobile_otp")
        @Expose
        private String mobileOtp;
        @SerializedName("mobile_verified")
        @Expose
        private String mobileVerified;
        @SerializedName("is_active")
        @Expose
        private String isActive;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("logo")
        @Expose
        private Object logo;
        @SerializedName("occupation")
        @Expose
        private String occupation;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("photoid_type")
        @Expose
        private Object photoidType;
        @SerializedName("photoid")
        @Expose
        private Object photoid;
        @SerializedName("photoid_image")
        @Expose
        private Object photoidImage;
        @SerializedName("addressid_type")
        @Expose
        private Object addressidType;
        @SerializedName("addressid")
        @Expose
        private Object addressid;
        @SerializedName("addressid_image")
        @Expose
        private Object addressidImage;
        @SerializedName("telephone")
        @Expose
        private Object telephone;
        @SerializedName("website")
        @Expose
        private Object website;
        @SerializedName("gst_no")
        @Expose
        private String gstNo;
        @SerializedName("nationality")
        @Expose
        private String nationality;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("guestcategory")
        @Expose
        private Object guestcategory;
        @SerializedName("vip")
        @Expose
        private String vip;
        @SerializedName("countrycode")
        @Expose
        private Object countrycode;
        @SerializedName("cform")
        @Expose
        private Object cform;
        @SerializedName("ice_name")
        @Expose
        private String iceName;
        @SerializedName("ice_no")
        @Expose
        private String iceNo;
        @SerializedName("facebook")
        @Expose
        private String facebook;
        @SerializedName("twitter")
        @Expose
        private String twitter;
        @SerializedName("linkedin")
        @Expose
        private String linkedin;
        @SerializedName("highestEducation")
        @Expose
        private String highestEducation;
        @SerializedName("joiningDate")
        @Expose
        private String joiningDate;
        @SerializedName("companyPincode")
        @Expose
        private String companyPincode;
        @SerializedName("companyCountry")
        @Expose
        private String companyCountry;
        @SerializedName("companyState")
        @Expose
        private String companyState;
        @SerializedName("companyCity")
        @Expose
        private String companyCity;
        @SerializedName("education")
        @Expose
        private Object education;
        @SerializedName("interest")
        @Expose
        private String interest;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("email_token")
        @Expose
        private String emailToken;
        @SerializedName("company_name")
        @Expose
        private String companyName;
        @SerializedName("cmpaddress")
        @Expose
        private String cmpaddress;
        @SerializedName("email_verified")
        @Expose
        private Object emailVerified;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("age")
        @Expose
        private Object age;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("phone")
        @Expose
        private Object phone;
        @SerializedName("passport")
        @Expose
        private Object passport;
        @SerializedName("passport_place")
        @Expose
        private Object passportPlace;
        @SerializedName("passport_date")
        @Expose
        private Object passportDate;
        @SerializedName("passport_expiry")
        @Expose
        private Object passportExpiry;
        @SerializedName("visa")
        @Expose
        private Object visa;
        @SerializedName("visa_from")
        @Expose
        private Object visaFrom;
        @SerializedName("visa_to")
        @Expose
        private Object visaTo;
        @SerializedName("visa_type")
        @Expose
        private Object visaType;
        @SerializedName("visa_place")
        @Expose
        private Object visaPlace;
        @SerializedName("cmpaddress1")
        @Expose
        private String cmpaddress1;
        @SerializedName("cmplandmark")
        @Expose
        private String cmplandmark;
        @SerializedName("aboutme")
        @Expose
        private String aboutme;
        @SerializedName("cmpgstin")
        @Expose
        private Object cmpgstin;
        @SerializedName("blood_group")
        @Expose
        private Object bloodGroup;
        @SerializedName("fcmtoken")
        @Expose
        private String fcmtoken;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public Object getSalutation() {
            return salutation;
        }

        public void setSalutation(Object salutation) {
            this.salutation = salutation;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public Object getGst() {
            return gst;
        }

        public void setGst(Object gst) {
            this.gst = gst;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public Object getPhotoidVerified() {
            return photoidVerified;
        }

        public void setPhotoidVerified(Object photoidVerified) {
            this.photoidVerified = photoidVerified;
        }

        public Object getSameasphotoid() {
            return sameasphotoid;
        }

        public void setSameasphotoid(Object sameasphotoid) {
            this.sameasphotoid = sameasphotoid;
        }

        public String getAddressidVerified() {
            return addressidVerified;
        }

        public void setAddressidVerified(String addressidVerified) {
            this.addressidVerified = addressidVerified;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getMobileOtp() {
            return mobileOtp;
        }

        public void setMobileOtp(String mobileOtp) {
            this.mobileOtp = mobileOtp;
        }

        public String getMobileVerified() {
            return mobileVerified;
        }

        public void setMobileVerified(String mobileVerified) {
            this.mobileVerified = mobileVerified;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Object getLogo() {
            return logo;
        }

        public void setLogo(Object logo) {
            this.logo = logo;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Object getPhotoidType() {
            return photoidType;
        }

        public void setPhotoidType(Object photoidType) {
            this.photoidType = photoidType;
        }

        public Object getPhotoid() {
            return photoid;
        }

        public void setPhotoid(Object photoid) {
            this.photoid = photoid;
        }

        public Object getPhotoidImage() {
            return photoidImage;
        }

        public void setPhotoidImage(Object photoidImage) {
            this.photoidImage = photoidImage;
        }

        public Object getAddressidType() {
            return addressidType;
        }

        public void setAddressidType(Object addressidType) {
            this.addressidType = addressidType;
        }

        public Object getAddressid() {
            return addressid;
        }

        public void setAddressid(Object addressid) {
            this.addressid = addressid;
        }

        public Object getAddressidImage() {
            return addressidImage;
        }

        public void setAddressidImage(Object addressidImage) {
            this.addressidImage = addressidImage;
        }

        public Object getTelephone() {
            return telephone;
        }

        public void setTelephone(Object telephone) {
            this.telephone = telephone;
        }

        public Object getWebsite() {
            return website;
        }

        public void setWebsite(Object website) {
            this.website = website;
        }

        public String getGstNo() {
            return gstNo;
        }

        public void setGstNo(String gstNo) {
            this.gstNo = gstNo;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getGuestcategory() {
            return guestcategory;
        }

        public void setGuestcategory(Object guestcategory) {
            this.guestcategory = guestcategory;
        }

        public String getVip() {
            return vip;
        }

        public void setVip(String vip) {
            this.vip = vip;
        }

        public Object getCountrycode() {
            return countrycode;
        }

        public void setCountrycode(Object countrycode) {
            this.countrycode = countrycode;
        }

        public Object getCform() {
            return cform;
        }

        public void setCform(Object cform) {
            this.cform = cform;
        }

        public String getIceName() {
            return iceName;
        }

        public void setIceName(String iceName) {
            this.iceName = iceName;
        }

        public String getIceNo() {
            return iceNo;
        }

        public void setIceNo(String iceNo) {
            this.iceNo = iceNo;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getLinkedin() {
            return linkedin;
        }

        public void setLinkedin(String linkedin) {
            this.linkedin = linkedin;
        }

        public String getHighestEducation() {
            return highestEducation;
        }

        public void setHighestEducation(String highestEducation) {
            this.highestEducation = highestEducation;
        }

        public String getJoiningDate() {
            return joiningDate;
        }

        public void setJoiningDate(String joiningDate) {
            this.joiningDate = joiningDate;
        }

        public String getCompanyPincode() {
            return companyPincode;
        }

        public void setCompanyPincode(String companyPincode) {
            this.companyPincode = companyPincode;
        }

        public String getCompanyCountry() {
            return companyCountry;
        }

        public void setCompanyCountry(String companyCountry) {
            this.companyCountry = companyCountry;
        }

        public String getCompanyState() {
            return companyState;
        }

        public void setCompanyState(String companyState) {
            this.companyState = companyState;
        }

        public String getCompanyCity() {
            return companyCity;
        }

        public void setCompanyCity(String companyCity) {
            this.companyCity = companyCity;
        }

        public Object getEducation() {
            return education;
        }

        public void setEducation(Object education) {
            this.education = education;
        }

        public String getInterest() {
            return interest;
        }

        public void setInterest(String interest) {
            this.interest = interest;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getEmailToken() {
            return emailToken;
        }

        public void setEmailToken(String emailToken) {
            this.emailToken = emailToken;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCmpaddress() {
            return cmpaddress;
        }

        public void setCmpaddress(String cmpaddress) {
            this.cmpaddress = cmpaddress;
        }

        public Object getEmailVerified() {
            return emailVerified;
        }

        public void setEmailVerified(Object emailVerified) {
            this.emailVerified = emailVerified;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public Object getAge() {
            return age;
        }

        public void setAge(Object age) {
            this.age = age;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public Object getPhone() {
            return phone;
        }

        public void setPhone(Object phone) {
            this.phone = phone;
        }

        public Object getPassport() {
            return passport;
        }

        public void setPassport(Object passport) {
            this.passport = passport;
        }

        public Object getPassportPlace() {
            return passportPlace;
        }

        public void setPassportPlace(Object passportPlace) {
            this.passportPlace = passportPlace;
        }

        public Object getPassportDate() {
            return passportDate;
        }

        public void setPassportDate(Object passportDate) {
            this.passportDate = passportDate;
        }

        public Object getPassportExpiry() {
            return passportExpiry;
        }

        public void setPassportExpiry(Object passportExpiry) {
            this.passportExpiry = passportExpiry;
        }

        public Object getVisa() {
            return visa;
        }

        public void setVisa(Object visa) {
            this.visa = visa;
        }

        public Object getVisaFrom() {
            return visaFrom;
        }

        public void setVisaFrom(Object visaFrom) {
            this.visaFrom = visaFrom;
        }

        public Object getVisaTo() {
            return visaTo;
        }

        public void setVisaTo(Object visaTo) {
            this.visaTo = visaTo;
        }

        public Object getVisaType() {
            return visaType;
        }

        public void setVisaType(Object visaType) {
            this.visaType = visaType;
        }

        public Object getVisaPlace() {
            return visaPlace;
        }

        public void setVisaPlace(Object visaPlace) {
            this.visaPlace = visaPlace;
        }

        public String getCmpaddress1() {
            return cmpaddress1;
        }

        public void setCmpaddress1(String cmpaddress1) {
            this.cmpaddress1 = cmpaddress1;
        }

        public String getCmplandmark() {
            return cmplandmark;
        }

        public void setCmplandmark(String cmplandmark) {
            this.cmplandmark = cmplandmark;
        }

        public String getAboutme() {
            return aboutme;
        }

        public void setAboutme(String aboutme) {
            this.aboutme = aboutme;
        }

        public Object getCmpgstin() {
            return cmpgstin;
        }

        public void setCmpgstin(Object cmpgstin) {
            this.cmpgstin = cmpgstin;
        }

        public Object getBloodGroup() {
            return bloodGroup;
        }

        public void setBloodGroup(Object bloodGroup) {
            this.bloodGroup = bloodGroup;
        }

        public String getFcmtoken() {
            return fcmtoken;
        }

        public void setFcmtoken(String fcmtoken) {
            this.fcmtoken = fcmtoken;
        }

    }

}
