package com.flo.floapp.Profile;

public class ProfileUpdateRequest {
    String _FirstName;
    String _lastName;
    String _DOB;
    String _EMIL;
    String _NUMBER;
    String _Nationality;
    String _IceName;
    String _IceNumber;
    String _Bloodgroup;
    String _gender;
    String _HighestEDu;
    String _Occupation;
    String _SelfAdrsLine1;
    String _SelfAdrsLine2;
    String _SelfAdrsPIN;
    String _SelfAdrsCity;
    String _SelfAdrsState;
    String _SelfAdrsCountry;
    String _OfcAdrsLine1;
    String _OfcAdrsLine2;
    String _OfcAdrsPIN;
    String _OfcAdrsCity;
    String _OfcAdrsState;
    String _OfcAdrsCountry;
    String _CompanyName;
    String _GstNumber;
    String _About;
    String _Interest;
    String _FbUrl;
    String _TwiterURL;
    String _LInkedInurl;
    String _UserImage;

    public String get_FirstName() {
        return _FirstName;
    }

    public void set_FirstName(String _FirstName) {
        this._FirstName = _FirstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_DOB() {
        return _DOB;
    }

    public void set_DOB(String _DOB) {
        this._DOB = _DOB;
    }

    public String get_EMIL() {
        return _EMIL;
    }

    public void set_EMIL(String _EMIL) {
        this._EMIL = _EMIL;
    }

    public String get_NUMBER() {
        return _NUMBER;
    }

    public void set_NUMBER(String _NUMBER) {
        this._NUMBER = _NUMBER;
    }

    public String get_Nationality() {
        return _Nationality;
    }

    public void set_Nationality(String _Nationality) {
        this._Nationality = _Nationality;
    }

    public String get_IceName() {
        return _IceName;
    }

    public void set_IceName(String _IceName) {
        this._IceName = _IceName;
    }

    public String get_IceNumber() {
        return _IceNumber;
    }

    public void set_IceNumber(String _IceNumber) {
        this._IceNumber = _IceNumber;
    }

    public String get_Bloodgroup() {
        return _Bloodgroup;
    }

    public void set_Bloodgroup(String _Bloodgroup) {
        this._Bloodgroup = _Bloodgroup;
    }

    public String get_gender() {
        return _gender;
    }

    public void set_gender(String _gender) {
        this._gender = _gender;
    }

    public String get_HighestEDu() {
        return _HighestEDu;
    }

    public void set_HighestEDu(String _HighestEDu) {
        this._HighestEDu = _HighestEDu;
    }

    public String get_Occupation() {
        return _Occupation;
    }

    public void set_Occupation(String _Occupation) {
        this._Occupation = _Occupation;
    }

    public String get_SelfAdrsLine1() {
        return _SelfAdrsLine1;
    }

    public void set_SelfAdrsLine1(String _SelfAdrsLine1) {
        this._SelfAdrsLine1 = _SelfAdrsLine1;
    }

    public String get_SelfAdrsLine2() {
        return _SelfAdrsLine2;
    }

    public void set_SelfAdrsLine2(String _SelfAdrsLine2) {
        this._SelfAdrsLine2 = _SelfAdrsLine2;
    }

    public String get_SelfAdrsPIN() {
        return _SelfAdrsPIN;
    }

    public void set_SelfAdrsPIN(String _SelfAdrsPIN) {
        this._SelfAdrsPIN = _SelfAdrsPIN;
    }

    public String get_SelfAdrsCity() {
        return _SelfAdrsCity;
    }

    public void set_SelfAdrsCity(String _SelfAdrsCity) {
        this._SelfAdrsCity = _SelfAdrsCity;
    }

    public String get_SelfAdrsState() {
        return _SelfAdrsState;
    }

    public void set_SelfAdrsState(String _SelfAdrsState) {
        this._SelfAdrsState = _SelfAdrsState;
    }

    public String get_SelfAdrsCountry() {
        return _SelfAdrsCountry;
    }

    public void set_SelfAdrsCountry(String _SelfAdrsCountry) {
        this._SelfAdrsCountry = _SelfAdrsCountry;
    }

    public String get_OfcAdrsLine1() {
        return _OfcAdrsLine1;
    }

    public void set_OfcAdrsLine1(String _OfcAdrsLine1) {
        this._OfcAdrsLine1 = _OfcAdrsLine1;
    }

    public String get_OfcAdrsLine2() {
        return _OfcAdrsLine2;
    }

    public void set_OfcAdrsLine2(String _OfcAdrsLine2) {
        this._OfcAdrsLine2 = _OfcAdrsLine2;
    }

    public String get_OfcAdrsPIN() {
        return _OfcAdrsPIN;
    }

    public void set_OfcAdrsPIN(String _OfcAdrsPIN) {
        this._OfcAdrsPIN = _OfcAdrsPIN;
    }

    public String get_OfcAdrsCity() {
        return _OfcAdrsCity;
    }

    public void set_OfcAdrsCity(String _OfcAdrsCity) {
        this._OfcAdrsCity = _OfcAdrsCity;
    }

    public String get_OfcAdrsState() {
        return _OfcAdrsState;
    }

    public void set_OfcAdrsState(String _OfcAdrsState) {
        this._OfcAdrsState = _OfcAdrsState;
    }

    public String get_OfcAdrsCountry() {
        return _OfcAdrsCountry;
    }

    public void set_OfcAdrsCountry(String _OfcAdrsCountry) {
        this._OfcAdrsCountry = _OfcAdrsCountry;
    }

    public String get_CompanyName() {
        return _CompanyName;
    }

    public void set_CompanyName(String _CompanyName) {
        this._CompanyName = _CompanyName;
    }

    public String get_GstNumber() {
        return _GstNumber;
    }

    public void set_GstNumber(String _GstNumber) {
        this._GstNumber = _GstNumber;
    }

    public String get_About() {
        return _About;
    }

    public void set_About(String _About) {
        this._About = _About;
    }

    public String get_Interest() {
        return _Interest;
    }

    public void set_Interest(String _Interest) {
        this._Interest = _Interest;
    }

    public String get_FbUrl() {
        return _FbUrl;
    }

    public void set_FbUrl(String _FbUrl) {
        this._FbUrl = _FbUrl;
    }

    public String get_TwiterURL() {
        return _TwiterURL;
    }

    public void set_TwiterURL(String _TwiterURL) {
        this._TwiterURL = _TwiterURL;
    }

    public String get_LInkedInurl() {
        return _LInkedInurl;
    }

    public void set_LInkedInurl(String _LInkedInurl) {
        this._LInkedInurl = _LInkedInurl;
    }

    public String get_UserImage() {
        return _UserImage;
    }

    public void set_UserImage(String _UserImage) {
        this._UserImage = _UserImage;
    }

    public ProfileUpdateRequest(String _FirstName, String _lastName, String _DOB, String _EMIL, String _NUMBER, String _Nationality, String _IceName, String _IceNumber, String _Bloodgroup, String _gender, String _HighestEDu, String _Occupation, String _SelfAdrsLine1, String _SelfAdrsLine2, String _SelfAdrsPIN, String _SelfAdrsCity, String _SelfAdrsState, String _SelfAdrsCountry, String _OfcAdrsLine1, String _OfcAdrsLine2, String _OfcAdrsPIN, String _OfcAdrsCity, String _OfcAdrsState, String _OfcAdrsCountry, String _CompanyName, String _GstNumber, String _About, String _Interest, String _FbUrl, String _TwiterURL, String _LInkedInurl, String _UserImage) {
        this._FirstName = _FirstName;
        this._lastName = _lastName;
        this._DOB = _DOB;
        this._EMIL = _EMIL;
        this._NUMBER = _NUMBER;
        this._Nationality = _Nationality;
        this._IceName = _IceName;
        this._IceNumber = _IceNumber;
        this._Bloodgroup = _Bloodgroup;
        this._gender = _gender;
        this._HighestEDu = _HighestEDu;
        this._Occupation = _Occupation;
        this._SelfAdrsLine1 = _SelfAdrsLine1;
        this._SelfAdrsLine2 = _SelfAdrsLine2;
        this._SelfAdrsPIN = _SelfAdrsPIN;
        this._SelfAdrsCity = _SelfAdrsCity;
        this._SelfAdrsState = _SelfAdrsState;
        this._SelfAdrsCountry = _SelfAdrsCountry;
        this._OfcAdrsLine1 = _OfcAdrsLine1;
        this._OfcAdrsLine2 = _OfcAdrsLine2;
        this._OfcAdrsPIN = _OfcAdrsPIN;
        this._OfcAdrsCity = _OfcAdrsCity;
        this._OfcAdrsState = _OfcAdrsState;
        this._OfcAdrsCountry = _OfcAdrsCountry;
        this._CompanyName = _CompanyName;
        this._GstNumber = _GstNumber;
        this._About = _About;
        this._Interest = _Interest;
        this._FbUrl = _FbUrl;
        this._TwiterURL = _TwiterURL;
        this._LInkedInurl = _LInkedInurl;
        this._UserImage = _UserImage;
    }
}
