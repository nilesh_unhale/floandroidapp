package com.flo.floapp.Profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.R;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    View view;
    TextView text_WelcomeProfile, text_NameProfile, text_emailProfile, text_DOBProfile, text_addressProfile,
                text_educationProfile, text_occupationProfile, text_CompanyProfile, text_IntrestProfile,text_mOBILEProfile;
    ImageView imageView_iv_editProfile;
    String _FNAME,_LNAME,_EMAIL,_DOB ,mUserId,_Userimage,_MOBILE;
    CircleImageView imageView_userphoto;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        initUI();
        return view;
    }

    public void initUI(){
        text_WelcomeProfile = view.findViewById(R.id.text_WelcomeProfile);
        text_NameProfile = view.findViewById(R.id.text_NameProfile);
        text_emailProfile = view.findViewById(R.id.text_emailProfile);
        text_DOBProfile = view.findViewById(R.id.text_DOBProfile);
        text_addressProfile = view.findViewById(R.id.text_addressProfile);
        text_educationProfile = view.findViewById(R.id.text_educationProfile);
        text_occupationProfile = view.findViewById(R.id.text_occupationProfile);
        text_CompanyProfile = view.findViewById(R.id.text_CompanyProfile);
        text_IntrestProfile = view.findViewById(R.id.text_IntrestProfile);
        text_mOBILEProfile = view.findViewById(R.id.text_mOBILEProfile);
        imageView_iv_editProfile = view.findViewById(R.id.imageView_iv_editProfile);
        imageView_userphoto = view.findViewById(R.id.imageView_userphoto);

        mUserId = PreferenceUtil.readString(getActivity(), PreferenceStrings.USER_ID,"");
        _EMAIL = PreferenceUtil.readString(getActivity(), PreferenceStrings.EMAIL,"");
        _MOBILE = PreferenceUtil.readString(getActivity(), PreferenceStrings.MOBILE,"");

        text_emailProfile.setText(_EMAIL);
        text_mOBILEProfile.setText(_MOBILE);

        getProfile(mUserId);

        imageView_iv_editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityUpdateProfile.class);
                intent.putExtra("userId",mUserId);
                intent.putExtra("firstName",_FNAME);
                intent.putExtra("lastName",_LNAME);
                intent.putExtra("email",_EMAIL);
                intent.putExtra("number",_MOBILE);
                getActivity().startActivity(intent);
            }
        });
    }

    private void getProfile(final String mUserId) {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_GET_PROFILE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray array_profile = reg_jsonObject.getJSONArray("response");
                                JSONObject obj_profile = array_profile.getJSONObject(0);
                                 _FNAME = obj_profile.getString("first");
                                 _LNAME = obj_profile.getString("last");
                                 _EMAIL = obj_profile.getString("email");
                                 _Userimage = obj_profile.getString("userimage");
                                 _DOB = obj_profile.getString("dob");
                                String _ADDRESS = obj_profile.getString("address");
                                String _PINCODE = obj_profile.getString("pincode");
                                _MOBILE = obj_profile.getString("mobile");
                                String _GENDER = obj_profile.getString("gender");
                                String _OCCUPATION = obj_profile.getString("occupation");
                                String _CITY = obj_profile.getString("city");
                                String _STATE = obj_profile.getString("state");
                                String _COUNTRY = obj_profile.getString("country");
                                String _NATIONALITY = obj_profile.getString("nationality");
                                String _URL_FACEBOOK = obj_profile.getString("facebook");
                                String _URL_TWITTER = obj_profile.getString("twitter");
                                String _URL_LINKED_IN = obj_profile.getString("linkedin");
                                String _HIGEST_EDU = obj_profile.getString("highestEducation");
                                String _INTEREST = obj_profile.getString("interest");
                                String _COMPANY_NAME = obj_profile.getString("company_name");
                                String _LANDMARK = obj_profile.getString("landmark");
                                String _AGE = obj_profile.getString("age");

                                setDetails(_DOB,_HIGEST_EDU,_OCCUPATION,_COMPANY_NAME,_INTEREST,_ADDRESS,_MOBILE);

                                if (!_Userimage.equals(null)) {
                                    PreferenceUtil.writeString(getActivity(), PreferenceStrings.USER_IMAGE, _Userimage);
                                }
                                text_NameProfile.setText(_FNAME);
                                text_emailProfile.setText(_EMAIL);

                                Glide.with(getActivity())
                                        .load(Config.URL_USER_IMAGE+_Userimage)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .error(R.drawable.icon_profile_user)
                                        .into(imageView_userphoto);


                               // text_WelcomeProfile.setText(_FNAME);
                               // imageView_iv_editProfile = view.findViewById(R.id.imageView_iv_editProfile);
                            }
                            else {
                                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", mUserId);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    @Override
    public void onResume() {
        super.onResume();
        getProfile(mUserId);

        Glide.with(getActivity())
                .load(Config.URL_USER_IMAGE+_Userimage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.icon_profile_user)
                .into(imageView_userphoto);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }


    private void setDetails(String dob, String higest_edu, String occupation, String company_name,
                            String interest, String address, String mobile) {

        if (!dob.equals("null")){
        text_DOBProfile.setText(dob);
        }
        if (!higest_edu.equals("null")){
            text_educationProfile.setText(higest_edu);
        }
        if (!occupation.equals("null")){
            text_occupationProfile.setText(occupation);
        }
        if (!occupation.equals("null")){
            text_CompanyProfile.setText(company_name);
        }
        if (!interest.equals("null")){
            text_IntrestProfile.setText(interest);
        }
        if (!address.equals("null")){
            text_addressProfile.setText(address);
        }
        if (!mobile.equals("null")){
            text_mOBILEProfile.setText(mobile);
        }
        
    }


}
