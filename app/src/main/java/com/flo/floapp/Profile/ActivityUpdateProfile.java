package com.flo.floapp.Profile;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flo.floapp.FLOUrlUtils.Config;
import com.flo.floapp.FontFamily.FontChangeCrawler;
import com.flo.floapp.GUESTDetaills.PermanentResidency.PermanentResidency;
import com.flo.floapp.Login.Login;
import com.flo.floapp.MultipartRequest.MultipartRequest;
import com.flo.floapp.MyPreferences.AdapterPreference;
import com.flo.floapp.MyPreferences.PreferenceRequestModel;
import com.flo.floapp.MyPreferences.PreferenceResponceModel;
import com.flo.floapp.R;
import com.flo.floapp.Register.OTPVerify;
import com.flo.floapp.Retrofit.APIService;
import com.flo.floapp.Retrofit.ApiUtils;
import com.flo.floapp.SharePreference.PreferenceStrings;
import com.flo.floapp.SharePreference.PreferenceUtil;
import com.flo.floapp.Volley.MyApplication;
import com.flo.floapp._AllowSSL.HttpsTrustManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class ActivityUpdateProfile extends AppCompatActivity{

    FontChangeCrawler fontChanger;
    EditText et_firstNameProfile, et_lastNameProfile, et_emailProfile, et_mobileProfile
            ,et_occupationProfile,et_gstNumber,et_comapnyname,
            et_ADDRESSLINE1_Profile, et_ADDRESSLINE2_Profile, et_PINCODE_Profile,
            et_bill_PINCODE,et_bill_ADDRESSLINE2,et_bill_ADDRESSLINE1,
            et_ICEnAME,et_ICEnUMBER,et_fb,et_linkedin,et_twiter, et_aboutme,et_interest;
    TextView text_titleNameProfile, text_emailProfile, text_MOBILEProfile,
            text_NATIONALITYProfile, text_ADDRESS_Profile,text_dobProfile;
    Button buttonSave;
    ImageView iv_userImageUpdate;
    String[] list_country_name,list_country_Id,list_state_name,list_state_Id,
            list_occupation_Id ,list_occupation_name,list_interest_id,list_interest_name;

    String[] mlist_nationality_name, mlist_nationality_Id;
    private int mYear, mMonth, mDay;

    String _user_Id,user_country,user_state,user_city,company_country,company_state,company_city,
            mOccupation,mInterest,mGender,encodedImage,_firstName,_lastNmae,_Dob,_email,mBloodGroup
            ,mHigestEducation,nationalityId,_Number;
    RadioGroup radioGroupGender;
    RadioButton radioButtonMale,radioButtonFemale;

    Spinner spinner_user_city,companyAdd_spinner_state,companyAdd_spinner_company_country,
            companyAdd_spinner_company_city,spinner_user_state,spinner_user_country,
            spinner_bloogGroup,spinner_highestEdu,et_nationalityProfile;

    int PICK_IMAGE_REQUEST = 111;
    Bitmap bitmap;
    ProgressDialog progressDialog;
    private static final int REQUEST_CAMERA= 1;
    private static final int REQUEST_READ_STORAGE= 1;
    private static final int REQUEST_WRITE_STORAGE= 1;
    Boolean isLoginfb,isLoginGoogle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.update_profile);

        ActivityCompat.requestPermissions(ActivityUpdateProfile.this , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);

        //-font change
        fontChanger = new FontChangeCrawler(getAssets(), "fonts/Raleway-Medium.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

        initUI();
        getCountryList();
        getCompanyCountry();
        getNationality();
    }

    public void initUI(){
        radioGroupGender  =(RadioGroup)findViewById(R.id.rgProfile);
        radioButtonMale  =(RadioButton) findViewById(R.id.rb_male);
        radioButtonFemale  =(RadioButton) findViewById(R.id.rb_female);

        et_firstNameProfile = findViewById(R.id.et_firstNameProfile);
        et_lastNameProfile = findViewById(R.id.et_lastNameProfile);
        et_emailProfile = findViewById(R.id.et_emailProfile);
        et_mobileProfile = findViewById(R.id.et_mobileProfile);
        et_nationalityProfile = findViewById(R.id.et_nationalityProfile);
        et_ADDRESSLINE1_Profile = findViewById(R.id.et_ADDRESSLINE1_Profile);
        et_ADDRESSLINE2_Profile = findViewById(R.id.et_ADDRESSLINE2_Profile);
        et_PINCODE_Profile = findViewById(R.id.et_PINCODE_Profile);

        et_ICEnAME = findViewById(R.id.et_iceName);
        et_ICEnUMBER = findViewById(R.id.et_iceNumber);
        et_occupationProfile = findViewById(R.id.et_occupationProfile);
        et_gstNumber = findViewById(R.id.et_gstNumber);
        et_comapnyname = findViewById(R.id.et_comapnyname);
        et_fb = findViewById(R.id.et_fb);
        et_linkedin = findViewById(R.id.et_linkedin);
        et_twiter = findViewById(R.id.et_twiter);

        et_aboutme = findViewById(R.id.et_aboutme);
        et_interest = findViewById(R.id.et_interest);

        et_bill_PINCODE = findViewById(R.id.et_bill_PINCODE);
        et_bill_ADDRESSLINE2 = findViewById(R.id.et_bill_ADDRESSLINE2);
        et_bill_ADDRESSLINE1 = findViewById(R.id.et_bill_ADDRESSLINE1);

        text_titleNameProfile = findViewById(R.id.text_titleNameProfile);
        text_emailProfile = findViewById(R.id.text_emailProfile);
        text_MOBILEProfile = findViewById(R.id.text_MOBILEProfile);
      //  text_NATIONALITYProfile = findViewById(R.id.text_NATIONALITYProfile);
        text_ADDRESS_Profile = findViewById(R.id.text_ADDRESS_Profile);
        text_dobProfile = findViewById(R.id.text_dobProfile);
        buttonSave = findViewById(R.id.button_save_profile);

        spinner_user_city = findViewById(R.id.userAdd_spinner_city);
        spinner_user_state = findViewById(R.id.userAdd_spinner_state);
        spinner_user_country = findViewById(R.id.userAdd_spinner_country);

        companyAdd_spinner_state = findViewById(R.id.companyAdd_spinner_state);
        companyAdd_spinner_company_city = findViewById(R.id.companyAdd_spinner_city);
        companyAdd_spinner_company_country = findViewById(R.id.companyAdd_spinner_coutry);
        spinner_bloogGroup = findViewById(R.id.spinner_bloogGroup);
        spinner_highestEdu = findViewById(R.id.spinner_highestEdu);

        et_ICEnAME = findViewById(R.id.et_iceName);
        et_ICEnUMBER = findViewById(R.id.et_iceNumber);
        iv_userImageUpdate = findViewById(R.id.iv_userImageUpdate);

        Intent intent = getIntent();
        _user_Id = intent.getStringExtra("userId");
        _firstName = intent.getStringExtra("firstName");
        _lastNmae = intent.getStringExtra("lastName");
        _email = intent.getStringExtra("email");
        _Number = intent.getStringExtra("number");

        PreferenceUtil.readString(getApplicationContext(), PreferenceStrings.GENDER,"");
           et_firstNameProfile.setText(_firstName);
            et_lastNameProfile.setText(_lastNmae);
            et_emailProfile.setText(_email);
            et_mobileProfile.setText(_Number);




       /* Glide.with(ActivityUpdateProfile.this)
                .load(Config.URL_IMAGE + allImgs.get(0))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_property_thumbnail)
                .into(iv_userImageUpdate);*/

        iv_userImageUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });
        spinner_highestEdu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mHigestEducation = spinner_highestEdu.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner_bloogGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 mBloodGroup = spinner_bloogGroup.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        radioButtonMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = String.valueOf(radioGroupGender.getCheckedRadioButtonId());
                mGender = "1";
            }
        });

        radioButtonFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = String.valueOf(radioGroupGender.getCheckedRadioButtonId());
                mGender = "2";

            }
        });

        text_dobProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                c.set(1999, Calendar.JANUARY , 1);
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityUpdateProfile.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                text_dobProfile.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mGender,text_dobProfile, String mBloodGroup,mHigestEducation
                // user_country company_country
                // company_state  user_state
                // company_city    user_city

                String mName = et_firstNameProfile.getText().toString().trim();
                String mLastName = et_lastNameProfile.getText().toString().trim();
                String mEmail = et_emailProfile.getText().toString().trim();
                String mMobile = et_mobileProfile.getText().toString().trim();
             //   String mNationality = et_nationalityProfile.getText().toString().trim();
                String mUserAddressLine1 = et_ADDRESSLINE1_Profile.getText().toString().trim();
                String mUserAddressLine2 = et_ADDRESSLINE2_Profile.getText().toString().trim();
                String mUserAddressPin = et_PINCODE_Profile.getText().toString().trim();

                String mOccupation = et_occupationProfile.getText().toString().trim();
                String mGstNumber = et_gstNumber.getText().toString().trim();
                String mCompanyName = et_comapnyname.getText().toString().trim();
                String mFb_link = et_fb.getText().toString().trim();
                String mLinkedIn_link = et_linkedin.getText().toString().trim();
                String mTwitter_link = et_twiter.getText().toString().trim();
                String mBill_PINCODE = et_bill_PINCODE.getText().toString().trim();
                String mBillAddressLine1 = et_bill_ADDRESSLINE1.getText().toString().trim();
                String mBill_ADDRESSLINE2 = et_bill_ADDRESSLINE2.getText().toString().trim();
                String mICEnAME = et_ICEnAME.getText().toString().trim();
                String mIceNumber = et_ICEnUMBER.getText().toString().trim();
                String mAboutMe = et_aboutme.getText().toString().trim();
                String mInterest = et_interest.getText().toString().trim();

                if (radioGroupGender.getCheckedRadioButtonId() == -1)
                {
                    Toast.makeText(ActivityUpdateProfile.this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    updateProfile(mName,mLastName,mEmail,mMobile,"",mUserAddressLine1,mUserAddressLine2,mUserAddressPin,
                            mOccupation,mGstNumber,mCompanyName,mFb_link,mLinkedIn_link,mTwitter_link,mBill_PINCODE,
                            mBillAddressLine1,mBill_ADDRESSLINE2,mGender,text_dobProfile.getText().toString(),mBloodGroup,mHigestEducation
                            ,user_country,user_country,company_country,company_state,user_state,company_city,user_city
                            ,mICEnAME,mIceNumber,mAboutMe,mInterest);
                }




            }
        });


    }

    private void updateProfile(final String mName, final String mLastName, final String mEmail, final String mMobile,
                               final String mNationality, final String mUserAddressLine1,
                               final String mUserAddressLine2, final String mUserAddressPin, final String mOccupation,
                               final String mGstNumber, final String mCompanyName, final String mFb_link, final String
                                       mLinkedIn_link, final String mTwitter_link, final String mBill_PINCODE,
                               final String mBillAddressLine1, final String mBill_ADDRESSLINE2, final String mGender,
                               final String s, final String mBloodGroup, final String mHigestEducation,
                               final String user_country, final String userCountry, final String company_country,
                               final String company_state, final String user_state, final String company_city,
                               final String user_city, final String mICEnAME, final String mIceNumber,
                               final String mAboutMe, final String mInterest)
    {
        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Updating Details...");
        pDialog.show();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_UPDATE_PROFILE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                                JSONObject object = reg_jsonObject.getJSONObject("response");
                                String _Nationality = object.getString("nationality");
                                PreferenceUtil.writeString(getApplicationContext(),PreferenceStrings.USER_NATIONALITY,_Nationality);


                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("first", mName);
                params.put("last", mLastName);
               // params.put("image", userIamge);
                params.put("user_id", _user_Id);
                params.put("dob", s);
                params.put("ice_name", mICEnAME);
                params.put("ice_no", mIceNumber);
                params.put("gst_no", mGstNumber);
                params.put("gender", mGender);
                params.put("aboutme", mAboutMe);
                params.put("interest", mInterest);
                params.put("highestEducation", mHigestEducation);
                params.put("cmplandmark", "xx");
                params.put("companyCity", "33");
                params.put("companyCity", company_city);
                params.put("companyState", "33");
                params.put("companyState", company_state);
                params.put("companyCountry", company_country);
                params.put("companyCountry", "333");
                params.put("companyPincode", mBill_PINCODE);
                params.put("cmpaddress1", mBill_ADDRESSLINE2);
                params.put("cmpaddress", mBillAddressLine1);
                params.put("joiningDate", "12/12/1990");
                params.put("company_name", mCompanyName);
                params.put("occupation", mOccupation);
                params.put("landmark", "ddsds");
                params.put("city", user_city);
                params.put("state", user_state);
                params.put("country", user_country);
                params.put("pincode", mUserAddressPin);
                params.put("address1", mUserAddressLine2);
                params.put("address", mUserAddressLine1);
                params.put("nationality", nationalityId);
                params.put("linkedin", mLinkedIn_link);
                params.put("twitter", mTwitter_link);
                params.put("facebook", mFb_link);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    //---get country
    private void getCountryList()
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_COUNTRY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_country_Id = new String[MainArray.length()];
                            list_country_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_country_Id[i] = mId;
                                    list_country_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                        R.layout.spinner_item, list_country_name);
                                Adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_user_country.setAdapter(Adapter1);
                                spinner_user_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        getStates(list_country_Id[position].toString().trim());
                                        user_country = list_country_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //----------------get states ------------
    private void getStates(final String id) {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_STATE,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            list_state_Id = new String[MainArray.length()];
                            list_state_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    list_state_Id[i] = mId;
                                    list_state_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                        R.layout.spinner_item, list_state_name);
                                Adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_user_state.setAdapter(Adapter2);
                                spinner_user_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        user_state = list_state_Id[position].toString().trim();
                                        getCity(user_state);

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("country_id", id);
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCity(final String user_state)
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req1";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Config.URL_CITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            Log.d("flo city:",reg_jsonObject.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {

                                JSONArray MainArray = reg_jsonObject.getJSONArray("response");
                                final String[] list_city_Id = new String[MainArray.length()];
                                String[] list_city_name = new String[MainArray.length()];
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    Log.e("flo c:","id :"+mId +" name :"+mname);
                                    list_city_Id[i] = mId;
                                    list_city_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter3 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                        R.layout.spinner_item, list_city_name);
                                Adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_user_city.setAdapter(Adapter3);
                                spinner_user_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                      //  getCity(list_state_Id[position].toString().trim());
                                        user_city = list_city_Id[position].toString().trim();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });


                            } else {
                                Toast.makeText(getApplicationContext(), "No City Found", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("state_id", user_state);
                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(2 * 1000, 2, 2);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //company address details---------
   public void getCompanyCountry(){
       HttpsTrustManager.allowAllSSL();
       String tag_string_req = "string_req";
       StringRequest strReq = new StringRequest(Request.Method.GET,
               Config.URL_COUNTRY,
               new com.android.volley.Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
                       try {
                           JSONObject reg_jsonObject = new JSONObject(response);
                           String res_Status = reg_jsonObject.getString("status");
                           final String res_Message = reg_jsonObject.getString("message");
                           JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                           list_country_Id = new String[MainArray.length()];
                           list_country_name = new String[MainArray.length()];
                           if (res_Status.equals("true")) {
                               for (int i = 0; i < MainArray.length(); i++) {
                                   JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                   String mId = propertyOBJ.getString("id");
                                   String mname = propertyOBJ.getString("name");
                                   list_country_Id[i] = mId;
                                   list_country_name[i] = mname;
                               }

                               ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                       R.layout.spinner_item, list_country_name);
                               Adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                               companyAdd_spinner_company_country.setAdapter(Adapter2);
                               companyAdd_spinner_company_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                   @Override
                                   public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                       getStateCompany(list_country_Id[position].toString().trim());
                                       company_country = list_country_Id[position].toString().trim();
                                                                          }
                                   @Override
                                   public void onNothingSelected(AdapterView<?> parent) {
                                   }
                               });

                           } else {
                               Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                           }
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }

                   }
               }, new com.android.volley.Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

           }
       });
       strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
       MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
   }
public void getStateCompany(final String st){
    HttpsTrustManager.allowAllSSL();
    String tag_string_req = "string_req";
    StringRequest strReq = new StringRequest(Request.Method.POST,
            Config.URL_STATE,
            new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject reg_jsonObject = new JSONObject(response);
                        String res_Status = reg_jsonObject.getString("status");
                        final String res_Message = reg_jsonObject.getString("message");
                        JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                        list_state_Id = new String[MainArray.length()];
                        list_state_name = new String[MainArray.length()];
                        if (res_Status.equals("true")) {
                            for (int i = 0; i < MainArray.length(); i++) {
                                JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                String mId = propertyOBJ.getString("id");
                                String mname = propertyOBJ.getString("name");
                                list_state_Id[i] = mId;
                                list_state_name[i] = mname;
                            }
                            ArrayAdapter<String> Adapter2 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                    R.layout.spinner_item, list_state_name);
                            Adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            companyAdd_spinner_state.setAdapter(Adapter2);
                            companyAdd_spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    getCityCompany(list_state_Id[position].toString().trim());
                                    company_state = list_state_Id[position].toString().trim();
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new com.android.volley.Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

        }
    }){
        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<String, String>();
            params.put("country_id", st);
            return params;
        }
    };
    strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
}
public void getCityCompany(final String sta){
    HttpsTrustManager.allowAllSSL();
    String tag_string_req = "string_req";
    StringRequest strReq = new StringRequest(Request.Method.POST,
            Config.URL_CITY,
            new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject reg_jsonObject = new JSONObject(response);
                        Log.d("flo city:",reg_jsonObject.toString());
                        String res_Status = reg_jsonObject.getString("status");
                        final String res_Message = reg_jsonObject.getString("message");
                        if (res_Status.equals("true")) {

                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            final String[] list_city_Id = new String[MainArray.length()];
                            String[] list_city_name = new String[MainArray.length()];
                            for (int i = 0; i < MainArray.length(); i++) {
                                JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                String mId = propertyOBJ.getString("id");
                                String mname = propertyOBJ.getString("name");
                                list_city_Id[i] = mId;
                                list_city_name[i] = mname;
                            }
                            ArrayAdapter<String> Adapter3 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                    R.layout.spinner_item, list_city_name);
                            Adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            companyAdd_spinner_company_city.setAdapter(Adapter3);
                            companyAdd_spinner_company_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    getCity(list_state_Id[position].toString().trim());
                                    company_city = list_city_Id[position].toString().trim();
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "No City Found", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new com.android.volley.Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getApplicationContext(), "Please Check your internet connection" , Toast.LENGTH_LONG).show();

        }
    }){
        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<String, String>();
            params.put("state_id", sta);
            return params;
        }
    };
    strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
}


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();

            try {


                     bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                     iv_userImageUpdate.setImageBitmap(bitmap);
                     uploadImage();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {

        HttpsTrustManager.allowAllSSL();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.show();
        MultipartRequest volleyMultipartRequest = new MultipartRequest(Request.Method.POST, Config.URL_UPDATE_IMAGE,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pDialog.dismiss();
                        try {
                            JSONObject reg_jsonObject = new JSONObject(new String(response.data));
                            Log.d("res",response.data.toString());
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            if (res_Status.equals("true")) {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(getApplicationContext(), ""+res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        String message = null;
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", _user_Id);
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);


    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    //---\
    private void getNationality()
    {
        HttpsTrustManager.allowAllSSL();
        String tag_string_req = "string_req";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                Config.URL_NATIONALITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject reg_jsonObject = new JSONObject(response);
                            String res_Status = reg_jsonObject.getString("status");
                            final String res_Message = reg_jsonObject.getString("message");
                            JSONArray MainArray = reg_jsonObject.getJSONArray("response");

                            mlist_nationality_Id = new String[MainArray.length()];
                            mlist_nationality_name = new String[MainArray.length()];
                            if (res_Status.equals("true")) {
                                for (int i = 0; i < MainArray.length(); i++) {
                                    JSONObject propertyOBJ = MainArray.getJSONObject(i);
                                    String mId = propertyOBJ.getString("id");
                                    String mname = propertyOBJ.getString("name");
                                    mlist_nationality_Id[i] = mId;
                                    mlist_nationality_name[i] = mname;
                                }
                                ArrayAdapter<String> Adapter1 = new ArrayAdapter<String>(ActivityUpdateProfile.this,
                                        R.layout.spinner_item, mlist_nationality_name);
                                et_nationalityProfile.setAdapter(Adapter1);
                                et_nationalityProfile.setSelection(85);
                                et_nationalityProfile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        nationalityId = mlist_nationality_Id[position].toString().trim();

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });

                            } else {
                                Toast.makeText(getApplicationContext(), "" + res_Message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong" , Toast.LENGTH_LONG).show();

            }
        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}




